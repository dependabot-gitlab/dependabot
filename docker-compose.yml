# Definition for running deployed version of dependabot-gitlab using docker-compose
# In case of using custom docker-compose.yml, it needs to be mounted inside 'worker' service
#   with path exposed via 'SETTINGS__UPDATER_TEMPLATE_PATH' envronment variables

# Dependabot-gitlab base image
x-base-image: &base_image "${BASE_IMAGE:-andrcuns/dependabot-gitlab:3.44.0-alpha.1}"

# Option documentation
# https://gitlab.com/dependabot-gitlab/dependabot/-/blob/master/doc/environment.md
x-environment: &environment_variables
  BASE_IMAGE: *base_image
  # Ecosystem specific image pattern, where package_ecosystem is one of the supported ecosystems:
  #  bundler
  #  npm
  #  gomod
  #  pip
  #  docker
  #  composer
  #  pub
  #  cargo
  #  nuget
  #  maven
  #  gradle
  #  mix
  #  terraform
  #  elm
  #  gitsubmodule
  #  bun
  #  docker-compose
  #
  SETTINGS__UPDATER_IMAGE_PATTERN: "${UPDATER_IMAGE_PATTERN:-andrcuns/dependabot-gitlab-%<package_ecosystem>s:3.44.0-alpha.1}"
  # Persistance
  REDIS_URL: redis://redis:6379
  MONGODB_URL: mongodb:27017
  # Rails
  RAILS_ENV: production
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#concurrency
  RAILS_MAX_THREADS:
  RAILS_MIN_THREADS:
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#application
  SECRET_KEY_BASE: ${SECRET_KEY_BASE:-key}
  SETTINGS__LOG_COLOR:
  SETTINGS__LOG_LEVEL:
  SETTINGS__COMMANDS_PREFIX:
  SETTINGS__ANONYMOUS_ACCESS:
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#access
  SETTINGS__GITLAB_ACCESS_TOKEN:
  SETTINGS__GITHUB_ACCESS_TOKEN:
  SETTINGS__GITLAB_URL:
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#gitlab-api
  SETTINGS__GITLAB_API_MAX_RETRY:
  SETTINGS__GITLAB_API_MAX_RETRY_INTERVAL:
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#webhooks
  SETTINGS__DEPENDABOT_URL:
  SETTINGS__CREATE_PROJECT_HOOK:
  SETTINGS__GITLAB_AUTH_TOKEN:
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#configuration-file
  SETTINGS__CONFIG_BASE_FILENAME:
  SETTINGS__CONFIG_FILENAME:
  SETTINGS__CONFIG_BRANCH:
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#service
  SETTINGS__DEPLOY_MODE: compose
  SETTINGS__UPDATE_RETRY:
  SETTINGS__EXPIRE_RUN_DATA:
  SETTINGS__DELETE_UPDATER_CONTAINER:
  SETTINGS__UPDATER_CONTAINER_STARTUP_TIMEOUT:
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#project-registration
  SETTINGS__PROJECT_REGISTRATION:
  SETTINGS__PROJECT_REGISTRATION_CRON:
  SETTINGS__PROJECT_REGISTRATION_ALLOW_PATTERN:
  SETTINGS__PROJECT_REGISTRATION_IGNORE_PATTERN:
  SETTINGS__PROJECT_REGISTRATION_RUN_ON_BOOT:
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#metrics
  SETTINGS__METRICS:
  # https://dependabot-gitlab.gitlab.io/dependabot/config/environment.html#sentry
  SENTRY_DSN:
  SETTINGS__SENTRY_TRACES_SAMPLE_RATE:
  SETTINGS__SENTRY_PROFILES_SAMPLE_RATE:
  SETTINGS__SENTRY_IGNORED_ERRORS:
  # Custom ssl certificate file for GitLab instance
  SSL_CERT_FILE: /etc/ssl/custom/tls-ca-bundle.pem

# Updater container definition, app dynamically loads this definition when scheduling dependency updater containers
# If custom volumes or variables are required for dependency update process, it should be placed here.
# Be aware that any volumes are in the context of the docker (dind) service, if you want to mount local directories,
# they should be mounted in the dind service first. The local directory in the updater volume should then
# reference the path inside the dind service.
x-updater:
  network_mode: "host"
  environment:
    SETTINGS__APP_COMPONENT: updater
    <<: *environment_variables

services:
  redis:
    image: ${REDIS_IMAGE:-redis:7.4.2}
    volumes:
      - redis-data:${REDIS_DATA_DIR:-/data}

  mongodb:
    image: ${MONGODB_IMAGE:-mongo:8.0.5}
    volumes:
      - mongodb-data:${MONGODB_DATA_DIR:-/data/db}

  docker:
    image: ${DOCKER_DIND_IMAGE:-docker:28.0-dind}
    privileged: true
    environment:
      DOCKER_TLS_CERTDIR: "/certs"
    volumes:
      - docker-images:/var/lib/docker
      - docker-certs-ca:/certs/ca
      - docker-certs-client:/certs/client
      - ${GITLAB_SSL_CERT_FILE:-custom-certs}:/etc/ssl/custom/tls-ca-bundle.pem:ro

  migration:
    image: *base_image
    depends_on:
      - mongodb
    volumes:
      - cache:/home/dependabot/app/tmp/cache
    environment:
      <<: *environment_variables
    entrypoint: kube/compose-entrypoint.sh
    command: rails db:migrate

  web:
    image: *base_image
    depends_on:
      - redis
      - migration
    volumes:
      - cache:/home/dependabot/app/tmp/cache
      - ${GITLAB_SSL_CERT_FILE:-custom-certs}:/etc/ssl/custom/tls-ca-bundle.pem:ro
    environment:
      <<: *environment_variables
      RAILS_SERVE_STATIC_FILES: "true"
      SETTINGS__APP_COMPONENT: web
    ports:
      - 3000:3000
    entrypoint: kube/compose-entrypoint.sh
    command: rails server
    healthcheck:
      test: ["CMD", "curl", "-f", "localhost:3000/healthcheck"]
      interval: 10s
      timeout: 2s
      retries: 18
      start_period: 5s

  worker:
    image: *base_image
    depends_on:
      - redis
      - migration
    volumes:
      - cache:/home/dependabot/app/tmp/cache
      - docker-certs-client:/certs/client:ro
      - ${GITLAB_SSL_CERT_FILE:-custom-certs}:/etc/ssl/custom/tls-ca-bundle.pem:ro
    environment:
      <<: *environment_variables
      DOCKER_HOST: tcp://docker:2376
      DOCKER_CERT_PATH: "/certs/client"
      DOCKER_TLS_VERIFY: 1
      SETTINGS__APP_COMPONENT: worker
      SETTINGS__UPDATER_TEMPLATE_PATH: ${UPDATER_TEMPLATE_PATH:-docker-compose.yml}
      SETTINGS__SIDEKIQ_ALIVE_KEY_TTL:
    entrypoint: kube/compose-entrypoint.sh
    command: sidekiq
    healthcheck:
      test: ["CMD", "curl", "-f", "localhost:7433/healthcheck"]
      interval: 10s
      timeout: 2s
      retries: 18
      start_period: 5s

  background-tasks:
    image: *base_image
    depends_on:
      - web
      - worker
    volumes:
      - cache:/home/dependabot/app/tmp/cache
      - ${GITLAB_SSL_CERT_FILE:-custom-certs}:/etc/ssl/custom/tls-ca-bundle.pem:ro
    environment:
      <<: *environment_variables
    entrypoint: kube/compose-entrypoint.sh
    command: rails background_tasks:run_post_deploy_tasks

volumes:
  redis-data:
  mongodb-data:
  cache:
  docker-images:
  docker-certs-ca:
  docker-certs-client:
  custom-certs:
