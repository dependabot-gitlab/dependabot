# frozen_string_literal: true

source "https://rubygems.org"

ruby "~> 3.3"

gem "anyway_config", "~> 2.7"
gem "bcrypt", "~> 3.1"
gem "bootsnap", ">= 1.4.2", require: false
gem "dartsass-sprockets", "~> 3.2"
gem "dependabot-omnibus", "0.300.0"
gem "grape", "~> 2.3"
gem "grape-entity", "~> 1.0"
gem "grape-kaminari", "~> 0.4.5"
gem "grape-swagger", "~> 2.1"
gem "graphql-client", "~> 0.25.0"
gem "importmap-rails", "~> 2.1"
gem "json_schemer", "~> 2.4"
gem "kaminari-actionview", "~> 1.2"
gem "kaminari-mongoid", "~> 1.0"
gem "kubeclient", "~> 4.12"
gem "lograge", "~> 0.14.0"
gem "mongoid", "~> 9.0"
# mongoid_rails_migrations fork with support for background migrations
gem "mongoid_rails_migrations", "~> 1.6", github: "andrcuns/mongoid_rails_migrations", ref: "6eb0463f6198cc612806a4696126da64d33cb357" # rubocop:disable Layout/LineLength
# sync parser version with ruby version of dependabot-core
gem "parser", "~> 3.3.6.0"
gem "puma", "~> 6.6"
gem "rails", "~> 8.0.2"
gem "rainbow", "~> 3.1"
gem "redis", "~> 5.4"
gem "request_store", "~> 1.7"
gem "request_store-sidekiq", "~> 0.1.0"
gem "sentry-rails", "~> 5.23", require: false
gem "sentry-sidekiq", "~> 5.22", require: false
gem "sidekiq", "~> 7.3.9"
gem "sidekiq_alive", "~> 2.4.0", require: false
gem "sidekiq-cron", "~> 2.1"
gem "sprockets-rails", "~> 3.5"
gem "stackprof", "~> 0.2.27", require: false
gem "stimulus-rails", "~> 1.3"
gem "terminal-table", "~> 4.0"
gem "turbo-rails", "~> 2.0"
gem "tzinfo-data", "~> 1.2025"
gem "uri", "~> 0.13.2" # pin uri for compatibility with dependabot-core
gem "warning", "~> 1.5", require: false
gem "yabeda-prometheus", "~> 0.9.1", require: false
gem "yabeda-sidekiq", "~> 0.12.0", require: false

group :test do
  gem "debug", "~> 1.10", require: false
  gem "factory_bot_rails", "~> 6.4"
  gem "faker", "~> 3.5"
  gem "git", "~> 3.0", require: false
  gem "httparty", "~> 0.22.0"
  gem "mustache", "~> 1.1", require: false
  gem "pry-byebug", "~> 3.10", require: false
  gem "reek", "~> 6.4", require: false
  gem "rspec", "~> 3.13"
  gem "rspec_junit_formatter", "~> 0.6.0"
  gem "rspec-rails", "~> 7.1.1"
  gem "rspec-sidekiq", "~> 5.1", require: false
  gem "rubocop", "~> 1.74.0", require: false
  gem "rubocop-factory_bot", "~> 2.27", require: false
  gem "rubocop-performance", "~> 1.24.0", require: false
  gem "rubocop-rails", "~> 2.30", require: false
  gem "rubocop-rspec", "~> 3.5", require: false
  gem "rubocop-rspec_rails", "~> 2.31", require: false
  gem "simplecov", "~> 0.22.0", require: false
  gem "simplecov-cobertura", "~> 2.1.0", require: false
  gem "simplecov-console", "~> 0.9.3", require: false
end

group :development do
  gem "grape-swagger-entity", "~> 0.5.5"
  gem "grape-swagger-representable", "~> 0.2.2"
  gem "pry-rails", "~> 0.3.11"
  gem "semver2", "~> 3.4", require: false
  gem "solargraph", "~> 0.52.0", require: false
  gem "solargraph-rails", "~> 1.1", require: false
  gem "solargraph-rspec", "~> 0.4.1", require: false
end

group :assets do
  gem "bootstrap", "~> 5.3"
end
