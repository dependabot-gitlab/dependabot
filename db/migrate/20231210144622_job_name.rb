# frozen_string_literal: true

class JobName < Mongoid::Migration
  def self.up
    Update::Job.all.each do |job|
      next job.destroy if job.project.nil? # delete jobs not attached to a project

      job.update!(name: "#{job.project.name}:#{job.package_ecosystem}:#{job.directory}")
    end

    Update::Job.create_indexes
  end

  def self.down; end
end
