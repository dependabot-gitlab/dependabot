# frozen_string_literal: true

class RunStatusString < Mongoid::Migration
  def self.up
    Update::Run.batch_size(1000).each do |run|
      run.update_attributes!(status: run.failures.any? ? Update::Run::FAILED : Update::Run::SUCCESS)
    end
  end

  def self.down; end
end
