# frozen_string_literal: true

class DependencyFileDir < Mongoid::Migration
  def self.up
    MergeRequest.batch_size(1000).each do |mr|
      mr.update_attributes!(dependency_file_dir: mr.directory) if mr.dependency_file_dir.nil?
    end
  end

  def self.down; end
end
