# frozen_string_literal: true

class MergedVulnerabilities < Mongoid::Migration
  # :reek:TooManyStatements
  # :reek:NestedIterators
  # rubocop:disable Metrics/MethodLength, Metrics/BlockLength, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity

  def self.up
    begin
      # Drop all indexes to remove any extra array indexes and recreate defined ones
      Vulnerability.collection.indexes.drop_all
      Vulnerability.create_indexes
    rescue StandardError => e
      ApplicationHelper.log(:warn, "Failed to drop and recreate indexes for Vulnerability: #{e.message}")
    end

    Github::Vulnerabilities::Fetcher::PACKAGE_ECOSYSTEMS.each_key do |package_ecosystem|
      vulnerabilities = {}

      ApplicationHelper.log(:info, "Merging vulnerabilities for #{package_ecosystem} package ecosystem")
      Vulnerability.where(package_ecosystem: package_ecosystem).batch_size(1000).each do |vulnerability|
        vulnerable_version_range = vulnerability.vulnerable_version_range
        first_patched_version = vulnerability.first_patched_version
        next if vulnerable_version_range.is_a?(Array) && first_patched_version.is_a?(Array)

        vuln_id = vulnerability._id
        package = vulnerability.package
        unique_id = "#{vulnerability.id}-#{package}"
        existing_vuln = vulnerabilities[unique_id]

        if existing_vuln
          ApplicationHelper.log(:info, "  merging vulnerability #{vuln_id} for package #{package}")
          existing_vuln.first_patched_version.push(first_patched_version) if first_patched_version
          existing_vuln.vulnerable_version_range.push(vulnerable_version_range)
          existing_vuln.save!

          VulnerabilityIssue.where(vulnerability_id: vuln_id).each do |vulnerability_issue|
            ApplicationHelper.log(:info, "    reassigning vulnerability for issue #{vulnerability_issue.iid}")
            vulnerability_issue.vulnerability = existing_vuln
            vulnerability_issue.save!
          end

          vulnerability.destroy
        else
          ApplicationHelper.log(:info, "  updating vulnerability #{vuln_id} for package #{package}")
          vulnerability.update_attributes!(
            vulnerable_version_range: [vulnerable_version_range],
            first_patched_version: [first_patched_version].compact
          )
          vulnerabilities[unique_id] = vulnerability
        end
      end
    end
  end

  def self.down; end
  # rubocop:enable Metrics/MethodLength, Metrics/BlockLength, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
end
