# frozen_string_literal: true

class LogEntryIndex < Mongoid::Migration
  def self.up
    Update::LogEntry.create_indexes
  end

  def self.down; end
end
