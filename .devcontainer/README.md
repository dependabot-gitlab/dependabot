# devcontainers

Devcontainer setup for local development with VS Code.

## Core

This setup is meant for local development as well as system and E2E test execution against container mocking interaction with https://gitlab.com.

## Updater

This setup is designed for debugging different ecosystems against real instance of GitLab.
