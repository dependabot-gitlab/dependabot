#!/bin/bash

set -e

workspace=$1

source "${workspace}/.gitlab/script/utils.sh"

if [[ "${ECOSYSTEM}" == "" ]]; then
  log_with_header "Update npm packages"
  npm install
  log_success "success!"
  echo ""

  bash ${workspace}/.gitlab/script/build-core-helpers.sh bundler
  echo ""
fi

if [[ "${PROXY_MOCKED_REQUESTS}" == "true" ]]; then
  log_with_header "Setting smocker to proxy all gitlab requests"
  set_mock "proxy"
  log_success "success!"
  echo ""
fi

log_with_header "Setup database"
bundle exec rake db:create_indexes
log_success "success!"
echo ""

if [[ "${ECOSYSTEM}" == "" ]]; then
  log_success "Setup completed for 'core' container! To start web server and sidekiq process, run 'start_app' command in the terminal."
else
  log_success "Setup completed for '${ECOSYSTEM}' container!"
fi
