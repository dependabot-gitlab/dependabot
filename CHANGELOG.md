## 3.44.0-alpha.1 (2025-03-07)

### 📦 Dependency updates (1 change)

- [Bump dependabot-omnibus from 0.299.0 to 0.300.0](dependabot-gitlab/dependabot@87afc9a5952f5206858c4b221ee111ad4e58e00d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3469

### 🔧 CI changes (1 change)

- [Switch to glab cli for release job](dependabot-gitlab/dependabot@4c64704ba3674632c22ab9e38e713b87e8d43ec3) by @andrcuns. See merge request dependabot-gitlab/dependabot!3468

## 3.43.1-alpha.1 (2025-03-06)

### 🐞 Bug Fixes (1 change)

- [Store repo contents path in environment variable](dependabot-gitlab/dependabot@3968610b10f5a16a0dca0ac8a0b2f51c65ff1572) by @andrcuns. See merge request dependabot-gitlab/dependabot!3467

### 📦🔧 Development dependency updates (1 change)

- [Bump rubocop-factory_bot from 2.26.1 to 2.27.0](dependabot-gitlab/dependabot@28ebafecd0eec862aaf761175d4707fca040eb46) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3466

### 🔧 CI changes (1 change)

- [Do not auto-bump version if release-test fails](dependabot-gitlab/dependabot@024331d0001a1c095c094602dfee363b962850b0) by @andrcuns.

## 3.43.0-alpha.1 (2025-03-05)

### ⚠️ Security updates (1 change)

- [[Security] Bump rack from 3.1.10 to 3.1.11](dependabot-gitlab/dependabot@d439e3f46d74cd69de8b927dde4b1b3bd44af4a3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3464

### 📦 Dependency updates (5 changes)

- [Bump dependabot-omnibus from 0.298.0 to 0.299.0](dependabot-gitlab/dependabot@565e7d1a684cc5e60f05621e20aeccc5c3ffc974) by @andrcuns. See merge request dependabot-gitlab/dependabot!3451
- [Bump turbo-rails from 2.0.11 to 2.0.13](dependabot-gitlab/dependabot@b69a2687f9a7482e680e625f252534271802e04c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3461
- [Update docker Docker tag to v28.0.1](dependabot-gitlab/dependabot@470322537a41519f1ca624c53c9514e3f588ea4f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3448
- [Bump mongoid from 9.0.5 to 9.0.6](dependabot-gitlab/dependabot@3bb9eece43109f948dc1f623a84c7b8492f1daff) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3444
- [Bump anyway_config from 2.7.0 to 2.7.1](dependabot-gitlab/dependabot@a24d7a301903501261922e180f9183e8ac03bc4c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3443

### 📦🔧 Development dependency updates (7 changes)

- [Bump rubocop from 1.72.2 to 1.73.2](dependabot-gitlab/dependabot@7a32f2c461ba26a420c3495d7cd4cc82694d0fcb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3462
- [Bump prettier from 3.5.1 to 3.5.3](dependabot-gitlab/dependabot@8dd6750a738ece453d4b9a33ed99a8188fb494f6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3460
- [Bump axios from 1.7.9 to 1.8.1](dependabot-gitlab/dependabot@66a9090481efbf911cfd3bbb82e5791f2bfaf81b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3459
- [Bump @types/node from 22.13.4 to 22.13.9](dependabot-gitlab/dependabot@b8e439d275c493baf339df85072bcd1320cc600e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3458
- [Bump rubocop-rails from 2.30.1 to 2.30.3](dependabot-gitlab/dependabot@c4ecc440aecbeceecab0280145c9627d4d0bc50c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3456
- [Bump solargraph from 0.51.2 to 0.52.0](dependabot-gitlab/dependabot@b9ee770a1b4d261e2a01e2047963846d1ac0007d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3453
- [Bump git from 2.3.3 to 3.0.0](dependabot-gitlab/dependabot@d0af5c8f48465589bdc6faa6552c2eb02cff851b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3449

### 🔧 CI changes (1 change)

- [Update quay.io/containers/skopeo Docker tag to v1.18.0](dependabot-gitlab/dependabot@06c733777786b0477d071f706967eaba43fe6def) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3454

### 🧰 Maintenance (1 change)

- [Disable experimental type stripping in e2e tests](dependabot-gitlab/dependabot@38e98706826127ca493bc6a6383004b5bef5e494) by @andrcuns. See merge request dependabot-gitlab/dependabot!3463

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/kubernetes from 2.35.1 to 2.36.0 in /deploy](dependabot-gitlab/dependabot@edf9b29a23cef8ab68a929d1f63c24423d398ffa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3457
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@b335c0763f4e97dc7ee8e3b24ac1819507107e92) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3452

## 3.42.0-alpha.1 (2025-02-21)

### 📦 Dependency updates (2 changes)

- [Bump dependabot-omnibus from 0.297.2 to 0.298.0](dependabot-gitlab/dependabot@e6e48de54d61a88df4f99fa0cb0ffd8b614d10bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3437
- [Update docker Docker tag to v28](dependabot-gitlab/dependabot@270bd05c61a17ac2a124ad55e2884bffb2090f87) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3436

### 🚀 Deployment changes (1 change)

- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@91104a79020b42936751e801e9520c4ddf178f46) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3438

## 3.41.0-alpha.1 (2025-02-20)

### ⚠️ Security updates (1 change)

- [[Security] Bump nokogiri from 1.18.2 to 1.18.3](dependabot-gitlab/dependabot@43314bb17340fb0eded9370edcc6f37310cab420) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3430

### 🚀 New features (1 change)

- [Add support for bun ecosystem](dependabot-gitlab/dependabot@37e7b4bded542e3eaab656021ca86d2190d0ec26) by @andrcuns. See merge request dependabot-gitlab/dependabot!3418

### 📦 Dependency updates (2 changes)

- [Bump dependabot-omnibus from 0.297.0 to 0.297.2](dependabot-gitlab/dependabot@19dca8337508fc28230bdcf5988e0ee448608fe8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3434
- [Bump redis from 5.3.0 to 5.4.0](dependabot-gitlab/dependabot@a4d688bed6412f1a6cc633226e30b47e4ec4b116) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3433

### 📦🔧 Development dependency updates (9 changes)

- [Bump simplecov-console from 0.9.2 to 0.9.3](dependabot-gitlab/dependabot@34f2d53e7d9dae8f5bfa1a8da325df6e041e47c2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3429
- [Bump rubocop from 1.72.1 to 1.72.2](dependabot-gitlab/dependabot@6c23ec65aa9fdf49a5682bcd65cf40627636c4ef) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3427
- [Bump prettier from 3.5.0 to 3.5.1](dependabot-gitlab/dependabot@13541fdcdcd348e146dace7a112e4e61e602b611) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3426
- [Bump docker-compose from 1.1.0 to 1.1.1](dependabot-gitlab/dependabot@adb87680a193be3890661d1d8da5fb2a9c9d63bf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3425
- [Bump allure-playwright from 3.1.0 to 3.2.0](dependabot-gitlab/dependabot@d647d5cb9a849caba5d638df775869a11a9cbb10) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3424
- [Bump @types/node from 22.13.1 to 22.13.4](dependabot-gitlab/dependabot@6ec83d1893e4b0c3d8a06b81c333c54c49904eec) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3423
- [Bump rubocop-rspec from 3.4.0 to 3.5.0](dependabot-gitlab/dependabot@da5a3c677a5d0880c70ca269c783be86957bd2ed) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3421
- [Bump rubocop-rails from 2.30.0 to 2.30.1](dependabot-gitlab/dependabot@111361b0a4179fbcc9b9b953642508f5a477a1da) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3420
- [Bump rubocop-rails from 2.29.1 to 2.30.0](dependabot-gitlab/dependabot@1f72fb2676af5bdc17ae498b434fd455a983c22c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3416

### 🔧 CI changes (3 changes)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.22](dependabot-gitlab/dependabot@687c4a3e7a83d9dcb593169bfd44f1e1e63eaf91) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3428
- [Run full dependency update flow test before automated version bumps](dependabot-gitlab/dependabot@3f3fef3fbf8ba0db8e838486fa36236786a85d4c) by @andrcuns. See merge request dependabot-gitlab/dependabot!3422
- [Build nightly images before automated release](dependabot-gitlab/dependabot@663a738d890037c1aaa11b162687fabe0fc7aa29) by @andrcuns. See merge request dependabot-gitlab/dependabot!3419

## 3.40.1-alpha.1 (2025-02-16)

### 🐞 Bug Fixes (2 changes)

- [Use correct environment variable name for job id](dependabot-gitlab/dependabot@b3e83b4828fa0cdad020a9d64890b04094becd20) by @andrcuns. See merge request dependabot-gitlab/dependabot!3417
- [Set dependency update run id](dependabot-gitlab/dependabot@208588cded185d87e747829fe245f9e38cc84279) by @andrcuns. See merge request dependabot-gitlab/dependabot!3413

### 📦 Dependency updates (1 change)

- [Bump sidekiq from 7.3.8 to 7.3.9](dependabot-gitlab/dependabot@b0d436163770af5a9e9e1f8eece7f9e14560d4bd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3411

### 📦🔧 Development dependency updates (3 changes)

- [Bump rubocop-performance from 1.23.1 to 1.24.0](dependabot-gitlab/dependabot@2e9af2c1e05b1f457ebbfe0b37e671f732e05342) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3415
- [Bump rubocop from 1.72.0 to 1.72.1](dependabot-gitlab/dependabot@0da14fba634709014bac57c8dd8ed611cc099933) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3414
- [Bump rubocop from 1.71.2 to 1.72.0](dependabot-gitlab/dependabot@6b3e07523fd4572009861a150498e2d8c2284940) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3412

### 🚀 Deployment changes (1 change)

- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@99e21303ca07992fefc8d4cd9fce82e7317f63fc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3410

## 3.40.0-alpha.1 (2025-02-13)

### ⚠️ Security updates (2 changes)

- [[Security] Bump rack from 3.1.9 to 3.1.10](dependabot-gitlab/dependabot@1b8f76af527ecc1a1ed1ba44f077e67409ebb587) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3408
- [[Security] Bump net-imap from 0.5.2 to 0.5.6](dependabot-gitlab/dependabot@7e735672bd9b8da7f786d16a8e10f250f060162e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3405

### 📦 Dependency updates (5 changes)

- [Bump dependabot-omnibus from 0.292.0 to 0.297.0](dependabot-gitlab/dependabot@a09e27adc2fcf20910c9df3b6fb4ea3ff95a3b72) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3409
- [Bump anyway_config from 2.6.4 to 2.7.0](dependabot-gitlab/dependabot@06f224c2f86095068a7d638754bd882ae2c870e1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3407
- [Bump grape from 2.2.0 to 2.3.0](dependabot-gitlab/dependabot@e6e0e5a9e1e24d61b17fe06a8ddbc911b8e48d63) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3401
- [Bump sentry-sidekiq from 5.22.3 to 5.22.4](dependabot-gitlab/dependabot@66092589c507f83ecd44c01e066cc180c9ca1dfc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3398
- [Bump sentry-rails from 5.22.3 to 5.22.4](dependabot-gitlab/dependabot@eed205670b7d4ad25b2b3473363bdac8d6c16c3e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3397

### 📦🔧 Development dependency updates (3 changes)

- [Bump prettier from 3.4.2 to 3.5.0](dependabot-gitlab/dependabot@083b739edebbe28ec8ad624a46528900b405c507) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3403
- [Bump @types/node from 22.13.0 to 22.13.1](dependabot-gitlab/dependabot@2c1151b66e020e006ef5032e78d8a3daf076d688) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3402
- [Bump rspec-rails from 7.1.0 to 7.1.1](dependabot-gitlab/dependabot@1fa74a4376b91ea5db57e05653de8cc786943833) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3399

## 3.39.3-alpha.1 (2025-02-07)

### 🐞 Bug Fixes (1 change)

- [Do not mount ssl cert volume in updater container if certs are not configured](dependabot-gitlab/dependabot@0b243fb21997eb8f1330bf22983dc195f183cecc) by @andrcuns. See merge request dependabot-gitlab/dependabot!3396

## 3.39.2-alpha.1 (2025-02-06)

### 🐞 Bug Fixes (1 change)

- [Correctly handle default compose ssl cert setup](dependabot-gitlab/dependabot@5d465ebebb89d84deec04a951259ab0853694669) by @andrcuns. See merge request dependabot-gitlab/dependabot!3395

## 3.39.1-alpha.1 (2025-02-05)

### 🐞 Bug Fixes (3 changes)

- [Fix background migrations](dependabot-gitlab/dependabot@82bf56cdeb710a71c1f9d1f02710b24b08540976) by @andrcuns. See merge request dependabot-gitlab/dependabot!3372
- [Fix passing sanitized credentials to dependabot-core subprocess](dependabot-gitlab/dependabot@8c4240038de2e1ffa1470894e0c666cbb8979f9a) by @andrcuns. See merge request dependabot-gitlab/dependabot!3392
- [Fixed incorrect MR being closed as 'superseded'](dependabot-gitlab/dependabot@ef81208ff9f5733121da9995e0f8e54fac5c663a) by @mickverm. See merge request dependabot-gitlab/dependabot!3361

### 📦 Dependency updates (14 changes)

- [Bump json_schemer from 2.3.0 to 2.4.0](dependabot-gitlab/dependabot@1218efe175d9aa46699af1dc6b5a172047027bf5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3386
- [Bump sentry-sidekiq from 5.22.2 to 5.22.3](dependabot-gitlab/dependabot@bd46101426e9edeebda144e6f20908c75fdff0fb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3382
- [Bump mongoid from 9.0.4 to 9.0.5](dependabot-gitlab/dependabot@83edbe6301a4f06bb36130876884e058d0e37cd5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3384
- [Bump sentry-rails from 5.22.2 to 5.22.3](dependabot-gitlab/dependabot@b058dbcc1c04e95af159b17419262986c0c58210) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3381
- [Bump puma from 6.5.0 to 6.6.0](dependabot-gitlab/dependabot@9efc24b547f846b6e52163514f001f3a35738f9c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3379
- [Bump terminal-table from 3.0.2 to 4.0.0](dependabot-gitlab/dependabot@184faa3afe2f71982237dd13fa4aae1c2cb8a4cb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3378
- [Bump sentry-sidekiq from 5.22.1 to 5.22.2](dependabot-gitlab/dependabot@eda2b096c9a10ba473d4e9b9c4be3716273a9b34) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3368
- [Bump sentry-rails from 5.22.1 to 5.22.2](dependabot-gitlab/dependabot@ec9db59c994d41e8ecc761697e7028520e7755bd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3367
- [Update docker Docker tag to v27.5.1](dependabot-gitlab/dependabot@c0e2e9fb92aad64d4e576f86cd912104ccd2c4e8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3364
- [Bump sidekiq from 7.3.7 to 7.3.8](dependabot-gitlab/dependabot@6d0f091bab295a1ef5c7196318ff2eb091f6fc6d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3359
- [Bump sidekiq-cron from 2.0.1 to 2.1.0](dependabot-gitlab/dependabot@6ecff71373e65a8486371db3b0797de34a4b5026) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3354
- [Bump tzinfo-data from 1.2024.2 to 1.2025.1](dependabot-gitlab/dependabot@aea6cc34c74a1d2bc333f20097845a78c67630ac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3351
- [Bump stackprof from 0.2.26 to 0.2.27](dependabot-gitlab/dependabot@bb3d3682c73342e687a2c576752738a5374d04af) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3350
- [Update docker Docker tag](dependabot-gitlab/dependabot@a43f259a232774619becf63292ba076f14725cd7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3349

### 📦🔧 Development dependency updates (22 changes)

- [Bump @readme/openapi-parser from 2.6.0 to 2.7.0](dependabot-gitlab/dependabot@017106737d867caad2471cadd9f0e619ceed718e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3375
- [Bump rubocop from 1.71.1 to 1.71.2](dependabot-gitlab/dependabot@5337fe83e767a19b0de092400e9d0220222b0a2c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3391
- [Bump allure-playwright from 3.0.9 to 3.1.0](dependabot-gitlab/dependabot@a67fa17b4743cf93406d1a9ba4e9727b4ef6f21e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3390
- [Bump @types/node from 22.10.10 to 22.13.0](dependabot-gitlab/dependabot@38af0f5b3b888d85b9b6886f2bbd7f17a1ab3920) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3389
- [Bump @playwright/test from 1.50.0 to 1.50.1](dependabot-gitlab/dependabot@3050af11a78b83899b328472fa1cf2e88547693b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3388
- [Bump solargraph from 0.51.1 to 0.51.2](dependabot-gitlab/dependabot@fbdec15e28c58e0e4a3d6e8922601bf34a5ddfe9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3387
- [Bump rubocop from 1.71.0 to 1.71.1](dependabot-gitlab/dependabot@047674f57c350c2e54e17747d9b80f646cf783d8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3385
- [Bump solargraph-rspec from 0.4.0 to 0.4.1](dependabot-gitlab/dependabot@10ab5dfadac5f68acf2db43a9fbf041daed96b25) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3380
- [Bump vitepress from 1.6.0 to 1.6.3](dependabot-gitlab/dependabot@8bd325ea51f80b75be617ce9b8be1c54907eb90e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3377
- [Bump @types/node from 22.10.7 to 22.10.10](dependabot-gitlab/dependabot@218714323c904b56291e5a5eacc25fe2134bba45) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3376
- [Bump @playwright/test from 1.49.1 to 1.50.0](dependabot-gitlab/dependabot@c1f227bccdb894e6d587ec74ed23d3edb9ed57e6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3374
- [Bump reek from 6.3.0 to 6.4.0](dependabot-gitlab/dependabot@f132ca9b5afcdf73f6ab3e6c66c82709dff26518) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3345
- [Bump rubocop from 1.70.0 to 1.71.0](dependabot-gitlab/dependabot@8e8539b5dc1048d2cea0e08aa70f4e43f4638cf1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3363
- [Bump @types/node from 22.10.5 to 22.10.7](dependabot-gitlab/dependabot@6f2deecbcef0d1e89a22f1fb03b6069f6137d519) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3356
- [Bump randomstring from 1.3.0 to 1.3.1](dependabot-gitlab/dependabot@0defc26a2390984beefb9c97d825ee331720516f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3347
- [Bump rubocop-rails from 2.29.0 to 2.29.1](dependabot-gitlab/dependabot@8ce29a91b0eebdf83c37720076ba33c823381279) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3369
- [Bump solargraph from 0.51.0 to 0.51.1](dependabot-gitlab/dependabot@d7d0db0b33604af52fea1ad5f46aa38fe9989718) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3365
- [Bump rubocop-rspec from 3.3.0 to 3.4.0](dependabot-gitlab/dependabot@b2c3be6aca36423b519e3cf1d7221e432f7b2dbd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3360
- [Bump vitepress from 1.5.0 to 1.6.0](dependabot-gitlab/dependabot@9166152fefb28a6bb8c7125392e8e7a84b7254b1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3358
- [Bump allure-playwright from 3.0.7 to 3.0.9](dependabot-gitlab/dependabot@6d893ae7b42b1e4571c68b6d36c01d924470de97) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3357
- [Bump solargraph from 0.50.0 to 0.51.0](dependabot-gitlab/dependabot@85086b9e60951c39c045e9d88e7bf532bcdebfc1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3355
- [Bump rubocop-rails from 2.28.0 to 2.29.0](dependabot-gitlab/dependabot@748aefdf7bcb60c30e4c5fbf22e664c97d2cac55) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3353

### 🔧 CI changes (3 changes)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.21](dependabot-gitlab/dependabot@ce0c6c6b4746b033c20e60967feb3eaf6a6953ff) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3373
- [Export extra node certs after file download](dependabot-gitlab/dependabot@940d832bdae4f4b136992d27c0dcd5311f750451) by @andrcuns. See merge request dependabot-gitlab/dependabot!3371
- [Increase wait for e2e test setup and correctly wait for multi-arch](dependabot-gitlab/dependabot@ea50282768c5cd4c6c3eb1ce62f5b4053651f193) by @andrcuns. See merge request dependabot-gitlab/dependabot!3371

### 🧰 Maintenance (2 changes)

- [Remove custom check for pending background migrations](dependabot-gitlab/dependabot@0925049017b736d571fe19420e44222f20faeddb) by @andrcuns. See merge request dependabot-gitlab/dependabot!3393
- [Update local ruby version and parser gem](dependabot-gitlab/dependabot@53c38cebcc6aa8ed6fa8babed9612deefc7ebab5) by @andrcuns. See merge request dependabot-gitlab/dependabot!3370

### 🚀 Deployment changes (3 changes)

- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@5edcb1927d34219b1e04a3b053c81af788e2409e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3366
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@0d132eda5029c8e438dbcbd14e5a8d1d321907bc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3352
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@c1322920992a40b71971db7b316108117a81eb05) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3344

## 3.39.0-alpha.1 (2025-01-10)

### 🚀 New features (2 changes)

- [Add option to set unique configuration entry name](dependabot-gitlab/dependabot@0fcf43ae448a3080ea55c6d0dc6b5264253f5564) by @andrcuns. See merge request dependabot-gitlab/dependabot!3336
- [Add support for custom GitLab certificates](dependabot-gitlab/dependabot@f95179906efe7699555c736e01bde4c76425db03) by @andrcuns. See merge request dependabot-gitlab/dependabot!3321

### 🔬 Improvements (1 change)

- [Enable native helpers command timeout](dependabot-gitlab/dependabot@12bac76f86ee69760c0f5d19b768be01f5471c07) by @andrcuns. See merge request dependabot-gitlab/dependabot!3318

### 🐞 Bug Fixes (7 changes)

- [Fix superseded mr closing for configs with directory glob](dependabot-gitlab/dependabot@0d25039e15942a1a01da0e6f78266811e9350976) by @andrcuns. See merge request dependabot-gitlab/dependabot!3332
- [Fix python ecosystem failing due to missing regexp](dependabot-gitlab/dependabot@e0c9aaa365f6e8fc43b4e73f278dac146850c0d6) by @andrcuns. See merge request dependabot-gitlab/dependabot!3331
- [Add missing repo_contents_path arg that breaks nuget updater](dependabot-gitlab/dependabot@8e76866e0700be0a93ef7e9d7da56f0959d706e2) by @andrcuns. See merge request dependabot-gitlab/dependabot!3330
- [Convert credentials to hash before passing to subprocess](dependabot-gitlab/dependabot@9e9d4098b495a3e3e419b575258f7d232f6c1f7d) by @andrcuns. See merge request dependabot-gitlab/dependabot!3312
- [Fix error class name in SharedHelpersPatch](dependabot-gitlab/dependabot@7f3b93e20cabee4c5efeec4e889c79fd866b24a2) by @andrcuns. See merge request dependabot-gitlab/dependabot!3311
- [Ensure job.json is created before fetching dependency files](dependabot-gitlab/dependabot@2840c3459d5f74bec4523a2edb759a7efeb1064e) by @andrcuns. See merge request dependabot-gitlab/dependabot!3307
- [Create dependabot-job object before file fetcher creation](dependabot-gitlab/dependabot@1b4f19c65f040ec2789836222516a5117989a999) by @andrcuns. See merge request dependabot-gitlab/dependabot!3306

### 📦 Dependency updates (7 changes)

- [Bump dependabot-omnibus from 0.291.0 to 0.292.0](dependabot-gitlab/dependabot@bb8c835844235d84447cf80ec9a0e91ecd3faa7d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3343
- [Bump mongoid from 9.0.3 to 9.0.4](dependabot-gitlab/dependabot@a68867849587723f7c69711d25939d2b7ec9f61e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3341
- [Bump grape-swagger from 2.1.1 to 2.1.2](dependabot-gitlab/dependabot@7a349d2b589756a0a92f32e2a8865965c4263a0e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3340
- [Bump dartsass-sprockets from 3.1.0 to 3.2.0](dependabot-gitlab/dependabot@861571848b7be52db25cd91c26a88ff9ef8ca279) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3333
- [[Breaking] Switch to official redis and mongo images for docker compose setup](dependabot-gitlab/dependabot@2595f7d1e88ec4d90340efc658d095caedfae883) by @andrcuns. See merge request dependabot-gitlab/dependabot!3322
- [Bump rails from 7.2.2.1 to 8.0.1](dependabot-gitlab/dependabot@63005edf9786fc35f0a99c2a489b3ec7ca0baf82) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3290
- [Bump importmap-rails from 2.0.3 to 2.1.0](dependabot-gitlab/dependabot@9c6e95766030f74cc959bc36cd41945d6d3a999d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3305

### 📦🔧 Development dependency updates (4 changes)

- [Bump rubocop from 1.69.2 to 1.70.0](dependabot-gitlab/dependabot@c5b1ce45ce23a9bd4283b45bf9d71c2c2c9ba0b6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3342
- [Bump @types/node from 22.10.2 to 22.10.5](dependabot-gitlab/dependabot@72afd13072a4847d899348f350633e48c2caafc5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3335
- [Bump rubocop-performance from 1.23.0 to 1.23.1](dependabot-gitlab/dependabot@45fd1e4ea1c88c1de63d0dd8c6f161db2523b5e4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3334
- [Bump rubocop-rails from 2.27.0 to 2.28.0](dependabot-gitlab/dependabot@6d30b088c45cb94e80d9d1ce3caa5165bf594798) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3313

### 🔧 CI changes (1 change)

- [Fix updated tag for e2e tests](dependabot-gitlab/dependabot@9fc8604a7a4f52e5c412b9653ce63f852d827139) by @andrcuns. See merge request dependabot-gitlab/dependabot!3327

### 🧰 Maintenance (11 changes)

- [Remove spring rubygem](dependabot-gitlab/dependabot@5529c7529048c2c162a847fd8269845c56733a81) by @andrcuns. See merge request dependabot-gitlab/dependabot!3329
- [Use fixtures in e2e tests](dependabot-gitlab/dependabot@76db69c8a3b759dc09d1b8f2b7ce6e9a0dc46fe7) by @andrcuns. See merge request dependabot-gitlab/dependabot!3328
- [Implement dependency update E2E test](dependabot-gitlab/dependabot@9e9d516be44fcabffa0e035efedae8bad77b6e9f) by @andrcuns. See merge request dependabot-gitlab/dependabot!3326
- [[Breaking] Remove api v1 code](dependabot-gitlab/dependabot@d6c0d0163d2c3e01acbb15bca10d641e74be98d8) by @andrcuns. See merge request dependabot-gitlab/dependabot!3323
- [[Breaking] Remove global vulnerability-alert option](dependabot-gitlab/dependabot@abaef8d7078b14e88425cb138481197df4af7280) by @andrcuns. See merge request dependabot-gitlab/dependabot!3324
- [Use single ssl mock endpoint in tests](dependabot-gitlab/dependabot@9e458e2f6583da8bd1e4427041894bc300a1b657) by @andrcuns. See merge request dependabot-gitlab/dependabot!3319
- [Remove rails_healthcheck gem](dependabot-gitlab/dependabot@01cb682013b2b10dcef925b94b0ae891a1d4b980) by @andrcuns. See merge request dependabot-gitlab/dependabot!3317
- [Update configuration defaults](dependabot-gitlab/dependabot@0c28ab5747c86a9aa81b129297f7eb10420c1e7a) by @andrcuns. See merge request dependabot-gitlab/dependabot!3316
- [Split devcontainer compose files](dependabot-gitlab/dependabot@8ec0574eabd539e50f9dfbd5aa5475533db6a8c7) by @andrcuns. See merge request dependabot-gitlab/dependabot!3315
- [Improve devcontainer setup](dependabot-gitlab/dependabot@d1ff3cef57b8ee996b429f6ad54ab96cbcd62107) by @andrcuns. See merge request dependabot-gitlab/dependabot!3314
- [Remove complex monkeypatch of helper subprocess method](dependabot-gitlab/dependabot@5bbb6cc9fb510d03cf6586d81b25134082495031) by @andrcuns. See merge request dependabot-gitlab/dependabot!3310

### 🚀 Deployment changes (3 changes)

- [Bump hashicorp/kubernetes from 2.35.0 to 2.35.1 in /deploy](dependabot-gitlab/dependabot@3a774cf37d4f19861965c09d44b26bc521b9509e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3309
- [Bump hashicorp/helm from 2.16.1 to 2.17.0 in /deploy](dependabot-gitlab/dependabot@5d6fbd07756331d02677c9a3297b10f9765922d2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3308
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@1a6ca68e7536e286bb814069cc60fdf23d685715) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3304

## 3.38.0-alpha.1 (2024-12-20)

### 📦 Dependency updates (6 changes)

- [Bump dependabot-omnibus from 0.290.0 to 0.291.0](dependabot-gitlab/dependabot@2d1318258d720ea12a6aa0d4f2f980d0f6d2ca08) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3303
- [Update docker Docker tag to v27.4.1](dependabot-gitlab/dependabot@6074c987ee11694f664372ea6a905598a6d93535) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3302
- [Bump warning from 1.4.0 to 1.5.0](dependabot-gitlab/dependabot@01a4f06dde06c0dca3433b2a255c776a79e77901) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3300
- [Bump sidekiq from 7.3.6 to 7.3.7](dependabot-gitlab/dependabot@df536c9542ddc7f072b468a503f66bee1a0e1e1d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3299
- [Bump sentry-sidekiq from 5.22.0 to 5.22.1](dependabot-gitlab/dependabot@161b449030aa801df6009a3b5b4112ddcc494df0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3298
- [Bump sentry-rails from 5.22.0 to 5.22.1](dependabot-gitlab/dependabot@2e25e9897dac311e5865f77709ad4061ea7b8e28) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3297

### 📦🔧 Development dependency updates (4 changes)

- [Bump debug from 1.9.2 to 1.10.0](dependabot-gitlab/dependabot@0558a30e9b53d34fdb28b416a4c8064a5b2e06c1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3301
- [[Security] Bump nanoid from 3.3.7 to 3.3.8](dependabot-gitlab/dependabot@766b7ac2ae5ec31ea0e60ce1cc3d9e9ffd138b42) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3296
- [Bump @types/node from 22.10.1 to 22.10.2](dependabot-gitlab/dependabot@797473cf6c658019ec7431b9b9aff68f1a0c580e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3295
- [Bump @playwright/test from 1.49.0 to 1.49.1](dependabot-gitlab/dependabot@acb42cc4a7c1afd399bd9572b7f1de5ab70cdd3a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3294

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/kubernetes from 2.34.0 to 2.35.0 in /deploy](dependabot-gitlab/dependabot@2c19da09c85a26f61a083e98e8176d2ae94bd9da) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3293

## 3.37.0-alpha.1 (2024-12-15)

### 🔬 Improvements (1 change)

- [Create dependabot job json file and enable native nuget updater](dependabot-gitlab/dependabot@4d0e3c91538a704affad78bc1ff99e5576cbedd2) by @andrcuns. See merge request dependabot-gitlab/dependabot!3291

### 🐞 Bug Fixes (1 change)

- [Add run_id index to LogEntry model](dependabot-gitlab/dependabot@6da9e8b224dfa8d27b6060114c314cef6f50ad69) by @andrcuns. See merge request dependabot-gitlab/dependabot!3292

### 🚀 Deployment changes (1 change)

- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@b50cd56ca3c88c1f59533615decd3841ee5e0b75) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3289

## 3.35.1-alpha.1 (2024-12-12)

### ⚠️ Security updates (1 change)

- [[Security] Bump actionpack from 7.2.2 to 7.2.2.1](dependabot-gitlab/dependabot@58611d3e4aa314f2afe6e6daf080d8b652ed5a55) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3282

### 🐞 Bug Fixes (1 change)

- [Add workaround for nuget ecosystem which requires DEPENDABOT_JOB_PATH](dependabot-gitlab/dependabot@cb1f5035bdd43eb0c66f5c523282e1c9d88abfbc) by @andrcuns. See merge request dependabot-gitlab/dependabot!3284

### 📦 Dependency updates (1 change)

- [Update docker Docker tag](dependabot-gitlab/dependabot@aea68a82b0741cc3d8133ffcb0e70680634422c7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3280

### 📦🔧 Development dependency updates (2 changes)

- [Bump prettier from 3.4.1 to 3.4.2](dependabot-gitlab/dependabot@d6db01e9ea4b459341581aa037d864322b983038) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3277
- [Bump allure-playwright from 3.0.6 to 3.0.7](dependabot-gitlab/dependabot@3354a3e59c9b5d26604186fc57ca23765e9f464e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3276

### 🔧 CI changes (2 changes)

- [Update mongo Docker tag to v8.0.4](dependabot-gitlab/dependabot@8477ad2bab45258992f6c592e4a6a9ce65716c87) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3279
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.20](dependabot-gitlab/dependabot@c8f500a96a0a4fdbe1f90b3e6fe2d43be4859f2a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3275

### 📄 Documentation updates (1 change)

- [Fix incorrect reference to example code](dependabot-gitlab/dependabot@71a871c5618b4b8e32554f569dbb4284bfa9c2b5) by @tbraunjones. See merge request dependabot-gitlab/dependabot!3259

### 🚀 Deployment changes (1 change)

- [Fetch latest dev chart from gitlab package registry](dependabot-gitlab/dependabot@c0090db15cb2345c1f44a887047e5edd9b3899ac) by @andrcuns. See merge request dependabot-gitlab/dependabot!3278

## 3.35.0-alpha.1 (2024-12-06)

### ⚠️ Security updates (1 change)

- [[Security] Bump rails-html-sanitizer from 1.6.0 to 1.6.1](dependabot-gitlab/dependabot@52cd1e7c9f7e685e4ed8f1e7fb69760bbfe690fc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3270

### 📦 Dependency updates (3 changes)

- [Bump dependabot-omnibus from 0.288.0 to 0.289.0](dependabot-gitlab/dependabot@297bfc92f011515aabbd703532ea1edabc37bf7c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3274
- [Bump sentry-sidekiq from 5.21.0 to 5.22.0](dependabot-gitlab/dependabot@6bcfc17c8c5582fe5eb265ad2bc76ded0b934b64) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3272
- [Bump puma from 6.4.3 to 6.5.0](dependabot-gitlab/dependabot@85801de410b2fabb0a7ccc8e81b7f99ae382827f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3260

### 📦🔧 Development dependency updates (8 changes)

- [Bump git from 2.3.2 to 2.3.3](dependabot-gitlab/dependabot@b76f78455db6e467c15792e4c6c9d7c90f7d966d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3273
- [Bump rubocop from 1.69.0 to 1.69.1](dependabot-gitlab/dependabot@16521ce488cd423263d66c03d1461bf4c1f55aee) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3269
- [Bump prettier from 3.3.3 to 3.4.1](dependabot-gitlab/dependabot@153ed95884ef3e9a4d3234c329b69124badc07e5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3268
- [Bump @types/node from 22.9.3 to 22.10.1](dependabot-gitlab/dependabot@e47d931cec992e41911cfe17e2887e920ff3c61d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3267
- [Bump rubocop-performance from 1.22.1 to 1.23.0](dependabot-gitlab/dependabot@e74870dfbd2fb5cad3b6161cc3c16ad5dad14d30) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3248
- [Bump rubocop from 1.68.0 to 1.69.0](dependabot-gitlab/dependabot@7292fea4d11212ba78010a38b24b702ced881b4c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3263
- [Bump @types/node from 22.9.0 to 22.9.3](dependabot-gitlab/dependabot@1a56d1a0ee169c1955300a19e4553ca5815cd44f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3262
- [Bump @playwright/test from 1.48.2 to 1.49.0](dependabot-gitlab/dependabot@9501901066fa83966ccf20fd9e7c036d91f91106) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3261

### 🔧 CI changes (1 change)

- [Update quay.io/containers/skopeo Docker tag to v1.17.0](dependabot-gitlab/dependabot@d76410a9e765913bc6a8096383bf7a6463861949) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3256

### 🚀 Deployment changes (3 changes)

- [Bump hashicorp/kubernetes from 2.33.0 to 2.34.0 in /deploy](dependabot-gitlab/dependabot@a70d9068792d95edf4fc4fcf95f6fee4bd57d834) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3266
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@3651df217eb53a1f264613cee63dbaa9f474f13d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3265
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@d989fa9c2c3f0ddc71535f8e37b4dcf976e39173) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3258

## 3.34.0-alpha.1 (2024-11-22)

### 📦 Dependency updates (2 changes)

- [Bump dependabot-omnibus from 0.287.0 to 0.288.0](dependabot-gitlab/dependabot@5514f2a60bb554ae19099d53150adb0ff5896ee4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3257
- [Bump sidekiq from 7.3.5 to 7.3.6](dependabot-gitlab/dependabot@c97099f30bef6eaa94e672434f1afd03de2f8318) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3255

## 3.33.0-alpha.1 (2024-11-20)

### 📦 Dependency updates (2 changes)

- [Bump dependabot-omnibus from 0.286.0 to 0.287.0](dependabot-gitlab/dependabot@61885ba95e097fe8d61a00db983576325195fd67) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3254
- [Bump mongoid_rails_migrations from 1.4.0 to 1.6.1](dependabot-gitlab/dependabot@a9c7ef1af5e110aedfef59418156e09a7b712eca) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3252

### 📦🔧 Development dependency updates (2 changes)

- [Bump git from 2.3.1 to 2.3.2](dependabot-gitlab/dependabot@035fcf788486c24b6d7af4915b90ea085536b88f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3253
- [Bump vue from 3.5.12 to 3.5.13](dependabot-gitlab/dependabot@fa517b217f2893c351abe93bdf7f1126ef4d2f07) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3251

### 🚀 Deployment changes (1 change)

- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@bc17306c8f4336d7f10799cc1536add55ca91f10) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3250

## 3.32.0-alpha.1 (2024-11-15)

### 📦 Dependency updates (4 changes)

- [Bump dependabot-omnibus from 0.285.0 to 0.286.0](dependabot-gitlab/dependabot@e475607b7bceb33ad6d24328095f9a280bbbfd34) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3249
- [Bump sidekiq-cron from 2.0.0 to 2.0.1](dependabot-gitlab/dependabot@3d78bbf7192a54b4c04ecd27739aca658a0081c0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3247
- [Bump graphql-client from 0.23.0 to 0.24.0](dependabot-gitlab/dependabot@6d34f737921b9b048cfc330239df9230ca24d704) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3246
- [Bump mongoid from 9.0.2 to 9.0.3](dependabot-gitlab/dependabot@857de77595cd9201e3a95f22bfa3656f59a08281) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3244

## 3.31.0-alpha.1 (2024-11-12)

### 🚀 New features (1 change)

- [Add option to force ssl for app access](dependabot-gitlab/dependabot@61d73acc4c6667e62d8b1ac9f8b1b0b8d571af63) by @andrcuns. See merge request dependabot-gitlab/dependabot!3241

### 📦 Dependency updates (1 change)

- [Bump sidekiq-cron from 1.12.0 to 2.0.0](dependabot-gitlab/dependabot@53aa09cc545b6bd2ff05bb0c34663933b22c8f81) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3242

### 📦🔧 Development dependency updates (3 changes)

- [Bump @types/node from 22.8.7 to 22.9.0](dependabot-gitlab/dependabot@2cdf326cfa13ba29e3d4774235f0aa344c7e90b9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3243
- [Bump rspec-rails from 7.0.1 to 7.1.0](dependabot-gitlab/dependabot@3c3d35e88bd571a5318352bdf01d37bf915ba64d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3240
- [Bump allure-playwright from 3.0.5 to 3.0.6](dependabot-gitlab/dependabot@b2e030c36d17ac7e80a6420d1324570df66ac2de) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3235

### 🧰 Maintenance (1 change)

- [Update pre-commit config](dependabot-gitlab/dependabot@2bf0164836d88b487636a854cb351201e6878950) by @andrcuns. See merge request dependabot-gitlab/dependabot!3241

## 3.30.0-alpha.1 (2024-11-10)

### 📦 Dependency updates (2 changes)

- [Bump dependabot-omnibus from 0.283.0 to 0.285.0](dependabot-gitlab/dependabot@6bf87b9187f23f215d399f014ec0a9834efd76bd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3239
- [Bump sidekiq from 7.3.4 to 7.3.5](dependabot-gitlab/dependabot@9287430ad5b893889064de01ca859a93f4867609) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3237

### 📦🔧 Development dependency updates (2 changes)

- [Bump vitepress from 1.4.1 to 1.5.0](dependabot-gitlab/dependabot@2c2b5cbee7ccedd9bf9fb7e5732d7ac969592018) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3236
- [Bump @types/node from 22.8.1 to 22.8.7](dependabot-gitlab/dependabot@773dfe124e5e1aa5711d282383be485ee151d93c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3234

## 3.29.0-alpha.1 (2024-11-01)

### ⚠️ Security updates (1 change)

- [[Security] Bump rexml from 3.3.6 to 3.3.9](dependabot-gitlab/dependabot@af681985ed35e37725801d9170a1ad0f54bac36f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3229

### 📦 Dependency updates (2 changes)

- [Bump dependabot-omnibus from 0.282.0 to 0.283.0](dependabot-gitlab/dependabot@40c24cf7be2a382f965c6e1a53c04773385ecd8e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3233
- [Bump rails from 7.2.1.2 to 7.2.2](dependabot-gitlab/dependabot@6ad5e995b26b0b093f9439563561e99eb0ad079c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3230

### 📦🔧 Development dependency updates (6 changes)

- [Bump rubocop from 1.67.0 to 1.68.0](dependabot-gitlab/dependabot@5dfc8eea35934b5e97d550bc1129a4089c1b416f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3231
- [Bump @types/node from 22.7.7 to 22.8.1](dependabot-gitlab/dependabot@215e9d947f7993c33c55b1cad5f8da70393be9d1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3228
- [Bump @playwright/test from 1.48.1 to 1.48.2](dependabot-gitlab/dependabot@7e8b16a9a90c3373bfa7ea97bd38b30059cb57ae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3227
- [Bump rubocop-rspec from 3.1.0 to 3.2.0](dependabot-gitlab/dependabot@7a68b63374ef16e4c2eff663c8b0481855a2bc60) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3225
- [Bump rubocop-rails from 2.26.2 to 2.27.0](dependabot-gitlab/dependabot@ae4723dcdae7608aae4f508670ea2abeca6a02ae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3224
- [Bump factory_bot_rails from 6.4.3 to 6.4.4](dependabot-gitlab/dependabot@eaa5811385071d6b18c432657e36254cbe265399) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3223

### 🔧 CI changes (1 change)

- [Update mongo Docker tag to v8.0.3](dependabot-gitlab/dependabot@cb3da60fd901ada0145f05943d84a5526f391ad2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3226

### 🚀 Deployment changes (1 change)

- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@663bf594d02bc8ed64712f11373e598867c2fc6d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3222

## 3.28.0-alpha.1 (2024-10-25)

### 📦 Dependency updates (3 changes)

- [Bump dependabot-omnibus from 0.281.0 to 0.282.0](dependabot-gitlab/dependabot@28c0ecd37f8e25ed9a0c689f7289ae75ee92c71d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3221
- [Bump sidekiq from 7.3.2 to 7.3.4](dependabot-gitlab/dependabot@1dec8a6bb8569036ac239f975ca0be485c288e1d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3219
- [Bump rails from 7.2.1.1 to 7.2.1.2](dependabot-gitlab/dependabot@fac0072e6cf193bd5400d69c97ebce102b9a92d0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3218

### 📦🔧 Development dependency updates (6 changes)

- [Bump git from 2.3.0 to 2.3.1](dependabot-gitlab/dependabot@afde33ec5ba283a63c4b54e27d307d860606d458) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3220
- [Bump @types/node from 22.7.5 to 22.7.7](dependabot-gitlab/dependabot@811f66d478370f796286226d37c7744bfec92c19) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3216
- [Bump @playwright/test from 1.48.0 to 1.48.1](dependabot-gitlab/dependabot@4822a411eaa94ba790f2d625bd06d0e2f839b660) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3215
- [Bump git from 1.19.1 to 2.3.0](dependabot-gitlab/dependabot@5db565709a5682139cb60322e0cafe36bd748180) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3110
- [Bump faker from 3.4.2 to 3.5.1](dependabot-gitlab/dependabot@a3cc1891afbcd2aa94485b814052804c2edee790) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3212
- [Bump docker-compose from 0.24.8 to 1.1.0](dependabot-gitlab/dependabot@ccf12f4776b6d4e89da3381bad213059395f6f0a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3183

### 🔧 CI changes (1 change)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.19](dependabot-gitlab/dependabot@ee6f89989d78cfb5fc4d6f2507d3bdcec059135a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3217

### 🧰 Maintenance (1 change)

- [Improve devcontainer setup](dependabot-gitlab/dependabot@c1fa66cb59e65d298cfb36f8b8932aaacb2fa37d) by @andrcuns. See merge request dependabot-gitlab/dependabot!3213

### 📄 Documentation updates (1 change)

- [Update documentation on directories support for standalone mode](dependabot-gitlab/dependabot@25bea8acd6342d55798627590e7c35d4daf53c86) by @andrcuns. See merge request dependabot-gitlab/dependabot!3211

### 🚀 Deployment changes (3 changes)

- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@663bf594d02bc8ed64712f11373e598867c2fc6d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3222
- [Bump hashicorp/helm from 2.16.0 to 2.16.1 in /deploy](dependabot-gitlab/dependabot@257a05beeb1fe61ae0cae56174e92b91eab3bfcf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3214
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@673c7ad39efefede32b8009e7c9674fa016e3892) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3210

## 3.27.0-alpha.1 (2024-10-17)

### ⚠️ Security updates (3 changes)

- [[Security] Bump actiontext from 7.2.1 to 7.2.1.1](dependabot-gitlab/dependabot@631425929ea00da29a24fa418edccd14b4b644c5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3206
- [[Security] Bump actionpack from 7.2.1 to 7.2.1.1](dependabot-gitlab/dependabot@6a34975fad8ff5c7ed4dd607cfae310b638c3981) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3205
- [[Security] Bump actionmailer from 7.2.1 to 7.2.1.1](dependabot-gitlab/dependabot@3f74b07e05937beb577400925bc718d6a966de6b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3204

### 📦 Dependency updates (7 changes)

- [Bump dependabot-omnibus from 0.277.0 to 0.281.0](dependabot-gitlab/dependabot@b099233799bcebbac71d7f418deb3bca584082b6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3209
- [Bump rails from 7.2.1 to 7.2.1.1](dependabot-gitlab/dependabot@962bb2d94f95a48972bbf04512fb1ba86cafce29) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3202
- [Bump turbo-rails from 2.0.10 to 2.0.11](dependabot-gitlab/dependabot@d94d6297be50ce3dde177d920f36a292bf17d1f8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3201
- [Bump importmap-rails from 2.0.2 to 2.0.3](dependabot-gitlab/dependabot@a9ab6d308c811b499e72ae44f7159a234cb3eea4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3192
- [Bump sentry-rails from 5.20.1 to 5.21.0](dependabot-gitlab/dependabot@8e68d38c563e916ddf0fba687c7da08080c17725) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3190
- [Bump importmap-rails from 2.0.1 to 2.0.2](dependabot-gitlab/dependabot@080ad825f3ccaf88b18924f5fba07c0c27c3e117) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3187
- [Bump sentry-rails from 5.19.0 to 5.20.1](dependabot-gitlab/dependabot@758dd1f49ec696a63045e985262213dca92bdf65) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3177

### 📦🔧 Development dependency updates (10 changes)

- [Bump rubocop from 1.66.1 to 1.67.0](dependabot-gitlab/dependabot@bc19f1d354b3f8bf2d411be45e58e8c6cd96a16d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3203
- [Bump allure-playwright from 3.0.3 to 3.0.5](dependabot-gitlab/dependabot@51b01b06f08c37d4fc8bb23680583f5369d61b4c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3199
- [Bump vitepress from 1.3.4 to 1.4.1](dependabot-gitlab/dependabot@36df9ed4e933097c4908818d4b389216f989281b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3200
- [Bump @types/node from 22.7.4 to 22.7.5](dependabot-gitlab/dependabot@32d5a14be3609996b966dc9dac06860706305e65) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3198
- [Bump @playwright/test from 1.47.2 to 1.48.0](dependabot-gitlab/dependabot@815bb2fd7f3c030658b529250a383e5a668408e1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3197
- [Bump rubocop-rspec from 3.0.5 to 3.1.0](dependabot-gitlab/dependabot@98be50afdc504830061a1b9fbacf68eabe2135f3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3186
- [Bump allure-playwright from 3.0.1 to 3.0.3](dependabot-gitlab/dependabot@54100d1206cb1c444db9e32a2f15f822282ca48d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3182
- [[Security] Bump rollup from 4.21.0 to 4.22.5](dependabot-gitlab/dependabot@4ec03e620e12d4a100e7fbfc2385594c20892b84) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3185
- [Bump vue from 3.5.8 to 3.5.10](dependabot-gitlab/dependabot@21c1076b222a581eed42de71aa90a5e4c0a5f983) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3184
- [Bump @types/node from 22.5.5 to 22.7.4](dependabot-gitlab/dependabot@48c0bfd5cd0c434e18b1c41ffa10557b7cf0ad56) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3181

### 🔧 CI changes (2 changes)

- [Add environment destroy job](dependabot-gitlab/dependabot@fe047a98a62493b355b2e73cc5777cb6e24af281) by @andrcuns. See merge request dependabot-gitlab/dependabot!3188
- [Add option to skip automatic deploy from main](dependabot-gitlab/dependabot@d7bd4edea1434ad038616612b360cf2e136dd641) by @andrcuns. See merge request dependabot-gitlab/dependabot!3179

### 🧰 Maintenance (1 change)

- [Remove ruby-lsp dependency](dependabot-gitlab/dependabot@4c226e3a6eb7bb83f60812db03307d4247efd2e9) by @andrcuns. See merge request dependabot-gitlab/dependabot!3194

### 🚀 Deployment changes (6 changes)

- [Bump hashicorp/kubernetes from 2.32.0 to 2.33.0 in /deploy](dependabot-gitlab/dependabot@e03e93deb6314a7d7e6f4abe3867b495a8abe707) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3196
- [Bump hashicorp/helm from 2.15.0 to 2.16.0 in /deploy](dependabot-gitlab/dependabot@d5a00a32e853bf2467a1279c3e1e5b2be2d8b869) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3195
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@101c88a17ff6bd6db11722d4a9b2308c8ff6d879) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3193
- [Move deployment to self managed k8s cluster](dependabot-gitlab/dependabot@f6e6854f427bce1e5263994afc8186f1df082c12) by @andrcuns. See merge request dependabot-gitlab/dependabot!3189
- [Bump hashicorp/google from 6.3.0 to 6.4.0 in /deploy](dependabot-gitlab/dependabot@f2051f92490cf19cff03555e31b0645a150a669f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3180
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@b8ae1598c868d82c163fd422f2f582b2abd37334) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3175

## 3.26.0-alpha.1 (2024-09-24)

### ⚠️ Security updates (1 change)

- [[Security] Bump webrick from 1.8.1 to 1.8.2](dependabot-gitlab/dependabot@992249c0e1d810f13ed7fa540112678e5c305607) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3172

### 📦 Dependency updates (1 change)

- [Bump dependabot-omnibus from 0.276.0 to 0.277.0](dependabot-gitlab/dependabot@5facc1a0671a52d6b456ff9f224dac3f74f6b4bc) by @andrcuns. See merge request dependabot-gitlab/dependabot!3173

### 🧰 Maintenance (1 change)

- [Update chart versions in readme when bumping app version](dependabot-gitlab/dependabot@259af1ed069593f90a1a0276c76d8dd83599113e) by @andrcuns. See merge request dependabot-gitlab/dependabot!3174

## 3.25.0-alpha.1 (2024-09-24)

### ⚠️ Security updates (1 change)

- [[Security] Bump google-protobuf from 3.25.1 to 3.25.5](dependabot-gitlab/dependabot@01531f3c91ad95b643fbdaa76410fda4b4ef99fa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3152

### 🐞 Bug Fixes (1 change)

- [Do not spam limit reach messages when no update present for dependency](dependabot-gitlab/dependabot@95b46b42b47834f1770a144341174eef56922ba9) by @andrcuns. See merge request dependabot-gitlab/dependabot!3160

### 📦 Dependency updates (13 changes)

- [Bump turbo-rails from 2.0.9 to 2.0.10](dependabot-gitlab/dependabot@920693de91aa4b614c750e7ce9d1a2504d51758c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3166
- [Update node Docker tag to v22](dependabot-gitlab/dependabot@07dc23c72140dc4fbc1898b429b329c3ad39c374) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3163
- [Bump grape from 2.1.3 to 2.2.0](dependabot-gitlab/dependabot@b5ff3be375aac78222e63b5e5d33a3d35ef8c3e5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3162
- [Update docker Docker tag to v27.3.1](dependabot-gitlab/dependabot@c0e91babfe7783e82e5f40293c1e3f1eed924189) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3161
- [Bump rails from 7.1.4 to 7.2.1](dependabot-gitlab/dependabot@13a28932633467f00fc2bdb700c6554e49793822) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3158
- [Bump mongoid from 9.0.1 to 9.0.2](dependabot-gitlab/dependabot@3cdc4b08690512c0d91160aab63a3f7d01193ed2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3157
- [Bump grape-swagger from 2.1.0 to 2.1.1](dependabot-gitlab/dependabot@5d29f2e168146640bb170f55e91d07ac547620de) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3156
- [Update docker Docker tag](dependabot-gitlab/dependabot@e9dbdf2d6ad9b1b381237ec99d9ea8ee60c8cde0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3153
- [Bump dependabot-omnibus from 0.275.0 to 0.276.0](dependabot-gitlab/dependabot@f2491af0044ae31c70d498997c5a7357f525fa05) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3151
- [Bump turbo-rails from 2.0.8 to 2.0.9](dependabot-gitlab/dependabot@0fa8a40394a4e73303106422640ff57e3f446a3d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3150
- [Bump puma from 6.4.2 to 6.4.3](dependabot-gitlab/dependabot@c8721586d8257dbfe81d289ca90387559db01b80) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3149
- [Bump turbo-rails from 2.0.7 to 2.0.8](dependabot-gitlab/dependabot@066e6d4ce42ae0adcff9390bc30a80fdd9f60530) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3147
- [Bump grape from 2.1.3 to 2.2.0](dependabot-gitlab/dependabot@c760f2bd76b940851cf6122d03dc931e321accb1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3138

### 📦🔧 Development dependency updates (12 changes)

- [[Security] Bump vite from 5.4.1 to 5.4.7](dependabot-gitlab/dependabot@9090490ad37618226e4fd928677602e12e5f3c5c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3171
- [Bump vue from 3.5.6 to 3.5.8](dependabot-gitlab/dependabot@084a3f3ddbda51857f9d48170269f07201a5ff02) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3170
- [Bump allure-playwright from 3.0.0 to 3.0.1](dependabot-gitlab/dependabot@90fd83fa9857cf766c2b9e39fcb449517d2b3452) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3169
- [Bump @playwright/test from 1.47.1 to 1.47.2](dependabot-gitlab/dependabot@9f23856450209655f3b7cf495da9725ae5aa655a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3168
- [Bump rubocop-rails from 2.26.1 to 2.26.2](dependabot-gitlab/dependabot@9cbe3c9fcdedeee664d653699cc97b0833c887ed) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3159
- [Bump simplecov-console from 0.9.1 to 0.9.2](dependabot-gitlab/dependabot@3da855bb2d346b9531941f2a946f1ef46efc324b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3148
- [Bump rubocop-performance from 1.22.0 to 1.22.1](dependabot-gitlab/dependabot@5b778649e06e2d6969591d3ea33e8d0e05731d1a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3146
- [Bump allure-playwright from 2.15.1 to 3.0.0](dependabot-gitlab/dependabot@db29aff7e3f79a01d3efe2e9f94ac7fa26c48d38) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3143
- [Bump vue from 3.5.3 to 3.5.6](dependabot-gitlab/dependabot@a8adfed330773266805dc3b1675ed3111414d81b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3144
- [Bump @types/node from 22.5.4 to 22.5.5](dependabot-gitlab/dependabot@92caf14769cbcb75887f610bcbf995336c9d6904) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3142
- [Bump @playwright/test from 1.47.0 to 1.47.1](dependabot-gitlab/dependabot@e9c171befa24d78969c73ceb6924261848a3a865) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3141
- [Bump rubocop-performance from 1.21.1 to 1.22.0](dependabot-gitlab/dependabot@577b8169fbde77565ec6e0aa852321f2adb63767) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3139

### 🔧 CI changes (2 changes)

- [Remove separate job for breaking change detection](dependabot-gitlab/dependabot@59eef577c2a0eadb09e3b9134e43059e4b3796cc) by @andrcuns. See merge request dependabot-gitlab/dependabot!3165
- [Automatically release new version on dependabot-core version updates](dependabot-gitlab/dependabot@200119399082b7f0acb09e31f3f24a82cb9dd4df) by @andrcuns. See merge request dependabot-gitlab/dependabot!3164

### 🚀 Deployment changes (4 changes)

- [Bump hashicorp/google from 6.2.0 to 6.3.0 in /deploy](dependabot-gitlab/dependabot@d816d313bea0171eb52b54a1b76efc1d7e4038c7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3167
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@9fa5feaa8ae719ce16c06b36e97693a3c3c83f07) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3155
- [Bump hashicorp/google from 6.1.0 to 6.2.0 in /deploy](dependabot-gitlab/dependabot@cd63cc35d4a7283850467bcd83987ae94f96f62e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3140
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@52f74d1be499a423fca7b2b39b5d55e74122965b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3137

## 3.24.0-alpha.1 (2024-09-13)

### 📦 Dependency updates (6 changes)

- [Bump turbo-rails from 2.0.6 to 2.0.7](dependabot-gitlab/dependabot@f31be4e92451619f2f7589cc702695021e28a669) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3135
- [Bump dependabot-omnibus from 0.274.0 to 0.275.0](dependabot-gitlab/dependabot@ad3d090ecda12b7727e9eaef607b8cbbedc01869) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3133
- [Update docker Docker tag to v27.2.1](dependabot-gitlab/dependabot@dd76baf951ce9e7b31dd48b118a0d0cf2f50eafa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3131
- [Bump dependabot-omnibus from 0.273.0 to 0.274.0](dependabot-gitlab/dependabot@266b46c9b21498d45627c144132d735d84626926) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3122
- [Bump tzinfo-data from 1.2024.1 to 1.2024.2](dependabot-gitlab/dependabot@e3704433c7eac322c4bad5fc516729fc93934e52) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3121
- [Bump sidekiq from 7.3.1 to 7.3.2](dependabot-gitlab/dependabot@72b44efbddc9a70e274ebfe9907a90fc4be12c6a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3120

### 📦🔧 Development dependency updates (6 changes)

- [Bump grape-swagger-entity from 0.5.4 to 0.5.5](dependabot-gitlab/dependabot@7dc85e5187fb4fc97f8c8953e62d819b0c6c6849) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3130
- [Bump vue from 3.4.38 to 3.5.3](dependabot-gitlab/dependabot@c6b9b13e5c6b813386ba6b9f9bf42f41140545c6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3128
- [Bump @types/node from 22.5.2 to 22.5.4](dependabot-gitlab/dependabot@ca67d2b4c134d52a685f62c44b49c26b2745a2e0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3127
- [Bump @playwright/test from 1.46.1 to 1.47.0](dependabot-gitlab/dependabot@70b438a7642b064db52be7b98c4f7cf1131d59f1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3126
- [Bump rubocop-rspec from 3.0.4 to 3.0.5](dependabot-gitlab/dependabot@86ea6f3a4e826f525c75ee817d89cddde67a17ef) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3124
- [Bump rubocop-rails from 2.26.0 to 2.26.1](dependabot-gitlab/dependabot@52c927cdbf1a29a9de7bff65d384d88cd66cf0b9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3123

### 🔧 CI changes (2 changes)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-terraform Docker tag to v3.16](dependabot-gitlab/dependabot@da07debf96aed402af642c5ff125e38139eddb29) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3136
- [Add workaround for arm docker builds and remove arm gem cache](dependabot-gitlab/dependabot@21089b343a947b5cfb45d975bb2118bd7c8e4e7b) by @andrcuns. See merge request dependabot-gitlab/dependabot!3129

### 📄 Documentation updates (2 changes)

- [Correctly resolve $refs for parameters in api docs](dependabot-gitlab/dependabot@aaa5329f63f7d22eb6e4d8df6257b4f673667112) by @andrcuns. See merge request dependabot-gitlab/dependabot!3134
- [Change backticks to ticks in doc](dependabot-gitlab/dependabot@ab912babb45b5f3490ff260c2d6cc9d1200b6fc2) by @laurentheirendt. See merge request dependabot-gitlab/dependabot!3119

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 6.0.1 to 6.1.0 in /deploy](dependabot-gitlab/dependabot@dcea0339f0d7e7c5a62eacb924e652998bec0687) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3125

## 3.23.0-alpha.1 (2024-09-04)

### 🚀 New features (1 change)

- [Add support for directory glob patterns](dependabot-gitlab/dependabot@0c6e5af9fc75b3c3fcb345e57ed9d0b448d388d8) by @andrcuns. See merge request dependabot-gitlab/dependabot!3106

### 🐞 Bug Fixes (1 change)

- [Correctly handle hooks after config directory rename](dependabot-gitlab/dependabot@ca4a25a5306ca46e455f79d0d1fc97d85267e9df) by @andrcuns. See merge request dependabot-gitlab/dependabot!3109

### 📦 Dependency updates (2 changes)

- [Bump dependabot-omnibus from 0.272.0 to 0.273.0](dependabot-gitlab/dependabot@0ed8e43f14da209084585fc188245495921d9a31) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3107
- [Update docker Docker tag](dependabot-gitlab/dependabot@88bc06ea10993b317d4ae4ee6d65be5d1ea0b478) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3104

### 📦🔧 Development dependency updates (9 changes)

- [Bump rubocop from 1.66.0 to 1.66.1](dependabot-gitlab/dependabot@627692eef4f860bd47e798fde79b92135e9cc40b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3116
- [Bump rspec-rails from 7.0.0 to 7.0.1](dependabot-gitlab/dependabot@f318b8f6006e98e4f948969d1ad9f4f0471b6e94) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3115
- [Bump rspec-rails from 6.1.4 to 7.0.0](dependabot-gitlab/dependabot@7d71f92e66c4f77662636d58012afa21b588e9cf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3114
- [Bump @types/node from 22.5.0 to 22.5.2](dependabot-gitlab/dependabot@0ecec0e73eb1689997857ecb052ee53fa6d47511) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3113
- [Bump rubocop from 1.65.1 to 1.66.0](dependabot-gitlab/dependabot@009d9ade450d0d3cf0afc6ec0a46cfb8754d4f38) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3111
- [Bump vitepress from 1.3.3 to 1.3.4](dependabot-gitlab/dependabot@059340f669f09b31ef3c13b3864a68b17534cd62) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3101
- [Bump @types/node from 22.4.1 to 22.5.0](dependabot-gitlab/dependabot@bfb9e6d872d0c8adcbc96fa226ada3d62b80d9fc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3100
- [Bump solargraph-rspec from 0.3.0 to 0.4.0](dependabot-gitlab/dependabot@cf4f642ea7b9eb75194a1f532a536460ec923960) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3098
- [Bump rubocop-rails from 2.25.1 to 2.26.0](dependabot-gitlab/dependabot@063ec421d04cee7c4af646b3860e08c073d8d9ad) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3097

### 🔧 CI changes (3 changes)

- [Update eeacms/rsync Docker tag to v2.6](dependabot-gitlab/dependabot@95dca2ac77a2741870df0149c827360fa18d9bab) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3118
- [Update quay.io/containers/skopeo Docker tag to v1.16.1](dependabot-gitlab/dependabot@d570aa3dc4c40a05e02d55e49095787a3b462540) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3117
- [Fix coverage regex](dependabot-gitlab/dependabot@ab143f03a0be9c45e0bf38eb9513b588d62221a2) by @andrcuns.

### 🧰 Maintenance (2 changes)

- [Downgrade dev ruby version](dependabot-gitlab/dependabot@3fa297495596fae75ebd42bc95d09d0510397f49) by @andrcuns. See merge request dependabot-gitlab/dependabot!3106
- [Pin parser version](dependabot-gitlab/dependabot@6512c78cbb6f4a60f46e33d44834330a8ddc7a64) by @andrcuns. See merge request dependabot-gitlab/dependabot!3108

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/google from 5.42.0 to 6.0.1 in /deploy](dependabot-gitlab/dependabot@6735b3b67a9cecb34b79efd08b46ed4d13cec009) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3112
- [Bump hashicorp/google from 5.41.0 to 5.42.0 in /deploy](dependabot-gitlab/dependabot@2575cc5969dcce57fcbf124794ccac45ba34db7c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3099

## 3.22.0-alpha.1 (2024-08-24)

### ⚠️ Security updates (1 change)

- [[Security] Bump rexml from 3.3.4 to 3.3.6](dependabot-gitlab/dependabot@d2e02eb3c116d10c4d8e8d6b54195b075ed1e8cf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3095

### 📦 Dependency updates (2 changes)

- [Bump rails from 7.1.3.4 to 7.1.4](dependabot-gitlab/dependabot@c0d4c7f4482123c0faf171b65a85bee1326e0067) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3094
- [Bump dependabot-omnibus from 0.271.0 to 0.272.0](dependabot-gitlab/dependabot@8651dcf84856f9c9cda434c2ecfc5d937517aded) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3093

### 🔧 CI changes (1 change)

- [Update pipeline cancel rules](dependabot-gitlab/dependabot@f64d18ebf5e1b9490d452debadfd7edad8e1ddb0) by @andrcuns. See merge request dependabot-gitlab/dependabot!3092

### 🚀 Deployment changes (1 change)

- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@012fa01546d8a0849bf4666f9c50b0858576bbb8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3096

## 3.21.0-alpha.1 (2024-08-22)

### ⚠️ Security updates (1 change)

- [[Security] Bump fugit from 1.9.0 to 1.11.1](dependabot-gitlab/dependabot@9af11963e2f1c8b58bde771c42779dbc9960b442) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3089

### 🔬 Improvements (2 changes)

- [Add warning when base configuration is not a valid path](dependabot-gitlab/dependabot@bb2cf665df67c51edca1b6d124e71ffd44a0b000) by @andrcuns. See merge request dependabot-gitlab/dependabot!3091
- [Deprecate updates key in favor of update-options in base configuration](dependabot-gitlab/dependabot@c42b999ec13637e1b7a4410978dbdf42a6a58702) by @andrcuns. See merge request dependabot-gitlab/dependabot!3058

### 📦 Dependency updates (9 changes)

- [Bump redis from 5.2.0 to 5.3.0](dependabot-gitlab/dependabot@de614a2b02cd855fa23ba662863eb8c762987f21) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3090
- [Update docker Docker tag to v27.1.2](dependabot-gitlab/dependabot@aee1e2c94dc4e2d8dd7575db37161ca7587468a2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3075
- [Bump stimulus-rails from 1.3.3 to 1.3.4](dependabot-gitlab/dependabot@6454085da32c1c4a8bc46adc6cf33895b29a4e6b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3080
- [Bump sidekiq from 7.3.0 to 7.3.1](dependabot-gitlab/dependabot@3f291bcd31f7115df31c36d2db36ec2ebfea91a9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3077
- [Bump dependabot-omnibus from 0.268.0 to 0.271.0](dependabot-gitlab/dependabot@717ba3af85c71361970c338bdb6360c45af6b2ac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3076
- [Bump sentry-rails from 5.18.2 to 5.19.0](dependabot-gitlab/dependabot@1f72eea2671a53e28769e3987978117565a29ebe) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3073
- [Bump bootsnap from 1.18.3 to 1.18.4](dependabot-gitlab/dependabot@d3745add934d0e8e210f13bf00f01aa4f9235d0a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3060
- [Bump dependabot-omnibus from 0.267.0 to 0.268.0](dependabot-gitlab/dependabot@ac2298e209f15ca1dddb315985e69bb24202a10a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3059
- [Bump sprockets-rails from 3.5.1 to 3.5.2](dependabot-gitlab/dependabot@db18b12dc32d22cba58500fc32abea8b1aa3a44c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3056

### 📦🔧 Development dependency updates (11 changes)

- [Bump vitepress from 1.3.2 to 1.3.3](dependabot-gitlab/dependabot@18864c2dc56bc9590509d9faea5ebf267300e7dc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3087
- [Bump @types/node from 22.2.0 to 22.4.1](dependabot-gitlab/dependabot@6e5d2b1312b55c26461108e1c4da3c76e12b7767) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3086
- [Bump @playwright/test from 1.46.0 to 1.46.1](dependabot-gitlab/dependabot@e48efbab63544a494c69e3c1ee08dea94f1c6882) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3085
- [Bump rspec-rails from 6.1.3 to 6.1.4](dependabot-gitlab/dependabot@1dcf3c29d94a5bb123e6f3862dcb3d8e84e5a7e2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3078
- [Bump vue from 3.4.34 to 3.4.37](dependabot-gitlab/dependabot@cd9770b3e62cb2e3072d3de1ab09838c8d48bf15) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3072
- [Bump @types/node from 22.1.0 to 22.2.0](dependabot-gitlab/dependabot@e02e6e5decb7277eb43c115d8ff308207cacca3f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3071
- [Bump @playwright/test from 1.45.3 to 1.46.0](dependabot-gitlab/dependabot@2a7555767288fbd09bc9233aa19ab8fa1b5097aa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3070
- [Bump rubocop-rspec from 3.0.3 to 3.0.4](dependabot-gitlab/dependabot@177c85a0b0256cc2632b0d273fca6f62fa36a501) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3065
- [Bump vitepress from 1.3.1 to 1.3.2](dependabot-gitlab/dependabot@cb95d225b0ea1730f3de8d9331de067461f02b7c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3063
- [Bump @types/node from 22.0.0 to 22.1.0](dependabot-gitlab/dependabot@9f0841ac2fa931b585c63532db108f81c7cc7e14) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3062
- [Bump rubocop from 1.65.0 to 1.65.1](dependabot-gitlab/dependabot@0d9082f410951cbe105abf4868afd4da96d2cb6f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3057

### 🔧 CI changes (2 changes)

- [Update quay.io/containers/skopeo Docker tag to v1.16.0](dependabot-gitlab/dependabot@2ce8f7eb7ba57dd2f52f66e57290e6de1b0725bf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3081
- [Update eeacms/rsync Docker tag to v2.5](dependabot-gitlab/dependabot@7bd3c35e707adf7e2fcd2a74cca4d3df44596f23) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3066

### 🧰 Maintenance (1 change)

- [Bump local development ruby version](dependabot-gitlab/dependabot@9bda40b6e56a79e06bac036eca8e112023fd91f1) by @andrcuns.

### 🚀 Deployment changes (8 changes)

- [Bump hashicorp/kubernetes from 2.31.0 to 2.32.0 in /deploy](dependabot-gitlab/dependabot@0815868b813b13b3212437c68820b0d03507a98b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3084
- [Bump hashicorp/helm from 2.14.1 to 2.15.0 in /deploy](dependabot-gitlab/dependabot@34445954e03ed9aefffd533131c7174861f52cc5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3083
- [Bump hashicorp/google from 5.40.0 to 5.41.0 in /deploy](dependabot-gitlab/dependabot@9c79b419c2de7185b30c48f2a111faf5147a86dc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3082
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@8b1e8efbd534c537f5e5fa326fee92ca9b5bee0b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3079
- [Bump hashicorp/helm from 2.14.0 to 2.14.1 in /deploy](dependabot-gitlab/dependabot@fddb4d68e8871bd8d58b97af9b7d0b3fab1d1323) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3069
- [Bump hashicorp/google from 5.39.1 to 5.40.0 in /deploy](dependabot-gitlab/dependabot@a9101dcab7795dd00b64e81b50d4ab2e66eda16b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3068
- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@c34620ec5ba8dc3af08f5c58479020a5c04c9bb0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3067
- [Bump hashicorp/google from 5.38.0 to 5.39.1 in /deploy](dependabot-gitlab/dependabot@7cfb1fd8a4a541112e1ee63548f08795dfcf2cfd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3061

## 3.20.1-alpha.1 (2024-07-31)

### 🐞 Bug Fixes (1 change)

- [Correctly validate configuration file](dependabot-gitlab/dependabot@fc893a23d0d2625ae9e66c652a5cac29d1c75cec) by @andrcuns. See merge request dependabot-gitlab/dependabot!3055

## 3.20.0-alpha.1 (2024-07-30)

### 🚀 New features (2 changes)

- [Add support for common updates options](dependabot-gitlab/dependabot@428479fa6e77b38c9f33489ae8cd64ab6ab237d1) by @andrcuns. See merge request dependabot-gitlab/dependabot!3048
- [Add support for directories configuration option](dependabot-gitlab/dependabot@5100eca98af47376b59302e28529e2f8e8422e54) by @andrcuns. See merge request dependabot-gitlab/dependabot!3035

### 📦 Dependency updates (3 changes)

- [Bump mongoid from 9.0.0 to 9.0.1](dependabot-gitlab/dependabot@305264e1c17baf88d2c22e8a37a76311c1db0ab2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3054
- [Bump dependabot-omnibus from 0.266.0 to 0.267.0](dependabot-gitlab/dependabot@6122d1b012282fab9192ab61a19083ae9110880c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3050
- [Bump sentry-rails from 5.18.1 to 5.18.2](dependabot-gitlab/dependabot@650b79ddfa4753d815cca7d54c52e24ae385f77f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3046

### 📦🔧 Development dependency updates (5 changes)

- [Bump vue from 3.4.33 to 3.4.34](dependabot-gitlab/dependabot@ec1b69ffa9006c280d5a0f64ca1392559b62d221) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3053
- [Bump @types/node from 20.14.11 to 22.0.0](dependabot-gitlab/dependabot@3223ec0f4056dbecfc57a0592fc02abceaeb295e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3052
- [Bump vue from 3.4.31 to 3.4.33](dependabot-gitlab/dependabot@bc3b0b5a7f1897ebfcc3d24dd1e6c47f0fae1822) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3040
- [Bump @types/node from 20.14.10 to 20.14.11](dependabot-gitlab/dependabot@cc24c776b6cf129ec0222b4d2f1140f0d82baa53) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3039
- [Bump @playwright/test from 1.45.1 to 1.45.3](dependabot-gitlab/dependabot@9fa4a4a25f08447e4e311033f643da0a78c8e43a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3038

### 🔧 CI changes (1 change)

- [Run docker build gem cache only for image builds](dependabot-gitlab/dependabot@2eacda4f57aa6b3330c21be109a091d03b4efadb) by @andrcuns. See merge request dependabot-gitlab/dependabot!3049

### 🧰 Maintenance (1 change)

- [Fix dockerfile non matching case warnings](dependabot-gitlab/dependabot@8426ca554c85cec86f3e53da93879d8e7bbcc2f9) by @andrcuns. See merge request dependabot-gitlab/dependabot!3034

### 📄 Documentation updates (1 change)

- [Update documentation for automated project adding](dependabot-gitlab/dependabot@07cba8bda16ece94f0fc34afbfa3e4a280635bf9) by @andrcuns. See merge request dependabot-gitlab/dependabot!3036

### 🚀 Deployment changes (4 changes)

- [Bump gitlab-org/cluster-integration/gitlab-agent/agentk in /deploy/agent](dependabot-gitlab/dependabot@8f91f4ac093a3747443a93aaa64d29380d9ac600) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3051
- [Use gitlab-agent for kubernetes integration](dependabot-gitlab/dependabot@8041a2f6824c558ea7f71d21c7c8258770454a06) by @andrcuns. See merge request dependabot-gitlab/dependabot!3044
- [Add gitlab-agent configuration file](dependabot-gitlab/dependabot@8971bbd7f27421fa38f7289925aa604d1ee901f7) by @andrcuns. See merge request dependabot-gitlab/dependabot!3041
- [Bump hashicorp/google from 5.37.0 to 5.38.0 in /deploy](dependabot-gitlab/dependabot@43e015d6c1a03f71175bd688c7e8dc0232bf4fdd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3037

## 3.19.0-alpha.1 (2024-07-21)

### 📦 Dependency updates (4 changes)

- [Bump rexml from 3.3.1 to 3.3.2](dependabot-gitlab/dependabot@b1aae8f09d654fc0bfb467246176f05b6acf6f0d) by @andrcuns. See merge request dependabot-gitlab/dependabot!3033
- [Bump dependabot-omnibus from 0.265.0 to 0.266.0](dependabot-gitlab/dependabot@caea8b3978dd15dbcbec2334eec3321037bee2dd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3032
- [Bump turbo-rails from 2.0.5 to 2.0.6](dependabot-gitlab/dependabot@f86d801a326f8757fbb5d0c47995f94b455ee8d7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3031
- [Bump grape from 2.1.2 to 2.1.3](dependabot-gitlab/dependabot@0af642be645af4a3152737c335df95601087070a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3025

### 📦🔧 Development dependency updates (3 changes)

- [Bump faker from 3.4.1 to 3.4.2](dependabot-gitlab/dependabot@93ed3d5124ca656d310d7321cfc2fcd7459a212a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3029
- [Bump vitepress from 1.3.0 to 1.3.1](dependabot-gitlab/dependabot@4eabe0c61cfd2c926b51811a3ea2b2f9103af736) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3028
- [Bump prettier from 3.3.2 to 3.3.3](dependabot-gitlab/dependabot@2565d62bc5d0baee16f1a258a716fe76dedffdfa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3027

### 🔧 CI changes (1 change)

- [Update quay.io/containers/skopeo Docker tag to v1.15.2](dependabot-gitlab/dependabot@218239f44380355d48a242efe04bada5ca3d11bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3030

### 🧰 Maintenance (1 change)

- [Fix dockerfile non matching case warnings](dependabot-gitlab/dependabot@8426ca554c85cec86f3e53da93879d8e7bbcc2f9) by @andrcuns. See merge request dependabot-gitlab/dependabot!3034

### 📄 Documentation updates (1 change)

- [Update documentation on how releases are versioned](dependabot-gitlab/dependabot@43d28291e12dd27d91178c2147ba37fe1de55190) by @andrcuns.

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 5.36.0 to 5.37.0 in /deploy](dependabot-gitlab/dependabot@c287cddd88034d6a7d02e7f32ea1a76c41746fba) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3026

## 3.18.1-alpha.1 (2024-07-13)

### 📦 Dependency updates (6 changes)

- [Bump json-schema from 4.3.0 to 4.3.1](dependabot-gitlab/dependabot@964602df4f7401dfd04f17640ef09bc020bb1ad3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3024
- [Downgrade parser from 3.3.4.0 to 3.3.2.0](dependabot-gitlab/dependabot@1a1d2a75ff174490b6d30322f39247519a274813) by @andrcuns. See merge request dependabot-gitlab/dependabot!3023
- [Bump dependabot-omnibus from 0.264.0 to 0.265.0](dependabot-gitlab/dependabot@594c78b292780fb8db555a00523d779f016a3df6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3020
- [Bump dependabot-omnibus from 0.263.0 to 0.264.0](dependabot-gitlab/dependabot@bffa70de90b98383f7207749bb6fb8edad014a61) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3013
- [Bump sentry-rails from 5.18.0 to 5.18.1](dependabot-gitlab/dependabot@dc5e3324bd694645db4aa9e5d2571bf1e98e7c1d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3010
- [Bump sentry-sidekiq from 5.18.0 to 5.18.1](dependabot-gitlab/dependabot@be9b1fb2b276515efbdd9efb8bedfa8f85bbc12f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3011

### 📦🔧 Development dependency updates (7 changes)

- [Bump rubocop-rspec from 3.0.2 to 3.0.3](dependabot-gitlab/dependabot@83c7c7c3236cd34d9ebf9008e8caaafb0082180f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3021
- [Bump solargraph-rspec from 0.2.2 to 0.3.0](dependabot-gitlab/dependabot@88bbee4fbf02ee4262ca7afbb55a42f9766e6a4e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3019
- [Bump rubocop from 1.64.1 to 1.65.0](dependabot-gitlab/dependabot@b481e679a577cad376d3760499e24d314064c58a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3018
- [Bump vitepress from 1.2.3 to 1.3.0](dependabot-gitlab/dependabot@52691fe26b2b16a7e1d66dbcdaa1618f374b4738) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3017
- [Bump @types/node from 20.14.9 to 20.14.10](dependabot-gitlab/dependabot@01faca8b48ce8bec8341b967606dc0cb0af28451) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3016
- [Bump @playwright/test from 1.45.0 to 1.45.1](dependabot-gitlab/dependabot@a27b48e28de728b414db2fe60db276c873146139) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3015
- [Bump rubocop-rspec from 3.0.1 to 3.0.2](dependabot-gitlab/dependabot@7d320d306af64bc97462f5d881e985f4cf07da95) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3012

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 5.35.0 to 5.36.0 in /deploy](dependabot-gitlab/dependabot@ffa37bba4094b1e8d3115d8ba8534abc7ae8019d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3014

## 3.18.0-alpha.1 (2024-07-03)

### 🚀 New features (1 change)

- [Add support for author_details configuration](dependabot-gitlab/dependabot@b807c61ddcc1215ec468ffcde3a1aea50a09d69c) by @clupprich. See merge request dependabot-gitlab/dependabot!3003

### 📦 Dependency updates (4 changes)

- [Bump sidekiq from 7.2.4 to 7.3.0](dependabot-gitlab/dependabot@0d9856c0be4ede3b5d7a0bcba1834ffc031f8cdf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3008
- [Bump dependabot-omnibus from 0.262.0 to 0.263.0](dependabot-gitlab/dependabot@9fa1b8da255a08f7d81a9824f6c336b17d2e7fe3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3001
- [Bump grape from 2.1.1 to 2.1.2](dependabot-gitlab/dependabot@32bea0a2a6e72364523d3adadedc02769d7b3142) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3000
- [Bump sentry-rails from 5.17.3 to 5.18.0](dependabot-gitlab/dependabot@0133c1b100d994c2e43981a321be884bf89d7afb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2995

### 📦🔧 Development dependency updates (6 changes)

- [Bump vue from 3.4.30 to 3.4.31](dependabot-gitlab/dependabot@b94ed9026276e1e12cf8423c5abbdea86b41d093) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3007
- [Bump @types/node from 20.14.8 to 20.14.9](dependabot-gitlab/dependabot@11812adc4953f9147732168df335989d969ec036) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3006
- [Bump @playwright/test from 1.44.1 to 1.45.0](dependabot-gitlab/dependabot@04fe306147baf67dd709e5f4d2cf69e6bf30c7fd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3005
- [Bump rubocop-rails from 2.25.0 to 2.25.1](dependabot-gitlab/dependabot@4bdf7ed3a07b99a27e358dfa2da7e14fb130a6b2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3002
- [Bump vue from 3.4.29 to 3.4.30](dependabot-gitlab/dependabot@3356bd02b1e7fc246dce5031e75318155c30e521) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2993
- [Bump @types/node from 20.14.2 to 20.14.8](dependabot-gitlab/dependabot@a97fb1732201d3a7250a3ff3b4e8e691f0e7c89a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2992

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/google from 5.34.0 to 5.35.0 in /deploy](dependabot-gitlab/dependabot@48842df2a62f83ca771c9a49a87a0727b32421f2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!3004
- [Bump hashicorp/google from 5.33.0 to 5.34.0 in /deploy](dependabot-gitlab/dependabot@ca8cba04ee7fc7043eeede1f137089c8190ffd81) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2991

### dependencies (1 change)

- [Add solargraph-rspec gem dependency](dependabot-gitlab/dependabot@700c6cadc2098cd868fceae04ca2c7893131f4b3) by @andrcuns. See merge request dependabot-gitlab/dependabot!2994

## 3.17.0-alpha.1 (2024-06-24)

### ⚠️ Security updates (3 changes)

- [Add url origin validation to prevent SSRF](dependabot-gitlab/dependabot@6fd228741d0402d37a82426b6dafdc64dbf7e984) by @andrcuns. See merge request dependabot-gitlab/dependabot!2986
- [Remove user controlled method call](dependabot-gitlab/dependabot@1f447c76c12da7b04323d68da445251c166dc3c0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2987
- [Enable rails request forgery protection](dependabot-gitlab/dependabot@42a22d36d19a9ebbd12482b7737914fd6978e5ae) by @andrcuns. See merge request dependabot-gitlab/dependabot!2988

### 📦 Dependency updates (2 changes)

- [Bump grape from 2.0.0 to 2.1.1](dependabot-gitlab/dependabot@dadcc0135f98d4849208b286be51efe36d39a302) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2990
- [Bump dependabot-omnibus from 0.261.1 to 0.262.0](dependabot-gitlab/dependabot@89e1211e53b2401a5714e5eaa3fd522645399adb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2989

### 📦🔧 Development dependency updates (2 changes)

- [Bump pry-rails from 0.3.9 to 0.3.11](dependabot-gitlab/dependabot@d8e279a311baba20d296b51448bebe2ab990e877) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2985
- [Bump rspec-rails from 6.1.2 to 6.1.3](dependabot-gitlab/dependabot@6e79c218207f15e92107decde439721a2d340297) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2984

### 📄 Documentation updates (1 change)

- [Improve documentation on webhook configuration](dependabot-gitlab/dependabot@e396c9d76ef1e99147a7de933366634e0f0b1398) by @andrcuns.

## 3.16.1-alpha.1 (2024-06-20)

### 🐞 Bug Fixes (1 change)

- [Add missing weekday schedule to schema](dependabot-gitlab/dependabot@ae1d5dc073a5c20f6227af10fd72787373be3c77) by @andrcuns. See merge request dependabot-gitlab/dependabot!2965

### 📦 Dependency updates (9 changes)

- [Bump dependabot-omnibus from 0.261.0 to 0.261.1](dependabot-gitlab/dependabot@caaac29aca2f1767758f92dbba5967a05536bd50) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2983
- [Bump kubeclient from 4.11.0 to 4.12.0](dependabot-gitlab/dependabot@972d99145320d96e8144d2cb095cf3e62537526e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2982
- [Bump graphql-client from 0.22.0 to 0.23.0](dependabot-gitlab/dependabot@d0c5e7c1110ac6ddcd6558257191bc55bf720304) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2981
- [Bump dependabot-omnibus from 0.260.0 to 0.261.0](dependabot-gitlab/dependabot@a86f4982d6470c14b3045bc29f4b1a939ce30d5f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2964
- [Bump gitlab from `bb0a63d` to `60249d0`](dependabot-gitlab/dependabot@f04493e3244ffbe1d8efa085e5e75ff444bf991e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2963
- [Bump sprockets-rails from 3.5.0 to 3.5.1](dependabot-gitlab/dependabot@e37a90c18be4fdf64e4c4773b5e2d7dc5adc315f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2955
- [Bump dependabot-omnibus from 0.259.0 to 0.260.0](dependabot-gitlab/dependabot@cab4b123becf3faf00a55b5b2ba068d73fd892ce) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2954
- [Bump sprockets-rails from 3.4.2 to 3.5.0](dependabot-gitlab/dependabot@eef19cf0d553df974334e90bb8e42123bce8e2ff) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2952
- [Bump rails from 7.1.3.3 to 7.1.3.4](dependabot-gitlab/dependabot@499dca708f188049532e9627c34e522df71cbca6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2951

### 📦🔧 Development dependency updates (11 changes)

- [Bump vue from 3.4.27 to 3.4.29](dependabot-gitlab/dependabot@b07532ea5d05557e593f4b70150c1ce798f62143) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2980
- [Bump prettier from 3.3.1 to 3.3.2](dependabot-gitlab/dependabot@1ea6c7c40aff5b8734a8d534fa129e1c6eda849f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2979
- [Bump rubocop-performance from 1.21.0 to 1.21.1](dependabot-gitlab/dependabot@c9733d20f1502ce92a8ef7fe7c5818780cfda112) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2973
- [Bump rubocop-rspec from 2.31.0 to 3.0.1](dependabot-gitlab/dependabot@7a48efd229af65921b8a7c6b5c840cd36d5c24c6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2962
- [Bump vitepress from 1.2.2 to 1.2.3](dependabot-gitlab/dependabot@f621380442b5421fdd519867f4bebaf97155b6a9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2961
- [Bump prettier from 3.3.0 to 3.3.1](dependabot-gitlab/dependabot@49866a8a44e6f9b60c17a7e46d57a995ee028b24) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2960
- [Bump @types/node from 20.14.0 to 20.14.2](dependabot-gitlab/dependabot@cdda0d5b0b4c9301ed80efbf107a1c4868130b8d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2959
- [Bump rubocop-rspec from 2.30.0 to 2.31.0](dependabot-gitlab/dependabot@749f2553895eea43d9d8bf548da38afc3ba541ff) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2956
- [Bump prettier from 3.2.5 to 3.3.0](dependabot-gitlab/dependabot@d1811406b738a430fdf87c7110c49ce8277e68bf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2949
- [Bump @types/node from 20.12.12 to 20.14.0](dependabot-gitlab/dependabot@90fdb7983b025fd314f3fe637707892877ac5eb2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2948
- [Bump rubocop-rspec from 2.29.2 to 2.30.0](dependabot-gitlab/dependabot@23571113aa04bd71e5270195a86dbf8cc3385660) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2946

### 🔧 CI changes (6 changes)

- [Reduce amount of ci jobs in mr pipelines](dependabot-gitlab/dependabot@3a1961d089cc699ad23c467574044c94c193df73) by @andrcuns. See merge request dependabot-gitlab/dependabot!2971
- [Fix rule for docker gem cache](dependabot-gitlab/dependabot@2b447f8721d676c1298ae1e000ec30c1f05362b8) by @andrcuns. See merge request dependabot-gitlab/dependabot!2970
- [Add separate gem caching to ci docker builds](dependabot-gitlab/dependabot@cb4da5a9072f15a15ad6ef392a97abddd4f25656) by @andrcuns. See merge request dependabot-gitlab/dependabot!2968
- [Enable tls for dind ci jobs](dependabot-gitlab/dependabot@629257f0d76407d013d821a44411beac0439be23) by @andrcuns. See merge request dependabot-gitlab/dependabot!2967
- [Add label to force building arm images](dependabot-gitlab/dependabot@3aa3af571a8a64299f234076fd8ec5ef1bb465e6) by @andrcuns. See merge request dependabot-gitlab/dependabot!2967
- [Use latest tag for nightly builds](dependabot-gitlab/dependabot@9a6300163e9f10788b39315673239032395a9a31) by @andrcuns.

### 🧰 Maintenance (3 changes)

- [Do not overwrite installed ruby gems in docker builds](dependabot-gitlab/dependabot@87419067a88eb0eb70de21f897958a6323ed8246) by @andrcuns. See merge request dependabot-gitlab/dependabot!2971
- [Bump development ruby version to 3.3.2](dependabot-gitlab/dependabot@930e96c1f7c35476094b3062bab849a7cb3bfef5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2957
- [Add ruby-lsp development dependency](dependabot-gitlab/dependabot@50bc20ab1f74e03cc843db1403f7d39df4daea16) by @andrcuns. See merge request dependabot-gitlab/dependabot!2957

### 🚀 Deployment changes (5 changes)

- [Bump hashicorp/kubernetes from 2.30.0 to 2.31.0 in /deploy](dependabot-gitlab/dependabot@9510785d12842ca3233e51e091699570356363e1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2978
- [Bump hashicorp/helm from 2.13.2 to 2.14.0 in /deploy](dependabot-gitlab/dependabot@11fa9b5b2a9f2e8c302415f9705937571181173e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2977
- [Bump hashicorp/google from 5.32.0 to 5.33.0 in /deploy](dependabot-gitlab/dependabot@decdc8ec65cd38dcaac50658fbb80c214c1674ac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2976
- [Bump hashicorp/google from 5.31.1 to 5.32.0 in /deploy](dependabot-gitlab/dependabot@f0f5f6e61a22f2f7d79c40e178d38ae5f63c2da5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2958
- [Bump hashicorp/google from 5.30.0 to 5.31.1 in /deploy](dependabot-gitlab/dependabot@e3f012c4318b4e1cabe98ebabfbdf08f38a63a2e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2947

## 3.16.0-alpha.1 (2024-06-02)

### ⚠️ Security updates (1 change)

- [[Security] Bump rexml from 3.2.6 to 3.2.8](dependabot-gitlab/dependabot@e9e484a78d29257c73885c0a5b5036068c4990ef) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2920

### 🚀 New features (1 change)

- [Make dependabot-gitlab properly open sourced 🎉](dependabot-gitlab/dependabot@05f97d55cfa1923048e1e1eaf08acc8addb19790) by @andrcuns.

### 🐞 Bug Fixes (1 change)

- [Correctly store vulnerabilities with vulnererable and fixed version ranges](dependabot-gitlab/dependabot@59338873e53f9bd01385c1b125abeb789f971e95) by @andrcuns. See merge request dependabot-gitlab/dependabot!2938

### 📦 Dependency updates (6 changes)

- [Bump dependabot-omnibus from 0.258.0 to 0.259.0](dependabot-gitlab/dependabot@64d2852773991095034d2d34bf07c2c33d6e4dac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2942
- [Bump warning from 1.3.0 to 1.4.0](dependabot-gitlab/dependabot@864d677ec6a75c037412dcee5c8afa91792295c4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2934
- [Bump dependabot-omnibus from 0.256.0 to 0.258.0](dependabot-gitlab/dependabot@58423cae4501d1a8dc6af3a769a2d7bfeb41a8b1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2930
- [Bump rails from 7.1.3.2 to 7.1.3.3](dependabot-gitlab/dependabot@616ebe580287a39ad0c44a990f2fd9fa394fc8ae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2918
- [Bump grape-swagger from 2.0.3 to 2.1.0](dependabot-gitlab/dependabot@050f943536c2a7655135afaa127f286a3db93768) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2916
- [Bump dependabot-omnibus from 0.255.0 to 0.256.0](dependabot-gitlab/dependabot@a19b1f887e306859f8370c23581837fd0e207e26) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2905

### 📦🔧 Development dependency updates (14 changes)

- [Bump rubocop from 1.64.0 to 1.64.1](dependabot-gitlab/dependabot@44a3094357ffbb4b92a7f5321b83a30bd86364dd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2941
- [Bump faker from 3.3.1 to 3.4.1](dependabot-gitlab/dependabot@bd2076e9b4213df457c92a941dcb7628724fd761) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2939
- [Bump vitepress from 1.2.0 to 1.2.2](dependabot-gitlab/dependabot@996a17abce9fb15e1ea06dbaf214f2fddeaa5ceb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2937
- [Bump @playwright/test from 1.44.0 to 1.44.1](dependabot-gitlab/dependabot@5a4eb56a7b2feca8d5c319a1c3aa012ca40c1597) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2936
- [Bump rubocop from 1.63.5 to 1.64.0](dependabot-gitlab/dependabot@10f11be46c8953968be477a00926be1804be4635) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2928
- [Bump vitepress from 1.1.4 to 1.2.0](dependabot-gitlab/dependabot@5e48dfc783321a106d635a588aecd8292c7ca239) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2924
- [Bump @types/node from 20.12.11 to 20.12.12](dependabot-gitlab/dependabot@2b6b0aa21c02bb30e2b3ecef9c4235b9067f12d2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2923
- [Bump rubocop-rails from 2.24.1 to 2.25.0](dependabot-gitlab/dependabot@50c10a0e17deab2c60c33b26c86d8e3ff08fa6cb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2919
- [Bump rspec-sidekiq from 4.2.0 to 5.0.0](dependabot-gitlab/dependabot@5fb848bca2be55986b0cdad7cdab005f2eb6d017) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2917
- [Bump vue from 3.4.26 to 3.4.27](dependabot-gitlab/dependabot@63b3c2a6d18d64446d581ab130f6e7fce2127251) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2915
- [Bump @types/node from 20.12.10 to 20.12.11](dependabot-gitlab/dependabot@92f3c98b497164d10cdf2efb23e326bc09cd32f8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2914
- [Bump @playwright/test from 1.43.1 to 1.44.0](dependabot-gitlab/dependabot@489e1ce5b5db689613a581c7649b5793dd3bbd23) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2913
- [Bump rubocop from 1.63.4 to 1.63.5](dependabot-gitlab/dependabot@a4abda5f36d8704f74ca82c56cb15c46444128b3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2902
- [Bump @types/node from 20.12.8 to 20.12.10](dependabot-gitlab/dependabot@e71ba49e1080d717945555309eaf1f2dc7a5b0d0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2901

### 🔧 CI changes (9 changes)

- [Use latest tag for nightly builds](dependabot-gitlab/dependabot@9a6300163e9f10788b39315673239032395a9a31) by @andrcuns.
- [Update quay.io/containers/skopeo Docker tag to v1.15.1](dependabot-gitlab/dependabot@4ecce516d3e4c5686c5af16dd2dc8cd9d73b1b24) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2945
- [Add nightly builds](dependabot-gitlab/dependabot@6238f36129b32949d3a4edd2172ebd45eeb7a02a) by @andrcuns. See merge request dependabot-gitlab/dependabot!2933
- [Skip arm builds for most pipelines](dependabot-gitlab/dependabot@09d303bfc7cfa03ddd208503f261d7e736f1ef9c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2929
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-terraform Docker tag to v3.15](dependabot-gitlab/dependabot@494df5588d982476714b00b39c0fd86bd0f46204) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2927
- [Disable git http 2 protocol for system tests](dependabot-gitlab/dependabot@c9b32f6f6170423a1dc4e7ad5f844d0196c7a40b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2907
- [Replace deprecated brakeman job](dependabot-gitlab/dependabot@2ab2e381d3514e3a2b4ee1e1ab544cdfa31035b5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2903
- [Add retry to skopeo copy command](dependabot-gitlab/dependabot@3cefd919fa9116bb4a995ad3139eb4d3f14275d6) by @andrcuns. See merge request dependabot-gitlab/dependabot!2899
- [Skip e2e test report on release pipeline](dependabot-gitlab/dependabot@ec74769fa6c05823699a6380f2f4988ff32fb7ad) by @andrcuns. See merge request dependabot-gitlab/dependabot!2898

### 🧰 Maintenance (4 changes)

- [Update github graphql schema](dependabot-gitlab/dependabot@e2cbf63807b2af031314a890cb12aff15e740cd7) by @andrcuns. See merge request dependabot-gitlab/dependabot!2940
- [Disable security mrs for standalone test run](dependabot-gitlab/dependabot@399d449b009fec90f1a2f5e3c48c5a754a1f724b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2925
- [Add retry for bundle install commands in docker builds](dependabot-gitlab/dependabot@347f331ed00ebed6cf991308064b977671a97958) by @andrcuns. See merge request dependabot-gitlab/dependabot!2909
- [Remove job router abstraction](dependabot-gitlab/dependabot@a6d99e6ec00dc7a6550816ce6c526c09e42bccd1) by @andrcuns. See merge request dependabot-gitlab/dependabot!2900

### 📄 Documentation updates (1 change)

- [Add swift and devcontainers images to main readme](dependabot-gitlab/dependabot@b130dcaa365dc05adfd4d486edc72679bb249e8a) by @andrcuns. See merge request dependabot-gitlab/dependabot!2888

### 🚀 Deployment changes (5 changes)

- [Bump hashicorp/google from 5.29.1 to 5.30.0 in /deploy](dependabot-gitlab/dependabot@8cb2431fcb6e698b67e14d239b652e67ed55c723) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2935
- [Bump hashicorp/google from 5.28.0 to 5.29.1 in /deploy](dependabot-gitlab/dependabot@889d6c5069ce48ccf68a216516dd080cbdb5c7aa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2922
- [Bump hashicorp/kubernetes from 2.29.0 to 2.30.0 in /deploy](dependabot-gitlab/dependabot@264813c8a170d0fcaff403539ba2caff21951881) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2912
- [Bump hashicorp/helm from 2.13.1 to 2.13.2 in /deploy](dependabot-gitlab/dependabot@2acd70e5e12cc3b1054a4f5ae215788eebba7e5f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2911
- [Bump hashicorp/google from 5.27.0 to 5.28.0 in /deploy](dependabot-gitlab/dependabot@df504a501300ab73b588a4747c894aa3fe4646d3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2910

## 3.15.0-alpha.1 (2024-05-06)

### 🚀 New features (2 changes)

- [Add option to skip allow/ignore rule check for notify_release endpoint update trigger](dependabot-gitlab/dependabot@99a3a79212d808635a29b7b75eb792d563b797f4) by @andrcuns. See merge request dependabot-gitlab/dependabot!2889
- [Add support for swift and devcontainers ecosystems](dependabot-gitlab/dependabot@8f34e47ea64321d3baeade9c8823093fee82b743) by @andrcuns. See merge request dependabot-gitlab/dependabot!2879

### 🐞 Bug Fixes (2 changes)

- [Correctly skip dependency when it is fully ignored](dependabot-gitlab/dependabot@e33b8651ba4fd4b325296e3fc4250798ad967809) by @andrcuns. See merge request dependabot-gitlab/dependabot!2894
- [Add missing ignore_rules parameter for NotifyRelease trigger](dependabot-gitlab/dependabot@a29dbc1c1062a80fec56ddb0206a63a557277a23) by @andrcuns. See merge request dependabot-gitlab/dependabot!2891

### 📦 Dependency updates (4 changes)

- [Bump dependabot-omnibus from 0.254.0 to 0.255.0](dependabot-gitlab/dependabot@997edbc8d0710c6a026bc41c669ef958bed40536) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2885
- [Bump request_store from 1.6.0 to 1.7.0](dependabot-gitlab/dependabot@b9599d00010dfe3eedbc538def4f04114ed9809c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2882
- [Bump mongoid from 8.1.5 to 9.0.0](dependabot-gitlab/dependabot@c306eee863032bb657061ce3887cb2fdf145d0c7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2881
- [Bump anyway_config from 2.6.3 to 2.6.4](dependabot-gitlab/dependabot@911649f541950ba834a9e2b8605f551f557a0803) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2877

### 📦🔧 Development dependency updates (6 changes)

- [Bump @types/node from 20.12.7 to 20.12.8](dependabot-gitlab/dependabot@fa235d7838d6000a6d0abf39dcfbce43c69f2052) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2896
- [Bump rubocop-rspec from 2.29.1 to 2.29.2](dependabot-gitlab/dependabot@9dab855b771d671365d9eed9048c52b254844a7c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2883
- [Bump httparty from 0.21.0 to 0.22.0](dependabot-gitlab/dependabot@9aa2253f4839958566908d053336f3d900c47916) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2875
- [Bump vitepress from 1.1.3 to 1.1.4](dependabot-gitlab/dependabot@30bb761d2f8cfc497f076c2e99260747fcf39f95) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2873
- [Bump bootstrap from 5.3.2 to 5.3.3](dependabot-gitlab/dependabot@4c8f0c26d414623ead30539bb070a5bc55be3ec6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2871
- [Bump rubocop from 1.63.3 to 1.63.4](dependabot-gitlab/dependabot@2e37986ad0777a4459e8b65b7d7ea26e4874f406) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2869

### 🔧 CI changes (7 changes)

- [Skip e2e test report on release pipeline](dependabot-gitlab/dependabot@ec74769fa6c05823699a6380f2f4988ff32fb7ad) by @andrcuns. See merge request dependabot-gitlab/dependabot!2898
- [Sync docker and node versions across CI](dependabot-gitlab/dependabot@f49acf9c8353b28a0669bd0ea2cff535e7c2f2e5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2892
- [Update docker and node version for e2e test job](dependabot-gitlab/dependabot@ba33220370a930cba212d0d9c5aa0ff7de85091f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2890
- [Always run cache job in merge requests](dependabot-gitlab/dependabot@701664bf3bd87ea8da8eb24c7051bde3942bb0c4) by @andrcuns. See merge request dependabot-gitlab/dependabot!2884
- [Update terraform image version](dependabot-gitlab/dependabot@1f5215c7b4b59df28f1aedef29e2b6cc161079bc) by @andrcuns. See merge request dependabot-gitlab/dependabot!2880
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.18](dependabot-gitlab/dependabot@0d7c7ae1b22a42aa2e052fc806c89d9d9847b305) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2876
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/node Docker tag to v22](dependabot-gitlab/dependabot@a16383951392263ab40d0776818bcede6184cf40) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2870

### 🧰 Maintenance (5 changes)

- [Improve logging of completely ignored dependency](dependabot-gitlab/dependabot@46faee5a75a588331767b955a2306c7ed96d0466) by @andrcuns. See merge request dependabot-gitlab/dependabot!2895
- [Remove explicit faraday-retry dependency](dependabot-gitlab/dependabot@f90dc26633396cb0baa7b00da2a80b2a3112493a) by @andrcuns. See merge request dependabot-gitlab/dependabot!2893
- [Bump development ruby version to 3.3.1](dependabot-gitlab/dependabot@e80f1b17bcc9f18fc37b399756279e27cca5f354) by @andrcuns. See merge request dependabot-gitlab/dependabot!2890
- [Add curl retry when fetching jemalloc](dependabot-gitlab/dependabot@72e019a903cc6245ce9c8c26c64314370df62a66) by @andrcuns. See merge request dependabot-gitlab/dependabot!2886
- [Update bundled with version](dependabot-gitlab/dependabot@fa3cffb78b9526a4803387f145c921e67cdce756) by @andrcuns. See merge request dependabot-gitlab/dependabot!2880

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/google from 5.26.0 to 5.27.0 in /deploy](dependabot-gitlab/dependabot@887f01d41efbcf64370121bdbec662a5aac3bfc9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2897
- [Bump hashicorp/google from 5.25.0 to 5.26.0 in /deploy](dependabot-gitlab/dependabot@c8eeb88dfaf660283e037c3d73cc5eced1322620) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2872

## 3.14.8-alpha.1 (2024-04-27)

### ⚠️ Security updates (1 change)

- [[Security] Bump sidekiq from 7.2.3 to 7.2.4](dependabot-gitlab/dependabot@af45a27acb451ba51e6fc8b18d0ab426976ef8c7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2868

### 🚀 New features (1 change)

- [Add option to pass update-type to auto-merge rules](dependabot-gitlab/dependabot@fa40f8b812f52aff44183e6a41204c87489668de) by @andrcuns. See merge request dependabot-gitlab/dependabot!2853

### 🐞 Bug Fixes (1 change)

- [Fix auto-merge allow/ignore rule schema](dependabot-gitlab/dependabot@8635b60c296f7e60d5ed273da8591fbccb3166ec) by @andrcuns. See merge request dependabot-gitlab/dependabot!2855

### 📦 Dependency updates (3 changes)

- [Bump sidekiq from 7.2.2 to 7.2.3](dependabot-gitlab/dependabot@263910dde61bf24671ebdc17988388404989e1e7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2867
- [Bump grape-swagger from 2.0.2 to 2.0.3](dependabot-gitlab/dependabot@25b581dace6e278b7399dc4c7d041d84ab911956) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2866
- [Bump dependabot-omnibus from 0.253.0 to 0.254.0](dependabot-gitlab/dependabot@a912536fa4d730238135edfdb0a5c6828c42c7bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2865

### 📦🔧 Development dependency updates (3 changes)

- [Bump spring from 4.2.0 to 4.2.1](dependabot-gitlab/dependabot@9649130d9318a65ab0d270eeda86057ae14431d9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2862
- [Bump rubocop from 1.63.2 to 1.63.3](dependabot-gitlab/dependabot@36e760ada71bbb654bbd2f8ab938c43dbf5b0f64) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2861
- [Bump vitepress from 1.1.0 to 1.1.3](dependabot-gitlab/dependabot@8a2105e6011107491eab0cc140705232aa18a40b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2858

### 🔧 CI changes (1 change)

- [Remove deprecated 'docker' runner tag](dependabot-gitlab/dependabot@1d17876f6b33a6482120fba675356211728125b8) by @andrcuns. See merge request dependabot-gitlab/dependabot!2854

### 🧰 Maintenance (1 change)

- [Raise error if empty list of files to update is returned](dependabot-gitlab/dependabot@4abd258ee30a192ce3944ddbbe40b03689a823ab) by @andrcuns. See merge request dependabot-gitlab/dependabot!2860

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/helm from 2.13.0 to 2.13.1 in /deploy](dependabot-gitlab/dependabot@68ee55bb8b6953ab97bda54e4dce861f18f6ae6b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2857
- [Bump hashicorp/google from 5.24.0 to 5.25.0 in /deploy](dependabot-gitlab/dependabot@2bb982a85a99e6055ea7b4c1d5b3a2dc00b71b25) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2856

## 3.14.7-alpha.1 (2024-04-20)

### 🐞 Bug Fixes (1 change)

- [Remove optional version param from compose](dependabot-gitlab/dependabot@2cea5cffa815b233e1a49ed84048bbf94cfb36c8) by @andrcuns. See merge request dependabot-gitlab/dependabot!2850

### 📦 Dependency updates (1 change)

- [Bump dependabot-omnibus from 0.251.0 to 0.253.0](dependabot-gitlab/dependabot@bc6e10ed4ef34aba982e922cc4399a01da4c7a8b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2849

### 📦🔧 Development dependency updates (1 change)

- [Bump grape-swagger-entity from 0.5.3 to 0.5.4](dependabot-gitlab/dependabot@8b696e56a3d27cadc57279960b1660906b9e7811) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2851

## 3.14.6-alpha.1 (2024-04-18)

### 🐞 Bug Fixes (1 change)

- [Correctly handle hex registry secrets](dependabot-gitlab/dependabot@aab2920555cdc059c966b4f49ce4d5889c0a52c2) by @andrcuns. See merge request dependabot-gitlab/dependabot!2847

### 📦 Dependency updates (6 changes)

- [Bump redis from 5.1.0 to 5.2.0](dependabot-gitlab/dependabot@518af2e1fb6b11ace066d5cbe6508987960a5b0f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2844
- [Update docker Docker tag to v26.0.1](dependabot-gitlab/dependabot@b47b65ea4aa1e3e362f846c33a9a77668a588d8b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2839
- [Bump sentry-sidekiq from 5.17.2 to 5.17.3](dependabot-gitlab/dependabot@c340025c27c7a8346f5cf5798701b8c5bd034206) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2838
- [Bump sentry-rails from 5.17.2 to 5.17.3](dependabot-gitlab/dependabot@b2e88a7571a3b24c7f1ffd30eac1ca837ccae254) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2837
- [Bump graphql-client from 0.21.0 to 0.22.0](dependabot-gitlab/dependabot@f487046192b18f204684f1a02ea1e3534c5d49a7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2834
- [Bump grape-entity from 1.0.0 to 1.0.1](dependabot-gitlab/dependabot@e295f45296ef78c5b744a8cf4033fcc398ce74dd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2833

### 📦🔧 Development dependency updates (12 changes)

- [Bump rspec-sidekiq from 4.1.0 to 4.2.0](dependabot-gitlab/dependabot@3c5e4b431d94c099221c2d558e0c3c878d95cde5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2846
- [Bump rubocop from 1.63.1 to 1.63.2](dependabot-gitlab/dependabot@efd79720ad171b4b0f08c2c4d0c6444552a025f8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2845
- [Bump vue from 3.4.21 to 3.4.22](dependabot-gitlab/dependabot@1a5283195bb9d06c756dd3fdf98c2a465a0f3be3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2843
- [Bump @playwright/test from 1.43.0 to 1.43.1](dependabot-gitlab/dependabot@3a7158219e4b48059cadf8d1ca93be3d74846d89) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2842
- [Bump rubocop from 1.63.0 to 1.63.1](dependabot-gitlab/dependabot@e6a367d79f940175db0b3e8befd0f7d14e050877) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2835
- [Bump vitepress from 1.0.2 to 1.1.0](dependabot-gitlab/dependabot@dd59a54a5276eaff1599dfd1ca722a0ba33bffd8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2832
- [Bump docker-compose from 0.24.7 to 0.24.8](dependabot-gitlab/dependabot@d9c462ecda3336565371810dd29a0d6f002efcdd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2828
- [Bump @types/node from 20.12.2 to 20.12.7](dependabot-gitlab/dependabot@171628a184d2c16cd6bec914fd6d7933e85be6e5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2831
- [Bump spring from 4.1.3 to 4.2.0](dependabot-gitlab/dependabot@8ee57a3f08a685325fe001a26fa09ee0cf8deb1e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2829
- [Bump allure-playwright from 2.15.0 to 2.15.1](dependabot-gitlab/dependabot@5af5f154012b4621652bfbe80f4e2ffbfb7d18be) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2827
- [Bump @playwright/test from 1.42.1 to 1.43.0](dependabot-gitlab/dependabot@5a548c547c48c726af8f368a0ea105b5c2ab2bcd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2825
- [Bump rubocop from 1.62.1 to 1.63.0](dependabot-gitlab/dependabot@1259e1c72461bbaf1cc9649a1524766210df3bcb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2822

### 🔧 CI changes (1 change)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.17](dependabot-gitlab/dependabot@0abc355d22ae5ec1919b288be6dbee001b8a9ce6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2848

### 🚀 Deployment changes (4 changes)

- [Bump hashicorp/kubernetes from 2.27.0 to 2.29.0 in /deploy](dependabot-gitlab/dependabot@1779e202e01b13de17786fa097ae907692e69949) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2841
- [Bump hashicorp/google from 5.23.0 to 5.24.0 in /deploy](dependabot-gitlab/dependabot@bb304c47c6a0376dd3e6a93711468e1b136b5f13) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2840
- [Bump hashicorp/helm from 2.12.1 to 2.13.0 in /deploy](dependabot-gitlab/dependabot@5c05264e92c8d1f7e75463335563aca6fbe419c9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2824
- [Bump hashicorp/google from 5.22.0 to 5.23.0 in /deploy](dependabot-gitlab/dependabot@1720d1e32a408c6b369724f528f26e0fb162c29a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2823

## 3.14.5-alpha.1 (2024-04-07)

### 📦 Dependency updates (4 changes)

- [Bump dependabot-omnibus from 0.250.0 to 0.251.0](dependabot-gitlab/dependabot@272a8db002bb3d9a78b9363cf4cf12f92c7bb2ce) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2821
- [Bump sentry-sidekiq from 5.17.1 to 5.17.2](dependabot-gitlab/dependabot@77cf13fa2e1050ad2b636110f2f88fba1065f175) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2819
- [Bump sentry-rails from 5.17.1 to 5.17.2](dependabot-gitlab/dependabot@9609fde0e73ffa67377a1ab9fa06623321da1dd1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2818
- [Bump dependabot-omnibus from 0.249.0 to 0.250.0](dependabot-gitlab/dependabot@2f17da87ff32825aa0aa8a9f365ff6e816badfc9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2816

### 📦🔧 Development dependency updates (6 changes)

- [Bump rubocop-rspec from 2.28.0 to 2.29.1](dependabot-gitlab/dependabot@a30eebe2d13e6027978ccdeaabc059861b42735f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2820
- [Bump faker from 3.3.0 to 3.3.1](dependabot-gitlab/dependabot@f229e0fff596f231047d3202d431bf2ac3e61fff) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2817
- [Bump vitepress from 1.0.1 to 1.0.2](dependabot-gitlab/dependabot@1e74aace7b1689c46a7e5e84f0714fa6408aa5e4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2815
- [Bump @types/node from 20.11.30 to 20.12.2](dependabot-gitlab/dependabot@f9cbb28da67eb1b10552f51c877650fda64e0999) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2814
- [Bump rubocop-performance from 1.20.2 to 1.21.0](dependabot-gitlab/dependabot@96cf9a34edb77438d528656d022c697e4d82a23a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2811
- [Bump rubocop-rspec from 2.27.1 to 2.28.0](dependabot-gitlab/dependabot@e14e24c73841c7aed685b7a63f1b97c6b30263cd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2810

### 🔧 CI changes (1 change)

- [Update quay.io/containers/skopeo Docker tag to v1.15.0](dependabot-gitlab/dependabot@9e91b74d9aeb12c7ec967ef677abaeefa3f38ac6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2812

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 5.21.0 to 5.22.0 in /deploy](dependabot-gitlab/dependabot@2814bd7b819f3c5bf02a587c4c4343fd5c7d2e74) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2813

## 3.14.4-alpha.1 (2024-03-29)

### ⚠️ Security updates (1 change)

- [[Security] Bump rdoc from 6.6.2 to 6.6.3.1](dependabot-gitlab/dependabot@f1df9bd2c96065a7f7c14e4f29d99abf01ff5d22) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2803

### 🐞 Bug Fixes (2 changes)

- [Make sure confidentail option default always exists](dependabot-gitlab/dependabot@9fcfa450472123e8d0b1cce9eb07963964565055) by @andrcuns. See merge request dependabot-gitlab/dependabot!2809
- [Use correct requirement update strategy types](dependabot-gitlab/dependabot@13d75e44a87d224b6f046f40c3190e2aba970f6b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2808

### 📦 Dependency updates (4 changes)

- [Bump dependabot-omnibus from 0.248.0 to 0.249.0](dependabot-gitlab/dependabot@73a57324cde91b0f835d69ae611e7bc906399d55) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2806
- [Bump json-schema from 4.2.0 to 4.3.0](dependabot-gitlab/dependabot@85112b4b4aeaf98709eac8972c1739ac0bbed1b9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2805
- [Bump dependabot-omnibus from 0.247.0 to 0.248.0](dependabot-gitlab/dependabot@2cc8dc437db438a9556893e7ebb8adcbf31155ac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2794
- [Update docker Docker tag to v26](dependabot-gitlab/dependabot@01c044ca35b6e030529f1847a5e41ab11186b540) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2793

### 📦🔧 Development dependency updates (6 changes)

- [Bump debug from 1.9.1 to 1.9.2](dependabot-gitlab/dependabot@82969a37c012536e83afcdb3518d0f6fbf70297a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2807
- [Bump faker from 3.2.3 to 3.3.0](dependabot-gitlab/dependabot@2068ee3ad12f80381fbd77b372e8722d12702f4c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2802
- [Bump allure-playwright from 2.14.1 to 2.15.0](dependabot-gitlab/dependabot@59d7a2b72d3f9a2ec6780502fd8d013856bff1db) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2800
- [Bump vitepress from 1.0.0-rc.45 to 1.0.1](dependabot-gitlab/dependabot@391fdde04f95898b0e28e6565ecae0355a867c72) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2801
- [Bump @types/node from 20.11.28 to 20.11.30](dependabot-gitlab/dependabot@80e41dd24fbd6c00f4e4f6ba99e3c5c3d968f5be) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2799
- [Bump rubocop-rails from 2.24.0 to 2.24.1](dependabot-gitlab/dependabot@7f8fd4f32e00106294bd89c5ffcd24a35d3bd656) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2796

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 5.20.0 to 5.21.0 in /deploy](dependabot-gitlab/dependabot@cd60420a6c90accb591ed7f3fc03e322c4ac5a02) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2798

## 3.14.3-alpha.1 (2024-03-20)

### 📦 Dependency updates (9 changes)

- [Bump sentry-sidekiq from 5.17.0 to 5.17.1](dependabot-gitlab/dependabot@53faaefba0724ea4c7091073639e06e4535460b8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2787
- [Bump sentry-rails from 5.17.0 to 5.17.1](dependabot-gitlab/dependabot@7a12076e035e4a55afe63bd0d6ff0c5a9b7a26bf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2786
- [Bump json-schema from 4.1.1 to 4.2.0](dependabot-gitlab/dependabot@d3ff8e12516519996a71351472d3a360be0f90ba) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2785
- [Bump dependabot-omnibus from 0.246.0 to 0.247.0](dependabot-gitlab/dependabot@f07130cc9527f47ab4ec41f22b18349ac19f0453) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2784
- [Bump sentry-sidekiq and sentry-rails](dependabot-gitlab/dependabot@53cd9fab278700b1f042117cf59eb702452dcabc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2783
- [Bump sentry-rails and sentry-sidekiq](dependabot-gitlab/dependabot@8af02c4dfe8c4fefca468d12f9b16882b6d4f40f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2782
- [Bump graphql-client from 0.20.0 to 0.21.0](dependabot-gitlab/dependabot@b4b39bbe58531b1b47d0f4a6ce0a8d106a5a0b31) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2781
- [Bump yabeda-sidekiq from 0.11.0 to 0.12.0](dependabot-gitlab/dependabot@b2859c11bd459f2c0db1382341ff28184dfb29c4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2777
- [Bump turbo-rails from 2.0.4 to 2.0.5](dependabot-gitlab/dependabot@2a68a64b1e4ebe17b9ca04277b6667fad08bfc49) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2776

### 📦🔧 Development dependency updates (6 changes)

- [Bump rspec-rails from 6.1.1 to 6.1.2](dependabot-gitlab/dependabot@b7c2dec1eedff1a624c9a915f71c735caeb0682f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2791
- [Bump docker-compose from 0.24.6 to 0.24.7](dependabot-gitlab/dependabot@98ef240d336a0fc30e8336af9e60653a3a482182) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2790
- [Bump @types/node from 20.11.25 to 20.11.28](dependabot-gitlab/dependabot@42f8c652674edace8a054e75f5fbddf2f6a0cb16) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2789
- [Bump rubocop from 1.62.0 to 1.62.1](dependabot-gitlab/dependabot@77e7d7518c05393e5aaeaa6123909efe40661efe) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2780
- [Bump allure-playwright from 2.13.0 to 2.14.1](dependabot-gitlab/dependabot@f2915e3ff69ef295570c1eff4c86bc8cd347b349) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2779
- [Bump rubocop from 1.61.0 to 1.62.0](dependabot-gitlab/dependabot@d9df30c4fe9b554443b93b9b37e8b090de07af24) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2774

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/google from 5.19.0 to 5.20.0 in /deploy](dependabot-gitlab/dependabot@0824bfb9a11fb5ef1d67eb5ffdb3a447c2cd26d5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2788
- [Bump hashicorp/kubernetes from 2.26.0 to 2.27.0 in /deploy](dependabot-gitlab/dependabot@9aa5a69dbca77ded58a052b7dcecfd6ffe803c5f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2778

## 3.14.2-alpha.1 (2024-03-07)

### ⚠️ Security updates (2 changes)

- [[Security] Bump yard from 0.9.35 to 0.9.36](dependabot-gitlab/dependabot@8012453dbcaa97d456ebaf215e9804b1171a37e5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2764
- [[Security] Bump yard from 0.9.34 to 0.9.35](dependabot-gitlab/dependabot@9837ba41a3972bc414b104c19a845eca5e81e94e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2760

### 🐞 Bug Fixes (2 changes)

- [Always return repo_contents_path for cloning](dependabot-gitlab/dependabot@a47c4747664ea437e96028a9d314e13d756327b9) by @andrcuns. See merge request dependabot-gitlab/dependabot!2757
- [Fix json schema for auto-merge allow configuration](dependabot-gitlab/dependabot@4069845aa2011367d342ce386d8c806f178289f7) by @andrcuns. See merge request dependabot-gitlab/dependabot!2756

### 📦 Dependency updates (2 changes)

- [Bump dependabot-omnibus from 0.245.0 to 0.246.0](dependabot-gitlab/dependabot@c6444c1b9a1172ca9d0d4eec2fe914abde9dc31a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2763
- [Bump mongoid from 8.1.4 to 8.1.5](dependabot-gitlab/dependabot@1f48725c026d34ad4164a20be4a6ee98d3e12f92) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2758

### 📦🔧 Development dependency updates (11 changes)

- [Bump vitepress from 1.0.0-rc.44 to 1.0.0-rc.45](dependabot-gitlab/dependabot@2b9397e4d800aa94a3e599859b0abc435e7bc36c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2773
- [Bump @types/node from 20.11.24 to 20.11.25](dependabot-gitlab/dependabot@c1467d77ef25103c497bb3e8c836823341db9205) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2772
- [Bump rubocop-rspec from 2.27.0 to 2.27.1](dependabot-gitlab/dependabot@ebc7232e654d273f3907a8dc54fb9838431c284b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2766
- [Bump vue from 3.4.20 to 3.4.21](dependabot-gitlab/dependabot@4e9a69cc1bc418b2746f2dd5b1588dcadbaf824f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2769
- [Bump @types/node from 20.11.20 to 20.11.24](dependabot-gitlab/dependabot@4d8cb63a24a0908036ed79f87bda5d15a78d82aa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2768
- [Bump @playwright/test from 1.41.2 to 1.42.1](dependabot-gitlab/dependabot@f3e8f74f98af8a0f71d35bddd5c1c1caaf882f85) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2767
- [Bump rubocop-rails from 2.23.1 to 2.24.0](dependabot-gitlab/dependabot@3caa9bf4f990a245d355c83cc02cbece27a0f256) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2765
- [Bump rubocop-rspec from 2.26.1 to 2.27.0](dependabot-gitlab/dependabot@982ca9e6d347b27b36218b173700874a954ea681) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2762
- [Bump rubocop from 1.60.2 to 1.61.0](dependabot-gitlab/dependabot@4cd8ab5f65d9a22b1a386e947661457d98c006ac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2759
- [Bump vue from 3.4.19 to 3.4.20](dependabot-gitlab/dependabot@d35d87ee0180672a8a34e66c43d11b5c48e2f5da) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2755
- [Bump allure-playwright from 2.12.2 to 2.13.0](dependabot-gitlab/dependabot@7d6d4fe439fe8fd4bf5d155e520b571d3f556b51) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2754

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/kubernetes from 2.25.2 to 2.26.0 in /deploy](dependabot-gitlab/dependabot@9d1b9f9d9902cfe71607f5b547694e7a4388d70f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2771
- [Bump hashicorp/google from 5.15.0 to 5.19.0 in /deploy](dependabot-gitlab/dependabot@5b78a3582091ee324a5fb7521160ae4a93bdc24f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2770

## 3.14.1-alpha.1 (2024-02-26)

### 📦 Dependency updates (4 changes)

- [Bump dependabot-omnibus from 0.244.0 to 0.245.0](dependabot-gitlab/dependabot@b3815c9ca211b950676f5c32a60e37908b1a7aa4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2751
- [Bump turbo-rails from 2.0.3 to 2.0.4](dependabot-gitlab/dependabot@f530ebf1d31c5cffd35a663a9b4ee7246d722157) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2750
- [Bump rails from 7.1.3 to 7.1.3.2](dependabot-gitlab/dependabot@e5686f3ef96cc3f9637856c77253bd2f2966e7e2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2749
- [Update bitnami/mongodb Docker tag to v7.0.5](dependabot-gitlab/dependabot@97d2ae20bbf47d29dd2f7e4a40e8b2964243342e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2742

### 📦🔧 Development dependency updates (4 changes)

- [Bump docker-compose from 0.24.3 to 0.24.6](dependabot-gitlab/dependabot@32d47e9a9f91d1ab8d307056c117fbf35b10f02d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2753
- [Bump @types/node from 20.11.17 to 20.11.20](dependabot-gitlab/dependabot@84f78a414d94004578f47fe74a421d32c229ab24) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2752
- [Bump vitepress from 1.0.0-rc.42 to 1.0.0-rc.44](dependabot-gitlab/dependabot@4973497b987ac8a318fb69bc91224b855393e352) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2747
- [Bump allure-playwright from 2.12.0 to 2.12.2](dependabot-gitlab/dependabot@447c210e14fe817d14e12313b0aec3284f3a6d11) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2745

### 🔧 CI changes (1 change)

- [Update quay.io/containers/skopeo Docker tag to v1.14.2](dependabot-gitlab/dependabot@aeb099a67746a288a2b45cc2ea9957b771d6d972) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2743

## 3.14.0-alpha.1 (2024-02-17)

### 🚀 New features (1 change)

- [Add ability to see update logs in UI](dependabot-gitlab/dependabot@0294ea533db078d27803ecc6096a2a6477d3ad46) by @andrcuns. See merge request dependabot-gitlab/dependabot!2667

### 🔬 Improvements (2 changes)

- [Mark update run with warning status if it contains dependencies that were impossible to update](dependabot-gitlab/dependabot@80738ce9c6ea63dd8cd8bef91874bcdcce03d532) by @andrcuns. See merge request dependabot-gitlab/dependabot!2694
- [Add status icon for last job run](dependabot-gitlab/dependabot@79f150861249348d32c6c04bbe8be39aea4a5320) by @andrcuns. See merge request dependabot-gitlab/dependabot!2684

### 🐞 Bug Fixes (11 changes)

- [Use Dependabot::Credential class when passing credentials](dependabot-gitlab/dependabot@71a60f6ea2e4d8a41d01ea0941b5e19ff5e3889f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2734
- [Fix Update::Run background migration](dependabot-gitlab/dependabot@1df4bcbb7498a1d1bfbb284fe4d374ff23fb3473) by @andrcuns.
- [Add missing class attribute to Failure model](dependabot-gitlab/dependabot@db6f74409bc4fa3cd51a76b54882d64668019927) by @andrcuns. See merge request dependabot-gitlab/dependabot!2733
- [Only mount cache dir instead of whole tmp](dependabot-gitlab/dependabot@30b1d4398c0385e7b7dd3f42dccac705f09e7db0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2732
- [Fix private git registry configuration](dependabot-gitlab/dependabot@4af3d4baa04a4edd9fba519d992ed53c16464684) by @andrcuns. See merge request dependabot-gitlab/dependabot!2711
- [Correctly handle leading zero when parsing time value for cron](dependabot-gitlab/dependabot@6484b94b49f4d6224db2daf9d086934673a6b102) by @andrcuns. See merge request dependabot-gitlab/dependabot!2707
- [Scroll to notification when triggering or disabling a job](dependabot-gitlab/dependabot@00b347d58a43d145e9237b93db47968ec7f67bcf) by @andrcuns. See merge request dependabot-gitlab/dependabot!2697
- [Add missing status icon for running update job](dependabot-gitlab/dependabot@48c610829adbbb91a2aa1d35dc1f85666da88ae7) by @andrcuns. See merge request dependabot-gitlab/dependabot!2685
- [Correctly order log entries in UI](dependabot-gitlab/dependabot@d22e0c0d9034c5e49053e5df1a9921703c8c6674) by @andrcuns. See merge request dependabot-gitlab/dependabot!2677
- [Use correct exception class when rescuing schema error](dependabot-gitlab/dependabot@5b36f7e7a0b407acb941d4ec5c3bb705881ad727) by @andrcuns. See merge request dependabot-gitlab/dependabot!2668
- [Delete log entries and failures when jobs are removed](dependabot-gitlab/dependabot@f0afd08e4550fd9b0ac01990ea05cecf54a22cb4) by @andrcuns. See merge request dependabot-gitlab/dependabot!2660

### 📦 Dependency updates (20 changes)

- [Bump turbo-rails from 2.0.2 to 2.0.3](dependabot-gitlab/dependabot@241b7ab281e2a26d3c68da26c87813f3c3ab510b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2740
- [Bump sidekiq from 7.2.1 to 7.2.2](dependabot-gitlab/dependabot@f83e0a4426a330a6f26930258143489098420624) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2739
- [Bump dependabot-omnibus from 0.243.0 to 0.244.0](dependabot-gitlab/dependabot@f8f75e704cc240488230a3f981b3babf134fbe38) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2738
- [Bump sidekiq_alive from 2.3.1 to 2.4.0](dependabot-gitlab/dependabot@557285ffa23e5cd7d35d85bf159dae9f320db542) by @andrcuns. See merge request dependabot-gitlab/dependabot!2735
- [Bump dependabot-omnibus from 0.239.0 to 0.243.0](dependabot-gitlab/dependabot@8b078307a7834e33229f3b25485f052fcbc79b77) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2731
- [Bump sidekiq_alive from `7695818` to `6fe2b7a`](dependabot-gitlab/dependabot@81fb55f33133e14686c9d0ea91635558788f0045) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2730
- [Bump request_store from 1.5.1 to 1.6.0](dependabot-gitlab/dependabot@07e883bc5bf6782d1807aec3d717b32f830c3ee9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2724
- [Bump turbo-rails from 1.5.0 to 2.0.2](dependabot-gitlab/dependabot@71e11902971f960c26f2230572eeab28afe8c698) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2725
- [Bump redis from 5.0.8 to 5.1.0](dependabot-gitlab/dependabot@67690c0f04d02274be970426035c85835398b745) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2723
- [Bump graphql-client from 0.19.0 to 0.20.0](dependabot-gitlab/dependabot@e29cb867b063f0c6dd82f295b0b2a5f13ef8d21c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2722
- [Bump yabeda-sidekiq from 0.10.0 to 0.11.0](dependabot-gitlab/dependabot@cb940bb3e02c0a32a1552731ebe208e2c53a73ed) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2721
- [Bump anyway_config from 2.6.2 to 2.6.3](dependabot-gitlab/dependabot@48a9da024838af36e7698c120fe7d1e48ac9b01e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2718
- [Bump grape-swagger from 2.0.1 to 2.0.2](dependabot-gitlab/dependabot@d7e4eb6c74c163fb9758366264f18127f9fdf6e8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2708
- [Bump tzinfo-data from 1.2023.4 to 1.2024.1](dependabot-gitlab/dependabot@69c9aedc26bbc93d86ad0dcbbae05073a9ec8c00) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2705
- [Bump bootsnap from 1.18.2 to 1.18.3](dependabot-gitlab/dependabot@53a6ae65ef21b22b19f19a6e2cc306da92ee11a6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2704
- [Bump bootsnap from 1.17.1 to 1.18.2](dependabot-gitlab/dependabot@3679491ae7efe252ca62fb1fd11a27863cbea941) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2703
- [Bump graphql-client from 0.18.0 to 0.19.0](dependabot-gitlab/dependabot@0afd98b258ec0968ddc365a7e3abb804004ea09b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2691
- [Bump sidekiq from 7.2.0 to 7.2.1](dependabot-gitlab/dependabot@c3d3eebf2322ea55c5bb88a2bcc67830a66915b2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2672
- [Bump rails from 7.1.2 to 7.1.3](dependabot-gitlab/dependabot@5a3fb9430a10496e5df7fbc3d510cef41be108d9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2661
- [Bump stackprof from 0.2.25 to 0.2.26](dependabot-gitlab/dependabot@2c0dfc8f7c96cb221ff982938c2a58bf553b64a8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2658

### 📦🔧 Development dependency updates (23 changes)

- [Bump vue from 3.4.15 to 3.4.18](dependabot-gitlab/dependabot@380aa11bbff35dba0402e32c53cd1110422446d5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2729
- [Bump vitepress from 1.0.0-rc.41 to 1.0.0-rc.42](dependabot-gitlab/dependabot@77056da14dfea25d9532b51d2f088d45e34b3954) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2728
- [Bump @types/node from 20.11.16 to 20.11.17](dependabot-gitlab/dependabot@5fea15bb4f7f74afcb536a241e218e542c9fdf99) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2727
- [Bump allure-playwright from 2.11.1 to 2.12.0](dependabot-gitlab/dependabot@4fbfe4804586600e1d840830393a49b17712b9b2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2715
- [Bump vitepress from 1.0.0-rc.40 to 1.0.0-rc.41](dependabot-gitlab/dependabot@5869e6bfc65ec39f783cb1637263f9ae7ce53f97) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2717
- [Bump prettier from 3.2.4 to 3.2.5](dependabot-gitlab/dependabot@44cacda8f15332d4f35e79bd451e4a76fde777ca) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2716
- [Bump @types/node from 20.11.10 to 20.11.16](dependabot-gitlab/dependabot@6ddbf0c8fe5b418225fa00831aa68f59820ef8ad) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2714
- [Bump @playwright/test from 1.41.1 to 1.41.2](dependabot-gitlab/dependabot@87434f8df3da279bab504b130c9b93e5383a6eae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2713
- [Bump rspec from 3.12.0 to 3.13.0](dependabot-gitlab/dependabot@80bec39c693179120b909af1f92f3cfb352d74d8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2710
- [Bump grape-swagger-entity from 0.5.2 to 0.5.3](dependabot-gitlab/dependabot@3898b5e2a2489e9d15d4637bf9df9648b17f5220) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2709
- [Bump @types/node from 20.11.5 to 20.11.10](dependabot-gitlab/dependabot@dab9da66b8b557c3b61b33cbd08e2c53e9f4d5e3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2701
- [Bump rubocop and reek](dependabot-gitlab/dependabot@aaa67b5cf47be37e82791c1a03f830accf9e350a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2696
- [Bump reek from 6.2.0 to 6.3.0](dependabot-gitlab/dependabot@bd52c254b13afa6bc485040ab133a71e07ea2fd2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2695
- [Bump rspec-rails from 6.1.0 to 6.1.1](dependabot-gitlab/dependabot@1cf60f3532a52273dd3553105b18addd42cea589) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2693
- [Bump allure-playwright from 2.10.0 to 2.11.1](dependabot-gitlab/dependabot@01091c05216f04bfb11c70db6029eabd9f425a52) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2680
- [[Security] Bump vite from 5.0.11 to 5.0.12](dependabot-gitlab/dependabot@80a994750e62ae78af2a7b6d717f960c2d4c84c3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2683
- [Bump vitepress from 1.0.0-rc.36 to 1.0.0-rc.40](dependabot-gitlab/dependabot@bff936b56148b643748f636fd9b8a26eb5cf81d7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2682
- [Bump prettier from 3.2.2 to 3.2.4](dependabot-gitlab/dependabot@962a5f4dd406ba25644b9e43d987f906c484f5bd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2681
- [Bump @types/node from 20.11.2 to 20.11.5](dependabot-gitlab/dependabot@0c3cc8ba1ddc302df86ffb796035bc02f8e2283a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2679
- [Bump @playwright/test from 1.40.1 to 1.41.1](dependabot-gitlab/dependabot@9ba531011d6f9f934386d367fdcc88da0495eded) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2678
- [Bump vue from 3.4.6 to 3.4.14](dependabot-gitlab/dependabot@a85cbfd8657ea488e40727c0339a2de0da041b25) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2657
- [Bump prettier from 3.1.1 to 3.2.2](dependabot-gitlab/dependabot@e8197461a3bead39b39aadbb3c169b85d314ab23) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2656
- [Bump @types/node from 20.10.7 to 20.11.2](dependabot-gitlab/dependabot@60c7b20a2d1093109ebedff213cd1b774d16d951) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2655

### 🔧 CI changes (4 changes)

- [Migrate deployment to opentofu](dependabot-gitlab/dependabot@c490cecee9a27dd172eedb0f704aecc02f363815) by @andrcuns. See merge request dependabot-gitlab/dependabot!2737
- [Expose e2e test report via artifacts](dependabot-gitlab/dependabot@d585fd52b0d9159e4d92bc6676caa630f0b75ed9) by @andrcuns. See merge request dependabot-gitlab/dependabot!2702
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/node Docker tag to v21](dependabot-gitlab/dependabot@e52934c692cbc097494133c4b171dc6a757e89dd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2674
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-terraform Docker tag to v3.14](dependabot-gitlab/dependabot@3cf16ec19b27c470f52852a67b82d3096c83c306) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2670

### 🧰 Maintenance (12 changes)

- [Use immutable tags for mongodb and redis images](dependabot-gitlab/dependabot@ab61f5ef2a5c5ab03a74c10824bf342e1abac16c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2741
- [Order log entries by id](dependabot-gitlab/dependabot@b7d7fd90cd77def64c1530b3a784e5c010917f77) by @andrcuns. See merge request dependabot-gitlab/dependabot!2699
- [Update warning and failed status icons](dependabot-gitlab/dependabot@08115514ef28420459c9e46382614401d598f239) by @andrcuns. See merge request dependabot-gitlab/dependabot!2698
- [Adjust status icon size and add title](dependabot-gitlab/dependabot@1fe0cedc098b6bf994a362d307ba28b67c1372fa) by @andrcuns. See merge request dependabot-gitlab/dependabot!2687
- [Rename ecosystem table cell heading](dependabot-gitlab/dependabot@3d9d6d31009b05b66ce6a3a298c053642e4e24fd) by @andrcuns.
- [Add ability to start full dev setup manually](dependabot-gitlab/dependabot@be488afdb9682c57ae4e7311935b3132eb5542b2) by @andrcuns. See merge request dependabot-gitlab/dependabot!2671
- [Support running e2e tests in devcontainer setup](dependabot-gitlab/dependabot@2b9ab8a89ddcb1cae4edd63f371f252e1a263356) by @andrcuns. See merge request dependabot-gitlab/dependabot!2669
- [Split obsolete object closure in to separate classes ](dependabot-gitlab/dependabot@dd41162273d92b02ff5d9e268844288167eb1f2f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2665
- [Switch to json schema for all config options validation](dependabot-gitlab/dependabot@60e8fcb7006dbc9ed1caf720107ea9548284e0b4) by @andrcuns. See merge request dependabot-gitlab/dependabot!2663
- [Add additional debug logging when evaluating rules](dependabot-gitlab/dependabot@142f09ead9a03f29c68b9f8ee2c85cfc1dc48303) by @andrcuns. See merge request dependabot-gitlab/dependabot!2664
- [Store dependency files processed by update job](dependabot-gitlab/dependabot@fd3b8354f1f91899d4d617c119430433f18454e2) by @andrcuns. See merge request dependabot-gitlab/dependabot!2662
- [Fixes and improvements to devcontainer setup](dependabot-gitlab/dependabot@ac459d5a23a3c3527a822ed8336da23866c7bdd0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2659

### 🚀 Deployment changes (6 changes)

- [Bump hashicorp/google from 5.14.0 to 5.15.0 in /deploy](dependabot-gitlab/dependabot@01640f31ba40429091ec707989a39c4b8d9c063c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2726
- [Bump hashicorp/google from 5.13.0 to 5.14.0 in /deploy](dependabot-gitlab/dependabot@738dd1c10f5cb293b08ed1fd63a57056701e4ca1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2712
- [Bump hashicorp/google from 5.12.0 to 5.13.0 in /deploy](dependabot-gitlab/dependabot@0b0cc3641205556daa95afdf80f04714a4183f6f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2700
- [Bump hashicorp/google from 5.11.0 to 5.12.0 in /deploy](dependabot-gitlab/dependabot@271958e88650326b3e39072ab576dd0ba83ebfd4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2675
- [Bump hashicorp/kubernetes from 2.25.1 to 2.25.2 in /deploy](dependabot-gitlab/dependabot@81a38e3858e14774bab669fcb46db82e7dca759a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2654
- [Bump hashicorp/google from 5.10.0 to 5.11.0 in /deploy](dependabot-gitlab/dependabot@f1203e55c1fefb43fd1abbbff3fd10217d65cc77) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2653

## 3.13.3-alpha.1 (2024-01-14)

### 🔬 Improvements (1 change)

- [Allow to notify release for specific project](dependabot-gitlab/dependabot@055f4f38d83b56d55fc89bf67381cc136666d89f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2650

### 🐞 Bug Fixes (1 change)

- [Fix incorrectly named keyword argument in redis cache error handler](dependabot-gitlab/dependabot@88ba93d9f3de40eb751faf09c3e6bc3968c0843c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2645

### 📦 Dependency updates (5 changes)

- [Bump bootsnap from 1.17.0 to 1.17.1](dependabot-gitlab/dependabot@745a8f891e0643c7cce97b97ac39dee294cb10cb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2647
- [Bump sentry-sidekiq and sentry-rails](dependabot-gitlab/dependabot@908ecf68b972deb9f6f70fbb62709307cec8f4da) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2644
- [Bump sentry-rails and sentry-sidekiq](dependabot-gitlab/dependabot@c3729f035f49d366518004398ed6866a3b55cf16) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2643
- [Bump anyway_config from 2.6.1 to 2.6.2](dependabot-gitlab/dependabot@695aba60102c8bdfb9a7c91a0ff7bd589deb0b5b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2642
- [Bump puma from 6.4.1 to 6.4.2](dependabot-gitlab/dependabot@8042727e3b47c3ee54323627513dae044b3defe9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2640

### 📦🔧 Development dependency updates (3 changes)

- [Bump git from 1.19.0 to 1.19.1](dependabot-gitlab/dependabot@f6b6af8572a00719216c377c0976bd110d369a48) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2652
- [Bump faker from 3.2.2 to 3.2.3](dependabot-gitlab/dependabot@cd69cb764c324602aebccad4e7b2288fceab930a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2649
- [Bump rubocop-performance from 1.20.1 to 1.20.2](dependabot-gitlab/dependabot@68093f23f8d31f9f226981ea4b83b5479367d7bd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2641

### 🧰 Maintenance (1 change)

- [Strip leading zeros from cron expressions](dependabot-gitlab/dependabot@b9c99a82a8b6bc29667580b671f43b4f591b6701) by @andrcuns. See merge request dependabot-gitlab/dependabot!2651

## 3.13.2-alpha.1 (2024-01-08)

### 🔬 Improvements (1 change)

- [Add env secret support in registry url](dependabot-gitlab/dependabot@07b5aa245b69ac1567fcd0f6ce26aba5925508af) by @adrien.schmuck. See merge request dependabot-gitlab/dependabot!2629

### 🐞 Bug Fixes (4 changes)

- [Fix existing approval check](dependabot-gitlab/dependabot@5ef74a48f918cae956c1b42b8bb03167e8ef2496) by @andrcuns. See merge request dependabot-gitlab/dependabot!2635
- [Fix approving already approved merge request](dependabot-gitlab/dependabot@ecbd80158fb3114bb6e05cea6df08cc5afbf8552) by @andrcuns. See merge request dependabot-gitlab/dependabot!2632
- [Fix failing job name migration](dependabot-gitlab/dependabot@992db59e5290c9609301e8255c100120555693da) by @andrcuns. See merge request dependabot-gitlab/dependabot!2631
- [Remove pod log streaming](dependabot-gitlab/dependabot@c00bb1eca8f5a70ce139e5473c4dd1fa73344859) by @andrcuns. See merge request dependabot-gitlab/dependabot!2623

### 📦 Dependency updates (6 changes)

- [Bump yabeda-prometheus from 0.9.0 to 0.9.1](dependabot-gitlab/dependabot@e9daa4596b0b9915a0ec2328664e899e253ab8a8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2627
- [Bump stimulus-rails from 1.3.0 to 1.3.3](dependabot-gitlab/dependabot@70dbc48dbb2a87412337be55b7c83f76a37b9897) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2626
- [Bump puma from 6.4.0 to 6.4.1](dependabot-gitlab/dependabot@867f0577d57004d6b5f20bcde7875eade2c0d8f7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2625
- [Bump importmap-rails from 1.2.3 to 2.0.1](dependabot-gitlab/dependabot@230c894f0c3cd5ed485f5cedb86dc7e95ae1e147) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2624
- [Bump dartsass-sprockets from 3.0.0 to 3.1.0](dependabot-gitlab/dependabot@db6cfb124208258545c87c7bbe7366fc2ac1253c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2616
- [Bump grape-swagger from 2.0.0 to 2.0.1](dependabot-gitlab/dependabot@dc3b288120f367900ec0b1bd46a7202f634fd3e9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2621

### 📦🔧 Development dependency updates (11 changes)

- [Bump vue from 3.4.5 to 3.4.6](dependabot-gitlab/dependabot@379885b5c334cc946fad627533cdce0b53282d92) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2639
- [Bump vitepress from 1.0.0-rc.35 to 1.0.0-rc.36](dependabot-gitlab/dependabot@cb07addc8b2592a800340f9a474c194d98e58795) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2638
- [Bump @types/node from 20.10.6 to 20.10.7](dependabot-gitlab/dependabot@99654bf380f719848eded37d5f6a3eb625a0d4cd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2637
- [Bump vitepress from 1.0.0-rc.34 to 1.0.0-rc.35](dependabot-gitlab/dependabot@1155a0ef662fb5bcf2e2aac396d78308006d4808) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2633
- [Bump rubocop-rspec from 2.26.0 to 2.26.1](dependabot-gitlab/dependabot@5657aec1806e1a1daf21e63e91cbb36262a1e03a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2630
- [Bump rubocop-rspec from 2.25.0 to 2.26.0](dependabot-gitlab/dependabot@d64455e944906aae7cef368ea42885ea37c1ccaa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2628
- [Bump factory_bot_rails from 6.4.2 to 6.4.3](dependabot-gitlab/dependabot@a8a53408450bf152ab5133d1ebf91257484c420f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2615
- [Bump vitepress from 1.0.0-rc.32 to 1.0.0-rc.34](dependabot-gitlab/dependabot@d89196fedd61190257c6d1f15f105665cd364f45) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2619
- [Bump @types/node from 20.10.5 to 20.10.6](dependabot-gitlab/dependabot@1f84bed73590d40a47192c2af4b2493f5504b071) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2618
- [Bump reek from 6.1.4 to 6.2.0](dependabot-gitlab/dependabot@d34d2ad463ecd64e0ae858d48a586e44f6eb30a1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2617
- [Bump git from 1.18.0 to 1.19.0](dependabot-gitlab/dependabot@b79a76b69e3620c8b5125dc0d6122ae46287d888) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2614

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/kubernetes from 2.24.0 to 2.25.1 in /deploy](dependabot-gitlab/dependabot@93306405c621d32612642d49f247705c3d4b0cae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2636

## 3.13.1-alpha.1 (2023-12-29)

### 📦 Dependency updates (1 change)

- [Bump dependabot-omnibus from 0.238.0 to 0.239.0](dependabot-gitlab/dependabot@3d6313f3cbde23bdb54a6cb5f4fd7fee2da9c4b4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2613

## 3.13.0-alpha.1 (2023-12-28)

### 🔬 Improvements (6 changes)

- [Use separate cards for each project on projects page](dependabot-gitlab/dependabot@f4166d3ea799df16f60eea04f64e8258c2bae977) by @andrcuns. See merge request dependabot-gitlab/dependabot!2609
- [Improve job state toggle switch component](dependabot-gitlab/dependabot@460af425515929fad73429d053ac98fa4de2d1c9) by @andrcuns. See merge request dependabot-gitlab/dependabot!2606
- [Add alert for invalid login](dependabot-gitlab/dependabot@8118bd49dad8a4902163942fda78ee8fae381772) by @andrcuns. See merge request dependabot-gitlab/dependabot!2600
- [Add confirmation modal for project deletion](dependabot-gitlab/dependabot@ee8f6a638f78a0f1f86c47e62df288d1a5680f50) by @andrcuns. See merge request dependabot-gitlab/dependabot!2600
- [Use modal for adding projects and improve error handling](dependabot-gitlab/dependabot@395bbf985aeb4ab9dab1861db9e3730dd0c7ba8b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2600
- [Refactor UI to use bootstrap for styling ](dependabot-gitlab/dependabot@fb20c5ca4adc0a03f6f3dad3c95ae01754d67545) by @andrcuns. See merge request dependabot-gitlab/dependabot!2593

### 🐞 Bug Fixes (3 changes)

- [Fix error notifications in projects page](dependabot-gitlab/dependabot@aa1de67be312f05ece32c1dab1d14e55e31f45cd) by @andrcuns. See merge request dependabot-gitlab/dependabot!2608
- [Fix cases when open merge requests are incorectly considered as obsolete](dependabot-gitlab/dependabot@85aa7ce98373e713c0c2ce07076fc61e730e0bba) by @andrcuns. See merge request dependabot-gitlab/dependabot!2607
- [Fix notification rendering on job trigger](dependabot-gitlab/dependabot@da4c405b9b3be568fa30da8070354a33ddf87226) by @andrcuns. See merge request dependabot-gitlab/dependabot!2604

### 📦 Dependency updates (2 changes)

- [Bump tzinfo-data from 1.2023.3 to 1.2023.4](dependabot-gitlab/dependabot@b4cf77c15d0c53749474cfb9492e7ac3860c1224) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2591
- [Bump anyway_config from 2.6.0 to 2.6.1](dependabot-gitlab/dependabot@0f5cf3d8d61a8c4b87513ca3510693ec7dc5f776) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2590

### 📦🔧 Development dependency updates (5 changes)

- [Bump rubocop-rails from 2.23.0 to 2.23.1](dependabot-gitlab/dependabot@ec3e1fee2c8615a287a7d4cbcc2cb25abc9903c2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2598
- [Bump rubocop-performance from 1.20.0 to 1.20.1](dependabot-gitlab/dependabot@498370089ed9efdaf84ca9b6a5999e309e6f1880) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2597
- [Bump vue from 3.3.12 to 3.3.13](dependabot-gitlab/dependabot@d300e388e29ced322dd527c9ef2bba7ae4aa3652) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2596
- [Bump @types/node from 20.10.4 to 20.10.5](dependabot-gitlab/dependabot@cf1285d586192954324aa5e2908f8e0388bc55d4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2595
- [Bump debug from 1.9.0 to 1.9.1](dependabot-gitlab/dependabot@53db1e78ef0c5583f1ac4c15f981a653f0972718) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2592

### 🔧 CI changes (4 changes)

- [Remove git clone and cache from don-interrupt job](dependabot-gitlab/dependabot@a2f195bc9fdffb530957c60267364babb98955c1) by @andrcuns. See merge request dependabot-gitlab/dependabot!2601
- [Remove custom container scan job](dependabot-gitlab/dependabot@621e311a261daaaaa86b1d5595ce0e9cd9295730) by @andrcuns. See merge request dependabot-gitlab/dependabot!2599
- [Add prefix to package-lock related caches](dependabot-gitlab/dependabot@ced679aa986f3759ce678ce9e1615d91a3f0b51f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2599
- [Add e2e test report](dependabot-gitlab/dependabot@bde9f791ed6a95497f796597b2537cddc882c957) by @andrcuns. See merge request dependabot-gitlab/dependabot!2594

### 🧰 Maintenance (4 changes)

- [Render last execution time as time ago](dependabot-gitlab/dependabot@fa33fc0b1fb4c5b1cd36fc9a7bc3fd8dbdc23953) by @andrcuns. See merge request dependabot-gitlab/dependabot!2611
- [Store last run status and finish time on project model](dependabot-gitlab/dependabot@e05a869119347f2a22c96886cf25d4247f6d1720) by @andrcuns. See merge request dependabot-gitlab/dependabot!2605
- [Fix empty-folder image path](dependabot-gitlab/dependabot@e5bc4160030eadd916c4f00856f09a9ec628ff4c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2603
- [Improve error message in project table view ](dependabot-gitlab/dependabot@6ad169092ac9392975ca08e45b61dba2d0b0777c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2602

### 📄 Documentation updates (4 changes)

- [Update docker image info on index page](dependabot-gitlab/dependabot@668cfccf15e3a25c53d61ed96fbac7e49c1c465d) by @andrcuns. See merge request dependabot-gitlab/dependabot!2612
- [Add separate section for local configuration file](dependabot-gitlab/dependabot@6c4e3237e68ef3193d5705f561647b5520236a2e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2612
- [Add search bar to documentation site](dependabot-gitlab/dependabot@b71280f66bddde8430e46756bb77fe595655e387) by @andrcuns. See merge request dependabot-gitlab/dependabot!2612
- [Improve documentation on how to get started](dependabot-gitlab/dependabot@613ba90a122cf42086adf272b90ceee26a61cc75) by @andrcuns. See merge request dependabot-gitlab/dependabot!2610

## 3.12.0-alpha.1 (2023-12-21)

### 🚀 New features (4 changes)

- [Add json schema for dependabot.yml configuration file](dependabot-gitlab/dependabot@eb64902ed02212fb2bb0fc2724b6383c0cdf5a6e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2582
- [Support auto-merge via merge-train endpoints ](dependabot-gitlab/dependabot@c75b184d386bf1192f85a1348d8bf25eef6f349f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2574
- [Add option to automatically approve dependency update merge request](dependabot-gitlab/dependabot@4ca74d03a6cc89ba31aa1be6d0d9cf04c6e726ce) by @andrcuns. See merge request dependabot-gitlab/dependabot!2573
- [Add option to disable job within UI](dependabot-gitlab/dependabot@adefc527d3cdeb66c28e8966ed32838df9130490) by @andrcuns. See merge request dependabot-gitlab/dependabot!2560

### 🐞 Bug Fixes (7 changes)

- [Fix local configuration variable name and improve error handling](dependabot-gitlab/dependabot@2912c1ecbc24aed51e95793df16ca09c1154e0a0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2589
- [Correctly handle project not found errors](dependabot-gitlab/dependabot@a58134215040ab37518c177d50d785a37f744568) by @andrcuns. See merge request dependabot-gitlab/dependabot!2584
- [Correctly handle missing configuration](dependabot-gitlab/dependabot@0f98d7ea654bc248a1a24838cb6b31a94ce33323) by @andrcuns. See merge request dependabot-gitlab/dependabot!2584
- [Fix configuration validation task](dependabot-gitlab/dependabot@0a19be80ca9aeefae4837a7bb9716b17d362a04e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2583
- [Add missing api request logging](dependabot-gitlab/dependabot@b2c75c4bd4c79edf81b6d10dbf7bf7eb98e69364) by @andrcuns. See merge request dependabot-gitlab/dependabot!2579
- [Do not trigger rebase for all ecosystem mrs with rebase strategy - auto](dependabot-gitlab/dependabot@6c54a3d26e9de6a44f6ee62cf31520a6e0b163f3) by @andrcuns. See merge request dependabot-gitlab/dependabot!2575
- [Fix re-enabling dependency update job](dependabot-gitlab/dependabot@70c2852489166c8f5e745ce82eeec716f3912aa7) by @andrcuns. See merge request dependabot-gitlab/dependabot!2569

### 📦 Dependency updates (4 changes)

- [Bump sentry-rails from 5.15.0 to 5.15.2](dependabot-gitlab/dependabot@b18ea2d06ae4aa9dceb6bb1be50057f600674f1f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2586
- [Bump sentry-sidekiq from 5.15.0 to 5.15.2](dependabot-gitlab/dependabot@9539c66f1859dbf4d493e9bc054bd9a4d91b8b22) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2587
- [Bump anyway_config from 2.5.4 to 2.6.0](dependabot-gitlab/dependabot@dd8444ef758c513bcc559ee060209f50d898ea12) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2585
- [Bump sidekiq-cron from 1.11.0 to 1.12.0](dependabot-gitlab/dependabot@eed73a0410a92928ac2cbe97a2afa223572ffe6c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2556

### 📦🔧 Development dependency updates (9 changes)

- [Bump vue from 3.3.11 to 3.3.12](dependabot-gitlab/dependabot@158ab8f112e067d9c6e7b7b4eae89e9ca26fcfe7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2577
- [Bump vitepress from 1.0.0-rc.31 to 1.0.0-rc.32](dependabot-gitlab/dependabot@9b329ba3d70d9536989de31937be186d11e47cdc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2576
- [Bump rubocop-rails from 2.22.2 to 2.23.0](dependabot-gitlab/dependabot@04f8d98c50d39bf1bff7941ddc87bc18166a9320) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2572
- [Bump rubocop-performance from 1.19.1 to 1.20.0](dependabot-gitlab/dependabot@ade77abda666ecde8b9052acb029fced08cfd6ee) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2571
- [Bump rubocop from 1.58.0 to 1.59.0](dependabot-gitlab/dependabot@10ba8ffea3051868cc4d7ace3f13da290b803e80) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2566
- [Bump prettier from 3.1.0 to 3.1.1](dependabot-gitlab/dependabot@f7243090495aee896605ab795143e056a7f1ae5b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2563
- [Bump vue from 3.3.10 to 3.3.11](dependabot-gitlab/dependabot@c7e851460d5d3a0b2777f844ef043a96d001bf02) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2564
- [[Security] Bump vite from 5.0.2 to 5.0.7](dependabot-gitlab/dependabot@c1a5d171d81c88f2c9819275937e4e71b06277bf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2565
- [Bump @types/node from 20.10.3 to 20.10.4](dependabot-gitlab/dependabot@534d73c580e6f7f99b59f363b02b5cd0d0323864) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2562

### 🔧 CI changes (2 changes)

- [Improve E2E test environment setup](dependabot-gitlab/dependabot@8ec3ebaeae1aae611a3d87b13d464f0cc84c78d1) by @andrcuns. See merge request dependabot-gitlab/dependabot!2560
- [Add environment stop to e2e tests](dependabot-gitlab/dependabot@f2434c5aa79f69fc142c4d0cf88d4851b86b0250) by @andrcuns. See merge request dependabot-gitlab/dependabot!2557

### 🧰 Maintenance (7 changes)

- [Use default logger for api request logging](dependabot-gitlab/dependabot@1469e90c6d24e832b4a0054ea3de5c41d0e48a05) by @andrcuns. See merge request dependabot-gitlab/dependabot!2580
- [Ignore error caused by merge train pipeline event](dependabot-gitlab/dependabot@f0487b6267620f517d36d0f49ca1c745225fa368) by @andrcuns. See merge request dependabot-gitlab/dependabot!2578
- [Refactor e2e test mocking setup](dependabot-gitlab/dependabot@d65d595dc27b4fe13b4768902ea8e38e0b6125f0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2570
- [Add dependency update e2e test](dependabot-gitlab/dependabot@e543aad772b952cce041be9aa51fe87ae628fa62) by @andrcuns. See merge request dependabot-gitlab/dependabot!2568
- [Add missing bootsnap to ecosystem images](dependabot-gitlab/dependabot@d7dba93f6859227872cc4f10c7db8d7681783142) by @andrcuns.
- [Precompile bootsnap code for faster boot time](dependabot-gitlab/dependabot@26da5ed5ba995c020893e5da9660388c95fa2281) by @andrcuns. See merge request dependabot-gitlab/dependabot!2567
- [Improve sentry sample rate configuration](dependabot-gitlab/dependabot@0abebd71ba370ab7a9a5bc52192554ca8d9f423f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2558

### 📄 Documentation updates (1 change)

- [Add badges to main README](dependabot-gitlab/dependabot@fc21865d461fee659202a7b23d638c379310c37e) by @andrcuns.

### 🚀 Deployment changes (3 changes)

- [Bump hashicorp/google from 5.8.0 to 5.10.0 in /deploy](dependabot-gitlab/dependabot@c339c6f76c20f12c7f9606555455d1e808ddabbb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2588
- [Bump hashicorp/google from 5.7.0 to 5.8.0 in /deploy](dependabot-gitlab/dependabot@439a61a00b87943b0b747e81c314dbc6f3788b2e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2561
- [Explicitly set sensitive values via set_sensitive method](dependabot-gitlab/dependabot@e16cd8deca8f211e06427c6bd7cb295fcf905f59) by @andrcuns. See merge request dependabot-gitlab/dependabot!2559

## 3.11.0-alpha.1 (2023-12-08)

### 🐞 Bug Fixes (1 change)

- [Do not spam no obsolete issues found messages to info](dependabot-gitlab/dependabot@6f10a601d5a0014d851bb1640204a72947001ede) by @andrcuns. See merge request dependabot-gitlab/dependabot!2542

### 📦 Dependency updates (2 changes)

- [Bump dependabot-omnibus from 0.237.0 to 0.238.0](dependabot-gitlab/dependabot@388b8532d8fdeef292b7599273174a7addd83d52) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2555
- [Bump sentry-rails and sentry-sidekiq](dependabot-gitlab/dependabot@3f1d468474c50475be36ff22c8d1633abf204905) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2551

### 📦🔧 Development dependency updates (5 changes)

- [Bump rubocop from 1.57.2 to 1.58.0](dependabot-gitlab/dependabot@19f863c67b717deb9d2eaf0f9c8f05fec3dcdaaa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2543
- [Bump solargraph from 0.49.0 to 0.50.0](dependabot-gitlab/dependabot@81390a774b71477eddb0233a9773512a360e05bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2553
- [Bump vue from 3.3.9 to 3.3.10](dependabot-gitlab/dependabot@60d9a3848a4fa110f9b8939ceacb0801d283defb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2550
- [Bump @types/node from 20.10.0 to 20.10.3](dependabot-gitlab/dependabot@05759c85ea1a9d9a79aec24e51bfbe097aafeae8) by @dependabot-bot.
- [Bump @playwright/test from 1.40.0 to 1.40.1](dependabot-gitlab/dependabot@0a91ae08dc617bfb6bde899f86463aef74d6e36a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2548

### 🔧 CI changes (5 changes)

- [Skip arm64 builds for nuget ecosystem](dependabot-gitlab/dependabot@15cd6e44f84ac094e1fae56439b125d1a4f21c50) by @andrcuns. See merge request dependabot-gitlab/dependabot!2555
- [Update quay.io/containers/skopeo Docker tag to v1.14.0](dependabot-gitlab/dependabot@ce42b6675d20bbd0d2ec14dba1fde046870b954b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2554
- [Correctly update standalone repo release version numbers in README](dependabot-gitlab/dependabot@6a034090ce67e20c526719ba73310f252ccee0fd) by @andrcuns. See merge request dependabot-gitlab/dependabot!2544
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/helm-terraform Docker tag to v3.13](dependabot-gitlab/dependabot@a9c57f266001dad70280915b5f356c00562c3846) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2545
- [Update terraform version for deployment pipeline](dependabot-gitlab/dependabot@d474995e9ef51123c6c66f48972b90b7e3f67bb5) by @andrcuns.

### 🧰 Maintenance (1 change)

- [Remove custom callback for sidekiq-alive queue removal](dependabot-gitlab/dependabot@fa3d53722ab25a2eccc3838f13f271040d8472cb) by @andrcuns.

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/helm from 2.11.0 to 2.12.1 in /deploy](dependabot-gitlab/dependabot@e13c33fba3d9dd43252fbd3fc329ca77f4a44262) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2546
- [Bump hashicorp/kubernetes from 2.23.0 to 2.24.0 in /deploy](dependabot-gitlab/dependabot@e288a5e17d15b7584166f903405460356bedb1f3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2547

## 3.10.0-alpha.1 (2023-11-29)

### 🚀 New features (2 changes)

- [Add option to delay auto-merge attempt](dependabot-gitlab/dependabot@964fabe3b79712eb43a4bc02667246dfa227ee23) by @andrcuns. See merge request dependabot-gitlab/dependabot!2539
- [Support for local configuration file ](dependabot-gitlab/dependabot@fdb9a8088dcb7e6345c96521c686d9bf678729e5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2536

### 📦 Dependency updates (1 change)

- [Bump sentry-rails and sentry-sidekiq](dependabot-gitlab/dependabot@c6eb051783be47e25d33fdf232df6ba35b4917d8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2537

### 📦🔧 Development dependency updates (3 changes)

- [Bump vue from 3.3.8 to 3.3.9](dependabot-gitlab/dependabot@96bc4d2a4807ef6f32f25b56666f19ceeee47f28) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2534
- [Bump vitepress from 1.0.0-rc.29 to 1.0.0-rc.31](dependabot-gitlab/dependabot@942f9ae863fbf7f5cc5f30f1a04783c484878041) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2533
- [Bump @types/node from 20.9.2 to 20.10.0](dependabot-gitlab/dependabot@f83a42d9d639b267a0d0459ad1edb3dbe6c8f0bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2532

### 🔧 CI changes (3 changes)

- [Update snyk cli version for container job](dependabot-gitlab/dependabot@13f3c7d92f2e0a8de460a0dea735ebd1b19353fb) by @andrcuns.
- [Use none instead of empty script for no-op version update](dependabot-gitlab/dependabot@3fe6e1ec4b44899567ec47761c6fa0ba28ea29af) by @andrcuns.
- [Add pre-defined options to manual web pipelines](dependabot-gitlab/dependabot@a354f4d411338882c3d0d52eb2000f96484678ba) by @andrcuns.

### 📄 Documentation updates (1 change)

- [Slightly improve warning messages in main documentation page](dependabot-gitlab/dependabot@3e57ad989a81239eab78b486e00dedb6f75f1e2f) by @andrcuns.

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 5.6.0 to 5.7.0 in /deploy](dependabot-gitlab/dependabot@38aba72f91f3869adbc31789bbaebb842e613a3b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2535

## 3.9.2-alpha.1 (2023-11-26)

### 🔧 CI changes (2 changes)

- [Add pre-defined options to manual web pipelines](dependabot-gitlab/dependabot@a354f4d411338882c3d0d52eb2000f96484678ba) by @andrcuns.
- [Tag standalone repo with clean semver](dependabot-gitlab/dependabot@8e6b0b00c18891846c7091ce01a0f30f512ccb3b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2531

## 3.9.1-alpha.1 (2023-11-26)

### 🔧 CI changes (1 change)

- [Add version update job](dependabot-gitlab/dependabot@46ad713f5d96d5d44904e7ac4cf4d40635d59525) by @andrcuns. See merge request dependabot-gitlab/dependabot!2530

### 📄 Documentation updates (1 change)

- [Remove main-latest tag mention from docs](dependabot-gitlab/dependabot@5df1bb45613a3a8e872abe4d7bfd7721302c0c87) by @andrcuns.

## 3.9.0-alpha.1 (2023-11-25)

### 🐞 Bug Fixes (1 change)

- [Fix confusing log entry about unsupported ecosystem for vulnerability detection](dependabot-gitlab/dependabot@3b91bd07eaa38cc841adc72c64eadded3907aa89) by @andrcuns. See merge request dependabot-gitlab/dependabot!2518

### 📦 Dependency updates (2 changes)

- [Bump mongoid from 8.1.3 to 8.1.4](dependabot-gitlab/dependabot@169c277a184ddcc372c0e9e5ad8854cbf73f78bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2525
- [Bump dependabot-omnibus from 0.236.0 to 0.237.0](dependabot-gitlab/dependabot@b1f857c3481487b93ffc97c95c84cd8868b890bd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2523

### 📦🔧 Development dependency updates (7 changes)

- [Bump factory_bot_rails from 6.4.0 to 6.4.2](dependabot-gitlab/dependabot@16033c01412e126bf5e7238eed65023187320f45) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2527
- [Bump spring from 4.1.2 to 4.1.3](dependabot-gitlab/dependabot@6a70161fd59bf0bb4b0cb29688bb3ac805199dc3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2526
- [Bump rspec-rails from 6.0.3 to 6.1.0](dependabot-gitlab/dependabot@928c89ab61e952ac7d42fe3ad827041f1f643a78) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2524
- [Bump @types/node from 20.8.10 to 20.9.2](dependabot-gitlab/dependabot@90c88c0e96eedaff92568c4a06e43711cf0427bc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2521
- [Bump vitepress from 1.0.0-rc.25 to 1.0.0-rc.29](dependabot-gitlab/dependabot@88332d452b6ebdb82ae11ed3785aacb994961f52) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2522
- [Bump @playwright/test from 1.39.0 to 1.40.0](dependabot-gitlab/dependabot@e81f30ffa59c7188c5c1df530bb9f275d12ea510) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2520
- [Bump rubocop-rails from 2.22.1 to 2.22.2](dependabot-gitlab/dependabot@0b5db41c9940a87bb92beb6f2a76bcd2c8872d4b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2517

### 🔧 CI changes (1 change)

- [Update standalone version bump task](dependabot-gitlab/dependabot@aa8dc0960bbfb1a4b88d860c7e02a07301cbcf15) by @andrcuns. See merge request dependabot-gitlab/dependabot!2529

### 📄 Documentation updates (1 change)

- [Remove main-latest tag mention from docs](dependabot-gitlab/dependabot@5df1bb45613a3a8e872abe4d7bfd7721302c0c87) by @andrcuns.

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 5.5.0 to 5.6.0 in /deploy](dependabot-gitlab/dependabot@b3607851a64644193205f2a8375e16f76d80e1a1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2519

## 3.8.0-alpha.1 (2023-11-19)

### 🐞 Bug Fixes (1 change)

- [Add missing methods for group updates logging](dependabot-gitlab/dependabot@a939b06bbf29026e2203251be1d717fc951b3922) by @andrcuns. See merge request dependabot-gitlab/dependabot!2515

### 📦 Dependency updates (7 changes)

- [Bump grape and grape-swagger](dependabot-gitlab/dependabot@2eb55ae7360ae415ab370ce56aa905cf35102827) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2512
- [Bump bcrypt from 3.1.19 to 3.1.20](dependabot-gitlab/dependabot@22f85ed5b8f0939d5bb1d34a71b7cb40716569eb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2511
- [Bump rails from 7.1.1 to 7.1.2](dependabot-gitlab/dependabot@a87fe7facb8f3c0e67a9a9cbd02a65d0eb4727b6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2503
- [Bump sentry-rails and sentry-sidekiq](dependabot-gitlab/dependabot@7558fdb87036a1ea176fb5f95f2fd4a8e645d2c0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2501
- [Bump sidekiq-cron from 1.10.1 to 1.11.0](dependabot-gitlab/dependabot@4e30f87e34e57ef3f952e0634bf25c997018ffc7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2491
- [Bump sidekiq from 7.1.6 to 7.2.0](dependabot-gitlab/dependabot@f7a804ae221f12234d831b8caa6de68c925e86c9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2489
- [Bump bootsnap from 1.16.0 to 1.17.0](dependabot-gitlab/dependabot@39a82e5e22576c977116f5c207b16d4f8fff53b4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2486

### 📦🔧 Development dependency updates (10 changes)

- [Bump factory_bot_rails from 6.2.0 to 6.4.0](dependabot-gitlab/dependabot@6cd89f27402c3744cc379927de07c2cdea9ee20c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2514
- [Bump spring from 4.1.1 to 4.1.2](dependabot-gitlab/dependabot@dc5207ef7e5d4e0a5d85071e870028ae560f470e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2508
- [Bump prettier from 3.0.3 to 3.1.0](dependabot-gitlab/dependabot@9cbab49ec88c44dff7da9136a60a7415b7fceeb4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2507
- [Bump docker-compose from 0.24.2 to 0.24.3](dependabot-gitlab/dependabot@18b0e1b7a6b1280932ed215a5a934007273959dd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2496
- [Bump vue from 3.3.7 to 3.3.8](dependabot-gitlab/dependabot@f319ba08111b38a070f3e17aa88395af6182f079) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2498
- [Bump @types/js-yaml from 4.0.8 to 4.0.9](dependabot-gitlab/dependabot@c35e112a0d3bcedf2bb8f25110e7f611e5c9d9dc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2504
- [Bump vitepress from 1.0.0-rc.24 to 1.0.0-rc.25](dependabot-gitlab/dependabot@22b4d16a759ec786d2b5aa6701990ab09f2e1378) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2497
- [Bump @types/node from 20.8.9 to 20.8.10](dependabot-gitlab/dependabot@d9e0d2bceb4483f4cb6cca1152c14561fd33d7e1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2495
- [Bump faker from 3.2.1 to 3.2.2](dependabot-gitlab/dependabot@d30c1f323e7efff7853c3b3e10fafed37d95eeae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2492
- [Bump @types/node from 20.8.8 to 20.8.9](dependabot-gitlab/dependabot@4ba65cc32e2bedfb052763e85bd822aca2720ebd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2480

### 🔧 CI changes (2 changes)

- [Fix fetching version in migration test job](dependabot-gitlab/dependabot@4dd972cd60fd383d8926036cb811c7887f223810) by @andrcuns. See merge request dependabot-gitlab/dependabot!2516
- [Do not run container scan in fork](dependabot-gitlab/dependabot@6db853395e2ced5c5a1e2faf8d41b2625257ee85) by @andrcuns. See merge request dependabot-gitlab/dependabot!2516

### 🧰 Maintenance (18 changes)

- [Extract obsolete entity closing in to separate operation](dependabot-gitlab/dependabot@21e8e35e9f52f1270661128620a12bc22f26a3e2) by @andrcuns. See merge request dependabot-gitlab/dependabot!2488
- [Extract vulnerability issue creation to operation](dependabot-gitlab/dependabot@089814c1af0f5cd44c8a6bb024505ef4017d29a6) by @andrcuns. See merge request dependabot-gitlab/dependabot!2488
- [Refactor update mr creation flow](dependabot-gitlab/dependabot@b70704b91cfa7425072df0471e9cdce24f4a62e0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2488
- [Move update based classes to Update namespace](dependabot-gitlab/dependabot@a109e18696b7be7391f3dfd6c34e907217e996c0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2487
- [Move classes from Dependencies namespace to Update namespace](dependabot-gitlab/dependabot@794891d748990113348cc3b7a28d0e1fbea4dcf6) by @andrcuns. See merge request dependabot-gitlab/dependabot!2487
- [Move routers and triggers to Job namespace](dependabot-gitlab/dependabot@f9af7be2a4b6870f9fb683e73a944b004eb7008e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2487
- [Move credentials service](dependabot-gitlab/dependabot@fd4057bf171a3b8f77206c2813f581811a983964) by @andrcuns. See merge request dependabot-gitlab/dependabot!2485
- [Move updated dependency class](dependabot-gitlab/dependabot@3e49973cf6696e6205b92cd71c09b4cf036587bd) by @andrcuns. See merge request dependabot-gitlab/dependabot!2484
- [Save dependabot-core logs to a file](dependabot-gitlab/dependabot@647c2e79605f0b7bb81d7cb3dc11bf521c362ad6) by @andrcuns. See merge request dependabot-gitlab/dependabot!2483
- [Store version in VERSION file](dependabot-gitlab/dependabot@6f13c466afc99933ace5f777a71b6f10a02a4ae3) by @andrcuns. See merge request dependabot-gitlab/dependabot!2482
- [Move log entry and error store classes](dependabot-gitlab/dependabot@5f29443960ac68fcfa8f22f0ef0b5d01b7adb880) by @andrcuns. See merge request dependabot-gitlab/dependabot!2481
- [Move gitlab client object](dependabot-gitlab/dependabot@f1956df26d128f5e741a49435b816623568f27ce) by @andrcuns. See merge request dependabot-gitlab/dependabot!2481
- [Move user and milestone finder classes](dependabot-gitlab/dependabot@3f733f315a4d3cad3aa511185d3d2b6d15a05e47) by @andrcuns. See merge request dependabot-gitlab/dependabot!2481
- [Extract merge request finder](dependabot-gitlab/dependabot@1b680919fc7ad9cba8f96745a0a470ad3bfef441) by @andrcuns. See merge request dependabot-gitlab/dependabot!2481
- [Extract gitlab webhook finder](dependabot-gitlab/dependabot@04e23a6ee67f6e7e809ff1211e0ecac2c63671f7) by @andrcuns. See merge request dependabot-gitlab/dependabot!2481
- [Split configuration parsing and options](dependabot-gitlab/dependabot@294f418b9afc5ee63a20aa038cc641a9d1db6823) by @andrcuns. See merge request dependabot-gitlab/dependabot!2479
- [Move github graphql methods to helper class](dependabot-gitlab/dependabot@856d9462055f12279675b47505fa23cb2fb2b00d) by @andrcuns. See merge request dependabot-gitlab/dependabot!2479
- [Change schedule to object value class](dependabot-gitlab/dependabot@d1ef5f7bf1e91aebeb093b58b85957c0c2c4d762) by @andrcuns. See merge request dependabot-gitlab/dependabot!2479

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/google from 5.4.0 to 5.5.0 in /deploy](dependabot-gitlab/dependabot@3bebd8282e936613bce5d73eaacb57784492a446) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2506
- [Bump hashicorp/google from 5.3.0 to 5.4.0 in /deploy](dependabot-gitlab/dependabot@525e67188136210a5f821e31785249c6ebe3fadf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2494

## 3.7.0-alpha.1 (2023-10-29)

### 🐞 Bug Fixes (3 changes)

- [Do not return closed issues when checking existing vulnerability issues](dependabot-gitlab/dependabot@e9e02e8529c1c692c2ac6ff8d9f032acf4e1b3dc) by @andrcuns. See merge request dependabot-gitlab/dependabot!2476
- [Do not swallow errors in dep update rake tasks](dependabot-gitlab/dependabot@f9b9876b0a74ac43e4c82c0197e13cb20d42e49e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2470
- [Pass correct repo_contents_path type](dependabot-gitlab/dependabot@fb3c3b68d7885133a2f93044ced8692867b9e401) by @andrcuns. See merge request dependabot-gitlab/dependabot!2453

### 📦 Dependency updates (7 changes)

- [Bump dependabot-omnibus from 0.235.0 to 0.236.0](dependabot-gitlab/dependabot@96ead22a90161c2fed8b3ce37effff32290065f6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2467
- [Bump rails from 7.0.8 to 7.1.1](dependabot-gitlab/dependabot@6ca5fa40397250a8bad5253bf5ccb9eb5b437b32) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2462
- [Bump redis from 5.0.7 to 5.0.8](dependabot-gitlab/dependabot@e8493aa7e605af2d2d010ddb73392827829310cf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2461
- [Bump mongoid from 8.1.2 to 8.1.3](dependabot-gitlab/dependabot@41211bc74e18e47a02bc51aa4550c49948b2a1ca) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2459
- [Bump dependabot-omnibus from 0.234.0 to 0.235.0](dependabot-gitlab/dependabot@117211108a2036ad9082fde3a502d2ca3186486c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2452
- [Bump sidekiq_alive from 2.3.0 to 2.3.1](dependabot-gitlab/dependabot@29e47043f6dcc5676017ff61606f09f40a969e08) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2451
- [Bump anyway_config from 2.5.3 to 2.5.4](dependabot-gitlab/dependabot@cb1fab6a82878afadc36b76947861a7b5a4c1924) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2450

### 📦🔧 Development dependency updates (12 changes)

- [Bump rubocop-rails from 2.21.2 to 2.22.1](dependabot-gitlab/dependabot@9bbe172c930d66b32de814fd893424654aaeb655) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2474
- [Bump rubocop-rspec from 2.24.1 to 2.25.0](dependabot-gitlab/dependabot@f753ae1378ca35d88de0ae6949df7b2f49d32531) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2475
- [Bump rspec-sidekiq from 4.0.2 to 4.1.0](dependabot-gitlab/dependabot@8b7c455f84ba6af7199260cd49cdf9ee6e9fee37) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2473
- [Bump rubocop from 1.57.1 to 1.57.2](dependabot-gitlab/dependabot@8280786f5185c35a84ec3b12fe68fa2b1584f508) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2468
- [Bump vue from 3.3.6 to 3.3.7](dependabot-gitlab/dependabot@583f071b0a2955ac85cb38ebf5a60b3a8a4540d6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2465
- [Bump vitepress from 1.0.0-rc.22 to 1.0.0-rc.24](dependabot-gitlab/dependabot@104f66ff53c1372af72e967cf59ea381f71d6fe3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2464
- [Bump @types/node from 20.8.6 to 20.8.8](dependabot-gitlab/dependabot@e076685e83de62da7f44fe00003a13c7f1fcfa0b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2463
- [Bump vue from 3.3.4 to 3.3.6](dependabot-gitlab/dependabot@b28256325b1e3162a15bd6df0551983a80f5074e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2458
- [Bump @types/js-yaml from 4.0.6 to 4.0.8](dependabot-gitlab/dependabot@8b263f507028bb2ceda70aa2494ffbb06ce1309b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2455
- [Bump @types/node from 20.8.4 to 20.8.6](dependabot-gitlab/dependabot@2f55925da5b32095c50b4b7609e30558faa43512) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2448
- [Bump vitepress from 1.0.0-rc.20 to 1.0.0-rc.22](dependabot-gitlab/dependabot@4e5c067c821060116239286bd30b50fb3e9323f1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2449
- [Bump @playwright/test from 1.38.1 to 1.39.0](dependabot-gitlab/dependabot@83f859b9b691d648a0e9074dc9cc52b259c61db5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2446

### 🔧 CI changes (4 changes)

- [Update secret file name in migration-test](dependabot-gitlab/dependabot@f1beb34cb7a84497a39da08b351d5c0b87ea4180) by @andrcuns. See merge request dependabot-gitlab/dependabot!2478
- [Disable provenance for docker builds](dependabot-gitlab/dependabot@f194dfaab355121f45fd10cbfd86a5f2972e9004) by @andrcuns. See merge request dependabot-gitlab/dependabot!2477
- [Move back to gitlab container registry for dev images](dependabot-gitlab/dependabot@620fb6b0ecdbcc9cd52bbeee1aadd1deff5714cd) by @andrcuns. See merge request dependabot-gitlab/dependabot!2469
- [Update test-migrations script](dependabot-gitlab/dependabot@c1c5a095eae908337a5f1bf07b403f21382ffa54) by @andrcuns. See merge request dependabot-gitlab/dependabot!2466

### 🧰 Maintenance (1 change)

- [Improve logging for handling vulnerability issues and obsolete merge requests](dependabot-gitlab/dependabot@b81b30fad6434f6154f7503c20c7250a4b77aed5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2472

### 🚀 Deployment changes (3 changes)

- [Fix updater image pattern for deployment](dependabot-gitlab/dependabot@3590a7c40ea38684ada37342c42141a97eeca331) by @andrcuns.
- [Bump hashicorp/google from 5.1.0 to 5.2.0 in /deploy](dependabot-gitlab/dependabot@c3124c42863bff114ab409e2a38c900336ee1788) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2454
- [Bump hashicorp/google from 5.0.0 to 5.1.0 in /deploy](dependabot-gitlab/dependabot@2692ab82a4cfd7f1609accbe1ed38665206fc46b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2445

## 3.6.0-alpha.1 (2023-10-15)

### 🐞 Bug Fixes (2 changes)

- [Do not add obsolete merge request comments to merged mrs](dependabot-gitlab/dependabot@fb530de08dafb0eaad7ae7896c1e6d3b89643a2f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2443
- [Only close obsolete merge requests if dependency is up to date](dependabot-gitlab/dependabot@bfed947de539c1e7a94a304a3a2dac36cf3b5774) by @andrcuns. See merge request dependabot-gitlab/dependabot!2444

### 📦 Dependency updates (6 changes)

- [Bump dependabot-omnibus from 0.233.0 to 0.234.0](dependabot-gitlab/dependabot@ad0e80db1f64742dde4811a0cb85256a7227a4aa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2441
- [Bump sentry-sidekiq and sentry-rails](dependabot-gitlab/dependabot@02ce4482365812554baf5e912ee18304f7ebc02b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2439
- [Bump lograge from 0.13.0 to 0.14.0](dependabot-gitlab/dependabot@32d1da60820cc2f88cc07739a8dbdad8236c1102) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2437
- [Bump dependabot-omnibus from 0.232.0 to 0.233.0](dependabot-gitlab/dependabot@49948135b65b57b90db0dc9a2fc4d9b77524f4cb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2432
- [Bump sidekiq from 7.1.5 to 7.1.6](dependabot-gitlab/dependabot@f916cca66fd28451048227b7d5a07e1bef511ee6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2436
- [Bump sidekiq from 7.1.4 to 7.1.5](dependabot-gitlab/dependabot@b4560eed79b29e859403aa5fc89f6fd2ea74e8c9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2429

### 📦🔧 Development dependency updates (6 changes)

- [Bump rubocop from 1.57.0 to 1.57.1](dependabot-gitlab/dependabot@c8e9f17640fb02d05325b7b92f092d0c3171571b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2442
- [Bump rubocop from 1.56.4 to 1.57.0](dependabot-gitlab/dependabot@a435b6ef52c638a47b4e3d624d8426f66cce55a9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2440
- [Bump @types/node from 20.8.0 to 20.8.4](dependabot-gitlab/dependabot@838f64dd69ed4d3371290f151d4c56d7a96bff68) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2434
- [[Security] Bump postcss from 8.4.30 to 8.4.31](dependabot-gitlab/dependabot@f08fbac206b47190959f448729f29c45af33e800) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2435
- [Bump @types/node from 20.6.5 to 20.8.0](dependabot-gitlab/dependabot@d736cfad7c2ef6da2d6f28c43ecda6448a61955b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2428
- [Bump rubocop-rails from 2.21.1 to 2.21.2](dependabot-gitlab/dependabot@7a5ed86e1039cd68e62c564e92fdccd43390d615) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2426

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/google from 4.84.0 to 5.0.0 in /deploy](dependabot-gitlab/dependabot@e9d5e04471431262ea43616af68c5af1199ba067) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2433
- [Bump hashicorp/google from 4.83.0 to 4.84.0 in /deploy](dependabot-gitlab/dependabot@f450724d211caad4552606ac989e230f95147d9f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2427

## 3.5.0-alpha.1 (2023-09-30)

### 🔬 Improvements (2 changes)

- [Add explicit responses for skipped webhook event handling](dependabot-gitlab/dependabot@36b7658aa8a12cbf95e0b59e815d8afb10e8fa2b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2425
- [Add validation for project name parameter](dependabot-gitlab/dependabot@02ce911d273fcbe5674d13b6595dc3838a234f51) by @andrcuns. See merge request dependabot-gitlab/dependabot!2423

### 📦 Dependency updates (1 change)

- [Update bitnami/mongodb Docker tag to v7](dependabot-gitlab/dependabot@83f3a07921ead958a1f3264914e8aaeedc832c85) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2422

### 📦🔧 Development dependency updates (1 change)

- [Bump rubocop from 1.56.3 to 1.56.4](dependabot-gitlab/dependabot@504ddc45e75dc594931cd996838266789737ed05) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2424

## 3.4.0-alpha.1 (2023-09-27)

### 🔬 Improvements (1 change)

- [Add divider to projects page](dependabot-gitlab/dependabot@28e4e299c618f5b1b40c5b4c454b709632c0bee3) by @andrcuns. See merge request dependabot-gitlab/dependabot!2411

### 🐞 Bug Fixes (1 change)

- [Fix sidekiq UI functions not working](dependabot-gitlab/dependabot@b8147a118230bcd472b2cc51187a40bd236d7b82) by @andrcuns. See merge request dependabot-gitlab/dependabot!2420

### 📦 Dependency updates (3 changes)

- [Bump puma from 6.3.1 to 6.4.0](dependabot-gitlab/dependabot@0f54ec3f52f1c1ae49f70414d08cd0e540095dad) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2412
- [Update docker Docker tag to v24.0.6](dependabot-gitlab/dependabot@f36ca7cf5638745725a8bef1de2dec24a8a76b5d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2405
- [Bump dependabot-omnibus from 0.231.0 to 0.232.0](dependabot-gitlab/dependabot@d04dd1638e5e171651293b9862c016b1199b59bf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2401

### 📦🔧 Development dependency updates (10 changes)

- [Bump vitepress from 1.0.0-rc.14 to 1.0.0-rc.20](dependabot-gitlab/dependabot@cbb5d628d0036ce6b892d299dfebbf79a5b6288e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2419
- [Bump @types/node from 20.6.2 to 20.6.5](dependabot-gitlab/dependabot@1583e238e497dceaf073a60357adab897bd86b1b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2418
- [Bump @playwright/test from 1.38.0 to 1.38.1](dependabot-gitlab/dependabot@2425643e4ca436850359b8891022f85dfc45cf63) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2417
- [Bump rubocop-rspec from 2.24.0 to 2.24.1](dependabot-gitlab/dependabot@4e1dcdf62a18d688a925198d838185008788b54a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2413
- [Bump @types/js-yaml from 4.0.5 to 4.0.6](dependabot-gitlab/dependabot@b3b278ea7f5d599f52b4dd495419c1197826271c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2408
- [Bump @types/node from 20.6.0 to 20.6.2](dependabot-gitlab/dependabot@f777782651e08bf7409fb3357b1e72198a45d3cd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2409
- [Bump vitepress from 1.0.0-rc.12 to 1.0.0-rc.14](dependabot-gitlab/dependabot@5c63c9b37626ba6ddbb5f2172e8f460407a20c78) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2410
- [Bump @playwright/test from 1.37.1 to 1.38.0](dependabot-gitlab/dependabot@a5e69772c79471edacc5236f61d3d066b3f22aaf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2407
- [Bump rubocop-performance from 1.19.0 to 1.19.1](dependabot-gitlab/dependabot@a53578b63f7044e306f09bc28d3d9ef4a2a5a46f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2404
- [Bump rubocop-rails from 2.21.0 to 2.21.1](dependabot-gitlab/dependabot@c0ba5379c96c93028bb08300ccf1de182dd61264) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2402

### 🔧 CI changes (4 changes)

- [Build arm images from main branch](dependabot-gitlab/dependabot@dbce8d13a7282baacf1687031d4df5f65299b509) by @andrcuns.
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/release-cli Docker tag to v0.16](dependabot-gitlab/dependabot@99bbdbd07ca90ae92fcfa58c759e29ae0ecdc740) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2415
- [Update quay.io/containers/skopeo Docker tag to v1.13.3](dependabot-gitlab/dependabot@e3db1523b36e3fe472883d4c7c4107a1e2e4c54d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2414
- [Compile assets in forked pipelines](dependabot-gitlab/dependabot@82733d9c363ee1fc02fef0a8d9d43eec8ad71746) by @andrcuns.

### 🧰 Maintenance (1 change)

- [Move divider after add new project button](dependabot-gitlab/dependabot@159a611007d7929e6283ce31dcc18ba424a4d7ca) by @andrcuns. See merge request dependabot-gitlab/dependabot!2421

### 📄 Documentation updates (1 change)

- [Add more information about mounting volumes inside the updater](dependabot-gitlab/dependabot@fa6d949330b605c0e10e8faa759543d1f3c5a7c8) by @jerbob92. See merge request dependabot-gitlab/dependabot!2403

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/google from 4.82.0 to 4.83.0 in /deploy](dependabot-gitlab/dependabot@494a6ba50884617b129ff3ac373c7973ca0353ae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2416
- [Bump hashicorp/google from 4.81.0 to 4.82.0 in /deploy](dependabot-gitlab/dependabot@6d3f1fcd5b37d7f57b0f5ac10e6af81537f958bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2406

## 3.3.0-alpha.1 (2023-09-13)

### 🔬 Improvements (1 change)

- [Do not clear search results after triggering job](dependabot-gitlab/dependabot@98d1b5e7adf8f178d92779818a6dbc1caa23aaa9) by @andrcuns. See merge request dependabot-gitlab/dependabot!2396

### 📦 Dependency updates (5 changes)

- [Bump dependabot-omnibus from 0.230.0 to 0.231.0](dependabot-gitlab/dependabot@c79ea27d0463e1925d359652e70755908dd61c7b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2398
- [Bump sidekiq from 7.1.3 to 7.1.4](dependabot-gitlab/dependabot@0ad6d7f8498abf7a3cd6dc6de751cffe91e562f2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2399
- [Bump anyway_config from 2.5.2 to 2.5.3](dependabot-gitlab/dependabot@15cc27ea445a34c8f1a7041d1e00e635c9674ef9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2397
- [Bump sidekiq from 7.1.2 to 7.1.3](dependabot-gitlab/dependabot@beb5853c9268478f5e34a757d8c10d29aa405f08) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2394
- [Bump rails from 7.0.7.2 to 7.0.8](dependabot-gitlab/dependabot@0c1cf809a3d54df0f943b36e35b6f2862514d26b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2389

### 📦🔧 Development dependency updates (4 changes)

- [Bump rubocop from 1.56.2 to 1.56.3](dependabot-gitlab/dependabot@7640ed5f6bbdf900ee69c35118180d4f812cf38a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2395
- [Bump vitepress from 1.0.0-rc.10 to 1.0.0-rc.12](dependabot-gitlab/dependabot@dedbfd2ef35465e4d43170e718359db059b33684) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2393
- [Bump @types/node from 20.5.9 to 20.6.0](dependabot-gitlab/dependabot@b5f25127f9eec3127446f35ae4c214fc46683858) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2392
- [Bump rubocop-rails from 2.20.2 to 2.21.0](dependabot-gitlab/dependabot@be08bd989eebd8e3e77ba866945a9fa696487a2d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2390

### 🧰 Maintenance (1 change)

- [Move sidekiq alive queue clearing to separate handler](dependabot-gitlab/dependabot@1627e4ed89e2d0f8dc587d1827607345feb2777a) by @andrcuns.

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 4.80.0 to 4.81.0 in /deploy](dependabot-gitlab/dependabot@5604f92a008e3ca08b73976291dd4a2b678ef528) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2391

## 3.2.0-alpha.1 (2023-09-09)

### 🚀 New features (1 change)

- [Add project search](dependabot-gitlab/dependabot@12a238f6a95734c5b99f3acac4129effbf44246a) by @andrcuns. See merge request dependabot-gitlab/dependabot!2384

### 🔬 Improvements (1 change)

- [Update UI texts](dependabot-gitlab/dependabot@37975c170497483e7e5f00cd6528f0249255f8ef) by @andrcuns. See merge request dependabot-gitlab/dependabot!2385

### 🐞 Bug Fixes (2 changes)

- [Correctly handle errors on closing obsolete mrs](dependabot-gitlab/dependabot@fde8bcd8e34734e1ad90eee0fc4a9660a6287e13) by @andrcuns.
- [Reimplement obsolete merge requests closing](dependabot-gitlab/dependabot@8bf9b1c7d16fa3cfe29ee8a82333fab42e7d951d) by @andrcuns.

### 📦 Dependency updates (1 change)

- [Bump dependabot-omnibus from 0.229.0 to 0.230.0](dependabot-gitlab/dependabot@dd0676695a95116046a4e2633f2a5de64443f790) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2386

### 📦🔧 Development dependency updates (1 change)

- [Bump rubocop-rspec from 2.23.2 to 2.24.0](dependabot-gitlab/dependabot@3bd100eba3cfccc4fdcdf71efcd5626f85ffb9ba) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2387

### 🧰 Maintenance (1 change)

- [Move sidekiq alive queue clearing to separate handler](dependabot-gitlab/dependabot@1627e4ed89e2d0f8dc587d1827607345feb2777a) by @andrcuns.

## 3.1.0-alpha.1 (2023-09-07)

### 🚀 New features (1 change)

- [Add pagination for projects page](dependabot-gitlab/dependabot@2c1221430642665c7939929b71bb51006a8214a2) by @andrcuns. See merge request dependabot-gitlab/dependabot!2380

### 📦 Dependency updates (3 changes)

- [Bump sidekiq_alive from 2.2.3 to 2.3.0](dependabot-gitlab/dependabot@869443117c076ee5be335a878394d16fad0695d1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2383
- [Update application version in docker-compose.yml](dependabot-gitlab/dependabot@806e6afd10bf361e76a908824ae97ebaa1e9a39d) by @BBboy01. See merge request dependabot-gitlab/dependabot!2379
- [Bump sentry-rails and sentry-sidekiq](dependabot-gitlab/dependabot@6cd8294f8ea2135e189d84db24db482ddea82157) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2381

### 📄 Documentation updates (1 change)

- [Update readme with information on release versioning](dependabot-gitlab/dependabot@4af2e5a2cecc418f70f2bd50c132b926b476e15e) by @andrcuns.

### 🚀 Deployment changes (1 change)

- [Add new_project path to ingress configuration](dependabot-gitlab/dependabot@ba14e3165eff0b42236c4135d158af5be65148e0) by @andrcuns.

## 3.0.0-alpha.1 (2023-09-06)

### 💥 Breaking changes (4 changes)

- [Use authorization for /metrics endpoint](dependabot-gitlab/dependabot@b3dc4deedecd20cb30e642319d05c719edf1f6e5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2377
- [Remove puma metrics and direct metrics store](dependabot-gitlab/dependabot@ad8c6cea677992ec4b3daa20d4a8a08a906b69aa) by @andrcuns. See merge request dependabot-gitlab/dependabot!2365
- [Overhaul prometheus metrics reporting](dependabot-gitlab/dependabot@c65c98b60b626709dca7b5efa69bac74fe143dea) by @andrcuns. See merge request dependabot-gitlab/dependabot!2362
- [Bump sidekiq from 6.5.8 to 7.1.2](dependabot-gitlab/dependabot@486aae128fc8c651cd9745227a3df1aed304c732) by @andrcuns. See merge request dependabot-gitlab/dependabot!2053

### 🐞 Bug Fixes (1 change)

- [Call webhook removal with correct arguments](dependabot-gitlab/dependabot@859ea2e5c5554db99c2ae7cd3cfdc4d39c8322a0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2357

### 📦 Dependency updates (4 changes)

- [Update dependency moby/moby to v24](dependabot-gitlab/dependabot@87c4053fe0c624d9f9154f3341fec8fc6bc6c888) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2370
- [Bump grape from 1.7.1 to 1.8.0](dependabot-gitlab/dependabot@09b41e2ac270e6e48bf0e30e9eb9f776f9e29cde) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2361
- [Bump dependabot-omnibus from 0.228.0 to 0.229.0](dependabot-gitlab/dependabot@2f3f014e502f620d291488049d695651437cd46f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2360
- [Bump redis from 5.0.6 to 5.0.7](dependabot-gitlab/dependabot@491654b80e093a809654f695cd4e8c559b71dc25) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2356

### 📦🔧 Development dependency updates (5 changes)

- [Bump prettier from 3.0.2 to 3.0.3](dependabot-gitlab/dependabot@68a0a7ea95dda522911c64a3bc17c2ccce692609) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2374
- [Bump @types/node from 20.5.7 to 20.5.9](dependabot-gitlab/dependabot@1edac3aac0669c059b3dfbd581cc13379a6f0f38) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2373
- [Bump rubocop from 1.56.1 to 1.56.2](dependabot-gitlab/dependabot@5d81e89dd854ce6d2bddb179dbb573e7f0dd3787) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2358
- [Bump vitepress from 1.0.0-rc.4 to 1.0.0-rc.10](dependabot-gitlab/dependabot@4826b317ed5f98e136c72723b60c498c99a3438c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2355
- [Bump @types/node from 20.5.1 to 20.5.7](dependabot-gitlab/dependabot@d680f212358366db080d54e108544d01e08b0fc1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2354

### 🔧 CI changes (5 changes)

- [Always start buildx builder](dependabot-gitlab/dependabot@44efd67419fcb1c49346a481a4fab8c391487e98) by @andrcuns. See merge request dependabot-gitlab/dependabot!2375
- [Update quay.io/containers/skopeo Docker tag to v1.13.2](dependabot-gitlab/dependabot@0c774e550675bfa1e59ecb282aaf0b6b992051b1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2368
- [Automatically manage dockerfile dependencies](dependabot-gitlab/dependabot@dd73c2693fde33878fcb847063395e23537b5136) by @andrcuns. See merge request dependabot-gitlab/dependabot!2367
- [Bump snyk image](dependabot-gitlab/dependabot@271e851d203b46f985d217f24138a35d375e0ae8) by @andrcuns.
- [Add collapsed env log output to e2e test job](dependabot-gitlab/dependabot@a79d8c22d49910a1eb0c0aebcbe75aa6d655012f) by @andrcuns.

### 🧰 Maintenance (4 changes)

- [Improve auth code for mounted rack apps](dependabot-gitlab/dependabot@1ca6b839e586ae56482217356acd94ab092650ca) by @andrcuns. See merge request dependabot-gitlab/dependabot!2378
- [Remove worker healthcheck exposing port](dependabot-gitlab/dependabot@bf65fadf3cfe3c9a2d47b93656eec5311dd4aad2) by @andrcuns. See merge request dependabot-gitlab/dependabot!2376
- [Fetch docker-compose from docker image](dependabot-gitlab/dependabot@b4ef0c72440f295eb067965557eb6d447bea0c9e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2372
- [Do not load metrics code in worker containers](dependabot-gitlab/dependabot@bfd9e70f490c01afae29973ed50a039f1ad73683) by @andrcuns. See merge request dependabot-gitlab/dependabot!2366

### 🚀 Deployment changes (2 changes)

- [Bump hashicorp/google from 4.79.0 to 4.80.0 in /deploy](dependabot-gitlab/dependabot@e23bc850ed161a620a888c8cf841223b6d2caa61) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2371
- [Enable metrics endpoint](dependabot-gitlab/dependabot@0faac6e6825bdee1130a08ae073399d2a4ca3992) by @andrcuns. See merge request dependabot-gitlab/dependabot!2363

## 2.0.0-alpha.4 (2023-08-27)

### ⚠️ Security updates (1 change)

- [[Security] Bump puma from 6.3.0 to 6.3.1](dependabot-gitlab/dependabot@f2c70886b72dd21b2faa4307fb7e93582dc82bf4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2321

### 🚀 New features (3 changes)

- [Support closing superseded mrs in standalone mode](dependabot-gitlab/dependabot@c608ad1c24a36498655cc1117b5e575d4ff2761a) by @andrcuns. See merge request dependabot-gitlab/dependabot!2341
- [Add severity labels to merge requests with vulnerabilities](dependabot-gitlab/dependabot@20cc817c02bc27dfe29ffcde9efd7f185121f308) by @andrcuns. See merge request dependabot-gitlab/dependabot!2324
- [Add support for vulnerability detection in standalone mode](dependabot-gitlab/dependabot@4708614bf3e2a0fcdd17160171a6b6488d046b2d) by @andrcuns. See merge request dependabot-gitlab/dependabot!2301

### 🔬 Improvements (3 changes)

- [Move background tasks to separate container](dependabot-gitlab/dependabot@8051ffe2067f5a581337c408847133baf03c0f4e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2334
- [Add support for sentry profiling functionality](dependabot-gitlab/dependabot@5351086967f4b34dcc0726f83cef3670c6baa7da) by @andrcuns. See merge request dependabot-gitlab/dependabot!2310
- [Log more details for processed vulnerability info](dependabot-gitlab/dependabot@a5b9684af413413e3a448483a13d7262e2fe6194) by @andrcuns. See merge request dependabot-gitlab/dependabot!2307

### 🐞 Bug Fixes (7 changes)

- [Only consider actual vulnerabilities when adding severity labels](dependabot-gitlab/dependabot@0e00fb9d0c7b679de81321fda2ac98c33063fa76) by @andrcuns. See merge request dependabot-gitlab/dependabot!2350
- [Remove obsolete merge request closing](dependabot-gitlab/dependabot@53386daf562781f97304892ae2a727fed492ea98) by @andrcuns. See merge request dependabot-gitlab/dependabot!2349
- [Cache labels already known to exist in project](dependabot-gitlab/dependabot@458c72997221afc72350e3448a41cc7eb992148f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2346
- [Correctly update cached project labels](dependabot-gitlab/dependabot@a771f1fd5456c84e1ee6d91cf14134c57050622e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2345
- [Correctly fetch source branch name for superseded mr in standalone mode](dependabot-gitlab/dependabot@856564f1da93b24b5b09c04a768f25aa510e3c63) by @andrcuns. See merge request dependabot-gitlab/dependabot!2343
- [Revert "Merge branch 'dependabot-bundler-dependabot-omnibus-and-faraday-retry-0.226.0' into 'main'"](dependabot-gitlab/dependabot@4f5186ec577dbbec3b4321e8048e793c6b93a89c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2319
- [Correctly handle label creation for vulnerability issues](dependabot-gitlab/dependabot@df92211e6188b1ffe6195239f359a8e41d8afeb9) by @andrcuns. See merge request dependabot-gitlab/dependabot!2308

### 📦 Dependency updates (6 changes)

- [Bump dependabot-omnibus and faraday-retry](dependabot-gitlab/dependabot@40904e6f2672a23b499e8828d25b76f25d7af839) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2342
- [Bump mongoid from 8.1.1 to 8.1.2](dependabot-gitlab/dependabot@698beeb6f41125fb0a47e7064b3f72b6cf8e0a43) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2339
- [Bump anyway_config from 2.5.1 to 2.5.2](dependabot-gitlab/dependabot@8c0cd9091da4e892dd1a66364aa0942ae695f832) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2337
- [Bump rails from 7.0.7 to 7.0.7.2](dependabot-gitlab/dependabot@e54b437d8c80f17cdbc102177541d6ae7c0362b2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2335
- [Bump dependabot-omnibus and faraday-retry](dependabot-gitlab/dependabot@bafd0a580f1fb9d2bdf0396bc46b147dfea6f333) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2316
- [Bump rails from 7.0.6 to 7.0.7](dependabot-gitlab/dependabot@ae9dc4e0d0b488b3acb2061e6d8a8129dcaa24c8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2315

### 📦🔧 Development dependency updates (16 changes)

- [Bump rspec-sidekiq from 4.0.1 to 4.0.2](dependabot-gitlab/dependabot@6e712fcd6775392d95992e92995da213c92ae669) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2347
- [Bump rspec-sidekiq from 4.0.0 to 4.0.1](dependabot-gitlab/dependabot@d0db69827f2f27d7a9ca19ac8993ea16b3595d3f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2338
- [Bump rubocop from 1.56.0 to 1.56.1](dependabot-gitlab/dependabot@a55d0bb6c5212b35575d7c96db0a9fc308bd7b67) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2336
- [Bump vitepress from 1.0.0-beta.7 to 1.0.0-rc.4](dependabot-gitlab/dependabot@52d7c98522f9ca289981709b4dbeb79f8e8e4099) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2328
- [Bump prettier from 3.0.1 to 3.0.2](dependabot-gitlab/dependabot@8b0edd3d7e6a90e2cadd8336d0fd3af5792dd07a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2327
- [Bump @types/node from 20.4.8 to 20.5.1](dependabot-gitlab/dependabot@647cc0864f90c0d4d0813dac581d25ad9ba94694) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2326
- [Bump @playwright/test from 1.36.2 to 1.37.1](dependabot-gitlab/dependabot@ca5ed39bb67f1fc1116aff04c4eee19b5dc27fec) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2325
- [Bump rspec-sidekiq from 3.1.0 to 4.0.0](dependabot-gitlab/dependabot@7d6b524638cea26b0038d00d29e3dce847a30de8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2322
- [Bump rubocop-performance from 1.18.0 to 1.19.0](dependabot-gitlab/dependabot@519883a8e418d1de0fb253cf80a13290f9836ee8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2323
- [Bump faker from 3.2.0 to 3.2.1](dependabot-gitlab/dependabot@d09ce218d856f00f1d79e79391e1e9134941ac03) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2318
- [Bump rubocop from 1.55.1 to 1.56.0](dependabot-gitlab/dependabot@e4de3cf577088c7467f766806eba48639eae5bcd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2313
- [Bump rubocop-rspec from 2.23.1 to 2.23.2](dependabot-gitlab/dependabot@4831419be142494f00c0914a6b26bdb6d3931965) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2314
- [Bump rubocop-rspec from 2.23.0 to 2.23.1](dependabot-gitlab/dependabot@7b194db3bd6334786f7900714de4f42a5a21346f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2305
- [Bump prettier from 3.0.0 to 3.0.1](dependabot-gitlab/dependabot@980b81f7de45f8fdfd753ea853274a5690a98aa4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2304
- [Bump handlebars from 4.7.7 to 4.7.8](dependabot-gitlab/dependabot@85474dbcaa445bb400c98593137fb5d337c651d8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2303
- [Bump @types/node from 20.4.5 to 20.4.8](dependabot-gitlab/dependabot@35d9fcf50e89b11bc645d09d3f8221f196303b2e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2302

### 🔧 CI changes (5 changes)

- [Use latest qemu emulators for image building](dependabot-gitlab/dependabot@2e13e9a681e76b69af01b2ffe1cd6f004542b7b3) by @andrcuns. See merge request dependabot-gitlab/dependabot!2344
- [Fix image build rules order](dependabot-gitlab/dependabot@fce639475f6efe6bb79b555583ebc1fb8c02173b) by @andrcuns.
- [Update rules for building images and running tests](dependabot-gitlab/dependabot@0cf01de972d40708290da54e05c25f8cac0405e0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2333
- [Introduce breaking changes changelog category](dependabot-gitlab/dependabot@80975c554aa563834723288f02608cfca0b81181) by @andrcuns. See merge request dependabot-gitlab/dependabot!2332
- [Remove legacy license scanning job](dependabot-gitlab/dependabot@ab68223ef4e5999fbc23035772fcbdddf96b6a58) by @andrcuns.

### 🧰 Maintenance (7 changes)

- [Improve logging for dependency mr creation process](dependabot-gitlab/dependabot@6e629a386c1f004a3cda44bd8dd7b278c0d350c5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2348
- [Use rails cache for project label caching](dependabot-gitlab/dependabot@034a26510aad47ac6724c704d2b6754d60d6d0c4) by @andrcuns. See merge request dependabot-gitlab/dependabot!2340
- [Use project dependencies for global setup in e2e tests](dependabot-gitlab/dependabot@1fbaf3e42cdbb627df7bb7d9213e5a7f41b83ee5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2331
- [Remove redundant rspec metadata](dependabot-gitlab/dependabot@8f26d85d6854d712156e981ea1909a545d73f143) by @andrcuns. See merge request dependabot-gitlab/dependabot!2309
- [Explicitly log when local vulnerability db is up to date](dependabot-gitlab/dependabot@ba3c05a7755e25e0217a96f20c73fe98fa8b9b56) by @andrcuns.
- [Add logger context for background migration job](dependabot-gitlab/dependabot@253227b3edeb47286852f5edd48ffb56fafa28ab) by @andrcuns. See merge request dependabot-gitlab/dependabot!2306
- [Refactor dependency vulnerability fetching](dependabot-gitlab/dependabot@2ee18be4fd191ff45576a9c23347f07390519904) by @andrcuns. See merge request dependabot-gitlab/dependabot!2298

### 📄 Documentation updates (3 changes)

- [Update documentation on main-latest docker tag usage](dependabot-gitlab/dependabot@8e53fd87757770a1399fd46d5270d62eb803bebe) by @andrcuns.
- [Document usage of images built from main branch](dependabot-gitlab/dependabot@4d21c4510b5a07777db10be28fc2796175143506) by @andrcuns.
- [Improve description of what dependabot-gitlab is or isn't](dependabot-gitlab/dependabot@5d814893120c7dde1b4c0bc1dcdb7d4ab30b122c) by @andrcuns.

### 🚀 Deployment changes (7 changes)

- [Reduce resource requests for backgroundTasksJob](dependabot-gitlab/dependabot@ce58f0ec657aca90625affeaf1b6ea913b09fe51) by @andrcuns.
- [Remove pullPolicy from deployment](dependabot-gitlab/dependabot@5cea10f33d8ba409916ca8bf7a151d285408f960) by @andrcuns.
- [Bump hashicorp/kubernetes from 2.22.0 to 2.23.0 in /deploy](dependabot-gitlab/dependabot@033df7c5ce480fe67ccbd6b35e6735075d6f82c2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2330
- [Bump hashicorp/google from 4.76.0 to 4.78.0 in /deploy](dependabot-gitlab/dependabot@17aaf2149c4c014ba1ca2886d41d38c89ddad84b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2329
- [Add sentry profiles and traces sample rates](dependabot-gitlab/dependabot@2235b723a1a86e777b02c855bd491323622ffc0c) by @andrcuns.
- [Ignore helm release metadata changes](dependabot-gitlab/dependabot@b51319e8206fea4df0322a6f70cc4da0f3695464) by @andrcuns.
- [Bump hashicorp/google from 4.75.1 to 4.76.0 in /deploy](dependabot-gitlab/dependabot@18336f72cf3a168085da12ad0387ceffd04a20e8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2300

## 2.0.0-alpha.3 (2023-08-05)

### 🐞 Bug Fixes (4 changes)

- [Correctly detect jobs to sync if job class name has changed](dependabot-gitlab/dependabot@861790915fd60ba933173e8c4b345fe99bf249d9) by @andrcuns. See merge request dependabot-gitlab/dependabot!2296
- [Move long running migrations to background job](dependabot-gitlab/dependabot@47e4120388dff38e2932faf6336cd8d836b5ba09) by @andrcuns. See merge request dependabot-gitlab/dependabot!2295
- [Create vulnerability issues in fork source](dependabot-gitlab/dependabot@6120f1ac30984909ae65a2265b843dd080653289) by @andrcuns. See merge request dependabot-gitlab/dependabot!2283
- [Cache labels only for specific project](dependabot-gitlab/dependabot@a52a451fbe18604579a332782d75033c0b48ff00) by @andrcuns. See merge request dependabot-gitlab/dependabot!2282

### 📦 Dependency updates (2 changes)

- [Bump anyway_config from 2.5.0 to 2.5.1](dependabot-gitlab/dependabot@02a75196ea9fe5c799a72cd6042a541c2432b4fe) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2297
- [Bump dependabot-omnibus from 0.224.0 to 0.225.0](dependabot-gitlab/dependabot@078fec51e3c719fb3e9d26a89e22759f490e7ff3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2294

### 📦🔧 Development dependency updates (6 changes)

- [Bump rubocop-rspec from 2.22.0 to 2.23.0](dependabot-gitlab/dependabot@834eb55ee4ea2088173322625fd006dd63b095ca) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2285
- [Bump rubocop from 1.55.0 to 1.55.1](dependabot-gitlab/dependabot@63e27354788dd9126a2ac03f835103d9f31bd5df) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2292
- [Bump @types/node from 20.4.4 to 20.4.5](dependabot-gitlab/dependabot@5bd68a93ee2e27ebec23c53a2c6aef0639703ad6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2289
- [Bump vitepress from 1.0.0-beta.6 to 1.0.0-beta.7](dependabot-gitlab/dependabot@8cc31599b2cf47d3c7d8258144127f4f6f4d8a5d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2291
- [Bump docker-compose from 0.24.1 to 0.24.2](dependabot-gitlab/dependabot@d1b2be9271b542815f6965ecead3f1032513aaad) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2290
- [Bump @playwright/test from 1.36.1 to 1.36.2](dependabot-gitlab/dependabot@34ed6a0bc20a2fdb539a0b4c446426154155b2d1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2288

### 🔧 CI changes (1 change)

- [Update quay.io/containers/skopeo Docker tag to v1.13.1](dependabot-gitlab/dependabot@d083ea018723c776d0b7534e6a893674c350e486) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2286

### 🧰 Maintenance (2 changes)

- [Initial code for grouping dependencies](dependabot-gitlab/dependabot@8d3d594e1604b9b15f580bdc459812db2ea3c399) by @andrcuns. See merge request dependabot-gitlab/dependabot!2293
- [Add support for groups key in configuration parser](dependabot-gitlab/dependabot@4a02eb96d9db4bdb2fce53be6dd97975f794b0ba) by @andrcuns. See merge request dependabot-gitlab/dependabot!2284

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 4.74.0 to 4.75.1 in /deploy](dependabot-gitlab/dependabot@2292b153492987e641b2b280b23aca6a51972d8d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2287

## 2.0.0-alpha.2 (2023-07-29)

### 🔬 Improvements (1 change)

- [Improve registry configuration validation and error logging](dependabot-gitlab/dependabot@5a4f34be3aa1ca63d7fecdfc6010ccb16c3c8a15) by @andrcuns. See merge request dependabot-gitlab/dependabot!2275

### 🐞 Bug Fixes (1 change)

- [Correctly persist log entry log level](dependabot-gitlab/dependabot@f4f1efb732737bddfc9e5874832499ad6077069f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2281

### 📦 Dependency updates (5 changes)

- [Bump dependabot-omnibus from 0.223.0 to 0.224.0](dependabot-gitlab/dependabot@6fdc909f5b9970f3761066fe8d8193f9a5e0c223) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2280
- [Bump lograge from 0.12.0 to 0.13.0](dependabot-gitlab/dependabot@f3e79fb075c62cdd885edea00516bb887f4ce7df) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2279
- [Bump dependabot-omnibus from 0.221.0 to 0.223.0](dependabot-gitlab/dependabot@9b220dbd9ce26488b3dc5ab97df92254d2079db9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2277
- [Bump anyway_config from 2.4.2 to 2.5.0](dependabot-gitlab/dependabot@3b0f822143f12d9f0fd7c107cd0968352236ba4b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2276
- [Bump sidekiq_alive from 2.2.2 to 2.2.3](dependabot-gitlab/dependabot@70abb972ed0f618be3dfac648d12ddeb341efd04) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2270

### 📦🔧 Development dependency updates (5 changes)

- [Bump rubocop from 1.54.2 to 1.55.0](dependabot-gitlab/dependabot@a9bc273acdfaedbfb5b01b72206d975bfafa49f5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2278
- [Bump vitepress from 1.0.0-beta.5 to 1.0.0-beta.6](dependabot-gitlab/dependabot@d280916632fc0a38d3823d2e2c74c3f9e7e86fdc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2274
- [Bump @types/node from 20.4.2 to 20.4.4](dependabot-gitlab/dependabot@088d7d5640869892e0b185dce10bdfe225926216) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2273
- [Bump @types/node from 20.4.1 to 20.4.2](dependabot-gitlab/dependabot@ebbff9e6e06ed9531c98531244366009cefdeaff) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2268
- [Bump @playwright/test from 1.35.1 to 1.36.1](dependabot-gitlab/dependabot@743a80f0c6b90a1c456d57417e95ada08a170df2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2267

### 🔧 CI changes (4 changes)

- [Update registry.gitlab.com/dependabot-gitlab/ci-images/buildkit Docker tag to v0.12](dependabot-gitlab/dependabot@3e399346b59b1daf042bdbb26fa11c1b31fb1883) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2271
- [Fix rules for compile-assets job](dependabot-gitlab/dependabot@aa2416331367a5bc4a80da7796a060b59d7be59a) by @andrcuns.
- [Add additional rules for running tests](dependabot-gitlab/dependabot@0597aae4866cde8f061fad0138047bcf59ee2b34) by @andrcuns. See merge request dependabot-gitlab/dependabot!2269
- [Update quay.io/containers/skopeo Docker tag to v1.13.0](dependabot-gitlab/dependabot@27aa22da2e7d39ac01ad0237db8e6f9217d5cefe) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2264

### 📄 Documentation updates (1 change)

- [Add link to notify release endpoint](dependabot-gitlab/dependabot@6583bc4bb8aaf3472c672c32fe73cf5d277234d6) by @andrcuns.

### 🚀 Deployment changes (3 changes)

- [Bump hashicorp/google from 4.73.1 to 4.74.0 in /deploy](dependabot-gitlab/dependabot@19dc279fdac6e16f0e6bf803d1c417d13956a4d5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2272
- [Bump hashicorp/kubernetes from 2.21.1 to 2.22.0 in /deploy](dependabot-gitlab/dependabot@bc92e915187fa4d5cfe2d4dc24084f51df3bcaf3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2266
- [Bump hashicorp/google from 4.72.1 to 4.73.1 in /deploy](dependabot-gitlab/dependabot@208763a37903383991ad9f9b28717fdc5d687f38) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2265

## 2.0.0-alpha.1 (2023-07-16)

### 🚀 New features (6 changes)

- [Add update runs api endpoints](dependabot-gitlab/dependabot@cd94aad84685c4b77b368b9c7eb2a477181cb76d) by @andrcuns. See merge request dependabot-gitlab/dependabot!2259
- [Add update_jobs api endpoint](dependabot-gitlab/dependabot@34bcceac4367a87ceb0f09cdf4b2b9162938d099) by @andrcuns. See merge request dependabot-gitlab/dependabot!2254
- [Add merge_requests api endpoint](dependabot-gitlab/dependabot@6d0aed5a52423a66f46625af0baa8f583796eade) by @andrcuns. See merge request dependabot-gitlab/dependabot!2253
- [Add ability to remove projects from UI](dependabot-gitlab/dependabot@34789d9d1c915c6dedcbee8b23687957c6537315) by @andrcuns. See merge request dependabot-gitlab/dependabot!2243
- [Add users api endpoint](dependabot-gitlab/dependabot@5e198cf1b160ad68f411ce20a754b037778c6b15) by @andrcuns. See merge request dependabot-gitlab/dependabot!2230
- [Add api v2 endpoints](dependabot-gitlab/dependabot@c70943b40cbbd97590e607caf6dec64fc96630f1) by @andrcuns. See merge request dependabot-gitlab/dependabot!2192

### 🔬 Improvements (4 changes)

- [Add pagination support for api v2 endpoints](dependabot-gitlab/dependabot@d7e73fc1f46f9b91a1486a90f646c6eee8eab974) by @andrcuns. See merge request dependabot-gitlab/dependabot!2252
- [Render inactive projects in a separate section](dependabot-gitlab/dependabot@e47c67ecad0b9efc7dee2e2297d273f1294bbd6a) by @andrcuns. See merge request dependabot-gitlab/dependabot!2245
- [Automatically generate api documentation and improve responses](dependabot-gitlab/dependabot@338ea1f6b6cb890a54ea140d6fcd1e4a34477e57) by @andrcuns. See merge request dependabot-gitlab/dependabot!2202
- [Refactor api implementation to use grape framework](dependabot-gitlab/dependabot@beb1762fd281bea7a54d288f3ea15d89cb8b415d) by @andrcuns. See merge request dependabot-gitlab/dependabot!2191

### 🐞 Bug Fixes (8 changes)

- [Create only log level index in migration](dependabot-gitlab/dependabot@627087a12ee571f78a8f9cd3612d4b2f560e2e7c) by @andrcuns.
- [Fix log entry log level migration](dependabot-gitlab/dependabot@0da9244ee3823805984e5a4ca2e514097ebdf1c4) by @andrcuns. See merge request dependabot-gitlab/dependabot!2260
- [Render all projects in projects page](dependabot-gitlab/dependabot@723514de6aeb53f532c69ba2e58fc5e1aaf5b2e5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2241
- [Use correct api token when removing project](dependabot-gitlab/dependabot@0d63b18aac902c319ab82ab741828dad95e00099) by @andrcuns. See merge request dependabot-gitlab/dependabot!2242
- [Delete webhook from gitlab instance when project is removed](dependabot-gitlab/dependabot@cf22f512223b2bf32ddd116d2a75ca4b4bd81654) by @andrcuns. See merge request dependabot-gitlab/dependabot!2240
- [Fix vulnerability and project registration cron job parameter updates](dependabot-gitlab/dependabot@2fc242cc52e5ce1ec85940a70e6c0654b2dda05f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2229
- [Fix discussion_id param type in api hook](dependabot-gitlab/dependabot@3b0ab64ce0446d0d6380572699aee0782c91c787) by @andrcuns. See merge request dependabot-gitlab/dependabot!2214
- [Fix api hooks parameter type definition](dependabot-gitlab/dependabot@463a2d75f58af62cfff31656334e4edf3c4c8770) by @andrcuns. See merge request dependabot-gitlab/dependabot!2207

### 📦 Dependency updates (12 changes)

- [Bump dependabot-omnibus from 0.220.0 to 0.221.0](dependabot-gitlab/dependabot@c24049f5d16e6b5fa9144a0b3b7022d15b645f5f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2256
- [Bump mongoid from 8.1.0 to 8.1.1](dependabot-gitlab/dependabot@380d8bb83e871551080121e60175789d2c68bf58) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2251
- [Bump sentry-rails from 5.9.0 to 5.10.0](dependabot-gitlab/dependabot@4d0f0ba7ffbf00361524f4cf7146491d82ba9d32) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2238
- [Bump mongoid from 8.0.4 to 8.1.0](dependabot-gitlab/dependabot@9a76296fb72a139bf03c3a6f919f213efe83392c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2232
- [Bump rails from 7.0.5.1 to 7.0.6](dependabot-gitlab/dependabot@a00ab8b005802e0bd7b965e4d9c2e32fb7f418c7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2231
- [Bump mongoid from 8.0.3 to 8.0.4](dependabot-gitlab/dependabot@40d5a599f1aa94c2114f08d43d1e704e2c646eee) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2228
- [Bump rails from 7.0.5 to 7.0.5.1](dependabot-gitlab/dependabot@2cc87d45ea0ff57ed15d0e0c84abfccd2acf6caa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2225
- [Bump bcrypt from 3.1.18 to 3.1.19](dependabot-gitlab/dependabot@479c995a117e17a97f526fcdb724c289b9b2d7bf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2220
- [Bump dependabot-omnibus from 0.218.0 to 0.220.0](dependabot-gitlab/dependabot@10e4a70421c891f45cbf96e08bd1e2da5b5a3cc5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2212
- [Bump dependabot-omnibus from 0.218.0 to 0.220.0](dependabot-gitlab/dependabot@5e9173f9d69703a0d6e5fa22ede31fe27adf356a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2204
- [Bump anyway_config from 2.4.1 to 2.4.2](dependabot-gitlab/dependabot@7f7949facded9b1b053b598275423676dd74feb2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2200
- [Bump puma from 6.2.2 to 6.3.0](dependabot-gitlab/dependabot@9bbcae35c5e07491f30cbf5b3059d44ede50ccf1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2193

### 📦🔧 Development dependency updates (23 changes)

- [Bump rubocop from 1.54.1 to 1.54.2](dependabot-gitlab/dependabot@5fd2d30d6a523d3103ff10fe58bb978d1473308e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2257
- [Bump grape-swagger-entity from 0.5.1 to 0.5.2](dependabot-gitlab/dependabot@d37fd7900d7c4f4a5449a825725b6102563c436b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2249
- [Bump prettier from 2.8.8 to 3.0.0](dependabot-gitlab/dependabot@d976c7e216e8b307bec2547ca1dc3aa1d89c2619) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2248
- [Bump @types/node from 20.3.3 to 20.4.1](dependabot-gitlab/dependabot@9021398d83c895cd74d076c12c449a71d7ec970b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2247
- [Bump rubocop from 1.54.0 to 1.54.1](dependabot-gitlab/dependabot@c83101d837c152575e8d2b0e12d34f59e0b07b39) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2237
- [Bump vitepress from 1.0.0-beta.3 to 1.0.0-beta.5](dependabot-gitlab/dependabot@3cd823439ad4d1ca854b497d1d4fa2138065e8f6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2236
- [Bump @types/node from 20.3.1 to 20.3.3](dependabot-gitlab/dependabot@455fbb4cfcf45997fe394461cd15212f2fe07a21) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2235
- [Bump rubocop from 1.53.1 to 1.54.0](dependabot-gitlab/dependabot@6e3288548a1eea0536d6a515d1d597bba2b59683) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2233
- [Bump rubocop from 1.53.0 to 1.53.1](dependabot-gitlab/dependabot@179ecdac2e48866a6091f55ebad4c295ec3943ac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2226
- [Bump vitepress from 1.0.0-beta.2 to 1.0.0-beta.3](dependabot-gitlab/dependabot@c242addb91f721634b425b08f47dc5ca4c8d858a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2224
- [Bump rubocop from 1.52.1 to 1.53.0](dependabot-gitlab/dependabot@e938fa7875fffd3a925d152055ce9c0c3ff97768) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2221
- [Bump rubocop-rails from 2.20.0 to 2.20.2](dependabot-gitlab/dependabot@b9b1c94b2bbabe95d74b07ad46218f93dab08262) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2219
- [Bump @types/node from 20.3.0 to 20.3.1](dependabot-gitlab/dependabot@2cf3190908dd176ef4d1ac3ec4879d32d2afce74) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2218
- [Bump @playwright/test from 1.35.0 to 1.35.1](dependabot-gitlab/dependabot@b662e4b9898ac808648e2dd84911bbefa2883879) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2217
- [Bump rubocop-rails from 2.19.1 to 2.20.0](dependabot-gitlab/dependabot@3336d49ca6795795b7ca17723b73793ad7da63fc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2216
- [Bump @types/node from 20.2.5 to 20.3.0](dependabot-gitlab/dependabot@8ccf5d18aa5414b6d9510c07ec777a8c9bfdc18d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2209
- [Bump vitepress from 1.0.0-beta.1 to 1.0.0-beta.2](dependabot-gitlab/dependabot@647c718a8bb8f68c67ed6e019033a9742c1c6044) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2210
- [Bump @playwright/test from 1.34.3 to 1.35.0](dependabot-gitlab/dependabot@3fcad89ede7251bc3f3f1ac7705f56ab03e74fc2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2208
- [Bump rubocop from 1.52.0 to 1.52.1](dependabot-gitlab/dependabot@af2489d3de6594064655c0e0dcd8620b1842fbcf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2205
- [Bump randomstring from 1.2.3 to 1.3.0](dependabot-gitlab/dependabot@de5a9aa2d0c52d71792cfbf78b0c4dcf8dcc0168) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2199
- [Bump rubocop from 1.51.0 to 1.52.0](dependabot-gitlab/dependabot@15cee933db33dbc498a672137a683c7c96bad428) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2195
- [Bump rspec-rails from 6.0.2 to 6.0.3](dependabot-gitlab/dependabot@902a982ada014ffe8995deaf48acd5d40d3ae95e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2194
- [Bump @types/node from 20.2.1 to 20.2.5](dependabot-gitlab/dependabot@0f9518a2d737650c3df8b7e082d5f1e8e6026c2c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2190

### 🔧 CI changes (4 changes)

- [Add missing docs base url for main pipeline runs](dependabot-gitlab/dependabot@76dc04fc8376ebb4cdb2e72bc5f11688d3bc5e42) by @andrcuns. See merge request dependabot-gitlab/dependabot!2258
- [Add link to documentation site in merge request UI](dependabot-gitlab/dependabot@400e4d589102a64098837452b2edf90dfcdf6279) by @andrcuns. See merge request dependabot-gitlab/dependabot!2255
- [Use latest-main tag to not overlap with release images](dependabot-gitlab/dependabot@b12861c4968ba9082fd7c1bf1c672ff86ef711b5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2189
- [Skip ecosystem builds in merge_train and simplify bundle install](dependabot-gitlab/dependabot@a93c97ebf30faf07914e49c244bb4bb02885281e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2186

### 🧰 Maintenance (11 changes)

- [Simplify model pagination in api implementation](dependabot-gitlab/dependabot@58d37b0df922bf8e363463eef8ee3b703e420c35) by @andrcuns. See merge request dependabot-gitlab/dependabot!2263
- [Process log entry and run status migration in batches](dependabot-gitlab/dependabot@0b5658c2c47eb6d19f2417ffc93432dbf72aad92) by @andrcuns.
- [Remove support for redundant rebase command](dependabot-gitlab/dependabot@bf43a327365d5ac313fc86b1d9c84f5ca3102fb5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2227
- [Fix devcontainer setup](dependabot-gitlab/dependabot@d6b6c339136dc8a3d88cd992a34b66b7f7d3c760) by @andrcuns. See merge request dependabot-gitlab/dependabot!2222
- [Remove async parsing of swagger doc](dependabot-gitlab/dependabot@177e53a736ea55d76917694200827015104ef036) by @andrcuns. See merge request dependabot-gitlab/dependabot!2213
- [Use v2 api when creating webhooks for projects](dependabot-gitlab/dependabot@6befe5a70994ee3cb89c669116f2f5474a5e89e5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2201
- [Add ui test for adding new project](dependabot-gitlab/dependabot@94d52f521feb7aa6247c0a8dbc8d94aee2e246d6) by @andrcuns. See merge request dependabot-gitlab/dependabot!2188
- [Add page objects to e2e tests](dependabot-gitlab/dependabot@52872aeec7702757316bc28e2cca4911cf472024) by @andrcuns. See merge request dependabot-gitlab/dependabot!2187
- [Remove capybara dependency](dependabot-gitlab/dependabot@7917684d8abb403bdc0d7159a62c08c298f1eb5b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2185
- [Make mock definitions in tests template-able](dependabot-gitlab/dependabot@e465c4218f332574f47abea3803e666dcfa6333c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2185
- [Use playwright for E2E testing](dependabot-gitlab/dependabot@806917c7f4e4a93c123a703aece4c49eb3615c98) by @andrcuns. See merge request dependabot-gitlab/dependabot!2184

### 📄 Documentation updates (1 change)

- [Improve documentation structure](dependabot-gitlab/dependabot@e1a2426ccd9ad03db318d0f0dbcbff00710e488f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2262

### 🚀 Deployment changes (11 changes)

- [Increase worker memory](dependabot-gitlab/dependabot@e20a2bbbd3318ead458897a4a413aa98ed3bf012) by @andrcuns.
- [Increase migration container deadline activeDeadlineSeconds](dependabot-gitlab/dependabot@0e2a14cf6718b48895119b1787d88694d5706687) by @andrcuns.
- [Wrap sensitive values using sensitive function](dependabot-gitlab/dependabot@90d866ba1db853d16f1ea5b296341c2da5402387) by @andrcuns. See merge request dependabot-gitlab/dependabot!2250
- [Bump hashicorp/google from 4.71.0 to 4.72.1 in /deploy](dependabot-gitlab/dependabot@102ab2a8a86b4aefd17309dba207ba90afae4b69) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2246
- [Bump hashicorp/google from 4.70.0 to 4.71.0 in /deploy](dependabot-gitlab/dependabot@c5f99a7a443a96b5c6d3d4a51dbd45f5ed7d4674) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2234
- [Bump hashicorp/google from 4.69.1 to 4.70.0 in /deploy](dependabot-gitlab/dependabot@e65ff2f6ad497bfccf9b7f4128959d36482794a5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2223
- [Bump hashicorp/google from 4.68.0 to 4.69.1 in /deploy](dependabot-gitlab/dependabot@17a0f6bcb4bc850d409362a1d6fd43f8c85ea728) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2215
- [Bump hashicorp/google from 4.67.0 to 4.68.0 in /deploy](dependabot-gitlab/dependabot@4b2d7d05f483c456f118ac0d2d74df9ec1c74272) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2203
- [Bump hashicorp/kubernetes from 2.20.0 to 2.21.1 in /deploy](dependabot-gitlab/dependabot@23c9d7462df403d052662686c8e8c64cfc797a26) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2198
- [Bump hashicorp/helm from 2.9.0 to 2.10.1 in /deploy](dependabot-gitlab/dependabot@1edae61012d8b926a282a4fad4fd9b2bd373500a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2197
- [Bump hashicorp/google from 4.66.0 to 4.67.0 in /deploy](dependabot-gitlab/dependabot@12471fffd61c7858e69287b1958c38b5a01c18e4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2196

## 1.0.0-alpha.10 (2023-07-15)

### 🧰 Maintenance (1 change)

- [Fix db seed script](dependabot-gitlab/dependabot@d4d803c7df49e48d72bb3bdfcbcc6149d3bd6104) by @andrcuns.

## 1.0.0-alpha.9 (2023-05-27)

### 🔬 Improvements (1 change)

- [Add ability to configure redis key ttl for worker healthcheck](dependabot-gitlab/dependabot@dd4ad21d2c6f9660e68bb5bdc86aa1a969d3ef5b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2181

### 📦 Dependency updates (2 changes)

- [Bump sidekiq_alive from 2.2.1 to 2.2.2](dependabot-gitlab/dependabot@2e79c6bdc286dbcd27008ae3b06e57491a5c5f77) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2177
- [Bump rails from 7.0.4.3 to 7.0.5](dependabot-gitlab/dependabot@c6862c3658e2e3aed4aeeae746df0c4e6cc3841d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2176

### 🔧 CI changes (6 changes)

- [Remove duplicate manual definition of deploy job](dependabot-gitlab/dependabot@5492d023b3326e00b796a579d730c08883946d56) by @andrcuns. See merge request dependabot-gitlab/dependabot!2181
- [Add deploy child pipeline only when images are built](dependabot-gitlab/dependabot@8ef87d97a06e602f5e626210becc9104455aa842) by @andrcuns. See merge request dependabot-gitlab/dependabot!2182
- [[BREAKING] Remove image publishing to gitlab container registry](dependabot-gitlab/dependabot@e578294ff3b9d62341585d3b536f3974d2f4f213) by @andrcuns.
- [Remove dependency on application image for migration test](dependabot-gitlab/dependabot@1c251d8256094a741d516ca37cf1e1930c6a2fd4) by @andrcuns. See merge request dependabot-gitlab/dependabot!2175
- [Update core base image location](dependabot-gitlab/dependabot@578f80a65ebe19c8fc758f02a0c450cc2deea5d0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2174
- [Build arm images only for release](dependabot-gitlab/dependabot@2e33216ad8a1fae2e729db6bc7b9651edbed9760) by @andrcuns.

### 🧰 Maintenance (3 changes)

- [Reduce amount of specific sidekiq queues](dependabot-gitlab/dependabot@b0feda6185db8c037d89a03574dc036cc1f695b2) by @andrcuns. See merge request dependabot-gitlab/dependabot!2181
- [Add e2e tests](dependabot-gitlab/dependabot@eb3d7ffddbf586fb41dcee1c517475f31d28f280) by @andrcuns. See merge request dependabot-gitlab/dependabot!2178
- [Revert transition to debug gem](dependabot-gitlab/dependabot@41644e3e08b2ca6a9c201c4ccc3aaf61a18e3365) by @andrcuns. See merge request dependabot-gitlab/dependabot!2179

### 🚀 Deployment changes (3 changes)

- [Use separate set block for imagePattern value](dependabot-gitlab/dependabot@3a5f1dc4975508b8c4a554351df0de8712177102) by @andrcuns. See merge request dependabot-gitlab/dependabot!2183
- [Fix incorrect image name for deploy pipeline](dependabot-gitlab/dependabot@d329e2778fd8c93e36f8d699fe1a64ba57931b1c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2180
- [Bump hashicorp/google from 4.65.2 to 4.66.0 in /deploy](dependabot-gitlab/dependabot@2993a377561dd8399d61dd0be0fd4cc19f2afdf6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2172

## 1.0.0-alpha.8 (2023-05-23)

### 📦 Dependency updates (1 change)

- [Bump dependabot-omnibus from 0.217.0 to 0.218.0](dependabot-gitlab/dependabot@0d89151cd28cc26c71c344703839e494c70ef62c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2171

### 📦🔧 Development dependency updates (2 changes)

- [Bump vitepress from 1.0.0-alpha.76 to 1.0.0-beta.1](dependabot-gitlab/dependabot@3be036235344f35740a2f0cb9bf9a4a78e997690) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2170
- [Bump rubocop-performance from 1.17.1 to 1.18.0](dependabot-gitlab/dependabot@5d556f5c8b78153b6f251fa0be64ea48f1e4c421) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2168

### 🔧 CI changes (3 changes)

- [Build arm images only for release](dependabot-gitlab/dependabot@2e33216ad8a1fae2e729db6bc7b9651edbed9760) by @andrcuns.
- [Fix rules for container and license scan jobs](dependabot-gitlab/dependabot@e5136e9cd4027fd509d540348b6241226abb1768) by @andrcuns. See merge request dependabot-gitlab/dependabot!2169
- [Update registry.gitlab.com/dependabot-gitlab/ci-images/node Docker tag to v20](dependabot-gitlab/dependabot@5d51537a7209652ffc827c7156de04e40f1ce7cc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2167

## 1.0.0-alpha.7 (2023-05-21)

### 🐞 Bug Fixes (2 changes)

- [Fix missing arm release images](dependabot-gitlab/dependabot@ca3c86815120e541a210468b7f7f96c02c051bb2) by @andrcuns. See merge request dependabot-gitlab/dependabot!2156
- [Fix project configuration sync from UI](dependabot-gitlab/dependabot@480458f4b4be6e8d6847e36c5741dce9ee35c5ad) by @andrcuns. See merge request dependabot-gitlab/dependabot!2152

### 📦 Dependency updates (1 change)

- [Update docker Docker tag in docker-compose.yml to v24](dependabot-gitlab/dependabot@cfd8944b4ee611387ce42d0edaaf61b0518899c9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2166

### 🔧 CI changes (10 changes)

- [Use skopeo image for image release job](dependabot-gitlab/dependabot@0fd8f6b0aaa30649f6d5f39252355addb922fd4c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2164
- [Skip migration test for non-db changes](dependabot-gitlab/dependabot@29bf26964f9a41e43aa62d85596f2a524187382b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2161
- [Update dependency codacy/codacy-coverage-reporter to v13.13.2](dependabot-gitlab/dependabot@74635d1b814d66182205196d1a50213ddf406d80) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2163
- [Update thiht/smocker Docker tag to v0.18.2](dependabot-gitlab/dependabot@2ce25e5f9cafe32f372ef60a27a20527620f387a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2162
- [Update CI docker images and improve auto version management](dependabot-gitlab/dependabot@b6c7588e7e3c8bcd796b1b9d968b5400a7bced8f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2160
- [Don't run terraform static checks on app code changes](dependabot-gitlab/dependabot@8a0f051ddb673e2972116408f9011717b0520509) by @andrcuns. See merge request dependabot-gitlab/dependabot!2157
- [Only build arm images in release pipelines](dependabot-gitlab/dependabot@506752910894189a8bd84fd7d532097ad136af8b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2155
- [Update ci images](dependabot-gitlab/dependabot@a78c72d98055cad0f10dbb445c832ce481c05ead) by @andrcuns. See merge request dependabot-gitlab/dependabot!2154
- [Only build all ecosystem images on dependency changes](dependabot-gitlab/dependabot@e7d467666c544353c1fa7d6e5e1661839fe2364e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2153
- [Improve skopeo command output in release job](dependabot-gitlab/dependabot@0259e555139471207ec00df53edba77b07be3a84) by @andrcuns.

### 🧰 Maintenance (3 changes)

- [Manage docker-compose.yml service image versions with renovate](dependabot-gitlab/dependabot@51597d27f6c93f824d2352a24092df5689121452) by @andrcuns.
- [Pass REDIS_URL explicitly to sidekiq and filter out empty password entry](dependabot-gitlab/dependabot@bee648f0df697809c3be7fdc4943cd485ac1d59b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2159
- [Do not fetch configuration twice in projects view](dependabot-gitlab/dependabot@dd44f4e19dc34774c450c50ecdae6b1bb571f118) by @andrcuns. See merge request dependabot-gitlab/dependabot!2158

## 1.0.0-alpha.6 (2023-05-18)

### 🐞 Bug Fixes (1 change)

- [Always pull updater image with default compose setup](dependabot-gitlab/dependabot@e850e5f2f23edfdb30fd9f53e20bf91e1ce0de6c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2143

### 📦 Dependency updates (1 change)

- [Bump sidekiq_alive from 2.2.0 to 2.2.1](dependabot-gitlab/dependabot@cd65e8a58f20500f86fc809972e9e2fa59099c84) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2141

### 📦🔧 Development dependency updates (2 changes)

- [Bump vitepress from 1.0.0-alpha.75 to 1.0.0-alpha.76](dependabot-gitlab/dependabot@ea04f14f2cfb898e96755c0dc63277d589096227) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2151
- [Bump @types/node from 20.1.0 to 20.2.1](dependabot-gitlab/dependabot@14012649b3f0880718cf5472c44641f261571810) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2150

### 🔧 CI changes (5 changes)

- [Reduce amount of job running in merge requests](dependabot-gitlab/dependabot@9ae98d4ed93b8d481a50eb1b153b88b1e2561240) by @andrcuns. See merge request dependabot-gitlab/dependabot!2149
- [Switch to skopeo for image release](dependabot-gitlab/dependabot@20364e1af30815e92909ec5f05e2935315d8d3b9) by @andrcuns. See merge request dependabot-gitlab/dependabot!2148
- [Set expiry for all artifacts created by CI jobs](dependabot-gitlab/dependabot@d6e4b6189d0d2afe23a8811f325e6293d2a08a19) by @andrcuns. See merge request dependabot-gitlab/dependabot!2147
- [Fix deployment from web triggered pipeline](dependabot-gitlab/dependabot@b935bd1bb5698df5e90fa400449e5b4d11923174) by @andrcuns.
- [Use child pipeline for application deployment](dependabot-gitlab/dependabot@ad3d9016b0756ad9e64f6eeb03a0806cae35f3b2) by @andrcuns. See merge request dependabot-gitlab/dependabot!2139

### 🧰 Maintenance (1 change)

- [Replace pry with debug gem](dependabot-gitlab/dependabot@920337d3e9132676d17165c4ac8b24f459e3e254) by @andrcuns. See merge request dependabot-gitlab/dependabot!2145

### 📄 Documentation updates (2 changes)

- [Use explicit app version in documentation and compose file](dependabot-gitlab/dependabot@f4d69043d89dbbb6fb543c1bba13e4ad242ed154) by @andrcuns. See merge request dependabot-gitlab/dependabot!2146
- [Add docker images list to main README](dependabot-gitlab/dependabot@925897eeac9412924bb8c22a7402e537ab3a283c) by @andrcuns.

### 🚀 Deployment changes (1 change)

- [Bump hashicorp/google from 4.64.0 to 4.65.2 in /deploy](dependabot-gitlab/dependabot@6e912074cbbec78979334f341ac8d625076dcdb5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2144

## 1.0.0-alpha.5 (2023-05-14)

### 📄 Documentation updates (1 change)

- [Update documentation on update rake task](dependabot-gitlab/dependabot@ab214e600410aeac5c93df04f3d8b5af1d43eaf4) by @andrcuns.

## 1.0.0-alpha.4 (2023-05-14)

### 🔬 Improvements (5 changes)

- [Log existing vulnerability issue url](dependabot-gitlab/dependabot@5b21f029e51f5a676349bfcbf921966c28852276) by @andrcuns. See merge request dependabot-gitlab/dependabot!2138
- [Log explicit reason for skipping merge request update](dependabot-gitlab/dependabot@38b64ac40689d9cc827c3307ccb318916ba5bd9c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2121
- [Raise error and add reply on comment actions for unmanaged merge requests](dependabot-gitlab/dependabot@5ff7030a3915d428c57d4dcb8bc0a7e9dd741f3a) by @andrcuns. See merge request dependabot-gitlab/dependabot!2119
- [Add response messages for unmatched note commands](dependabot-gitlab/dependabot@09f187939f7b2dbac4ef75b1d260de6c949728b1) by @andrcuns. See merge request dependabot-gitlab/dependabot!2118
- [Truncate error messages in UI](dependabot-gitlab/dependabot@dd40250b9454a7a0adf3f211f710c9307d763807) by @andrcuns. See merge request dependabot-gitlab/dependabot!2115

### 🐞 Bug Fixes (9 changes)

- [Fix private hex repo registry implementation](dependabot-gitlab/dependabot@744acb7a82c049f788f81df435fdf6d418862cbd) by @andrcuns. See merge request dependabot-gitlab/dependabot!2132
- [Fix notify release endpoint](dependabot-gitlab/dependabot@6ec53125180f2eadc0f79a42d9c6fe5836c1bbb0) by @andrcuns. See merge request dependabot-gitlab/dependabot!2131
- [Fix non scheduled mr update workflows](dependabot-gitlab/dependabot@e7f12b3d7595e53674e217e5ccccc5535ef7bb8d) by @andrcuns. See merge request dependabot-gitlab/dependabot!2127
- [Add missing error message if recreate mr fails](dependabot-gitlab/dependabot@6b08fb9a7096f9eff4b182b25b10a69ceaad9726) by @andrcuns. See merge request dependabot-gitlab/dependabot!2126
- [Fix logging indentation for mr update service](dependabot-gitlab/dependabot@8646ea956e7364057ebee73cdd1b09f3d1bb5531) by @andrcuns. See merge request dependabot-gitlab/dependabot!2124
- [Correctly handle failures during dependency updates](dependabot-gitlab/dependabot@db157d6c1ed2257903fd960e2c83e1271553fee3) by @andrcuns. See merge request dependabot-gitlab/dependabot!2123
- [Allow replaces-base key in maven-repository registry](dependabot-gitlab/dependabot@ac18cb156e45f9a85b12c5722f3bf85fbf2dea0b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2116
- [Add all mising option definitions to docker-compose.yml](dependabot-gitlab/dependabot@8d8f5f8aa71abc45bbf52a80cd69c70e7f3945e6) by @andrcuns. See merge request dependabot-gitlab/dependabot!2112
- [Do not try to fetch default branch before initializing gitlab client](dependabot-gitlab/dependabot@741263629b364b55de165ab2a0adb6eae4ab5e20) by @andrcuns. See merge request dependabot-gitlab/dependabot!2110

### 📦 Dependency updates (1 change)

- [Bump sidekiq-cron from 1.10.0 to 1.10.1](dependabot-gitlab/dependabot@388aa84e17c5ea4a9eff06137878b994f453b8c3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2128

### 📦🔧 Development dependency updates (5 changes)

- [Bump rubocop from 1.50.2 to 1.51.0](dependabot-gitlab/dependabot@26cc913c76ee76a9cac22be5cac32206de2427e9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2133
- [Bump capybara from 3.39.0 to 3.39.1](dependabot-gitlab/dependabot@061d756cb6b1c4ef7342e738e50d6420fc3c4ef8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2130
- [Bump vue from 3.2.47 to 3.3.1](dependabot-gitlab/dependabot@a02ac30c15329249b28bb9105e083db1394ccebf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2129
- [Bump rubocop-rspec from 2.21.0 to 2.22.0](dependabot-gitlab/dependabot@a910784e33d8e6a355c5d5107daaeb39144fc122) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2114
- [Bump @types/node from 20.0.0 to 20.1.0](dependabot-gitlab/dependabot@f824fcdb22a7950a21f6c93df463d48ac59d564b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2111

### 🔧 CI changes (1 change)

- [Add ability to force arm and ecosystem builds in mr pipelines](dependabot-gitlab/dependabot@ac22b24cb1f8f96b70d2ad0d7c71152ebc41d50d) by @andrcuns. See merge request dependabot-gitlab/dependabot!2117

### 🧰 Maintenance (7 changes)

- [Deprecate rebase comment command](dependabot-gitlab/dependabot@d893c07a1e9b8f8728201af00f1b05bb04dfe68e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2137
- [Improve version bump handling](dependabot-gitlab/dependabot@ad692770fc03482a7e930233474a03eef5335397) by @andrcuns. See merge request dependabot-gitlab/dependabot!2136
- [Allow stacking run contexts](dependabot-gitlab/dependabot@70257f6751e0332c37cc5f98f5d72f04cde63388) by @andrcuns. See merge request dependabot-gitlab/dependabot!2135
- [Add error message at the end of failed update run](dependabot-gitlab/dependabot@5aa068b849df9a9ed3b11a014c65d5b228e900b1) by @andrcuns.
- [Improve log tags for notify-release job](dependabot-gitlab/dependabot@0db88001694aff4435b87dc94668c24909ca7d42) by @andrcuns. See merge request dependabot-gitlab/dependabot!2134
- [Refactor update service and improve logging messages](dependabot-gitlab/dependabot@76f587aa766436fb0f466c97322266a2db30932c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2125
- [Show all truncated failures of one single run](dependabot-gitlab/dependabot@798f06e66774638ef899d028d21727590a4a346f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2122

### 📄 Documentation updates (2 changes)

- [Document limitations for gitlab maven repository authentication](dependabot-gitlab/dependabot@3ee261e9d3b4a992ba0e42e1e5c03da12a6a8140) by @andrcuns. See merge request dependabot-gitlab/dependabot!2120
- [Add instructions on how to use custom compose file for deployment](dependabot-gitlab/dependabot@d364bf1672b194e88273a62b84eb6d9bf2b36654) by @andrcuns. See merge request dependabot-gitlab/dependabot!2113

### main (1 change)

- [Add missing log message for project registration start](dependabot-gitlab/dependabot@ec9791f0550fe401aef5862104fef3c200cdf2a0) by @andrcuns.

## 1.0.0-alpha.3 (2023-05-06)

### ⚠️ Security updates (2 changes)

- [[Security] Bump commonmarker from 0.23.8 to 0.23.9](dependabot-gitlab/dependabot@5a65f9888a1e93a5f0829f700c2802fb5802a796) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2082
- [[Security] Bump nokogiri from 1.14.2 to 1.14.3](dependabot-gitlab/dependabot@2089eb206c8576ebe6e2c80945454e5900ce8846) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2083

### 🚀 New features (1 change)

- [Add support for hex-repository registry type](dependabot-gitlab/dependabot@ba9efb898100acb26e3872f69712af02017756ac) by @andrcuns. See merge request dependabot-gitlab/dependabot!2074

### 🐞 Bug Fixes (1 change)

- [Fix typo in merge request recreate message](dependabot-gitlab/dependabot@233b5e40c801de9a3283ea90c3f1ff18de8101b9) by @networkException. See merge request dependabot-gitlab/dependabot!2075

### 📦 Dependency updates (6 changes)

- [Bump dependabot-omnibus from `e7ceff1` to `0.217.0`](dependabot-gitlab/dependabot@d68ecef6ba3d3bb369b6d3b40ed5a5fa2f6fc135) by @andrcuns. See merge request dependabot-gitlab/dependabot!2109
- [Bump anyway_config from 2.4.0 to 2.4.1](dependabot-gitlab/dependabot@3d2c67b48b91a5d63bf4ca12d929761745cc3266) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2103
- [Bump dependabot-omnibus from `e7ceff1` to `f411621`](dependabot-gitlab/dependabot@55d1d61e15b3cefddb5405b65ac14df8b25c16a4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2102
- [Bump sentry-rails from 5.8.0 to 5.9.0](dependabot-gitlab/dependabot@1fbcfe5dbaf0794226c44f51f0f57a960822f7ad) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2095
- [Bump puma from 6.2.1 to 6.2.2](dependabot-gitlab/dependabot@38e726e407da1580eaa947b7f859e426da3d5030) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2091
- [Bump anyway_config from 2.3.1 to 2.4.0](dependabot-gitlab/dependabot@09708023089a1bce9dc7523e35abcd98332a0a0f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2073
- [dep: Update docker Docker tag to v23.0.3](dependabot-gitlab/dependabot@ba04c9ec05fc874ac1c210051f4795d34f645193) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2072

### 📦🔧 Development dependency updates (19 changes)

- [Bump rubocop-rspec from 2.20.0 to 2.21.0](dependabot-gitlab/dependabot@13cb09ba19ad858e183a6506f1e5eecb43784815) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2106
- [Bump @types/node from 18.16.0 to 20.0.0](dependabot-gitlab/dependabot@fd84c21a6c6b7b1f8e06fed29a9865c8820194be) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2105
- [Bump rspec-rails from 6.0.1 to 6.0.2](dependabot-gitlab/dependabot@e2d9ea89ad9610ca71480fc1281a054565cf87fa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2104
- [Bump vitepress from 1.0.0-alpha.74 to 1.0.0-alpha.75](dependabot-gitlab/dependabot@6fdcd268bd72371e763fb1bf08c1595b46eafd6c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2101
- [Bump vitepress from 1.0.0-alpha.73 to 1.0.0-alpha.74](dependabot-gitlab/dependabot@2de22917f12d71a3fdcb3f5477d8c3c5ccc44cf4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2099
- [Bump @types/node from 18.15.0 to 18.16.0](dependabot-gitlab/dependabot@9ca6754b8e7de3766708ac8c5e9f76257ed439b1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2098
- [Bump vitepress from 1.0.0-alpha.72 to 1.0.0-alpha.73](dependabot-gitlab/dependabot@307f551a773a676c3a74b6bd740223d7de6f519c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2097
- [Bump rubocop-rspec from 2.19.0 to 2.20.0](dependabot-gitlab/dependabot@4d227709c708d4f0511f60168b892d8f36dbff65) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2094
- [Bump rubocop from 1.50.1 to 1.50.2](dependabot-gitlab/dependabot@697d018eddbabcf82e4a6e835859735f237815d2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2092
- [Bump vitepress from 1.0.0-alpha.70 to 1.0.0-alpha.72](dependabot-gitlab/dependabot@14d35afa41b8676870a8aaff3418ec461bf87f78) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2088
- [Bump rubocop-rails from 2.19.0 to 2.19.1](dependabot-gitlab/dependabot@c2698936d246751f6cc6ae4cd21925be5d0eca4b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2087
- [Bump vitepress from 1.0.0-alpha.66 to 1.0.0-alpha.70](dependabot-gitlab/dependabot@392bed8df360310735fc92a7baa487e9dd22e248) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2086
- [Bump faker from 3.1.1 to 3.2.0](dependabot-gitlab/dependabot@134783a5ca9d417da67d478cad22a7c9e8c3ccfe) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2085
- [Bump vitepress from 1.0.0-alpha.65 to 1.0.0-alpha.66](dependabot-gitlab/dependabot@d04828f2557368536da09a44a9ccfd7b94ed2614) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2084
- [Bump rubocop from 1.50.0 to 1.50.1](dependabot-gitlab/dependabot@fd1f54f14453172dbbfb4aebafe42cf6c016460c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2081
- [Bump rubocop from 1.49.0 to 1.50.0](dependabot-gitlab/dependabot@64bd72e2cc616c5bafbed59adee5c4060aa91bf5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2080
- [Bump solargraph from 0.48.0 to 0.49.0](dependabot-gitlab/dependabot@0ad6b334344b8a17c758bfd188985357d3804af8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2078
- [Bump rubocop-performance from 1.16.0 to 1.17.1](dependabot-gitlab/dependabot@9dee67b806d8f7cf9d8b5ee283682c9a2a64eccc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2077
- [Bump rubocop-rails from 2.18.0 to 2.19.0](dependabot-gitlab/dependabot@526322554134e4c3e2468a486995c9c6bd628b60) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2076

### 🔧 CI changes (2 changes)

- [Sync dind and main docker versions](dependabot-gitlab/dependabot@c0207b85f07df2acdcbfb0d47e45cc34f9cafb7f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2108
- [Fix changelog generation between 2 alpha releases](dependabot-gitlab/dependabot@1bd79fea221c0e4a5e14eae49a750a6338d10b67) by @andrcuns.

### 🧰 Maintenance (2 changes)

- [Bump ruby dev version to 3.1.4](dependabot-gitlab/dependabot@924b20b205786ec52481cf2286aca0f06ab84bc5) by @andrcuns. See merge request dependabot-gitlab/dependabot!2107

## v1.0.0-alpha.2 (2023-04-05)

### 🚀 New features (1 change)

- [Add option to unsubscribe from created merge request](dependabot-gitlab/dependabot@5cf96ceeb2facb5fa9ca84203f4bce2c49134f81) by @andrcuns. See merge request dependabot-gitlab/dependabot!2054

### 📦 Dependency updates (7 changes)

- [Bump dependabot-omnibus from `fb3cff4` to `e7ceff1`](dependabot-gitlab/dependabot@640edbe4705d827ee718472053996797f146ba4e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2066
- [Bump puma from 6.2.0 to 6.2.1](dependabot-gitlab/dependabot@78726f369f9a1a38a119f1a7efaab297dc79f626) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2065
- [Bump tzinfo-data from 1.2023.2 to 1.2023.3](dependabot-gitlab/dependabot@434154a5cbe3509e6be4443278af020b05fc6d1b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2064
- [Bump puma from 6.1.1 to 6.2.0](dependabot-gitlab/dependabot@06554912fd5029f7da1d2ef82330c7c6011a4fff) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2063
- [Bump dependabot-omnibus from `0b297e3` to `fb3cff4`](dependabot-gitlab/dependabot@42d092718c0e011bd22b67951291a196926880a0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2059
- [Bump tzinfo-data from 1.2023.1 to 1.2023.2](dependabot-gitlab/dependabot@9f37670b9124d8473d3ea8baa703bed3a29ceb44) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2057
- [Bump tzinfo-data from 1.2022.7 to 1.2023.1](dependabot-gitlab/dependabot@300ba85a69b848002223113adf609679b5d9e7ba) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2052

### 📦🔧 Development dependency updates (6 changes)

- [Bump vitepress from 1.0.0-alpha.64 to 1.0.0-alpha.65](dependabot-gitlab/dependabot@cdd810732ac79625793345038230384ab024a643) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2070
- [Bump capybara from 3.38.0 to 3.39.0](dependabot-gitlab/dependabot@533f5d4d8edca7350560c5bf82710f94b180f03a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2067
- [Bump rubocop from 1.48.1 to 1.49.0](dependabot-gitlab/dependabot@16ff035c0e9bb0f3d387839060040d44743c0a0a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2068
- [Bump vitepress from 1.0.0-alpha.63 to 1.0.0-alpha.64](dependabot-gitlab/dependabot@fa47481c8ad79c80d2ec1437fe6b8c6213d53a73) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2061
- [Bump vitepress from 1.0.0-alpha.62 to 1.0.0-alpha.63](dependabot-gitlab/dependabot@c287835cab4d6e8c1a1693919d68b2558606a836) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2058
- [Bump vitepress from 1.0.0-alpha.61 to 1.0.0-alpha.62](dependabot-gitlab/dependabot@c613d9b1d47bde5d0cf6b852f801ece3e2dcfacf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2056

### 🔧 CI changes (7 changes)

- [Update app version in chart repo by creating merge request](dependabot-gitlab/dependabot@5279efa7d082e0fd66b233b1095eaad5636715c6) by @andrcuns. See merge request dependabot-gitlab/dependabot!2071
- [Bump node version to 18](dependabot-gitlab/dependabot@034bd3d10e133b0d387fda07658b4f3c9e06feea) by @andrcuns. See merge request dependabot-gitlab/dependabot!2069
- [Bump smocker version to 0.18.2](dependabot-gitlab/dependabot@eba75a208ffb0d2b275367eaf5274c28a3eaa163) by @andrcuns. See merge request dependabot-gitlab/dependabot!2069
- [Update docker Docker tag to v23.0.2](dependabot-gitlab/dependabot@e839b5b50c29400495070da2d452e0f495894175) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2062
- [Do not publish docs if release fails](dependabot-gitlab/dependabot@5801fc77c3dc9a9dc06c8fcbe78b4d212ceede38) by @andrcuns. See merge request dependabot-gitlab/dependabot!2055
- [Fix standalone version bump task](dependabot-gitlab/dependabot@7414684c12a922a1c57e0bbc49b0da254a0ed114) by @andrcuns. See merge request dependabot-gitlab/dependabot!2050
- [Fix chart app version bump task](dependabot-gitlab/dependabot@95e15f90f6556659ee46ec12fee9b14b88d8740f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2050

### 🧰 Maintenance (1 change)

- [Add debug log when using gitlab client with project specific token](dependabot-gitlab/dependabot@8331cb89a901d336242e74661689bf4ec969b1ef) by @andrcuns. See merge request dependabot-gitlab/dependabot!2060

## 1.0.0-alpha.1 (2023-03-23)

### 🚀 New features (1 change)

- [[BREAKING] Add support for per-ecosystem docker images](dependabot-gitlab/dependabot@b166eb6bba21a47d1c63d6897d877046e7282133) by @andrcuns. See merge request dependabot-gitlab/dependabot!2015

### 🔬 Improvements (1 change)

- [Configurable updater image name](dependabot-gitlab/dependabot@10601febedfc1ab7770af83e336c095abf36fbd9) by @andrcuns. See merge request dependabot-gitlab/dependabot!2049

### 🐞 Bug Fixes (1 change)

- [Return no-content on unsupported event_names for system hooks](dependabot-gitlab/dependabot@4a7a6a625254aa6c704e792d898646d9d17333fe) by @andrcuns. See merge request dependabot-gitlab/dependabot!2040

### 📦 Dependency updates (1 change)

- [Bump dependabot-omnibus from `40a090d` to `0b297e3`](dependabot-gitlab/dependabot@c780f15412c533fa33b25d907e509647b2cab2eb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2043

### 📦🔧 Development dependency updates (2 changes)

- [Bump vitepress from 1.0.0-alpha.60 to 1.0.0-alpha.61](dependabot-gitlab/dependabot@99a0723e4416e404886d73dca5856478d26ed61c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2042
- [Bump git from 1.17.2 to 1.18.0](dependabot-gitlab/dependabot@aabf3813cbdb239ecc0127501bfaf13c3fd09186) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2037

### 🔧 CI changes (3 changes)

- [Support publishing images to gitlab registry and dockerhub](dependabot-gitlab/dependabot@2efc4b0c75b5e261ff29c98b241eb48e6f697b9a) by @andrcuns. See merge request dependabot-gitlab/dependabot!2049
- [Support pre-release tags in ci rules](dependabot-gitlab/dependabot@d67f7d100ae3c166cc06d0ea52024c492cfe4e1b) by @andrcuns. See merge request dependabot-gitlab/dependabot!2045
- [Use daemonless buildkit](dependabot-gitlab/dependabot@69cd391f272864bcb7eeb65e4ec878cda77590be) by @andrcuns. See merge request dependabot-gitlab/dependabot!2035

### 🧰 Maintenance (8 changes)

- [Add oci image labels](dependabot-gitlab/dependabot@7298c708e35cd5fe10cc61e4d9b97859da786f12) by @andrcuns. See merge request dependabot-gitlab/dependabot!2049
- [Switch to pre-release alpha versioning](dependabot-gitlab/dependabot@b03b4fe91d0f12c9a03482263ef84176496f96a6) by @andrcuns.
- [Skip application scanning in container scan job](dependabot-gitlab/dependabot@031cc6424a2e8d8b5c7b1f0f0cfe243eac548f4e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2044
- [Fix release update and publish toolset](dependabot-gitlab/dependabot@f733e5e724750121e1c00c61bea80b37632c3170) by @andrcuns. See merge request dependabot-gitlab/dependabot!2039
- [Add context logging for update runner job](dependabot-gitlab/dependabot@696d59d7c917c3e87498ec9a249a2ab4ae036f09) by @andrcuns. See merge request dependabot-gitlab/dependabot!2038
- [Temporary disable standalone repo update](dependabot-gitlab/dependabot@5dba416bed81c152056407158d643c6fafdc977e) by @andrcuns.
- [Allow container scan job failure](dependabot-gitlab/dependabot@9b353d7a2bc06be80b8174998e08e727ab7df397) by @andrcuns.
- [Add ability to bump pre-release version number](dependabot-gitlab/dependabot@eb85aab215639bc6adbfd1fec569516a05689b5c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2036

### 📄 Documentation updates (1 change)

- [Improve documentation on approver/reviewer configuration](dependabot-gitlab/dependabot@4cc73446cdda2212fc098e89bb180a5ebc83157e) by @andrcuns. See merge request dependabot-gitlab/dependabot!2041

## 0.36.0 (2023-03-17)

### ⚠️ Security updates (1 change)

- [[Security] Bump rack from 2.2.6.2 to 2.2.6.3](dependabot-gitlab/dependabot@ff7e25e58571f2350b0362c54a671e19642924fb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2023

### 🐞 Bug Fixes (1 change)

- [Build images with buildkit](dependabot-gitlab/dependabot@59066d7dd5a4af47ac58784fe794c4747c3cfb97) by @andrcuns. See merge request dependabot-gitlab/dependabot!2033

### 📦 Dependency updates (4 changes)

- [Bump sidekiq-cron from 1.9.1 to 1.10.0](dependabot-gitlab/dependabot@49c3ea153c0751ae2f4d13f7cda4f5a816420398) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2029
- [Bump rails from 7.0.4.2 to 7.0.4.3](dependabot-gitlab/dependabot@c682b998c504633636376917d7fb57bd121211c5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2028
- [Bump sidekiq_alive from 2.1.9 to 2.2.0](dependabot-gitlab/dependabot@36dea83a5b256b2a0e778f9f11a21124ea4d9ac3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2008
- [Bump puma from 6.1.0 to 6.1.1](dependabot-gitlab/dependabot@6a26945848d3a6733fd6fda31330be75976aa022) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2007

### 📦🔧 Development dependency updates (13 changes)

- [Bump vitepress from 1.0.0-alpha.58 to 1.0.0-alpha.60](dependabot-gitlab/dependabot@90c1b0cf97f318b9085d7cd18d54c3785954b4e1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2032
- [Bump vitepress from 1.0.0-alpha.56 to 1.0.0-alpha.58](dependabot-gitlab/dependabot@e43f1f7497006bad199fd3efcf642a05b4335869) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2031
- [Bump rubocop from 1.48.0 to 1.48.1](dependabot-gitlab/dependabot@f2564cb74472e2cb6bc1e85529f86530c964639a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2030
- [Bump vitepress from 1.0.0-alpha.52 to 1.0.0-alpha.56](dependabot-gitlab/dependabot@6f83555cb5bc6ca67c4d0be9e4a2c758a90dcf5a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2027
- [Bump vitepress from 1.0.0-alpha.51 to 1.0.0-alpha.52](dependabot-gitlab/dependabot@2573ec55826cabcecff871a3433a28b4dcdf4387) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2026
- [Bump @types/node from 18.14.0 to 18.15.0](dependabot-gitlab/dependabot@c7024cd0a3556631cdebca9054fc1a6e1646f30e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2024
- [Bump rubocop-rspec from 2.18.1 to 2.19.0](dependabot-gitlab/dependabot@82ab4ac3756db1f36ac08052b7b5fbdee5ba97e7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2018
- [Bump rubocop from 1.47.0 to 1.48.0](dependabot-gitlab/dependabot@8ebd18e71ee6cb7913f4c6636e6b987c195101fc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2017
- [Bump vitepress from 1.0.0-alpha.49 to 1.0.0-alpha.51](dependabot-gitlab/dependabot@4968bea0fb72283f95b60e79b03aaa3f8e8c2901) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2022
- [Bump git from 1.15.0 to 1.17.2](dependabot-gitlab/dependabot@bfd6ab2ce9fdc7b78e466c1ef4e44ce8784c4bac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2021
- [Bump git from 1.14.0 to 1.15.0](dependabot-gitlab/dependabot@942dc7febc27b639b1f0808279bbf5a02ffbafb1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2010
- [Bump rubocop from 1.46.0 to 1.47.0](dependabot-gitlab/dependabot@67f8bc047ed13e551487e21d195315d860b5e866) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2009
- [Bump vitepress from 1.0.0-alpha.48 to 1.0.0-alpha.49](dependabot-gitlab/dependabot@c12fc13647e73e57d13ef1574dcfb5b9d87edf60) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2006

### 🔧 CI changes (3 changes)

- [Skip cache job when dependencies have not changed](dependabot-gitlab/dependabot@f75c0bab0d2161c9271712c20d32e96db923f8e8) by @andrcuns. See merge request dependabot-gitlab/dependabot!2025
- [Fix npm cache and move coverage download](dependabot-gitlab/dependabot@65b60b0bb3f1c438f5553ecdf9c5d74f6d2d29ac) by @andrcuns. See merge request dependabot-gitlab/dependabot!2013
- [Use common variable for specifying docker version](dependabot-gitlab/dependabot@2234497919ce3ca34071020ae74051f51f545208) by @andrcuns. See merge request dependabot-gitlab/dependabot!2005

### 🧰 Maintenance (3 changes)

- [Remove project registration namespace setting](dependabot-gitlab/dependabot@0e7b5b4cab762d7aa14cdea707bdfe08bc64d36f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2014
- [Split standalone and service mode implementations](dependabot-gitlab/dependabot@3aa7c7129651716210483598999e3854ade20ac7) by @andrcuns. See merge request dependabot-gitlab/dependabot!2012
- [Add commits mock definition](dependabot-gitlab/dependabot@82b8aaceb1498d3e56f2dbaeb11f1c991bf3cb4d) by @andrcuns. See merge request dependabot-gitlab/dependabot!2011

## 0.35.1 (2023-02-27)

### 🐞 Bug Fixes (1 change)

- [Fix log entry migration for initial installation](dependabot-gitlab/dependabot@24d493266931dcce4126a14b1eab85b9831dd36c) by @andrcuns. See merge request dependabot-gitlab/dependabot!2004

### 📦🔧 Development dependency updates (1 change)

- [dep: bump git from 1.13.2 to 1.14.0](dependabot-gitlab/dependabot@bce3e0ff5147401c4ebebb3e87c3063df6bd32b6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2003

## 0.35.0 (2023-02-26)

### 🐞 Bug Fixes (3 changes)

- [Fix update run log entry cleanup](dependabot-gitlab/dependabot@8cf52fb33eac3a59cf40c4ce09f1e19a133b5d64) by @andrcuns. See merge request dependabot-gitlab/dependabot!1999
- [Save correct package name for vulnerability issue object](dependabot-gitlab/dependabot@e17eac25339d9ff8fa237fc5d26dc1ca43ce58ab) by @andrcuns. See merge request dependabot-gitlab/dependabot!1977
- [Automerge stops dependabot when branch is unmergable](dependabot-gitlab/dependabot@549631886334ae3547d14084172ae45ab5346a60) by @syffer. See merge request dependabot-gitlab/dependabot!1976

### 📦 Dependency updates (3 changes)

- [dep: bump puma from 6.0.2 to 6.1.0](dependabot-gitlab/dependabot@e8a48e6ea5e92c6a1686156fab6eb89236d5f473) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1990
- [dep: bump sentry-rails from 5.7.0 to 5.8.0](dependabot-gitlab/dependabot@d56be9ed432131e31039a0f104be5147dc5fd872) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1983
- [dep: bump rails from 7.0.4.1 to 7.0.4.2](dependabot-gitlab/dependabot@957cb8a38bc79d83a2604082bcfd81280e66087d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1964

### 📦🔧 Development dependency updates (13 changes)

- [dep: bump vitepress from 1.0.0-alpha.47 to 1.0.0-alpha.48](dependabot-gitlab/dependabot@fc2bbddb8edbf05d9390919eb7886b7bc52118b4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!2001
- [dep: bump rubocop-rails from 2.17.4 to 2.18.0](dependabot-gitlab/dependabot@e7fa41c310e839f7d9d86179f27979757a261643) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1995
- [dep: bump rubocop from 1.45.1 to 1.46.0](dependabot-gitlab/dependabot@4c93893f14523e24396b04ef0b93ae0423cd8544) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1994
- [dep: bump vitepress from 1.0.0-alpha.46 to 1.0.0-alpha.47](dependabot-gitlab/dependabot@a5fffab9b2fcf6a07b6dc42233f5232fd2ae61ca) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1993
- [dep: bump @types/node from 18.13.0 to 18.14.0](dependabot-gitlab/dependabot@09c73ee4255338bd037cf1a93d4332b1ca99ab54) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1992
- [dep: bump vitepress from 1.0.0-alpha.45 to 1.0.0-alpha.46](dependabot-gitlab/dependabot@36887ee2c5b4d4a4fed755e1811d4b43ff8629c7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1989
- [dep: bump rubocop from 1.44.1 to 1.45.1](dependabot-gitlab/dependabot@4221b916af427f970aee1d968817041aaed24cb7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1987
- [dep: bump @types/node from 18.11.6 to 18.13.0](dependabot-gitlab/dependabot@c0e5e6b7092bef7938eabbd23252b16505868fab) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1986
- [dep: bump rubocop-performance from 1.15.2 to 1.16.0](dependabot-gitlab/dependabot@bee23f538745fde8a1fa5faa444ee98fc46d7988) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1985
- [dep: bump faker from 3.1.0 to 3.1.1](dependabot-gitlab/dependabot@91b5c8b78646e816ed5e53a0fe72ff7e475dee4d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1981
- [dep: bump git from 1.13.1 to 1.13.2](dependabot-gitlab/dependabot@62341bd5579033eb83f635b0b0349c7f341c746b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1980
- [dep: bump vitepress from 1.0.0-alpha.43 to 1.0.0-alpha.45](dependabot-gitlab/dependabot@5a155bfa1c44728e271de962812a005cff2b3315) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1975
- [dep: bump vitepress from 1.0.0-alpha.41 to 1.0.0-alpha.43](dependabot-gitlab/dependabot@f12d88634e29d5b385eab8861d4f23151bfe6a83) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1974

### 🔧 CI changes (5 changes)

- [Remove custom container scan job](dependabot-gitlab/dependabot@431f36bb5ea0446a6af52ac5547df8899790439c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1997
- [Update snyk cli version](dependabot-gitlab/dependabot@7d4c81bc415debc6ac4eb01c3ae4d307cf2ef28e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1991
- [Update docker docker tag to v23.0.1](dependabot-gitlab/dependabot@21f40a0b97f5c616fde2583ef3eacb25ec0649af) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1988
- [Improve error handling for container scan task](dependabot-gitlab/dependabot@7e1f2020d6cb650955d5b3a5d29d41c13afe6da7) by @andrcuns. See merge request dependabot-gitlab/dependabot!1982
- [chore(deps): update docker docker tag to v23](dependabot-gitlab/dependabot@5fc2eaf97928f119aa18e061245667c206435d44) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1979

### 🧰 Maintenance (2 changes)

- [Set created_at value for old log entries](dependabot-gitlab/dependabot@33e710743d145cc72ee97e1b9bd836b6a2b6ccab) by @andrcuns. See merge request dependabot-gitlab/dependabot!2002
- [Use delete to remove old log entries during migration](dependabot-gitlab/dependabot@09f21160b3104255b673e223bc48f83b4dd3674f) by @andrcuns. See merge request dependabot-gitlab/dependabot!2000

### 📄 Documentation updates (2 changes)

- [Update info on required scopes for api token](dependabot-gitlab/dependabot@c31bd36db2400b63e0dce1694b860349723d1fa0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1998
- [add information on GitLab access token role](dependabot-gitlab/dependabot@3289e52d0eb22fd7bcc60591e76644fc8beb6f1a) by @pdecat. See merge request dependabot-gitlab/dependabot!1978

## 0.34.0 (2023-01-29)

### 🚀 New features (2 changes)

- [Allow to configure gitlab api retries](dependabot-gitlab/dependabot@982fc06462b676c9299264ea3a9b83bfe422f06d) by @syffer. See merge request dependabot-gitlab/dependabot!1969
- [[BREAKING] Make open mr settings configuration more flexible and consistent](dependabot-gitlab/dependabot@ab3e13db25318db775d80e900e80641bb8478604) by @andrcuns. See merge request dependabot-gitlab/dependabot!1970

### 📦🔧 Development dependency updates (1 change)

- [dep: bump vitepress from 1.0.0-alpha.40 to 1.0.0-alpha.41](dependabot-gitlab/dependabot@28c804b11a3fd2ca0665d95b75f178453b7aaa7c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1973

### 🔧 CI changes (1 change)

- [Save and print error output of migration test](dependabot-gitlab/dependabot@d99f011af53cfe5130d780ae6a8f20c434a51cc9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1972

### 🧰 Maintenance (2 changes)

- [Add migration for open_security_mrs setting option](dependabot-gitlab/dependabot@4ca48666f8e1c042ad0699f54bcd8d113c957b00) by @andrcuns. See merge request dependabot-gitlab/dependabot!1972
- [Remove deprecated environment variable settings](dependabot-gitlab/dependabot@a7c9c652e0c5055a5f955507687a1eb30c9c125f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1971

## 0.33.1 (2023-01-27)

### ⚠️ Security updates (1 change)

- [dep: [security] bump commonmarker from 0.23.6 to 0.23.7](dependabot-gitlab/dependabot@912f514fc5bb9a3733b859a04a82a6c12e418e05) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1966

### 🐞 Bug Fixes (3 changes)

- [Lock threads when fetching dependency files](dependabot-gitlab/dependabot@7f03d9f9523d78d8782b5b38e3973b1eb99ce35a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1968
- [Correctly handle work_item type webhook events](dependabot-gitlab/dependabot@86d928f331b73118fa00949509ba9bd726f82d11) by @andrcuns. See merge request dependabot-gitlab/dependabot!1967
- [Do not log vulnerable warning when dependency is not vulnerable](dependabot-gitlab/dependabot@ee2b2fcd023b7cf0f10e1ae49b8694a464bffb20) by @andrcuns. See merge request dependabot-gitlab/dependabot!1954

### 📦 Dependency updates (3 changes)

- [dep: bump bootsnap from 1.15.0 to 1.16.0](dependabot-gitlab/dependabot@50a5410d4c7093422ede8da5100fed0784e64b54) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1963
- [dep: bump anyway_config from 2.3.0 to 2.3.1](dependabot-gitlab/dependabot@2eec7189f33267045524f53eba36b40ceae11524) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1958
- [dep: bump rails from 7.0.4 to 7.0.4.1](dependabot-gitlab/dependabot@78169452f25d94e77acae0efdc3c61c311c24d5c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1957

### 📦🔧 Development dependency updates (10 changes)

- [dep: bump rubocop from 1.44.0 to 1.44.1](dependabot-gitlab/dependabot@c8328701d05d101da8e498dcbdbe94ed589670b8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1965
- [dep: bump rubocop from 1.43.0 to 1.44.0](dependabot-gitlab/dependabot@26623c9b2eec4c288218489745267068571dae7d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1962
- [dep: bump vitepress from 1.0.0-alpha.38 to 1.0.0-alpha.40](dependabot-gitlab/dependabot@9ede33243e22beb9d590bc55c8b2838714e0b0ac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1960
- [dep: bump rubocop-rspec from 2.18.0 to 2.18.1](dependabot-gitlab/dependabot@a62c2077a2078327ce14644934bbb8e230f9609e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1959
- [dep: bump vitepress from 1.0.0-alpha.36 to 1.0.0-alpha.38](dependabot-gitlab/dependabot@8480be212f7fa4c2037a2b560df27d1df0257d48) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1956
- [dep: bump rubocop-rspec from 2.17.0 to 2.18.0](dependabot-gitlab/dependabot@482a2e95c5a9775cdf9053d147eda24a872ede33) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1955
- [dep: bump rubocop from 1.42.0 to 1.43.0](dependabot-gitlab/dependabot@a0bf8b353753d5a0421b4d60f99730aec9a526d2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1953
- [dep: bump reek from 6.1.3 to 6.1.4](dependabot-gitlab/dependabot@b53cfa9ff3c12236e25884cf6e2b579852f77717) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1952
- [dep: bump rubocop-rspec from 2.16.0 to 2.17.0](dependabot-gitlab/dependabot@d94ceffb98f22b727b2c5beee2b0593b03394f5d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1951
- [dep: bump git from 1.13.0 to 1.13.1](dependabot-gitlab/dependabot@ec192ccd360c510736ee1c4714c0f05b1194cb01) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1950

## 0.33.0 (2023-01-12)

### 🚀 New features (2 changes)

- [Add option to disable dependency update merge request creation](dependabot-gitlab/dependabot@189306d4321a3eddedf66f18deee36eec9091c29) by @andrcuns. See merge request dependabot-gitlab/dependabot!1948
- [Add option to disable open merge requests limit](dependabot-gitlab/dependabot@ea00763bededd021fb1d6b87e5d603137b0e3b19) by @andrcuns. See merge request dependabot-gitlab/dependabot!1947

### 🔬 Improvements (1 change)

- [Improve error handling for vulnerability issue creation](dependabot-gitlab/dependabot@fdb4594f6a5349cd3fad05115236481abd42d174) by @andrcuns. See merge request dependabot-gitlab/dependabot!1926

### 🐞 Bug Fixes (3 changes)

- [[BREAKING] Switch to kebab case for max branch length option](dependabot-gitlab/dependabot@52f2c4e2040cf7a2c52da1e2045565e2e5c0c76b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1946
- [Fix development and security commit message trailers setting](dependabot-gitlab/dependabot@ee19455e2d38974603853150ec429b5f41ab0569) by @andrcuns. See merge request dependabot-gitlab/dependabot!1934
- [Fix repeated label creation attempt](dependabot-gitlab/dependabot@d20cd19c2ff904bcc068cf2ae5bc1f52d4f331b6) by @andrcuns. See merge request dependabot-gitlab/dependabot!1924

### 📦 Dependency updates (5 changes)

- [dep: bump sidekiq_alive from 2.1.8 to 2.1.9](dependabot-gitlab/dependabot@a2e6be98b9925133c06aac6e5e243a0414776ba6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1949
- [dep: bump puma from 6.0.1 to 6.0.2](dependabot-gitlab/dependabot@b24c601e22c15931bd2a810b15e5aaa49067388e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1940
- [Bump sidekiq_alive to 2.1.8](dependabot-gitlab/dependabot@2abf35dbe9943d1c05ec80f4480626f149b4a896) by @andrcuns. See merge request dependabot-gitlab/dependabot!1936
- [dep: bump rubocop-rails from 2.17.3 to 2.17.4](dependabot-gitlab/dependabot@c3bb96f589b3b8bc890ac8d6c5e6aaf516a21577) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1932
- [dep: bump simplecov from 0.21.2 to 0.22.0](dependabot-gitlab/dependabot@562baf0ea7a3fb772e4485c3878fdfc6b0cb4399) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1923

### 📦🔧 Development dependency updates (9 changes)

- [dep: bump vitepress from 1.0.0-alpha.35 to 1.0.0-alpha.36](dependabot-gitlab/dependabot@ad4df848e150262e50633b5522f67e78b5c42ac8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1945
- [dep: bump spring from 4.1.0 to 4.1.1](dependabot-gitlab/dependabot@0da01ac180312f9c05642099efb834bc60104d36) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1943
- [dep: bump vitepress from 1.0.0-alpha.34 to 1.0.0-alpha.35](dependabot-gitlab/dependabot@0e6553f620e56f7d82159769605e3d7b78de8a38) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1942
- [dep: bump rubocop from 1.41.1 to 1.42.0](dependabot-gitlab/dependabot@e886208691658f79b847d005d3a8e02315a506f0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1941
- [dep: bump vitepress from 1.0.0-alpha.33 to 1.0.0-alpha.34](dependabot-gitlab/dependabot@96f551de6223f2d413e594737c040191c13cdd63) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1939
- [dep: bump httparty from 0.20.0 to 0.21.0](dependabot-gitlab/dependabot@35e538c64340f8ac27546ed73a87875af404ff5e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1938
- [dep: bump reek from 6.1.2 to 6.1.3](dependabot-gitlab/dependabot@01c86b4409bed14681a894c30829ebfa6d489c5c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1937
- [dep: bump rubocop-performance from 1.15.1 to 1.15.2](dependabot-gitlab/dependabot@f37e0758bc385058e2909b57c375e15be667139e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1931
- [dep: bump faker from 3.0.0 to 3.1.0](dependabot-gitlab/dependabot@3009145be0592b60d150491a2ad23d678330bdc4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1922

### 🔧 CI changes (3 changes)

- [Remove not running all tests in mr pipelines](dependabot-gitlab/dependabot@eb308633857bc4d7d32360cdd49df446eabb0711) by @andrcuns. See merge request dependabot-gitlab/dependabot!1936
- [Run slow jobs in merge train pipelines](dependabot-gitlab/dependabot@1195085dcb58f94c0822b58e15b0c68d79caf15e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1928
- [Remove allure test reports](dependabot-gitlab/dependabot@012e8ad423cb7604d849d9e59ccaad8b036289f9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1929

### 🧰 Maintenance (5 changes)

- [Move project sync button out of header](dependabot-gitlab/dependabot@c2029b24ad5b27a525de2ce7305e35c0681d8d18) by @andrcuns. See merge request dependabot-gitlab/dependabot!1935
- [Update compound indexes](dependabot-gitlab/dependabot@3b2c6fa82a5e32b30766b1746be88c87dc72e772) by @andrcuns. See merge request dependabot-gitlab/dependabot!1933
- [Add search indexes to models](dependabot-gitlab/dependabot@fd24d43d416541043358ccbbf20a4b2f5769ffdd) by @andrcuns. See merge request dependabot-gitlab/dependabot!1930
- [Add logging for obsolete mr and issue closing](dependabot-gitlab/dependabot@9aaa96950f491f51850a55eff1aa535941f01539) by @andrcuns. See merge request dependabot-gitlab/dependabot!1927
- [Rescue gitlab errors on dependency updates](dependabot-gitlab/dependabot@6114a1923c2e1065609726b7331930ba0abc7426) by @andrcuns. See merge request dependabot-gitlab/dependabot!1925

### 📄 Documentation updates (1 change)

- [Add information on required scopes for personal access tokens](dependabot-gitlab/dependabot@72deb505fc3c39420ce1df2afb9a8d56bd16ea7d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1944

## 0.32.0 (2022-12-23)

### 🚀 New features (2 changes)

- [Authentication and separate user support](dependabot-gitlab/dependabot@f9292c2818dac046dab0bfc14cd7615e0a8ec804) by @andrcuns. See merge request dependabot-gitlab/dependabot!1895
- [web ui updated based on gitlab css, order projects by name, add project form](dependabot-gitlab/dependabot@b67343e028b2910fa57b917bfb1bac2e6556726f) by @sanyatuning. See merge request dependabot-gitlab/dependabot!1883

### 📦 Dependency updates (19 changes)

- [dep: bump dependabot-omnibus from 0.214.0 to 0.215.0](dependabot-gitlab/dependabot@6d8212d48f36f538edaa9ca76627610927101016) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1898
- [dep: bump rubocop from 1.41.0 to 1.41.1](dependabot-gitlab/dependabot@37998450acfbba2d8afa196d610676afed5d8a6e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1917
- [dep: bump sidekiq-cron from 1.9.0 to 1.9.1](dependabot-gitlab/dependabot@8afd90353a5251f54fb6aeab116ea0ecfc61fa66) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1916
- [dep: bump rubocop from 1.40.0 to 1.41.0](dependabot-gitlab/dependabot@018261ef5a6d0fae1ddb001c0eedd3cc1c22d4f6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1914
- [dep: bump puma from 6.0.0 to 6.0.1](dependabot-gitlab/dependabot@3003a170ba324f03e2c5083283842da671fff857) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1913
- [dep: bump solargraph from 0.47.2 to 0.48.0](dependabot-gitlab/dependabot@4a476f2b781ad2e1122836fd3b542d993d5d34e0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1910
- [dep: bump git from 1.12.0 to 1.13.0](dependabot-gitlab/dependabot@5e593945fa32d1e2e6f9200ae4f70caafe8e6772) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1905
- [dep: [security] bump rails-html-sanitizer from 1.4.3 to 1.4.4](dependabot-gitlab/dependabot@733d40d82135b51e218ba8aaac08ace547942cfc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1907
- [dep: bump mongoid from 8.0.2 to 8.0.3](dependabot-gitlab/dependabot@53487a92ff39381a1584baed71d848903c1eefb5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1904
- [dep: bump rubocop-rspec from 2.15.0 to 2.16.0](dependabot-gitlab/dependabot@1648958829719eadb583163adf081cc8443a88a3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1903
- [dep: [security] bump nokogiri from 1.13.9 to 1.13.10](dependabot-gitlab/dependabot@5dc54a6bb980b833a16d87e2bafd2bc95fbf9308) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1900
- [dep: bump rubocop from 1.39.0 to 1.40.0](dependabot-gitlab/dependabot@e737d4b128864c80b61db3442ab9141e816bad4b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1899
- [dep: bump dependabot-omnibus from 0.213.0 to 0.214.0](dependabot-gitlab/dependabot@90c709298003afaf48eb01a19dad4b66b71b9ac5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1894
- [dep: bump sidekiq-cron from 1.8.0 to 1.9.0](dependabot-gitlab/dependabot@839c0a9693a11a4876bc2bb036ff83563173da29) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1890
- [dep: bump sentry-rails from 5.6.0 to 5.7.0](dependabot-gitlab/dependabot@fcff620165e769b9c8bcc30f80af9ea842cffcab) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1888
- [dep: bump allure-rspec from 2.19.0 to 2.20.0](dependabot-gitlab/dependabot@d35ccb7ec5200b9900fb8d5e11611c1171313864) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1886
- [dep: bump yabeda-puma-plugin from 0.7.0 to 0.7.1](dependabot-gitlab/dependabot@91ea9cd4f324ee4c00932fab9f6e1c93ed456dcc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1885
- [dep: bump bootsnap from 1.14.0 to 1.15.0](dependabot-gitlab/dependabot@e928ea62810f2d941a6468059714570d8a9120f6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1884
- [dep: bump bootsnap from 1.13.0 to 1.14.0](dependabot-gitlab/dependabot@dd5bd24ac3a81b8d9b32a03eecdc2720b15389b6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1880

### 📦 Development dependency updates (9 changes)

- [dep: bump vitepress from 1.0.0-alpha.32 to 1.0.0-alpha.33](dependabot-gitlab/dependabot@3e02d5b301a723d46d5263d80bb6ab0445a83034) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1915
- [dep: bump vitepress from 1.0.0-alpha.31 to 1.0.0-alpha.32](dependabot-gitlab/dependabot@745f9f7339a62884a443a7b955f293ad66cbd07f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1908
- [dep: bump vitepress from 1.0.0-alpha.30 to 1.0.0-alpha.31](dependabot-gitlab/dependabot@0d0480eb7d886b0c8f0f29b65ff1672cddcebbff) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1902
- [dep: bump vitepress from 1.0.0-alpha.29 to 1.0.0-alpha.30](dependabot-gitlab/dependabot@3045b324d66a48454e970baf78317fe433ac4ff6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1896
- [dep: bump reek from 6.1.1 to 6.1.2](dependabot-gitlab/dependabot@9480c6a55489b7f40a0fb4e2f0bf421b8475c172) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1882
- [dep: bump rubocop-rails from 2.17.2 to 2.17.3](dependabot-gitlab/dependabot@6a76fc1748a86be8b84b9d1f502f06bf88dce614) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1881
- [dep: bump rubocop-performance from 1.15.0 to 1.15.1](dependabot-gitlab/dependabot@50cbe7c177bbb732fd27fdf45e9af5d6e384fb17) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1879
- [dep: bump vitepress from 1.0.0-alpha.28 to 1.0.0-alpha.29](dependabot-gitlab/dependabot@6278db0aa5439264b821bb5c501b578c9f9cfa19) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1878
- [dep: bump rubocop from 1.38.0 to 1.39.0](dependabot-gitlab/dependabot@9b0b0d586e678f725e6aeff328ac1dcfd49cfd3d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1877

### 🔧 CI changes (5 changes)

- [Temporarily disable core image build](dependabot-gitlab/dependabot@fbc57f5cb16c1b67a3cf94ce9237173325a88b97) by @andrcuns. See merge request dependabot-gitlab/dependabot!1919
- [Run full image scan on dependency changes only](dependabot-gitlab/dependabot@2d5207c22e00945e6fecec91224981a2df8dd8fe) by @andrcuns. See merge request dependabot-gitlab/dependabot!1918
- [chore(deps): update andrcuns/allure-report-publisher docker tag to v1.4.0](dependabot-gitlab/dependabot@fde915122270a54747d087d177aa4e63522eab0f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1912
- [Update docker Docker tag to v20.10.22](dependabot-gitlab/dependabot@0f2c7a8d8512a4f4fb4cd5f452a41ff7989b5673) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1909
- [Use correct registry for fetching core image](dependabot-gitlab/dependabot@8b3f771d4dd12f091a70210fec3c0264cdfb8c7c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1901

### 🧰 Maintenance (6 changes)

- [Improve user creation and deletion task outputs](dependabot-gitlab/dependabot@bfeeb35611a21ea66618146e835a94c271ad1795) by @andrcuns. See merge request dependabot-gitlab/dependabot!1921
- [Bump development ruby version to 3.1.3](dependabot-gitlab/dependabot@7016aa227de3c40cf19d9a7c0f3ae7c5b44a3265) by @andrcuns. See merge request dependabot-gitlab/dependabot!1920
- [Add solargraph rails support](dependabot-gitlab/dependabot@c8cb5246d929576fd2845a9bdaf64272e12537b3) by @andrcuns. See merge request dependabot-gitlab/dependabot!1911
- [Block upgrade to sidekiq version 7](dependabot-gitlab/dependabot@c09960e00d50101180b101c78fdb6bd7726be5f2) by @andrcuns.
- [Update core image build script](dependabot-gitlab/dependabot@b47365772b3ea9aee51cf9ae1e410b466e2e30be) by @andrcuns.
- [Remove airborne gem](dependabot-gitlab/dependabot@da6cc6f51915c6335823018a756c6feb7b375834) by @andrcuns. See merge request dependabot-gitlab/dependabot!1891

## 0.31.1 (2022-11-14)

### 🐞 Bug Fixes (2 changes)

- [Make sure custom warning processor has always access to logger object](dependabot-gitlab/dependabot@08abb25f0009a75601af9624630c963e5a2b6eba) by @andrcuns. See merge request dependabot-gitlab/dependabot!1876
- [Fix configuration object comparison](dependabot-gitlab/dependabot@11a64a48d3045f9ec2f5dc93ef7e7bce6573955e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1875

## 0.31.0 (2022-11-09)

### 🚀 New features (1 change)

- [Add option to set confidential setting for vulnerability issues](dependabot-gitlab/dependabot@b54d012c74b626c98166b68ced5468055b300cc4) by @andrcuns. See merge request dependabot-gitlab/dependabot!1864

### 🐞 Bug Fixes (1 change)

- [Fix register_project rake task](dependabot-gitlab/dependabot@1ccfc629e4dbe4b0de4601ea38a7f9d5f94a950e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1872

### 📦 Dependency updates (2 changes)

- [dep: bump allure-rspec from 2.18.0 to 2.19.0](dependabot-gitlab/dependabot@59624394d0601b90b415d194da26b90d96cce0eb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1870
- [dep: bump sentry-rails from 5.5.0 to 5.6.0](dependabot-gitlab/dependabot@dcc793b935041de7692076b2919c6f2f1595e08c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1868

### 📦 Development dependency updates (1 change)

- [dep: bump vitepress from 1.0.0-alpha.27 to 1.0.0-alpha.28](dependabot-gitlab/dependabot@2ec4b67dbd4e2dd5460182ed08fa18358604c13b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1867

### 🔧 CI changes (1 change)

- [Update allure-report-publisher Docker tag to v1.3.0](dependabot-gitlab/dependabot@8268afc27e3eedc1b74a300a480e6d36f1bbbac5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1866

### 🧰 Maintenance (1 change)

- [Update devcontainer setup](dependabot-gitlab/dependabot@6a738a091796c2fff49479644ce0262167b53cf6) by @andrcuns. See merge request dependabot-gitlab/dependabot!1862

### 📄 Documentation updates (1 change)

- [Fix typo](dependabot-gitlab/dependabot@b6f543ffcb1129bd7360885d71d6b71886582a25) by @petrvyhnalek. See merge request dependabot-gitlab/dependabot!1865

## 0.30.0 (2022-11-05)

### 🚀 New features (2 changes)

- [Add support for maximum branch length configuration](dependabot-gitlab/dependabot@8464254c691099a4af5a1024a67c2370086ef172) by @tom.naessens. See merge request dependabot-gitlab/dependabot!1630
- [Improved externally hosted project documentation](dependabot-gitlab/dependabot@cd8fdcc7b606286eee2f1440abe9f7cef69585ae) by @andrcuns. See merge request dependabot-gitlab/dependabot!1832

### 🐞 Bug Fixes (1 change)

- [Check for error status on dep update](dependabot-gitlab/dependabot@08e60c8a27297b04c9ecbb54c9dc55fe500cae38) by @andrcuns. See merge request dependabot-gitlab/dependabot!1802

### 📦 Dependency updates (17 changes)

- [dep: bump capybara from 3.37.1 to 3.38.0](dependabot-gitlab/dependabot@e00c9fca9ae6ea2e7f50ea1a04207788f2b384e7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1861
- [dep: bump sidekiq from 6.5.7 to 6.5.8](dependabot-gitlab/dependabot@673eb651d95de4859f0655fe2f99bffeccbd8d7d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1860
- [dep: bump dry-validation from 1.8.1 to 1.10.0](dependabot-gitlab/dependabot@3ac965a866e23eee7d7635e613f35c58f96caeae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1859
- [dep: bump rubocop-rspec from 2.14.2 to 2.15.0](dependabot-gitlab/dependabot@c70b39809324b8663aff87087d39c182cd2798e7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1858
- [dep: bump faker from 2.23.0 to 3.0.0](dependabot-gitlab/dependabot@af380443ffb4802afde695fd331d7186e71de0b2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1854
- [dep: bump rubocop from 1.37.1 to 1.38.0](dependabot-gitlab/dependabot@40ecbd334ba2569d5a125d912f57d4b824a84a59) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1849
- [Update dependabot-omnibus to v0.213.0](dependabot-gitlab/dependabot@7bffa8f8db3eca63ac4b412b319dde405dff257a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1802
- [Bump local development version to 3.1](dependabot-gitlab/dependabot@b4fbebbfa44272077bca21f0c39b5a72fb84a02b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1802
- [dep: bump rubocop-rails from 2.17.1 to 2.17.2](dependabot-gitlab/dependabot@3d603ffe5b6899f57699f499e8b433e4c7b8a757) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1846
- [dep: bump rspec from 3.11.0 to 3.12.0](dependabot-gitlab/dependabot@0eb9b10817bb1a1b411653d772dca96b04036b77) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1844
- [dep: bump rubocop-rspec from 2.14.1 to 2.14.2](dependabot-gitlab/dependabot@9fd44d861aea3fc1982b6c50291f504a60fb46ba) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1841
- [dep: bump rubocop-rails from 2.17.0 to 2.17.1](dependabot-gitlab/dependabot@b6fe35c695dd18fca359b5591c3c818226640930) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1840
- [dep: bump yabeda-sidekiq from 0.9.0 to 0.10.0](dependabot-gitlab/dependabot@3bb7d87603e3863717b84b1622abd1837aede84a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1839
- [dep: bump rubocop-rspec from 2.14.0 to 2.14.1](dependabot-gitlab/dependabot@5594665d732e596d499789cd4fce77da4ba0bd9c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1835
- [dep: bump rubocop from 1.37.0 to 1.37.1](dependabot-gitlab/dependabot@7a6b02c9b6bfdaa2e88a16d15dba38aff6c03486) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1834
- [dep: bump yabeda-puma-plugin from 0.6.0 to 0.7.0](dependabot-gitlab/dependabot@c8233c3163d3171c179074d4eefb3b60cb5e5518) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1833
- [dep: bump rubocop-rspec from 2.13.2 to 2.14.0](dependabot-gitlab/dependabot@9d5e2538bff3b94cdcc65933141f13016877507d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1831

### 📦 Development dependency updates (5 changes)

- [dep: bump vitepress from 1.0.0-alpha.26 to 1.0.0-alpha.27](dependabot-gitlab/dependabot@8d45c1d79c3b445dbf02022a035f8b712defd72c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1857
- [dep: bump vitepress from 1.0.0-alpha.25 to 1.0.0-alpha.26](dependabot-gitlab/dependabot@e769914be66646db6d5157838edb7a97c857bde8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1845
- [dep: bump @types/node from 18.11.5 to 18.11.6](dependabot-gitlab/dependabot@ed7990ed04e25f74768a6ac9733fa8b3547ff13a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1842
- [dep: bump vitepress from 1.0.0-alpha.22 to 1.0.0-alpha.25](dependabot-gitlab/dependabot@3a20af09bd4e4cf0c838b6321bfdc5fcac1547f1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1838
- [dep: bump @types/node from 18.11.3 to 18.11.5](dependabot-gitlab/dependabot@ac449f06f8fb9a10edd52710220985ab0f36b300) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1837

### 🔧 CI changes (5 changes)

- [Fix copying core-image from docker registry](dependabot-gitlab/dependabot@7b12816d10c7f524cdb97139b4599091db4dffc8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1855
- [Publish documentation on release only](dependabot-gitlab/dependabot@c918c9045580750200d56715fd4c88d49a85cf98) by @andrcuns. See merge request dependabot-gitlab/dependabot!1851
- [Set large runner tag dynamically](dependabot-gitlab/dependabot@d597975fa3540c1e8bbc8d1984e579464edf9563) by @andrcuns. See merge request dependabot-gitlab/dependabot!1850
- [deps: update docker docker tag to v20.10.21](dependabot-gitlab/dependabot@5bf40fef5f0c4277d207f5e44bc20cdca1713755) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1843
- [chore(deps): update andrcuns/allure-report-publisher docker tag to v1.2.0](dependabot-gitlab/dependabot@bd6a06faebad74a95bb136fa4e39f0798681fe59) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1829

### 🧰 Maintenance (4 changes)

- [Disable rspec threadsafe option](dependabot-gitlab/dependabot@8ef0e288984d1019d188c1d823557d105f9aaee6) by @andrcuns. See merge request dependabot-gitlab/dependabot!1856
- [Remove mentions sanitize monkeypatch](dependabot-gitlab/dependabot@b6eb083819184ba9fcae9265638ad92ed88ed447) by @andrcuns. See merge request dependabot-gitlab/dependabot!1852
- [Update license to reflect dependabot-core restrictions](dependabot-gitlab/dependabot@203a80866c357456f8866fbb7676d6e34ae15c5c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1836
- [Move github graphql api schema to separate folder](dependabot-gitlab/dependabot@c58d5fa9e8b35edc3b5ff094c37693ac1eec7ddb) by @andrcuns. See merge request dependabot-gitlab/dependabot!1830

## 0.29.0 (2022-10-22)

### 🚀 New features (1 change)

- [Ignore pattern for automatic project registration](dependabot-gitlab/dependabot@6e259a49a241ddac3be19838b5364d0f48418823) by @andrcuns. See merge request dependabot-gitlab/dependabot!1817

### 🔬 Improvements (3 changes)

- [Add parameter to trigger automatic project registration after application boot](dependabot-gitlab/dependabot@3ca99f946bf66ab1475575a2b4c24b6b463bd1b5) by @andrcuns. See merge request dependabot-gitlab/dependabot!1828
- [Add sidekiq-alive queue cleanup and improve configuration setup](dependabot-gitlab/dependabot@bb9193713d1313ed3f383c9ccac7c1f4bc44c26f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1810
- [Gracefully handle invalid configuration error on automatic registration](dependabot-gitlab/dependabot@739ff926cfd58ace58317b8518a859c813c4f5af) by @andrcuns. See merge request dependabot-gitlab/dependabot!1808

### 🐞 Bug Fixes (4 changes)

- [Sync project configuration in project registration job](dependabot-gitlab/dependabot@01e4b12822a99f3ee325087710e7bd00ce831e14) by @andrcuns. See merge request dependabot-gitlab/dependabot!1825
- [Fix typo in healthcheck queue name prefix](dependabot-gitlab/dependabot@0d16c3bce32882ce4ff858d40664c6fd70da6930) by @andrcuns. See merge request dependabot-gitlab/dependabot!1823
- [Correctly evaluate multiple update-type auto-merge rules](dependabot-gitlab/dependabot@50ef36aee9d72a94e0da5a717c04c7272bdf0ae0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1819
- [Fix incorrect variable name in project registration error handling](dependabot-gitlab/dependabot@91d7df7703bf1397a1370f29bd061bf1afab0056) by @andrcuns.

### 📦 Dependency updates (5 changes)

- [dep: bump rubocop-rails from 2.16.1 to 2.17.0](dependabot-gitlab/dependabot@48198c61b69f83771d307576cb215106c40c3383) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1827
- [dep: bump rubocop from 1.36.0 to 1.37.0](dependabot-gitlab/dependabot@275de03d8f5f6b39bf3ae8c8fe201ea4b0430b9b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1826
- [dep: bump rspec-rails from 6.0.0 to 6.0.1](dependabot-gitlab/dependabot@cc1cee1d728b0bc72788f6a06a334e120edf43de) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1821
- [dep: bump sidekiq-alive-next from 2.2.0 to 2.2.1](dependabot-gitlab/dependabot@f5b4b0ca624b0d69d1f19c8415ddd38843dd061d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1816
- [dep: bump puma from 5.6.5 to 6.0.0](dependabot-gitlab/dependabot@c4e151e424dd7e08618cb5a20c012992038e5875) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1813

### 🔧 CI changes (4 changes)

- [chore(deps): update docker docker tag to v20.10.20](dependabot-gitlab/dependabot@f3a9cffaad264a5551e414295bbc67d997281267) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1824
- [chore(deps): update andrcuns/allure-report-publisher docker tag to v1.1.0](dependabot-gitlab/dependabot@82f32dd80ddead420b99537e2542767664fc4a77) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1818
- [Make sure main pipeline runs are not cancelled](dependabot-gitlab/dependabot@cbd5f20eb56908b42c25663b37c5eb0fd40c14c9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1812
- [Update docker Docker tag to v20.10.19](dependabot-gitlab/dependabot@16f34c682ee95892252d9f31007965a5b0dfb03b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1811

### 🧰 Maintenance (9 changes)

- [Lock threads only when updating dependency files](dependabot-gitlab/dependabot@3e5d42f7d7701bb09d1862b0bbba5f7d49c81c1b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1820
- [Update to sidekiq-alive-next gem](dependabot-gitlab/dependabot@63660ca32c1bac2a4ff0950aa7c9c0904e158a9d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1814
- [Tweak sidekiq and job configuration](dependabot-gitlab/dependabot@ad32919a6a8770b00229fbf4470ad710f89fee5f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1809
- [Improve tag handling in project registration logging](dependabot-gitlab/dependabot@1ee8fc3f319030ed79c33c8616958a8beadbbdb1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1807
- [Correctly handle log context without details](dependabot-gitlab/dependabot@8497f058708cbdcb86b34138b3a5af20264d3921) by @andrcuns. See merge request dependabot-gitlab/dependabot!1807
- [Skip fetching configuration twice on automatic project registration](dependabot-gitlab/dependabot@c36c47cd684408d4bf4ef1f84c1c9d2064b2d117) by @andrcuns. See merge request dependabot-gitlab/dependabot!1807
- [Add issue templates](dependabot-gitlab/dependabot@4cf74c97c8516b00ee6adaabf9d777003ed89329) by @andrcuns. See merge request dependabot-gitlab/dependabot!1806
- [Fix release url in notification comment](dependabot-gitlab/dependabot@6830180a1e055dfcec9f78dd5d5f7bba682883e8) by @andrcuns.
- [Fix checking for already existing release notification comments](dependabot-gitlab/dependabot@a141c828a576a0828530bacd26f64c544b394049) by @andrcuns.

## 0.28.1 (2022-10-13)

### 🐞 Bug Fixes (3 changes)

- [Correctly handle archived gitlab projects](dependabot-gitlab/dependabot@00013365525d5d4fd8d522adc9343fc153a1dab8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1805
- [Correctly evaluate auto-merge allow/ignore rules](dependabot-gitlab/dependabot@fa2d9f311b0b1b94d467d02fc4c584bab5f00cc0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1804
- [Allow explicitly setting auto versioning strategy](dependabot-gitlab/dependabot@537dd6c110f8f4f08a5d8c657cb5e899025c0f4a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1801

### 📦 Development dependency updates (1 change)

- [dep: bump rspec-rails from 5.1.2 to 6.0.0](dependabot-gitlab/dependabot@9ec9e34d6324f697aedc45278e3397531fccab0a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1803

### 🔧 CI changes (3 changes)

- [Remove redundant setting of BUILD_PLATFORM in ci rules](dependabot-gitlab/dependabot@d49892528ca904c3b2427d9e052086eb5131bdaf) by @andrcuns.
- [Remove sha from ruby ci image](dependabot-gitlab/dependabot@420ef962bf02484bd811b9e32f5c987a69ab64b3) by @andrcuns.
- [Always run dependency scan on main pipelines](dependabot-gitlab/dependabot@b17808623ad1ff65b09f265e434eacb00f105870) by @andrcuns. See merge request dependabot-gitlab/dependabot!1800

### 🛠️ Chore (1 change)

- [Refactor project registration service](dependabot-gitlab/dependabot@9296d51f294714c20efcb9c92d9bfa1a3f1d9010) by @andrcuns. See merge request dependabot-gitlab/dependabot!1805

## 0.28.0 (2022-10-08)

### 🔬 Improvements (1 change)

- [Extract storage of update run data to separate models](dependabot-gitlab/dependabot@9c9f2accef42f29a62a48b6ac5a8491e1c40e488) by @andrcuns. See merge request dependabot-gitlab/dependabot!1797

### 🔧 CI changes (1 change)

- [chore(deps): update registry.gitlab.com/dependabot-gitlab/ci-images:ruby docker digest to 01d99fc](dependabot-gitlab/dependabot@b163d4650963bb8554205d8791159cc7da1d7f6e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1792

### 🛠️ Chore (2 changes)

- [Improve release creation automation](dependabot-gitlab/dependabot@6d9174645500312992e84855617113e5f0988c71) by @andrcuns. See merge request dependabot-gitlab/dependabot!1799
- [Add index to purge dependency update execution data after a period of time](dependabot-gitlab/dependabot@2f5f2162c026e9720a6dfe05b798f58ee584cf6e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1798

## 0.27.5 (2022-10-05)

### 🐞 Bug Fixes (1 change)

- [Correctly handle projects without configuration in migration](dependabot-gitlab/dependabot@fba27090add140e7b90fa73c07e04e01b98efeb1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1791

## 0.27.4 (2022-10-05)

### 🔬 Improvements (1 change)

- [Add severity label to vulnerability issues](dependabot-gitlab/dependabot@835cbe3d61f1488da28a5111681dd09900d2afd8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1787

### 🐞 Bug Fixes (1 change)

- [Correctly handle dependencies of same name in different ecosystems](dependabot-gitlab/dependabot@0c6312f206a04121cd7d5c38e46bd9f67c4b5fc1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1790

### 📦 Dependency updates (2 changes)

- [dep: bump sentry-sidekiq and sentry-rails](dependabot-gitlab/dependabot@9381f87783841c60fad979baeb1f6d03952484e8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1789
- [dep: bump sidekiq-cron from 1.7.0 to 1.8.0](dependabot-gitlab/dependabot@99049e12e60c2988d2238c1cb0685dd0535b1ad3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1788

### 🛠️ Chore (1 change)

- [Improve name mismatch handling when matching security advisories](dependabot-gitlab/dependabot@de0777e13083a93ee7884fc5ef83e0da048343ab) by @andrcuns. See merge request dependabot-gitlab/dependabot!1786

## 0.27.3 (2022-10-02)

### 🐞 Bug Fixes (1 change)

- [Correctly handle migration on initial installation](dependabot-gitlab/dependabot@819b0aab9458984cab3b3ecbe2699105dff3036d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1785

## 0.27.2 (2022-10-02)

### 🐞 Bug Fixes (4 changes)

- [Store update job failures in separate model](dependabot-gitlab/dependabot@edf4432a88261dfaa570d954dda90a38ffb9e777) by @andrcuns. See merge request dependabot-gitlab/dependabot!1780
- [Fix incorrect return value on mr creation in some cases](dependabot-gitlab/dependabot@ab2e4818b05bc3fd968c8fe034f91df3b6ec2429) by @andrcuns. See merge request dependabot-gitlab/dependabot!1779
- [Do not stop obsolete mr closing and vulnerability issue creation when open mr limit reached](dependabot-gitlab/dependabot@9ef60a0914c45e5db9ddcf89584d486a21db9b07) by @andrcuns. See merge request dependabot-gitlab/dependabot!1773

### 📦 Development dependency updates (2 changes)

- [dep: bump solargraph from 0.47.1 to 0.47.2](dependabot-gitlab/dependabot@2b926726d407c1b487f37432ebea2428b0a36b68) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1778
- [dep: bump rspec_junit_formatter from 0.5.1 to 0.6.0](dependabot-gitlab/dependabot@cb9a269067ce7c5c4436805ef82d91e6059eeffd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1775

### 🔧 CI changes (2 changes)

- [chore(deps): update registry.gitlab.com/dependabot-gitlab/ci-images:ruby docker digest to 61e257f](dependabot-gitlab/dependabot@7188da379e9a0198c0cc008fef39da72157f7d6c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1776
- [Make sure migration runs successfully in deploy test job](dependabot-gitlab/dependabot@1f1354ab7b45c5e48ef5a757e4196e1d7752119a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1774

### 🛠️ Chore (7 changes)

- [Restore deleted migration](dependabot-gitlab/dependabot@1b2a157724ef7f32bb771ea6398e608ec823d8c8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1781
- [Update github graphql schema](dependabot-gitlab/dependabot@69c811786eb2d6d3f8202f35f832e46192fa4353) by @andrcuns. See merge request dependabot-gitlab/dependabot!1784
- [Correctly unset removed attributes](dependabot-gitlab/dependabot@18194548508709d4203c07bb06c4bc14816c9f6b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1783
- [Update seeded test data](dependabot-gitlab/dependabot@e1f89c5aef900f32ea69c5ebac7b9b3c8a8559b8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1782
- [Clean up old run log and error array objects](dependabot-gitlab/dependabot@fc6cc3d2047bb69e3be80bfb74fe3f11a5cf6e3b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1782
- [Remove old migration for non existing model](dependabot-gitlab/dependabot@5fedd8f929df3debfca89b041526ec35b1d55826) by @andrcuns. See merge request dependabot-gitlab/dependabot!1782
- [Add validation for migrations completed successfully](dependabot-gitlab/dependabot@ca81f2451bcea64a86546a0dbd448db3e272f519) by @andrcuns. See merge request dependabot-gitlab/dependabot!1781
- [Add migration testing](dependabot-gitlab/dependabot@74f24485c2a68ff30120b5c4771e1275f7e02ff8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1777

### 📄 Documentation updates (1 change)

- [Add link to releases in Gitlab container registry](dependabot-gitlab/dependabot@e26c4517548afdcdc648655f60cfe2bfdd2b9563) by @andrcuns.

## 0.27.1 (2022-09-29)

### 🐞 Bug Fixes (1 change)

- [Remove global milestone cache](dependabot-gitlab/dependabot@e06096ebde7ef3e7ac04cfdec4a95c5c45df8b80) by @andrcuns. See merge request dependabot-gitlab/dependabot!1771

### 📦 Dependency updates (1 change)

- [dep: bump yabeda-sidekiq from 0.8.2 to 0.9.0](dependabot-gitlab/dependabot@af818afcb07a7daed35ac06971b1f680bd4c59c4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1769

### 📦 Development dependency updates (2 changes)

- [dep: bump solargraph from 0.47.0 to 0.47.1](dependabot-gitlab/dependabot@6619249aa573b2beb26b53b6202b24d025267b35) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1770
- [dep: bump solargraph from 0.46.0 to 0.47.0](dependabot-gitlab/dependabot@caad51faa3b8f12c62f7703285adc14058412708) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1767

### 🔧 CI changes (8 changes)

- [Fix release jobs and image publishing](dependabot-gitlab/dependabot@beafca15b52f296f76a9281f11e898309a5cd5d3) by @andrcuns.
- [chore(deps): update andrcuns/allure-report-publisher docker tag to v1](dependabot-gitlab/dependabot@e8e198bf7331ce578cfa7c69a15d89bf5db51eda) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1768
- [Add metrics report for standalone test runtime](dependabot-gitlab/dependabot@05efb9f0ef89cf7f2248ffafb3321d7fecc16aad) by @andrcuns. See merge request dependabot-gitlab/dependabot!1765
- [Update registry.gitlab.com/dependabot-gitlab/ci-images:ruby Docker digest to d8d532b](dependabot-gitlab/dependabot@f3fd55540497321749de9953461e11c50c8d44ef) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1766
- [Update registry.gitlab.com/dependabot-gitlab/ci-images:ruby Docker digest to c523584](dependabot-gitlab/dependabot@188249d8125af8af6398d15240f62801cf2c109c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1763
- [Run docker workflows on large runner](dependabot-gitlab/dependabot@8c1ebc4d01bc89b8c197a262b5ef52ba9fb2f670) by @andrcuns. See merge request dependabot-gitlab/dependabot!1762
- [Build multi-arch images on dep changes and master runs](dependabot-gitlab/dependabot@7a466620276f4f7782a9db87414b6fa4fa7796d9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1760
- [Replace regctl with buildx imagetools](dependabot-gitlab/dependabot@90b3a01db982d9b12353369c0405bc3b59b07eee) by @andrcuns. See merge request dependabot-gitlab/dependabot!1761

### 🛠️ Chore (1 change)

- [Add more information to log context](dependabot-gitlab/dependabot@f936c2ade081b7504cf6f3705bc0f309f86af70f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1764

## 0.27.0 (2022-09-23)

### 🔬 Improvements (1 change)

- [Add limited support for arm docker images](dependabot-gitlab/dependabot@dd07bea3ce645ded4f44ef2eeb77024282ad6b6c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1749

### 🐞 Bug Fixes (1 change)

- [Do not save execution log as single document](dependabot-gitlab/dependabot@58dae320ec916f00ed475f401c8a0a45de2ef79e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1756

### 📦 Dependency updates (5 changes)

- [dep: [security] bump commonmarker from 0.23.5 to 0.23.6](dependabot-gitlab/dependabot@fccdd1cabcfbc3612f9db0447fe4397efc70c2f1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1755
- [dep: bump sidekiq from 6.5.6 to 6.5.7](dependabot-gitlab/dependabot@9fb68c757a0de2fdc253308bb61b33131d8ba69c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1748
- [dep: bump yabeda-sidekiq from 0.8.1 to 0.8.2](dependabot-gitlab/dependabot@e3a2a7ccd911cc6ffb4535f9c7dfef6fd3a8c740) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1745
- [dep: bump rubocop-rails from 2.15.2 to 2.16.0](dependabot-gitlab/dependabot@fb6e2ea9e6b2ed102ed766cd50519aa4711ee9b4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1741
- [dep: bump rails from 7.0.3.1 to 7.0.4](dependabot-gitlab/dependabot@cec21b9ef20b41cc13e72f1379024b946340c5ae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1740

### 📦 Development dependency updates (5 changes)

- [dep: bump rubocop-rspec from 2.13.1 to 2.13.2](dependabot-gitlab/dependabot@4d91a65a0dec697d0902fdd71bf4e45d837fdb13) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1754
- [dep: bump spring from 4.0.0 to 4.1.0](dependabot-gitlab/dependabot@2217d726fdc4dfcbafd7ea37033496452c2b2c73) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1751
- [dep: bump rubocop-rails from 2.16.0 to 2.16.1](dependabot-gitlab/dependabot@5fcfba79075616c4c78b06527e21952024844499) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1747
- [dep: bump rubocop-rspec from 2.12.1 to 2.13.1](dependabot-gitlab/dependabot@4fc6e83ce50ae1b53eb16801fcb6342e61c1a10f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1743
- [dep: bump rubocop-performance from 1.14.3 to 1.15.0](dependabot-gitlab/dependabot@d201c1041a623afde12ab7a81e4431424c7fb27e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1742

### 🔧 CI changes (6 changes)

- [Always use buildkit builder for image builds](dependabot-gitlab/dependabot@cb76ba2961a16722e32ab44d5f2bcefee4597110) by @andrcuns. See merge request dependabot-gitlab/dependabot!1758
- [Always generate test report and coverage](dependabot-gitlab/dependabot@951a631ec596609c4f4406c9a51d83797f5496fe) by @andrcuns. See merge request dependabot-gitlab/dependabot!1757
- [chore(deps): update registry.gitlab.com/dependabot-gitlab/ci-images:ruby docker digest to 0ff5817](dependabot-gitlab/dependabot@6586619c1c9d32db731a908e02d73294c7e4227e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1753
- [chore(deps): update registry.gitlab.com/dependabot-gitlab/ci-images:ruby docker digest to 12a9f5a](dependabot-gitlab/dependabot@00710d894a5cddc0855bedaea36397d522e3dc07) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1750
- [Update andrcuns/allure-report-publisher docker tag to v0.11.0](dependabot-gitlab/dependabot@d49397649c0306e76000cc64dfc49b97ef72725a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1746
- [dep: update docker docker tag to v20.10.18](dependabot-gitlab/dependabot@6ef8ff76387097edcd90b48a0da49cc71cd429ff) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1744

## 0.26.1 (2022-09-09)

### 🐞 Bug Fixes (1 change)

- [Fix ui page styles](dependabot-gitlab/dependabot@3786c35655216e828d89a6c83c01acb55bbf51fc) by @andrcuns. See merge request dependabot-gitlab/dependabot!1739

## 0.26.0 (2022-09-09)

### 🚀 New features (1 change)

- [Add app version number to top right corner of UI](dependabot-gitlab/dependabot@c241a2c2ae2f5529a426dd39b858ad36e6d41a3a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1736

### 🔬 Improvements (1 change)

- [Add delay when retrying gitlab api requests](dependabot-gitlab/dependabot@b49690b3acfa690aa5b71802b4a15fffbcc0544a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1737

### 🐞 Bug Fixes (1 change)

- [Respect squash option when auto-merging in standalone mode](dependabot-gitlab/dependabot@f6533501724a20d373280f5733b3e3d026c730c0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1738

### 📦 Dependency updates (3 changes)

- [dep: bump dependabot-omnibus from 0.211.0 to 0.212.0](dependabot-gitlab/dependabot@767a32de00222f96e78a5cc89d24d6adbd2262d1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1735
- [dep: bump rubocop from 1.35.1 to 1.36.0](dependabot-gitlab/dependabot@a497ffd9897eebe82255c744c85bfa077c33bda2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1734
- [dep: bump sidekiq from 6.5.5 to 6.5.6](dependabot-gitlab/dependabot@4fe8db178713aab3438f2ef665b6a381c4648e7c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1732

### 📦 Development dependency updates (1 change)

- [dep: bump faker from 2.22.0 to 2.23.0](dependabot-gitlab/dependabot@6d129c4c7fb3a2cfda6dd6a6fa6132bba94119a9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1733

### 📄 Documentation updates (1 change)

- [Add various private gitlab registries usage examples](dependabot-gitlab/dependabot@6d5667c5a537d6490345635f358aabf17470edfd) by @andrcuns. See merge request dependabot-gitlab/dependabot!1730

## 0.25.2 (2022-08-29)

### 🐞 Bug Fixes (1 change)

- [Set rails env to production by default](dependabot-gitlab/dependabot@299674778fa82b9c2764736165229b248d0cdcd5) by @andrcuns. See merge request dependabot-gitlab/dependabot!1729

## 0.25.1 (2022-08-28)

### 🐞 Bug Fixes (3 changes)

- [Return correct recreate command result status](dependabot-gitlab/dependabot@6adc173f27d98e2c7be3ee73a1aa8066311456ac) by @andrcuns. See merge request dependabot-gitlab/dependabot!1728
- [Correctly handle mr recreate when dependency is up to date](dependabot-gitlab/dependabot@369fc566dc3d717793a3e129c851356291a1c778) by @andrcuns. See merge request dependabot-gitlab/dependabot!1727
- [Use correct gitlab access token on mr recreate](dependabot-gitlab/dependabot@f4840a00b452753d60cecdc61e34127886ec04bc) by @andrcuns. See merge request dependabot-gitlab/dependabot!1725

### 📦 Dependency updates (4 changes)

- [dep: bump dependabot-omnibus from 0.209.0 to 0.211.0](dependabot-gitlab/dependabot@da1239c60cdc3d8a339486f972c9aab2e8cd3d32) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1722
- [dep: bump puma from 5.6.4 to 5.6.5](dependabot-gitlab/dependabot@68390f9ca52506e6aa3823b86f83e77c6b9aeec6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1723
- [dep: bump sidekiq from 6.5.4 to 6.5.5](dependabot-gitlab/dependabot@59c46aeb9ffe8cfdd5809f72d17ba5ef022023bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1720
- [dep: bump solargraph from 0.45.0 to 0.46.0](dependabot-gitlab/dependabot@71b5aef1fe7db368629b10a015a097d52060378f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1719

### 📦 Development dependency updates (2 changes)

- [dep: bump rubocop from 1.35.0 to 1.35.1](dependabot-gitlab/dependabot@b2c87c3b29f7eb4e6354fdde12b61286ec978485) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1718
- [dep: bump git from 1.11.0 to 1.12.0](dependabot-gitlab/dependabot@dcba2a1c0375c24732bd974c4df4892f75dc8237) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1717

### 🔧 CI changes (2 changes)

- [Run dependency and license scan on dependency changes only](dependabot-gitlab/dependabot@a0e37cc00f8435b8a41d4c02e1bbdf69eb2aca6d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1726
- [chore(deps): update registry.gitlab.com/dependabot-gitlab/ci-images:ruby docker digest to cec6af7](dependabot-gitlab/dependabot@0b538bcc85192c06dbbaba222516007ddbdbb487) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1721

### 🛠️ Chore (1 change)

- [Ignore warning generated by sidekiq](dependabot-gitlab/dependabot@e9d023da223b50148b6546682ae3ffd070b5d397) by @andrcuns. See merge request dependabot-gitlab/dependabot!1724

## 0.25.0 (2022-08-18)

### 🚀 New features (1 change)

- [Support for project specific gitlab access tokens](dependabot-gitlab/dependabot@81affd260497ac472de946e0c30cf30f79a8b7f9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1676

### 🔬 Improvements (3 changes)

- [Add improved error message if project is missing default branch or repository](dependabot-gitlab/dependabot@4faecf9458355e7588dd8d3b74484100e678d213) by @andrcuns. See merge request dependabot-gitlab/dependabot!1713
- [Add custom ruby warning processing](dependabot-gitlab/dependabot@b4e36fdecf8ddc786860c40173c0498b852db023) by @andrcuns. See merge request dependabot-gitlab/dependabot!1696
- [Normalize log messages saved in database](dependabot-gitlab/dependabot@b6b5c2cb7b7bcbb229a18c890a20244a6646d99b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1681

### 🐞 Bug Fixes (5 changes)

- [Correctly handle non existing project when processing webhooks](dependabot-gitlab/dependabot@e5082990288da3fe83c2d6abeda1771532bcc73b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1690
- [Use project specific gitlab access token when processing webhooks](dependabot-gitlab/dependabot@99407ea3c4c97a77bb74b0231051e0ab89d08553) by @andrcuns. See merge request dependabot-gitlab/dependabot!1685
- [Fixup logging message saving](dependabot-gitlab/dependabot@c205a8eeff3fa17b795c53ceb3c3cbe9485b5cc2) by @andrcuns.
- [Correctly persist dependency update job log in database](dependabot-gitlab/dependabot@0e8865c7495eaca279dcc4dd6c6c5321b2deb4b4) by @andrcuns. See merge request dependabot-gitlab/dependabot!1679
- [Use correct gitlab access token for project sync in UI](dependabot-gitlab/dependabot@a8f1127d8db82bb234e4f22051739f3ee865e3b7) by @andrcuns. See merge request dependabot-gitlab/dependabot!1678

### 📦 Dependency updates (12 changes)

- [dep: bump sentry-sidekiq from 5.4.1 to 5.4.2](dependabot-gitlab/dependabot@b36be124c74dfb59fe3c232c28cfb4ba19f2ba25) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1716
- [dep: bump sentry-rails from 5.4.1 to 5.4.2](dependabot-gitlab/dependabot@57df4584c8b916ef8dd01f315455a9bd13ede283) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1715
- [dep: bump dependabot-omnibus from 0.208.0 to 0.209.0](dependabot-gitlab/dependabot@3f09d20110d9a42f4057e1d7982e7bb09e2be0c9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1714
- [dep: bump dependabot-omnibus from 0.207.0 to 0.208.0](dependabot-gitlab/dependabot@c70d25aecfd2c2935dd8d21cc93a24e053f9d8f5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1711
- [dep: bump pry-byebug from 3.10.0 to 3.10.1](dependabot-gitlab/dependabot@94f371444ac40df900cccbb63bd3cad745c58d80) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1712
- [dep: bump dependabot-omnibus from 0.204.0 to 0.207.0](dependabot-gitlab/dependabot@fcf91dfd5a22965d45cf61b44353e395620b2e9a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1704
- [dep: bump rubocop from 1.33.0 to 1.34.1](dependabot-gitlab/dependabot@217d7bf3450a11c29390a54805fca7e83d205620) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1700
- [dep: bump sidekiq from 6.5.3 to 6.5.4](dependabot-gitlab/dependabot@be509cbc9d869fa27296c94cc58005407da59836) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1699
- [dep: bump rubocop from 1.32.0 to 1.33.0](dependabot-gitlab/dependabot@3176f4d66c2f3508281f6663891f1dbce6feea00) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1689
- [dep: bump sidekiq from 6.5.1 to 6.5.3](dependabot-gitlab/dependabot@1e6954c7a7351eda64af29ce3a925820ab124d25) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1688
- [dep: bump dependabot-omnibus from 0.202.0 to 0.204.0](dependabot-gitlab/dependabot@afffa2056eb65c11d9d713090a4aa4b6835e3efe) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1683
- [dep: bump sentry-sidekiq from 5.3.1 to 5.4.1](dependabot-gitlab/dependabot@59d84fb334fb3b8621b48629d65835cd669226c7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1674

### 📦 Development dependency updates (2 changes)

- [dep: bump pry-byebug from 3.9.0 to 3.10.0](dependabot-gitlab/dependabot@dca728d42c9b68e34006b3e4020238ae792284c5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1710
- [dep: bump rubocop from 1.34.1 to 1.35.0](dependabot-gitlab/dependabot@2828dfaa735f90148e93eaef3ddda10c612eeed0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1707

### 🔧 CI changes (11 changes)

- [chore(deps): update andrcuns/allure-report-publisher docker tag to v0.10.0](dependabot-gitlab/dependabot@0f9515f14c47340ba5a2d602e9c378ef03e59f72) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1709
- [chore(deps): update registry.gitlab.com/dependabot-gitlab/ci-images:ruby docker digest to 82a0fce](dependabot-gitlab/dependabot@d1ec173d5af18b95ac2d71bc979bc606f3a7bee6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1708
- [Add initial support for multi-platform image building](dependabot-gitlab/dependabot@fc4b5ab91bd794221c05146d55403facc2e2375a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1706
- [Remove unused container scanning variable](dependabot-gitlab/dependabot@cda826b8fd25203286eb7da29a5a06f58c6b18fe) by @andrcuns. See merge request dependabot-gitlab/dependabot!1703
- [Use specific sha-id for default ci image](dependabot-gitlab/dependabot@3f9f69237ea304bcb459741b5c7c766e7617c4e0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1703
- [Skip coverage upload on failed pipeline](dependabot-gitlab/dependabot@467fb8825a347c4d3d0bce4119e276585ce28d88) by @andrcuns.
- [Update codacy reporter version and caching](dependabot-gitlab/dependabot@8b32907d17549fbac520fa06a6946b46f3f09d3c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1695
- [Add docker image container scan job](dependabot-gitlab/dependabot@437fc4a4f4623f208ba85d85832c897f52fdf93a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1694
- [chore(deps): update dependency bitnami/mongodb to v6](dependabot-gitlab/dependabot@0b71d16d3126cece1fc186e360602ae53ec6880c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1686
- [Use gitlab license scanner job](dependabot-gitlab/dependabot@c800f0d1b0e7409efb152f11f98f3ab42d0580cf) by @andrcuns. See merge request dependabot-gitlab/dependabot!1680
- [Add license scanning job](dependabot-gitlab/dependabot@7fa7b49faa07fb4c8bc32f049be865159e42b502) by @andrcuns. See merge request dependabot-gitlab/dependabot!1677

### 🛠️ Chore (10 changes)

- [Add option to print next changelog](dependabot-gitlab/dependabot@f7b2e47ea15ec787ddca28036fe92040db7d967b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1705
- [Set warning processing after logger is set up](dependabot-gitlab/dependabot@22dcbd9e5279ffa94d001a47cb6ebc3a567ae147) by @andrcuns. See merge request dependabot-gitlab/dependabot!1703
- [Add upgradable info to container scan output](dependabot-gitlab/dependabot@dc3e9aacff15622831142e0ac97c244aceb685d5) by @andrcuns. See merge request dependabot-gitlab/dependabot!1697
- [Fix order dependant spec](dependabot-gitlab/dependabot@6fb9a01879ab6771678da16587f27c67946d241b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1684
- [Remove redundant methods and wrappers](dependabot-gitlab/dependabot@dcc1e389bf595300bec3b0f39c8052e21e7b857d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1682
- [Remove sentry-ruby top level dependency](dependabot-gitlab/dependabot@e33a9271b59b02eed51b6e03856cdd82fd9b2c8a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1675
- [Add request store for global stores](dependabot-gitlab/dependabot@83f8014b3717331ba5a50df4bb00deb76be9a816) by @andrcuns. See merge request dependabot-gitlab/dependabot!1673
- [Extract common dependency update logic to common class](dependabot-gitlab/dependabot@3c7f5f5eec3d06f6fedbb27471ac2d7e76183ac9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1672
- [Extract credentials passing](dependabot-gitlab/dependabot@41b11d5d56242db46c4ca7ed5837eaa9d460c3ce) by @andrcuns. See merge request dependabot-gitlab/dependabot!1671
- [Extract common webhook handling logic](dependabot-gitlab/dependabot@120234dfdfb1fb45a6b3e6f69cbdb900b865471e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1670

### 📄 Documentation updates (1 change)

- [Document gitlab webhook token configuration](dependabot-gitlab/dependabot@fea429600f55ff3046e501a052b5799165f8e311) by @andrcuns. See merge request dependabot-gitlab/dependabot!1669

### dependencies (1 change)

- [Add faraday-retry gem](dependabot-gitlab/dependabot@73f1927bdaf67db514eafd0e362d8e82e88a0bc3) by @andrcuns.

## 0.24.0 (2022-07-29)

### 🚀 New features (1 change)

- [Add dry-run option](dependabot-gitlab/dependabot@d60f078eb9031cabc80c8a8b6d2e7fa5392c7073) by @andrcuns. See merge request dependabot-gitlab/dependabot!1648

### 🐞 Bug Fixes (2 changes)

- [Correctly substitute multiple env var values in auth fields](dependabot-gitlab/dependabot@2a91e4e57655fc99a0d1b1f9f4a8beeaa1338296) by @andrcuns. See merge request dependabot-gitlab/dependabot!1652
- [Convert python registry basic auth to token format](dependabot-gitlab/dependabot@5791c32e665665c65e97e0f49d9dfc24451487f2) by @andrcuns. See merge request dependabot-gitlab/dependabot!1650

### 📦 Dependency updates (14 changes)

- [dep: bump faker from 2.21.0 to 2.22.0](dependabot-gitlab/dependabot@edc7cb7f1d6dcbbd5475e7cd9a8771c42312ef0a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1668
- [dep: bump bootsnap from 1.12.0 to 1.13.0](dependabot-gitlab/dependabot@d9cf3ab46d09e6d53ded149c63c7fc699fb0d8be) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1667
- [dep: bump sentry-rails and sentry-ruby](dependabot-gitlab/dependabot@ee6b10a74b28cdf6368273d616f67895465793bd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1666
- [dep: bump dependabot-omnibus from 0.201.1 to 0.202.0](dependabot-gitlab/dependabot@b598c4b80f1f95b1cd4da6795d373a39286b0abc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1665
- [dep: bump dependabot-omnibus from 0.201.0 to 0.201.1](dependabot-gitlab/dependabot@a1c1fe424e5b40ccca227bf6542600dc06c5f0a1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1664
- [dep: bump sidekiq-cron from 1.6.0 to 1.7.0](dependabot-gitlab/dependabot@2c8b753818893738a00050e0cbb912646b6fb6e0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1663
- [dep: bump mongoid from 8.0.1 to 8.0.2](dependabot-gitlab/dependabot@2caa2bfdb0bb4c038e9126ce9f61abc0eb366bdc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1662
- [dep: bump dependabot-omnibus from 0.200.0 to 0.201.0](dependabot-gitlab/dependabot@3dbc708f82a0b2bcd7406421be2549d6be1890ce) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1661
- [dep: bump rubocop from 1.31.2 to 1.32.0](dependabot-gitlab/dependabot@f43a633e26ab6aa0e3013155d7a510989a40de67) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1660
- [dep: bump dependabot-omnibus from 0.199.0 to 0.200.0](dependabot-gitlab/dependabot@91976a1e2cf4a3dab069062ba38c2bc9ad569480) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1659
- [dep: bump mongoid from 7.4.0 to 8.0.1](dependabot-gitlab/dependabot@2fe99b487fb1d0ed80dfc82671513161f328634f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1658
- [dep: bump dependabot-omnibus from 0.198.0 to 0.199.0](dependabot-gitlab/dependabot@91044603ecbff502a357d0dee0bc322ebdba630b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1656
- [dep: Bump redis from 4.7.0 to 4.7.1](dependabot-gitlab/dependabot@e9520693dc49cd16f81723d36439b78b113c6a38) by @andrcuns. See merge request dependabot-gitlab/dependabot!1654
- [dep: bump dependabot-omnibus from 0.197.0 to 0.198.0](dependabot-gitlab/dependabot@979cfb780fed65d72151082b1fb70ac5e734cdc9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1647

### 📦 Development dependency updates (1 change)

- [dep: bump rubocop-performance from 1.14.2 to 1.14.3](dependabot-gitlab/dependabot@b7047f8a54cffe70edbf56bb13fdaf4f9742f511) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1655

### 🔧 CI changes (1 change)

- [chore(deps): update dependency andrcuns/allure-report-publisher to v0.9.0](dependabot-gitlab/dependabot@a02c764d7c8300759e7158d8b610e480ba24cd7b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1651

### 🛠️ Chore (2 changes)

- [Remove deprecated method usage](dependabot-gitlab/dependabot@5b58dfc134603b0accd232cbb540ed6fca52e01c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1653
- [Add support for building multiple core helpers](dependabot-gitlab/dependabot@96fd5ee30966f3507608f763bc470068874bfadb) by @andrcuns. See merge request dependabot-gitlab/dependabot!1649

## 0.23.0 (2022-07-15)

### 🚀 New features (2 changes)

- [Add option to set different commit trailers for dev dependency mrs](dependabot-gitlab/dependabot@71b365dc5100630a0ebbab807119e774f6effe45) by @andrcuns. See merge request dependabot-gitlab/dependabot!1638
- [Add option to set different commit trailers for security mrs](dependabot-gitlab/dependabot@77aeac540edaf9aead636bada8ebc59a1f2f1f83) by @andrcuns. See merge request dependabot-gitlab/dependabot!1637

### 📦 Dependency updates (11 changes)

- [dep: bump dependabot-omnibus from 0.196.4 to 0.197.0](dependabot-gitlab/dependabot@3a511f78342dfc2a3b15b7a5016437c89a0a4411) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1645
- [Bump debian version for redis and mongodb](dependabot-gitlab/dependabot@cdc263621c3173880f10fdca24828d8fbf9367f8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1644
- [dep: Update dependency bitnami/redis to v7](dependabot-gitlab/dependabot@702836e8652a71dfc598d9d0fbe417250ca074bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1587
- [dep: bump dependabot-omnibus from 0.196.3 to 0.196.4](dependabot-gitlab/dependabot@b4d1b4a1649f3c705f6766a4d0ba1be9f2ede1d5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1643
- [dep: bump dependabot-omnibus and gitlab](dependabot-gitlab/dependabot@d06e67af21f2f6e617d91175c1d90520d8c45db8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1640
- [dep: bump rails from 7.0.3 to 7.0.3.1](dependabot-gitlab/dependabot@f1f491584d8aa3e58980dc6da0bbb619d35a950a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1642
- [dep-dev: bump rubocop-rails from 2.15.1 to 2.15.2](dependabot-gitlab/dependabot@d2fc4ea1ee5e40afa4fb9efcd35653a2de5f5f15) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1636
- [dep-dev: bump rubocop from 1.31.1 to 1.31.2](dependabot-gitlab/dependabot@ef081ed22f1912fa774736f9d178a83d67533f17) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1635
- [dep: [security] bump rails-html-sanitizer from 1.4.2 to 1.4.3](dependabot-gitlab/dependabot@27533e075249b79260eb88bed52969ec82260d06) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1634
- [dep-dev: bump rubocop-rspec from 2.12.0 to 2.12.1](dependabot-gitlab/dependabot@951e7378d79b628f8b4ef6a04919d87e127ac875) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1632
- [dep-dev: bump rubocop-rspec from 2.11.1 to 2.12.0](dependabot-gitlab/dependabot@edd2b50370fae8185fe5701cf0d1def704de0a7d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1631

### 🛠️ Chore (2 changes)

- [Print changelog data when bumping version](dependabot-gitlab/dependabot@b0a4742f750e1ef891cb91698b5fd78e54d2c8fb) by @andrcuns. See merge request dependabot-gitlab/dependabot!1646
- [Deprecate branch name configurations via environment variables](dependabot-gitlab/dependabot@6288dfe781d4c7f5bd1658eccdd9f7c24af0a2e5) by @andrcuns. See merge request dependabot-gitlab/dependabot!1633

## 0.22.4 (2022-07-01)

### 📦 Dependency updates (4 changes)

- [dep: bump sidekiq-cron from 1.5.1 to 1.6.0](dependabot-gitlab/dependabot@8a7505141b519b34b91f1badd1ad585ed77335c2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1629
- [dep: bump dependabot-omnibus from 0.196.0 to 0.196.2](dependabot-gitlab/dependabot@49025cdb2298d5af41cee6e3d09d0eefb56c71e9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1628
- [dep-dev: bump rubocop from 1.30.1 to 1.31.1](dependabot-gitlab/dependabot@7c0c0f266f0a7f532f2ec7088292d3c6f926e3a2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1627
- [dep-dev: bump rubocop-rails from 2.15.0 to 2.15.1](dependabot-gitlab/dependabot@eb2a27780ad615d69c9829aaf5d0d930ed8caa11) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1624

## 0.22.3 (2022-06-25)

### 🐞 Bug Fixes (1 change)

- [Correctly set config for auto-merge: true option](dependabot-gitlab/dependabot@be8ac64bfadd6b2fd85a5ec0dbaf8080afd2e3a4) by @andrcuns. See merge request dependabot-gitlab/dependabot!1622

### 📦 Dependency updates (2 changes)

- [dep: bump dependabot-omnibus from 0.194.1 to 0.196.0](dependabot-gitlab/dependabot@707411f6d55dbdcb772c02355ed94693b0a7b48e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1623
- [dep: bump dependabot-omnibus from 0.194.0 to 0.194.1](dependabot-gitlab/dependabot@b42d54dd1462a385f5705a93decbf401f1f517eb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1621

## 0.22.2 (2022-06-21)

### 🐞 Bug Fixes (1 change)

- [Do not set auto-merge to true by default](dependabot-gitlab/dependabot@da98d17d3000ee74a7bfd6c5d2b7a693348ebce3) by @andrcuns. See merge request dependabot-gitlab/dependabot!1620

## 0.22.1 (2022-06-20)

### 🐞 Bug Fixes (1 change)

- [Correctly handle merge errors when auto merging dependency updates](dependabot-gitlab/dependabot@9a5b36ff078f42d1eefb17ce9b0c0036ad363d12) by @andrcuns. See merge request dependabot-gitlab/dependabot!1617

### 📦 Dependency updates (5 changes)

- [dep: bump dependabot-omnibus from 0.193.0 to 0.194.0](dependabot-gitlab/dependabot@53452c4e2fac43d85208ee13a06f97861041dc55) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1616
- [dep: bump dependabot-omnibus from 0.192.1 to 0.193.0](dependabot-gitlab/dependabot@c4093853ccfc881e2995fbd1e1041a0f049f9bb1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1614
- [dep: bump sidekiq from 6.5.0 to 6.5.1](dependabot-gitlab/dependabot@cbbdd6136a89a729ab699e1cbcded6590ab86a7a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1613
- [dep: bump dependabot-omnibus from 0.192.0 to 0.192.1](dependabot-gitlab/dependabot@c2bed200ca6a252165ae64f2e1ffe41b7fdf4fa9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1611
- [dep-dev: bump rubocop-rails from 2.14.2 to 2.15.0](dependabot-gitlab/dependabot@22309a121334163ee0b7ff490abdd5b66a90ceb3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1612

## 0.22.0 (2022-06-14)

### 🚀 New features (1 change)

- [Support for optional base configuration template](dependabot-gitlab/dependabot@06a642060f5274f96865f685eaad5243d1cf92a0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1608

### 📦 Dependency updates (1 change)

- [dep: bump dependabot-omnibus from 0.191.0 to 0.192.0](dependabot-gitlab/dependabot@8ba1a2aa1df57224de444b44cc418554f27dee28) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1607

### 🛠️ Chore (2 changes)

- [Raise error on incorrect base config updates definition](dependabot-gitlab/dependabot@c41c5de4d655def2178dbc48c8ed7f08a1ef222e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1610
- [Rename base configuration file option](dependabot-gitlab/dependabot@cd1a9f18acd54289941b9113550aa6ba0805dc98) by @andrcuns. See merge request dependabot-gitlab/dependabot!1609

## 0.21.1 (2022-06-11)

### 🔬 Improvements (1 change)

- [Add option to disable vulnerability alerts](dependabot-gitlab/dependabot@837c796e2a0f74a092b47f2625e700f2d143caaf) by @andrcuns. See merge request dependabot-gitlab/dependabot!1604

### 🐞 Bug Fixes (1 change)

- [Fetch correct obsolete vulnerability issues](dependabot-gitlab/dependabot@8c5448f20fcea45692076ed04377bbfffca54278) by @andrcuns. See merge request dependabot-gitlab/dependabot!1605

### 📦 Dependency updates (6 changes)

- [dep: bump sidekiq-cron from 1.5.0 to 1.5.1](dependabot-gitlab/dependabot@c34466fad311c95d67d9bcae4a4acbf38dff7a5f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1606
- [dep: bump sidekiq from 6.4.2 to 6.5.0](dependabot-gitlab/dependabot@a865fa06ec452d92664fc581c0b7a1d418c23073) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1600
- [dep: bump sidekiq-cron from 1.4.0 to 1.5.0](dependabot-gitlab/dependabot@1853b0c35558008e18c9910cb4a0ced95cfe7a64) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1602
- [dep-dev: bump rubocop-performance from 1.14.1 to 1.14.2](dependabot-gitlab/dependabot@f23ff1735357adbec8a1ec3e9a965781d48f3e38) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1601
- [dep: bump dependabot-omnibus from 0.190.1 to 0.191.0](dependabot-gitlab/dependabot@c6ffede174b97b86a3ace4264e1bdf3394862273) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1599
- [dep-dev: bump rubocop from 1.30.0 to 1.30.1](dependabot-gitlab/dependabot@2817a34317208235a3e9bc8e961cf4df452d7766) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1597

### 🔧 CI changes (2 changes)

- [Pin minor docker version in CI](dependabot-gitlab/dependabot@35744dfc674897b54ddcb45ec2a9c1784eb82d8d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1603
- [Update dependency docker to v20.10.17](dependabot-gitlab/dependabot@5417a0727a44a750a933c82df1f08d8b5f06b5bb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1598

## 0.21.0 (2022-06-06)

### 🚀 New features (1 change)

- [Add dependabot-core http request logging](dependabot-gitlab/dependabot@f9f12b32add92ee71aced982a3703fd3eea65b1f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1585

### 🔬 Improvements (2 changes)

- [Pretty print helpers output](dependabot-gitlab/dependabot@6a2bafc2ce748d2ca95da0308d111abfb87b597d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1591
- [Log dependabot core parsed package manager version](dependabot-gitlab/dependabot@67cd250ea23db003050079d6156ee36c37aae4f2) by @andrcuns. See merge request dependabot-gitlab/dependabot!1590

### 🐞 Bug Fixes (1 change)

- [Correct open mr url for monorepos with multiple same package manager directories](dependabot-gitlab/dependabot@663d04dea95b1c3ff8e6017374732f7465b9952d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1589

### 📦 Dependency updates (8 changes)

- [dep-dev: bump rubocop-performance from 1.14.0 to 1.14.1](dependabot-gitlab/dependabot@0e540defae724a4a5939de534aa58028432e7c9d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1596
- [Bump jemmaloc version to 5.3.0](dependabot-gitlab/dependabot@17ddc18c1d6caa0b126fec77b6f7bc81ea47d2c1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1588
- [dep: bump dependabot-omnibus from 0.190.0 to 0.190.1](dependabot-gitlab/dependabot@9056b9644d63640ad0be2c6526e8aba3c280535f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1586
- [dep: bump bootsnap from 1.11.1 to 1.12.0](dependabot-gitlab/dependabot@94ecb8526a57f5de4ccc43e598edcc388acd682e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1584
- [dep: [security] bump rack from 2.2.3 to 2.2.3.1](dependabot-gitlab/dependabot@2846bd65201d5d1016d2f94c19168c2c31a3f386) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1579
- [dep: bump dry-validation from 1.8.0 to 1.8.1](dependabot-gitlab/dependabot@9473e81c5b20e7313f5f2396b24217ef34cf8902) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1578
- [dep-dev: bump rubocop from 1.29.1 to 1.30.0](dependabot-gitlab/dependabot@708428779e2aeaef3f6fc11bd14d67857c53f9df) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1577
- [dep-dev: bump rubocop-performance from 1.13.3 to 1.14.0](dependabot-gitlab/dependabot@3f34fd31d0eae1dbebd4525649a1d909588ad91e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1575

### 🔧 CI changes (6 changes)

- [Simplify docker runner args](dependabot-gitlab/dependabot@b67b1c16e237d056d7bc138dc49e679f3a45abe8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1594
- [Move dependency cache to build stage](dependabot-gitlab/dependabot@ce7bdf4789169b0668cd0facc55cc3ad4f4ff52f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1595
- [Always run all tests](dependabot-gitlab/dependabot@cca5da11ff97888c5fc6d39dc56a02802e1a5577) by @andrcuns. See merge request dependabot-gitlab/dependabot!1593
- [Use separate token for allure test reports](dependabot-gitlab/dependabot@3f8847012dcd96ea3cc39c282732f4a7dd07a9d3) by @andrcuns.
- [dep-dev: Update dependency andrcuns/allure-report-publisher to v0.8.0](dependabot-gitlab/dependabot@f9dea4875674d49dc81cf97ab88078f406dd3f52) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1582
- [Remove ci image build](dependabot-gitlab/dependabot@6d9b2aa128638d75c78eec0c127c0f90fa37df6d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1580

### 🛠️ Chore (5 changes)

- [Replace credentials value in helper command output](dependabot-gitlab/dependabot@0d584a470d99e22b472f7788ee9ea8f9cbf06eb7) by @andrcuns. See merge request dependabot-gitlab/dependabot!1592
- [Refactor log helper method](dependabot-gitlab/dependabot@62ea2b1a720dbace1f35d7a0d1b2c91b4afe9fba) by @andrcuns. See merge request dependabot-gitlab/dependabot!1592
- [Add testing rake tasks](dependabot-gitlab/dependabot@51d0121c9d9da6d835b414706fc0736e2d468d94) by @andrcuns. See merge request dependabot-gitlab/dependabot!1583
- [Store update job log entries in database as hash](dependabot-gitlab/dependabot@87d77ec33a4f98ca7a03a0c976541cecdd89689b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1583
- [Migrate to main branch](dependabot-gitlab/dependabot@c2e35a922f54d17f81a071ea4bab2700ad6250e8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1581

## 0.20.2 (2022-05-24)

### 🐞 Bug Fixes (2 changes)

- [Add missing rust package mapping for security vulnerabilities](dependabot-gitlab/dependabot@fb9b6ba8051180fe15112e2bd8045d81ee4daa24) by @andrcuns. See merge request dependabot-gitlab/dependabot!1573
- [Only evaluate explicitly allowed registries when fetching config](dependabot-gitlab/dependabot@62244bdeabca0641e1bd8c7335a35978a840814a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1568

### 📦 Dependency updates (4 changes)

- [dep-dev: bump allure-rspec from 2.17.0 to 2.18.0](dependabot-gitlab/dependabot@3ad5dd6853c1d77a73a17a46cbd23b41d9ed83c5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1571
- [dep-dev: bump solargraph from 0.44.3 to 0.45.0](dependabot-gitlab/dependabot@e7aa89a9869f3b703c330f09d5549c9eca58f70b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1572
- [dep: bump dependabot-omnibus from 0.189.0 to 0.190.0](dependabot-gitlab/dependabot@ea0ca1f5ef4f8c34d23d2b9e1d507eea826a5ced) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1570
- [dep-dev: bump rubocop-rspec from 2.10.0 to 2.11.1](dependabot-gitlab/dependabot@be5bc4707772d208d6c63fc4e87d9633e1000889) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1559

### 🔧 CI changes (2 changes)

- [Set all ci jobs as interruptible by default](dependabot-gitlab/dependabot@7e8276a4048723b70a6aedfd1f801238a5b292ce) by @andrcuns. See merge request dependabot-gitlab/dependabot!1567
- [Build docker images with buildx docker plugin](dependabot-gitlab/dependabot@8efcc057b455bc630f4d40beea7efe7eeb39b353) by @andrcuns. See merge request dependabot-gitlab/dependabot!1562

### 🛠️ Chore (6 changes)

- [Remove thread from log message of standalone mode](dependabot-gitlab/dependabot@8b687b7a2457870684b339c913240f7cd1935fbe) by @andrcuns. See merge request dependabot-gitlab/dependabot!1569
- [[BREAKING] Disable metrics endpoint by default](dependabot-gitlab/dependabot@9df5fc9d1d2aa38bd3051ef037ad004f53f91bfa) by @andrcuns. See merge request dependabot-gitlab/dependabot!1566
- [Fix dependency update log message saving](dependabot-gitlab/dependabot@5ec98b84f889b598f62f479caa79e6c906e96eaf) by @andrcuns. See merge request dependabot-gitlab/dependabot!1565
- [Reuse same logger instance for sidekiq](dependabot-gitlab/dependabot@0f822be11987627db75c005c847825a7b8cd2f79) by @andrcuns. See merge request dependabot-gitlab/dependabot!1564
- [Persist dependency update job log in database](dependabot-gitlab/dependabot@3e08bec71b9750413313c25be1d6337b094e7c04) by @andrcuns. See merge request dependabot-gitlab/dependabot!1561
- [Move db querries to model classes](dependabot-gitlab/dependabot@a932ba8ee93f98266c8ca0fe55ea5da37d547e8e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1560

## 0.20.1 (2022-05-17)

### 🐞 Bug Fixes (1 change)

- [Correctly handle vulnerabilities without patched version](dependabot-gitlab/dependabot@6d37bdd9e705407f4644ce933b535a8df4b625b1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1557

### 📦 Dependency updates (3 changes)

- [dep: bump dependabot-omnibus from 0.188.0 to 0.189.0](dependabot-gitlab/dependabot@60e31c572115e204af2c4f83385eb61c168ea110) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1558
- [dep: bump sentry-rails, sentry-ruby and sentry-sidekiq](dependabot-gitlab/dependabot@9b49517843fa3103147eefad3c7a37c4c68ae5ac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1555
- [dep: bump dependabot-omnibus from 0.187.0 to 0.188.0](dependabot-gitlab/dependabot@f8b8fa47a1e9cea1ba2960fcce9a5df4de7345ac) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1554

### 🔧 CI changes (2 changes)

- [dep-dev: Update docker to 20.10.16](dependabot-gitlab/dependabot@7aaad62a788c34075eee3e0ffb8459339ea0a47d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1556
- [dep-dev: Update dependency docker to v20.10.16](dependabot-gitlab/dependabot@4f2979099eb366807c79c3c4403441fe03dc1707) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1550

## 0.20.0 (2022-05-16)

### 🚀 New features (1 change)

- [Security vulnerability alerts](dependabot-gitlab/dependabot@e536cbdd966b7482921bffac3b0c0ce0dadf3ab0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1540

### 🔬 Improvements (3 changes)

- [Add configurable assignee for vulnerability issues](dependabot-gitlab/dependabot@4fa9a92b918bf6d3433773be89526d51791c94a5) by @andrcuns. See merge request dependabot-gitlab/dependabot!1548
- [Add webhook to close vulnerability issue in local database](dependabot-gitlab/dependabot@1ae97867bfa6c241dc6feb868c0b574ec1dfaf45) by @andrcuns. See merge request dependabot-gitlab/dependabot!1546
- [Automatically close obsolete vulnerability issues](dependabot-gitlab/dependabot@dd0d29f42551ab8f7e3067e12759d6e41a6c63b1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1541

### 📦 Dependency updates (9 changes)

- [dep-dev: bump faker from 2.20.0 to 2.21.0](dependabot-gitlab/dependabot@b76b91132646245592bee3796e14fcc7ff99694c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1553
- [dep: bump dependabot-omnibus from 0.186.1 to 0.187.0](dependabot-gitlab/dependabot@8f406bd9c95c4bd292fd2b471ed69ea9eb57ad14) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1552
- [dep-dev: bump rubocop from 1.29.0 to 1.29.1](dependabot-gitlab/dependabot@503c5897c26b711e048e41c63e8bc919482b1969) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1551
- [dep: bump dependabot-omnibus from 0.185.0 to 0.186.1](dependabot-gitlab/dependabot@925e946498c242d01e63acb3cad01aa76bd1579e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1547
- [dep-dev: bump capybara from 3.37.0 to 3.37.1](dependabot-gitlab/dependabot@f13f74588f7060a5800685112ffd745368a3853e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1545
- [dep: bump rails from 7.0.2.4 to 7.0.3](dependabot-gitlab/dependabot@5fe98f7158dac95b2ebe170f927a2d97d51f369d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1544
- [dep: bump dependabot-omnibus from 0.184.0 to 0.185.0](dependabot-gitlab/dependabot@5f38f400d4b4ba864c9223f038e53da923579940) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1543
- [dep-dev: bump capybara from 3.36.0 to 3.37.0](dependabot-gitlab/dependabot@2d6e11e0583aecb02d8d69fdd6e4c869413faede) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1542
- [dep-dev: bump rubocop from 1.28.2 to 1.29.0](dependabot-gitlab/dependabot@c821aa7099c5317daf68c2c07b48062cd7369116) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1538

### 🔧 CI changes (2 changes)

- [Update kind to v0.12 and docker to 20.10.15](dependabot-gitlab/dependabot@4c6ad5fadd860fbf221c9c11357c6ea63f40ae0d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1539
- [dep-dev: Update dependency docker to v20.10.15](dependabot-gitlab/dependabot@13078ce86302efdaad4ce2621c637f1a68188643) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1536

### 🛠️ Chore (1 change)

- [Make helpers build script more portable](dependabot-gitlab/dependabot@10da8a2bafa35e69b07836cade6c9a7e3af93a06) by @andrcuns. See merge request dependabot-gitlab/dependabot!1541

### 📄 Documentation updates (1 change)

- [Document security vulnerability alert issues](dependabot-gitlab/dependabot@5ada980f6894db32d4bb9d53cdfcd75503631961) by @andrcuns. See merge request dependabot-gitlab/dependabot!1549

## 0.19.2 (2022-05-07)

### 🐞 Bug Fixes (1 change)

- [Correctly handle projects without config on release_notification](dependabot-gitlab/dependabot@06fcb6a1255187f6f66adad0f37437274b37ed2d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1535

### 📦 Dependency updates (3 changes)

- [dep-dev: bump rubocop from 1.28.2 to 1.29.0](dependabot-gitlab/dependabot@c821aa7099c5317daf68c2c07b48062cd7369116) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1538
- [dev-dep: Update dependency thiht/smocker to v0.18.2](dependabot-gitlab/dependabot@adae591932d34070e2bc814e42200aac8b6435f5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1532
- [dep: bump dependabot-omnibus from 0.183.0 to 0.184.0](dependabot-gitlab/dependabot@c796c959bf9845ae335a3f86fb4e492a582dc9b1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1531

### 🔧 CI changes (4 changes)

- [dep-dev: Update dependency moby/buildkit to v0.10.3](dependabot-gitlab/dependabot@e4bd9a6c938564ddfe896181c81093f871a1d1e3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1537
- [Fix coverage report publishing](dependabot-gitlab/dependabot@d9e9c8c6fac984deb847c9ffbbe9df04160af15d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1533
- [dep-dev: Update dependency andrcuns/allure-report-publisher to v0.7.0](dependabot-gitlab/dependabot@33407da542644e467746c5d121607fa598bb866d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1530
- [Automatically detect dependabot-core image version](dependabot-gitlab/dependabot@7205072b13d54319e31427a010d5166f6247d28c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1529

## 0.19.1 (2022-05-04)

### 🔬 Improvements (1 change)

- [Add manual rake task to trigger automatic project registration](dependabot-gitlab/dependabot@9e6a050d6eb7e747ef4cd1502191a75373bb94ca) by @andrcuns. See merge request dependabot-gitlab/dependabot!1528

### 🐞 Bug Fixes (1 change)

- [Remove custom ignored sentry error parsing](dependabot-gitlab/dependabot@7b43e3f986a6b9fa858b2065075443657fea5db3) by @andrcuns. See merge request dependabot-gitlab/dependabot!1527

### 📦 Dependency updates (3 changes)

- [dep: bump graphql-client from 0.17.0 to 0.18.0](dependabot-gitlab/dependabot@4146cd07a093d95e1b059a229a7d4ca996ba9788) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1526
- [dep-dev: Update dependency bitnami/mongodb to v5](dependabot-gitlab/dependabot@f4201395989eabf5ffbacec92ddbdb6e5fb490ce) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1524
- [dep: bump dependabot-omnibus from 0.182.4 to 0.183.0](dependabot-gitlab/dependabot@2acf5b6a783ac7de9d58571eccd0e0b0c12078cc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1522

### 🔧 CI changes (2 changes)

- [Use dev prefix for temp images](dependabot-gitlab/dependabot@d7df0701c68dd92e40e87d83a8d474e073745919) by @andrcuns. See merge request dependabot-gitlab/dependabot!1525
- [Wait for image release to update dependent repos](dependabot-gitlab/dependabot@5e3e8cb81a24b4d0e5cd5ee0059b8d6c0eacca75) by @andrcuns. See merge request dependabot-gitlab/dependabot!1521

### 📄 Documentation updates (1 change)

- [Update documentation on `CONFIG_BRANCH` setting](dependabot-gitlab/dependabot@f2ee0bdec29c66dda4d13340ccb96f0d5a6b844e) by @andrcuns.

## 0.19.0 (2022-04-29)

### 🚀 New features (3 changes)

- [Add option to ignore certain sentry errors](dependabot-gitlab/dependabot@c1410ddf00bf3f9f7017f674d4a1cb18f7a84d34) by @andrcuns. See merge request dependabot-gitlab/dependabot!1496
- [Add fixed vulnerability info to merge requests](dependabot-gitlab/dependabot@9cfb7c35c1283271c6a7def93ea59153dcff4215) by @andrcuns. See merge request dependabot-gitlab/dependabot!1477
- [Check security advisories when performing dependency update](dependabot-gitlab/dependabot@294d00d5526d783989badfe04e13f1f0fd092644) by @andrcuns. See merge request dependabot-gitlab/dependabot!1477

### 🐞 Bug Fixes (1 change)

- [Consider all dependencies when checking for obsolete mrs](dependabot-gitlab/dependabot@3f80f2366fb5ef3b18dec74ca98a1e3969516fd6) by @andrcuns. See merge request dependabot-gitlab/dependabot!1504

### 📦 Dependency updates (8 changes)

- [dep: bump sidekiq-cron from 1.3.0 to 1.4.0](dependabot-gitlab/dependabot@df0a8731d594866eec77c2c2fd4c8bf99b92ac27) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1518
- [dep: bump sentry-rails, sentry-ruby and sentry-sidekiq](dependabot-gitlab/dependabot@2ba39d5f8d00770ef23b6ec70a974e8074874e59) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1517
- [dep: bump rails from 7.0.2.3 to 7.0.2.4](dependabot-gitlab/dependabot@9c3bacd0f23a43b1ea86efe25856023cbd225e01) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1514
- [dep: bump dependabot-omnibus from 0.182.0 to 0.182.4](dependabot-gitlab/dependabot@169bb14bcdeec315a094fca7fb6de84210b401c1) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1511
- [dep-dev: bump rubocop from 1.28.1 to 1.28.2](dependabot-gitlab/dependabot@d2cf2139409468c8105ff18a7e2a5309297274f0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1508
- [dep-dev: bump rspec-rails from 5.1.1 to 5.1.2](dependabot-gitlab/dependabot@11b2c4895e30873f10e775d3128b30ebd97fe268) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1507
- [dep-dev: Update dependency thiht/smocker to v0.18.1](dependabot-gitlab/dependabot@0f74ca914903ac5a48523c4ba9640ac930b65662) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1499
- [dep-dev: bump rubocop from 1.27.0 to 1.28.1](dependabot-gitlab/dependabot@41556f5b0827c5f6ea8dc0ab6f6700a18bca7503) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1489

### 🔧 CI changes (7 changes)

- [Use latest tag as cache for docker build](dependabot-gitlab/dependabot@d4be6d72aa3b12965fc54a63e62b0010c1bc81e8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1520
- [Remove container-scan job](dependabot-gitlab/dependabot@0eff0a50654de0f37f6b9edb1dacc959eab9294e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1519
- [dep-dev: Update dependency moby/buildkit to v0.10.2](dependabot-gitlab/dependabot@1af632f1ce09b869babb9af5503a6f51d2bae191) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1516
- [Update container scanning options](dependabot-gitlab/dependabot@f5837d1c352135eeb80b3dc624b81d919ddd3991) by @andrcuns. See merge request dependabot-gitlab/dependabot!1515
- [dep-dev: Update dependency andrcuns/allure-report-publisher to v0.6.2](dependabot-gitlab/dependabot@ae1f7ff8ec395551198c30b57952032be6114e12) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1513
- [dep-dev Update dependency andrcuns/allure-report-publisher to v0.6.0](dependabot-gitlab/dependabot@7e512f058a610f0476f234f5e75096f2958d14d7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1502
- [.gitlab-ci and docker-compose dependency updates via renovate](dependabot-gitlab/dependabot@46b13efdf29f9ee5b38c9b59be09651ead07ed36) by @andrcuns. See merge request dependabot-gitlab/dependabot!1498

### 🛠️ Chore (9 changes)

- [Remove external precommit-hooks](dependabot-gitlab/dependabot@78778017bf595c8daed872113dc4008355dca97e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1506
- [Update debug logger messages](dependabot-gitlab/dependabot@5875252e1b40b594a577fa1b07f1dc6913a85cfc) by @andrcuns. See merge request dependabot-gitlab/dependabot!1505
- [Update pre-commit hook jumanjihouse/pre-commit-hooks to v2.1.6](dependabot-gitlab/dependabot@50c03197224e76f43dc05841eaa595c802745cbd) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1503
- [Improve vulnerability database update logging and error handling](dependabot-gitlab/dependabot@ffb860ff71ec9e6acfde8a2d592ccd20a8a4b558) by @andrcuns. See merge request dependabot-gitlab/dependabot!1497
- [Improve github graphql query error handling](dependabot-gitlab/dependabot@01a5b578f729e77a660b5eca242a3eed8169b439) by @andrcuns. See merge request dependabot-gitlab/dependabot!1495
- [Add missing vulnerability sidekiq queue](dependabot-gitlab/dependabot@baa0b44dfbebe3416485851653f77024450e2587) by @andrcuns. See merge request dependabot-gitlab/dependabot!1492
- [Use tagged logger to indicate action instead of class name](dependabot-gitlab/dependabot@147bb952ef42feacaa0e3c984a4e69373ee1893a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1477
- [Improve job logger context setting](dependabot-gitlab/dependabot@4888893223ce670307d4cd04825bf30581b62dac) by @andrcuns. See merge request dependabot-gitlab/dependabot!1477
- [Add capability to fetch vulnerability info from Github](dependabot-gitlab/dependabot@36e1dc50fc0dfb7324639de79bb4e1a58e79dcd9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1477

## 0.18.0 (2022-04-21)

### 🚀 New features (3 changes)

- [Add support for custom updater options](dependabot-gitlab/dependabot@55debb7d07943ea423e7974b1f9dc730bd78e398) by @andrcuns. See merge request dependabot-gitlab/dependabot!1472
- [Add project configuration sync button in UI](dependabot-gitlab/dependabot@5f61e97ad3519d32fbf8af0304dbb4edc061d500) by @andrcuns. See merge request dependabot-gitlab/dependabot!1464
- [Add auto-rebase with-assignee option](dependabot-gitlab/dependabot@88445f45d748db50280de8f099f7b814861bccd8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1458

### 🔬 Improvements (1 change)

- [Improve configured url handling](dependabot-gitlab/dependabot@1a6e9ea642ec56199025348f9f08f3b377754fc4) by @andrcuns. See merge request dependabot-gitlab/dependabot!1460

### 🐞 Bug Fixes (2 changes)

- [Respect config branch option when registering new project](dependabot-gitlab/dependabot@1b29facd766417352a0e39d183bd0531d5bd8e5e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1478
- [Correctly handle reopened mr with restored branch](dependabot-gitlab/dependabot@911a393750b9ea8c6debc7abe23f422ba4b9e6e0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1456

### 📦 Dependency updates (8 changes)

- [dep-dev: bump reek from 6.1.0 to 6.1.1](dependabot-gitlab/dependabot@3dec16b1bb7215a6b08ace4fde32b7efb4b9e3e2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1488
- [dep: bump dependabot/dependabot-core from 0.180.5 to 0.182.0](dependabot-gitlab/dependabot@efee8d15cb828d7f6abf9343af57148910f39839) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1484
- [dep-dev: bump rubocop-rspec from 2.9.0 to 2.10.0](dependabot-gitlab/dependabot@4c05321eefbfa57a78b494324c46c7ad0171b38b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1486
- [dep: bump sidekiq from 6.4.1 to 6.4.2](dependabot-gitlab/dependabot@dca621365b30ef48e05dea8de0178c075a73410e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1485
- [dep-dev: bump git from 1.10.2 to 1.11.0](dependabot-gitlab/dependabot@b838734798a60d5bc7550d21bb8cd9abf2f9cacb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1481
- [Bump nokogiri to 1.13.4](dependabot-gitlab/dependabot@e61f04f17188d8cb050063abb48445ecfb43053c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1471
- [dep-dev: bump ruby in /.gitlab/docker](dependabot-gitlab/dependabot@f069551934ffc86e4463ef5d8ed6e4268bd94ba8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1470
- [dep-dev: bump rubocop from 1.26.1 to 1.27.0](dependabot-gitlab/dependabot@d03524b4c029ad235a5451c6952a98c07f9f6753) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1457

### 🔧 CI changes (13 changes)

- [Move issue triage to toolbox project](dependabot-gitlab/dependabot@90964ca9cbe9fff9d55881d598b47c4388b56ebd) by @andrcuns. See merge request dependabot-gitlab/dependabot!1482
- [Bump allure-report-publisher to 0.5.3](dependabot-gitlab/dependabot@fa87e8f85d592396f2eea078c0b3e3b099e78f63) by @andrcuns. See merge request dependabot-gitlab/dependabot!1480
- [Bump allure-report-publisher to 0.5.2](dependabot-gitlab/dependabot@55df5d76afb753942da0c0de3857908e7b8ee612) by @andrcuns. See merge request dependabot-gitlab/dependabot!1476
- [Simplify standalone test setup](dependabot-gitlab/dependabot@97d7b03e69e9a44e679beceaa02c619f8f2efde9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1468
- [Remove deprecated bundle-audit dependency scan](dependabot-gitlab/dependabot@a4a72d9ddc2230fb1af5e913ab1e2e9bae76600c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1467
- [Ruby based CI image](dependabot-gitlab/dependabot@3a8b225b24d8b918e0913b4d9153bda74555a163) by @andrcuns. See merge request dependabot-gitlab/dependabot!1466
- [Update bundler ci version](dependabot-gitlab/dependabot@1bcb55e09726bf174d4a86d596fb8221784f3f27) by @andrcuns. See merge request dependabot-gitlab/dependabot!1465
- [Add automated handling of stale issues](dependabot-gitlab/dependabot@7eb7bb5623199672c055c165fb13d73da9dd81b5) by @andrcuns. See merge request dependabot-gitlab/dependabot!1463
- [Remove redundant gitlab access token var reassigning for release jobs](dependabot-gitlab/dependabot@c73381f11d6e0d7f1749746cfd9a7e0108f5b663) by @andrcuns. See merge request dependabot-gitlab/dependabot!1462
- [Use ci image for release job](dependabot-gitlab/dependabot@d103b8ac20941776d3dd2c126e5cf68de398c9a8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1461
- [Fix deployment job](dependabot-gitlab/dependabot@ec2210d1ad3472ad80e0b656a51c97f22f44c1d4) by @andrcuns. See merge request dependabot-gitlab/dependabot!1459
- [Update buildkit version to v0.10.1](dependabot-gitlab/dependabot@1ea2f73cfa0a73ef7bf372a340f72d54ca640b11) by @andrcuns. See merge request dependabot-gitlab/dependabot!1454
- [Push ci generated app images to separate registry](dependabot-gitlab/dependabot@ebb923bbc3b0e174c6bb3a44cabac397789333bb) by @andrcuns. See merge request dependabot-gitlab/dependabot!1452

### 🛠️ Chore (3 changes)

- [Temporary disable broken spec](dependabot-gitlab/dependabot@e538caeb9a29e6d5157d981c9d00c63de277b4f9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1479
- [Log mongodb, redis and sentry to separate files](dependabot-gitlab/dependabot@96a5e0d395ce766dc55fbcfead6d3bafba78e293) by @andrcuns. See merge request dependabot-gitlab/dependabot!1479
- [Add missing updater options migration](dependabot-gitlab/dependabot@b89b1bf5b9af7e04faaae7c4092e038b798f9d07) by @andrcuns. See merge request dependabot-gitlab/dependabot!1474

## 0.17.2 (2022-04-08)

### 🐞 Bug Fixes (2 changes)

- [Ignore dependabot commands for non dependabot merge requests](dependabot-gitlab/dependabot@fd4e2c364842da8fc1e76b835d495f2f8092816a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1448
- [Correctly pass configuration when updating out of sync jobs](dependabot-gitlab/dependabot@48d83f64f015b5c7e921e69dde83f57e8a3c412c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1445

### 📦 Dependency updates (6 changes)

- [dep: bump dependabot-omnibus from 0.180.4 to 0.180.5](dependabot-gitlab/dependabot@ef80501e7af166d4f599b87ba3bdbd7c8bbdc68e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1449
- [dep: bump sidekiq_alive from 2.1.4 to 2.1.5](dependabot-gitlab/dependabot@e30b00dfd70f0dcf46bc4e986ac70a0dbcb66710) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1450
- [dep: bump sidekiq-cron from 1.2.0 to 1.3.0](dependabot-gitlab/dependabot@8825e705aa7fe59ff7c262c73df15cb0235b9360) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1442
- [dep: bump dependabot-omnibus from 0.180.3 to 0.180.4](dependabot-gitlab/dependabot@86173a7b8385007bc016d2eb0465212034171583) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1441
- [dep: bump dependabot-omnibus from 0.180.2 to 0.180.3](dependabot-gitlab/dependabot@558995d0d3040454308a549a13a9354fa70d3a7c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1437
- [dep-dev: bump allure-rspec from 2.16.2 to 2.17.0](dependabot-gitlab/dependabot@a4afdceb317b01c2dad80df02d37d5cdbe631b2a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1438

### 🛠️ Chore (3 changes)

- [Fix flaky configuration parser spec](dependabot-gitlab/dependabot@6f04c761c88a37dc5fbc4cf8280daeb319719d6e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1447
- [Add docker-compose deploy test](dependabot-gitlab/dependabot@c33aaae170494b00372ebb134b6c2f928bafcc81) by @andrcuns. See merge request dependabot-gitlab/dependabot!1444
- [Use factories for object fabrication in tests](dependabot-gitlab/dependabot@48199118f3277fdd9c3e90ec38c0eb3a48f277e9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1440

## 0.17.1 (2022-04-04)

### 🔬 Improvements (1 change)

- [Always evaluate private registries auth fields from environment variables](dependabot-gitlab/dependabot@eab98234d5905e356311ab426ef2e84f5d46a68a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1434

### 🐞 Bug Fixes (1 change)

- [Add back log level rails config](dependabot-gitlab/dependabot@bd76e1fcb22667393c409741ef1dd73bdc26e66a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1436

## 0.17.0 (2022-04-04)

### 🐞 Bug Fixes (5 changes)

- [Allow replaces-base key in registries configuration](dependabot-gitlab/dependabot@b44838259fb4adcea927bdae8726bc3e85413cd4) by @andrcuns. See merge request dependabot-gitlab/dependabot!1433
- [Correctly close obsolete merge requests](dependabot-gitlab/dependabot@d347d072ac74668b2e732aded409178811ed3f50) by @andrcuns. See merge request dependabot-gitlab/dependabot!1429
- [Correctly handle forked project webhooks](dependabot-gitlab/dependabot@151782c1a5335ddc61dc6401578a4fa82244b6dd) by @andrcuns. See merge request dependabot-gitlab/dependabot!1418
- [Correctly handle obsolete mr closing for forks](dependabot-gitlab/dependabot@5bbc0e713c55ad5ecb93e679efa7d57d7978677e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1417
- [Rebase fork merge requests on no conflicts](dependabot-gitlab/dependabot@de093bf39e9a4f4fd334409cf4efdc25d45924c8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1414

### 📦 Dependency updates (12 changes)

- [dep: bump mongoid from 7.3.4 to 7.4.0](dependabot-gitlab/dependabot@c1614fcc173985a84d42770d583e658c02d9dc6d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1427
- [dep: bump puma from 5.6.2 to 5.6.4](dependabot-gitlab/dependabot@f55f1777917c2139d467eba745e8a0f4778bc995) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1428
- [dep: bump lograge from 0.11.2 to 0.12.0](dependabot-gitlab/dependabot@5f43541012ac0e9f5bca2a37b6d75fc33801142e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1426
- [dep: bump dependabot-omnibus from 0.180.1 to 0.180.2](dependabot-gitlab/dependabot@2a8e48cec2e3bd14ac28eb44059d1304892eac69) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1424
- [[BREAKING] dep: bump dependabot/dependabot-core from 0.180.0 to 0.180.1](dependabot-gitlab/dependabot@2468d28c80d045a40022020bdef610b0673599da) by @andrcuns. See merge request dependabot-gitlab/dependabot!1423
- [dep-dev: bump docker from 20.10.13 to 20.10.14 in /.gitlab/docker/ci](dependabot-gitlab/dependabot@e1d66fdca9b4eaac427b021f43f53dd43f992282) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1422
- [dep-dev: bump rubocop from 1.26.0 to 1.26.1](dependabot-gitlab/dependabot@afb0a7a77c073f7d52f63be382ad0531b5185cec) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1420
- [dep: bump dependabot-omnibus from 0.179.0 to 0.180.0](dependabot-gitlab/dependabot@ed30a525d5d7b908ca91b74585256c6267de1121) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1408
- [dep: bump sentry-rails, sentry-ruby and sentry-sidekiq](dependabot-gitlab/dependabot@89b25b2708bb539018eb1f6de8a6188b5c7719cc) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1409
- [dep-dev: bump rubocop-rails from 2.14.1 to 2.14.2](dependabot-gitlab/dependabot@059a738877bad49be18fe2b5536e29a29f5f0cfe) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1412
- [dep: bump dependabot-omnibus from 0.178.1 to 0.179.0](dependabot-gitlab/dependabot@625676825ac402ff5622ebc5b4a1ddacb620a86c) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1401
- [dep-dev: bump rubocop-rails from 2.14.0 to 2.14.1](dependabot-gitlab/dependabot@aa6537b8d9ca73077695cc365bd1ee78ec4b0838) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1402

### 🔧 CI changes (2 changes)

- [Fix pipeline definitions for forked projects](dependabot-gitlab/dependabot@ba16ce5c7d3257e5c96249a08b4e342654f5c399) by @andrcuns. See merge request dependabot-gitlab/dependabot!1416
- [Run all tests in parallel in single stage](dependabot-gitlab/dependabot@3a553c02cb3b8641de1d50fffa28cf2bca996150) by @andrcuns. See merge request dependabot-gitlab/dependabot!1415

### 🛠️ Chore (6 changes)

- [Capitalise rake task descriptions](dependabot-gitlab/dependabot@f5ae611209cc5739cedef566ae8350d76e050efa) by @andrcuns.
- [Mr update system spec](dependabot-gitlab/dependabot@f11f45e92597636368cd85cd8554d9dec58615cd) by @andrcuns. See merge request dependabot-gitlab/dependabot!1432
- [Improve system test coverage](dependabot-gitlab/dependabot@ec5ac69b9d961ae6661180671a71c2b9a886b365) by @andrcuns. See merge request dependabot-gitlab/dependabot!1431
- [Add tags for test reports](dependabot-gitlab/dependabot@ca63e1ee058a63e3cb9579fdb3dffdec22dafecc) by @andrcuns. See merge request dependabot-gitlab/dependabot!1430
- [Add system test setup](dependabot-gitlab/dependabot@7200a87f2b632bfd11b16c563703a30d55f3e4a4) by @andrcuns. See merge request dependabot-gitlab/dependabot!1419
- [Extract dependabot configuration in to separate model](dependabot-gitlab/dependabot@315fd495659ac030f9ccf345d5676e839c67aedc) by @andrcuns. See merge request dependabot-gitlab/dependabot!1404

## 0.16.0 (2022-03-16)

### 🚀 New features (2 changes)

- [Close obsolete merge requests if dependency is up to date](dependabot-gitlab/dependabot@6f643a388283d2e1d6401175643780cb4e99be3f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1398
- [[BREAKING] Add option to rebase mr on approval event](dependabot-gitlab/dependabot@bb1d11441069c6c7d355ed62c24646906f6ca211) by @andrcuns. See merge request dependabot-gitlab/dependabot!1395

### 🔬 Improvements (2 changes)

- [Add link to open mr list](dependabot-gitlab/dependabot@9a49e65c85ee32eef48239392ed35a3469336f24) by @andrcuns. See merge request dependabot-gitlab/dependabot!1388
- [Include group milestones in milestone search](dependabot-gitlab/dependabot@8b5b866d5d638438aafbcd2b2eefe19e1f8ddb62) by @andrcuns. See merge request dependabot-gitlab/dependabot!1387

### 🐞 Bug Fixes (3 changes)

- [Improve poor load speed of open merge requests links](dependabot-gitlab/dependabot@f2bc80cf5518f0c6e080fd489c9273aaa7607125) by @andrcuns. See merge request dependabot-gitlab/dependabot!1400
- [Correctly update project fork attributes](dependabot-gitlab/dependabot@68c289fe3329ee2ba0be92b86e8c6249af937ab2) by @andrcuns. See merge request dependabot-gitlab/dependabot!1393
- [Use correct url for open merge requests link](dependabot-gitlab/dependabot@f90235f3f7a21788e7f9a5a689da708667335817) by @andrcuns. See merge request dependabot-gitlab/dependabot!1391

### 📦 Dependency updates (2 changes)

- [dep-dev: bump rubocop-rails from 2.13.2 to 2.14.0](dependabot-gitlab/dependabot@b0a39e08484f127eba2925622c9ed6783560fd13) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1399
- [dep: bump dependabot-omnibus from 0.178.0 to 0.178.1](dependabot-gitlab/dependabot@521acd73aa76e033c1db10d6905e35eb6f651f43) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1396

### 🛠️ Chore (2 changes)

- [Pass auto-merge option to pull request creator](dependabot-gitlab/dependabot@19ff4c5a11b4eb02595bf8c71aa026a98d4e9be1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1390
- [Load updated rails defaults](dependabot-gitlab/dependabot@c2d8e3866b82c2e36bb9fdcb50fb2e96ee1a58c9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1389

## 0.15.3 (2022-03-12)

### 🐞 Bug Fixes (1 change)

- [Count only unique merge requests towards mr limit](dependabot-gitlab/dependabot@64e16dbdbcb7be9fd001217cd405ba484726fa91) by @andrcuns. See merge request dependabot-gitlab/dependabot!1386

### 📦 Dependency updates (14 changes)

- [dep: bump anyway_config from 2.2.3 to 2.3.0](dependabot-gitlab/dependabot@385f10667fa890473d975ac519d47f0174eb40e4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1384
- [dep-dev: bump docker from 20.10.12 to 20.10.13 in /.gitlab/docker/ci](dependabot-gitlab/dependabot@862beecc18f66d1c8819047563e6b02d9ac4db6e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1383
- [dep: bump dependabot-omnibus from 0.177.0 to 0.178.0](dependabot-gitlab/dependabot@79653ae7d61699136a70211156c553c4952e22c0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1381
- [dep-dev: bump rubocop from 1.25.1 to 1.26.0](dependabot-gitlab/dependabot@34a9dab3853819d904af7803ec2c7d7e3bb9e323) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1379
- [dep-dev: bump allure-rspec from 2.16.1 to 2.16.2](dependabot-gitlab/dependabot@683501a848b9aee7c123ce151fc947d8ce019626) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1380
- [dep: bump sentry-ruby, sentry-rails, rails and sentry-sidekiq](dependabot-gitlab/dependabot@c231c9f82f91430520af36931c7b67956d96dcf2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1377
- [dep: bump bootsnap from 1.10.3 to 1.11.1](dependabot-gitlab/dependabot@2f8dc8f7af286dd4a2f7ed0e44c1234427a413ed) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1374
- [dep: bump rails from 7.0.2.2 to 7.0.2.3](dependabot-gitlab/dependabot@78c3c6fede9dfa41136e13f33379ed546eedb283) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1375
- [dep-dev: bump rspec-rails from 5.1.0 to 5.1.1](dependabot-gitlab/dependabot@e4011e94a888ee20a4dd5a9ac561a9ac7ab718cb) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1373
- [dep-dev: bump faker from 2.19.0 to 2.20.0](dependabot-gitlab/dependabot@697abcb75d1c020f900cfe8544163e5d34c5bc29) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1372
- [dep-dev: bump rubocop-performance from 1.13.2 to 1.13.3](dependabot-gitlab/dependabot@f855c51c61d0ad9a38f0877b5caca45d453b6570) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1371
- [dep: bump dependabot-omnibus from 0.176.0 to 0.177.0](dependabot-gitlab/dependabot@c3cae6e524c38ddca6ad01400dd7f45bbc34e430) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1369
- [dep: bump dependabot-omnibus from 0.175.0 to 0.176.0](dependabot-gitlab/dependabot@91e44955c16d9a666cb5c174dc8f784e4722227e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1366
- [dep-dev: bump rubocop-rspec from 2.8.0 to 2.9.0](dependabot-gitlab/dependabot@5b403a3d00259f60e85a8ee244eb32bc1328df6f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1365

### 🔧 CI changes (1 change)

- [Bump ci dind image to 20.10.13](dependabot-gitlab/dependabot@ea4b162650e5b6741f5e7b71596a2775915a6554) by @andrcuns. See merge request dependabot-gitlab/dependabot!1385

### 📄 Documentation updates (1 change)

- [Document configuration default values](dependabot-gitlab/dependabot@82fef2543f9de31c8374f53fd2a3e3a367a81dd1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1368

## 0.15.2 (2022-02-28)

### 🔬 Improvements (2 changes)

- [Add colorized logs](dependabot-gitlab/dependabot@856f33c95bf68df6c304b2a231210d8b5c3a9f98) by @andrcuns. See merge request dependabot-gitlab/dependabot!1363
- [Log to error on shared helpers subprocess failure](dependabot-gitlab/dependabot@2d5723e85a7a0dbb1bcff4f437a49c263a683fe7) by @andrcuns. See merge request dependabot-gitlab/dependabot!1345

### 🐞 Bug Fixes (2 changes)

- [Correctly convert config entry after rails upgrade](dependabot-gitlab/dependabot@065ea148409999fb906821ebfbab945dbae08c0c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1361
- [Correctly handle mrs without conflict status present](dependabot-gitlab/dependabot@100d99fd6f910a2f6d287ab31440278ba2c4616c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1358

### 📦 Dependency updates (5 changes)

- [dep: bump rails from 6.1.4.6 to 7.0.2.2](dependabot-gitlab/dependabot@662ba575dfa156a147f7210df59ac6d34fd79686) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1361
- [dep: bump dependabot-omnibus from 0.174.1 to 0.175.0.](dependabot-gitlab/dependabot@01795b0ea4c0821db45f518dd2667a054d99e3a3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1353
- [dep: bump rails from 6.1.4.6 to 7.0.2.2](dependabot-gitlab/dependabot@aa7e51a7027baa0023b0a13045ed769488823a2e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1350
- [dep: bump dependabot-omnibus from 0.174.0 to 0.174.1](dependabot-gitlab/dependabot@dd9aae64ee55ec1a26b2fcd6f7c54ea6a0988250) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1346
- [dep: bump mongoid from 7.3.3 to 7.3.4](dependabot-gitlab/dependabot@5d3b9c8917d298d9911ac827eececf7ef82c2de2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1347

### 🔧 CI changes (1 change)

- [Remove test image usage](dependabot-gitlab/dependabot@9ae1a946977c04785d0b5f8b6f826ef1c0e12aaa) by @andrcuns. See merge request dependabot-gitlab/dependabot!1357

### 🛠️ Chore (3 changes)

- [Adjust log message padding](dependabot-gitlab/dependabot@573b3f82726aeba67d0bba399cb2a75b0caa0e81) by @andrcuns. See merge request dependabot-gitlab/dependabot!1364
- [Add spec for configuration fetching](dependabot-gitlab/dependabot@ffa8d90d7813a153e53d9bfa0cd8f9245b1a1b5b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1360
- [Add SECRET_KEY_BASE env variable for docker-compose.yml](dependabot-gitlab/dependabot@6a854b60d08bdb964f084348099088f010fe4193) by @andrcuns.

### 📄 Documentation updates (1 change)

- [Document missing configuration environment variables](dependabot-gitlab/dependabot@77770a05c2ec9219dc1e4575c6ea734ab20c1d92) by @andrcuns. See merge request dependabot-gitlab/dependabot!1362

## 0.15.1 (2022-02-21)

### 🐞 Bug Fixes (1 change)

- [Correctly remove credentials from SharedHelpers debug log messages](dependabot-gitlab/dependabot@f592a438d71e6b6c80302049273e6b5052f3f2b1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1344

## 0.15.0 (2022-02-21)

### 🚀 New features (1 change)

- [Allow mr auto-merge on approval event](dependabot-gitlab/dependabot@7590b96b27f2285b2999ee6e2e73332ce93bd9d0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1342

### 🔬 Improvements (2 changes)

- [Rename moved projects during project sync](dependabot-gitlab/dependabot@155d31b939d58b7669ebd37616aca241619f6e86) by @andrcuns. See merge request dependabot-gitlab/dependabot!1341
- [Log dependabot shared helper output to debug level](dependabot-gitlab/dependabot@20b21243d39a5db0a74472cbeff85a13036382da) by @andrcuns. See merge request dependabot-gitlab/dependabot!1340

### 📦 Dependency updates (3 changes)

- [dep: bump dependabot-omnibus from 0.173.0 to 0.174.0](dependabot-gitlab/dependabot@1183178d3223d6575bbb0e1cce325cd6a0b1f6b8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1337
- [dep: bump dry-validation from 1.7.0 to 1.8.0](dependabot-gitlab/dependabot@82db2007e56d48d9dcb40c328ae31e0bada364da) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1336
- [dep: bump dependabot-omnibus from 0.172.2 to 0.173.0](dependabot-gitlab/dependabot@874b36b16f8f9b75ece734a7b4c201d600c7faec) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1334

### 🛠️ Chore (1 change)

- [Improve dependabot helper logging](dependabot-gitlab/dependabot@bccd4e484c739ba824c606111696371bbb99dd3d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1343

## 0.14.2 (2022-02-14)

### 🐞 Bug Fixes (3 changes)

- [Rescue gitlab response on mr update](dependabot-gitlab/dependabot@b2e7c666fbe3635571add2d1848b7f3da4f7170a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1333
- [Always recreate mr on forks](dependabot-gitlab/dependabot@7bc8e7891715b6d2483b1243cf98631b471f6f28) by @andrcuns. See merge request dependabot-gitlab/dependabot!1332
- [Fix log call typo](dependabot-gitlab/dependabot@cd03091cfeb30956d4d78e8418d6478b89cdaf10) by @andrcuns. See merge request dependabot-gitlab/dependabot!1328

### 📦 Dependency updates (4 changes)

- [dep: bump dependabot-omnibus from 0.172.1 to 0.172.2](dependabot-gitlab/dependabot@c23b6fa4ed5b00f290795ffd289e3c52a2b968ce) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1322
- [dep: bump sentry-ruby, sentry-rails, rails and sentry-sidekiq](dependabot-gitlab/dependabot@d784c5dab577c62074525304d666cfe81dfb37c3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1326
- [dep: bump rails from 6.1.4.4 to 6.1.4.6](dependabot-gitlab/dependabot@7e5ef271f9c5f677559ba2e8e972687c8e82dcd3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1324
- [dep: bump puma from 5.6.1 to 5.6.2](dependabot-gitlab/dependabot@d7707a3743a566d539066bf1209eb7ee3303614a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1323

### 🛠️ Chore (4 changes)

- [Add Dependencies namespace for better grouping](dependabot-gitlab/dependabot@d78a24207e8e4e9bd68ad04e255dd8eebd588f9d) by @andrcuns. See merge request dependabot-gitlab/dependabot!1331
- [Move merge request persistence to creator class](dependabot-gitlab/dependabot@92f0ecd1819c413be42a1f8af71ce033b2a1943a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1331
- [Add execution context for mr update jobs](dependabot-gitlab/dependabot@88a1bc24069a8a3f4bba5e802ad2fea3f312826f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1330
- [Do not fetch dependency info on merge request rebase](dependabot-gitlab/dependabot@72068195a08989ffa935d69af768f100f3e9a2ec) by @andrcuns. See merge request dependabot-gitlab/dependabot!1330

## 0.14.1 (2022-02-11)

### 🐞 Bug Fixes (1 change)

- [Fix schedule hours validation](dependabot-gitlab/dependabot@2b3bf70bc35d41487646c9ce405cddce4aaf863b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1321

## 0.14.0 (2022-02-10)

### 🚀 New features (1 change)

- [Add ability to set random schedule hour range](dependabot-gitlab/dependabot@c5ea19677c783c35e8bcb6cbacf7a18559e23021) by @andrcuns. See merge request dependabot-gitlab/dependabot!1314

### 🔬 Improvements (1 change)

- [Refactor update service to run full update of single dep at a time](dependabot-gitlab/dependabot@6ff0c2c301751ad24fecb8713c5bbf02755e05a6) by @andrcuns. See merge request dependabot-gitlab/dependabot!1297

### 🐞 Bug Fixes (2 changes)

- [Update sidekiq logger patch](dependabot-gitlab/dependabot@0423277ab9e6f2c7ffa050801278fc00e2f0a2fd) by @andrcuns. See merge request dependabot-gitlab/dependabot!1313
- [Check mr is in opened state before updating](dependabot-gitlab/dependabot@886d28b6b098987ff07d037e7928e463a724769c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1307

### 📦 Dependency updates (11 changes)

- [dep: bump dependabot-omnibus from 0.171.5 to 0.172.1](dependabot-gitlab/dependabot@073cd14bb6cf9038aaa97cc3d248052ad193956d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1318
- [dep-dev: bump rspec from 3.10.0 to 3.11.0](dependabot-gitlab/dependabot@bc4938894ab94fbb0e1329c07d96878acdf27477) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1319
- [Bump dependabot-omnibus from 0.171.4 to 0.171.5](dependabot-gitlab/dependabot@c930a3b2daf8807ec5944adf963fdf29e16c39fe) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1310
- [Bump sidekiq from 6.4.0 to 6.4.1](dependabot-gitlab/dependabot@c4d1568d6b8c9226deb8330d1169c09f264a75aa) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1311
- [Bump rubocop from 1.25.0 to 1.25.1](dependabot-gitlab/dependabot@86f65aa3be2e7651d100bed4a7f146915afc7501) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1305
- [Bump allure-rspec from 2.16.0 to 2.16.1](dependabot-gitlab/dependabot@b7886ef9714ac7e5f59b52a1c7183cda60237637) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1306
- [Bump bootsnap from 1.10.2 to 1.10.3](dependabot-gitlab/dependabot@6aa21dbfd489ed6a05da9fc41e3c3114e40fff03) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1304
- [Bump dependabot-omnibus from 0.171.3 to 0.171.4](dependabot-gitlab/dependabot@027771b5c706938b2723085e658312c66d258eca) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1290
- [Bump puma from 5.6.0 to 5.6.1](dependabot-gitlab/dependabot@08b6d025bdf1e54fdfef70b9b9f80725d4ef69ee) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1291
- [Bump dependabot/dependabot-core from 0.171.2 to 0.171.3](dependabot-gitlab/dependabot@63ef3906590731c1a6683266f3fa340ceb226a86) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1287
- [Bump rspec-rails from 5.0.2 to 5.1.0](dependabot-gitlab/dependabot@47ddd1e8ae350e4066643cff7922979435f6973d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1288

### 🔧 CI changes (4 changes)

- [Remove codecov](dependabot-gitlab/dependabot@d9b1de32b24d22a5d0bf8bcf0a6033730de3ae5e) by @andrcuns. See merge request dependabot-gitlab/dependabot!1303
- [Update dependencies in CI image](dependabot-gitlab/dependabot@18a250f45981de4ff2915b98548f89816e21d450) by @andrcuns. See merge request dependabot-gitlab/dependabot!1295
- [Remove chart-testing from ci image](dependabot-gitlab/dependabot@9e30121eb67669bb0a8bef590cab0ec6d25a02c1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1294
- [Add helm diff and local version plugins](dependabot-gitlab/dependabot@dc63d3a7b758864b413fb459ee377e32f26f17ab) by @andrcuns. See merge request dependabot-gitlab/dependabot!1293

### 🛠️ Chore (7 changes)

- [Remove unnecessary UpdateErrors instance creation](dependabot-gitlab/dependabot@2ee7b5eb18dc0d78a2d9b86fc3185db7dd6c4cab) by @andrcuns. See merge request dependabot-gitlab/dependabot!1317
- [Refetch config from gitlab when webhooks are not configured](dependabot-gitlab/dependabot@717b9e4a2259514c80f9a6ff0f43c90526ff276c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1316
- [Temporary patch sidekiq job class](dependabot-gitlab/dependabot@6f30ed7526031310bea889522e96ca3860dc3dc0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1315
- [Lazy iterate gitlab projects on registration job](dependabot-gitlab/dependabot@9e7c79ef1d3cc59e60e32acf26a2c539773edfa6) by @andrcuns. See merge request dependabot-gitlab/dependabot!1308
- [Simplify update service setup](dependabot-gitlab/dependabot@d8ff1c27ca61642bab31f31cc62d2e5bb64b31b6) by @andrcuns. See merge request dependabot-gitlab/dependabot!1302
- [Remove redundant config methods](dependabot-gitlab/dependabot@d5f7535523105bcd8c794ab6e5c4999a0b53d5cc) by @andrcuns. See merge request dependabot-gitlab/dependabot!1301
- [Refactor config fetching](dependabot-gitlab/dependabot@17ff51fec808e28dc178418dc34b7be27bf4e3ac) by @andrcuns. See merge request dependabot-gitlab/dependabot!1296

### 📄 Documentation updates (1 change)

- [Remove latest master tag from documentation](dependabot-gitlab/dependabot@f739f3e3fd513bc901a9d41a062c3f542f0add49) by @andrcuns. See merge request dependabot-gitlab/dependabot!1309

## 0.13.0 (2022-01-26)

### 🚀 New features (1 change)

- [Support allow and ignore rules for auto-merge](dependabot-gitlab/dependabot@9a10516a07ce428d1ed26a0c268fc623723079c9) by @andrcuns. See merge request dependabot-gitlab/dependabot!1234

### 🔬 Improvements (3 changes)

- [Add directory to missing config entry error.](dependabot-gitlab/dependabot@002c48a405b230d7b214ebc8020f053a8b4ac176) by @cchantep. See merge request dependabot-gitlab/dependabot!1284
- [Better error handling during merge request update and reopen action](dependabot-gitlab/dependabot@ed09638e397c3a6b80991914176b27ed7975fd3f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1275
- [Remove manually closed merge request branch](dependabot-gitlab/dependabot@67500fbdcfd65243a0c2a0dfc87983a7c9b88b3c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1266

### 🐞 Bug Fixes (5 changes)

- [Change healthcheck test syntax to array notation](dependabot-gitlab/dependabot@8ac6e1f539aab492dbd888edb96c7fc656bf9516) by @GijsDJ. See merge request dependabot-gitlab/dependabot!1283
- [Do not register projects without default branch](dependabot-gitlab/dependabot@940dddc189e263f8be5d25306779556e85e47325) by @andrcuns. See merge request dependabot-gitlab/dependabot!1281
- [Add mr comment only if mr update failed](dependabot-gitlab/dependabot@dad84a7d8caf4363d26d881c00ef4059e2ebb1c0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1280
- [Clear logger execution context after job finished](dependabot-gitlab/dependabot@6732af98db4bf19a5787fff177e6fd0c5d5ca8b1) by @andrcuns. See merge request dependabot-gitlab/dependabot!1255
- [Use correct config class in rake task](dependabot-gitlab/dependabot@93b56dd8f00b144006e0294226a83a8a339d85c5) by @testn1. See merge request dependabot-gitlab/dependabot!1230

### 📦 Dependency updates (38 changes)

- [Bump puma from 5.5.2 to 5.6.0](dependabot-gitlab/dependabot@217bfdcbf6b8b38276f0faf88f47526bbc00e6b2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1282
- [Bump sentry-rails, sentry-ruby and sentry-sidekiq](dependabot-gitlab/dependabot@0be1f858c02bb9caaac29dd5b9461075eb29811f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1276
- [Bump rubocop-rspec from 2.7.0 to 2.8.0](dependabot-gitlab/dependabot@1640a3999a995edb63d932a2290a93c24b580239) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1279
- [Bump solargraph from 0.44.2 to 0.44.3](dependabot-gitlab/dependabot@6068173a803f4c7b998c1974985fe89932dfd495) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1274
- [Bump allure-rspec from 2.15.0 to 2.16.0](dependabot-gitlab/dependabot@f9e3e4cad7e674029ec598ecd1124bf04ab4c2ed) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1273
- [Bump bootsnap from 1.10.1 to 1.10.2](dependabot-gitlab/dependabot@70db8743b43eea41b905b1a16de5f2989e299144) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1272
- [Bump anyway_config from 2.2.2 to 2.2.3](dependabot-gitlab/dependabot@450a2ac6a76ed0fb5063046af45f68f25104009a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1271
- [Bump sentry-rails, sentry-ruby and sentry-sidekiq](dependabot-gitlab/dependabot@01f6343eecd57d61801bfae2f8c864a0c8af4fb4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1267
- [Bump sidekiq from 6.3.1 to 6.4.0](dependabot-gitlab/dependabot@b75cdf900e36f894d2c0e7f3926a93a37d358dfe) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1270
- [Bump sentry-sidekiq, sentry-rails and sentry-ruby](dependabot-gitlab/dependabot@be451771f7bac2bb1b74b320ee043cf3ee9bc715) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1265
- [Bump sentry-rails, sentry-ruby and sentry-sidekiq](dependabot-gitlab/dependabot@96203195c3d672993e58438b475e66b570516bae) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1263
- [Bump rubocop from 1.24.1 to 1.25.0](dependabot-gitlab/dependabot@162fc64daf053d77b87fab27fad28e1f6f1ddfbf) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1262
- [Bump bootsnap from 1.9.4 to 1.10.1](dependabot-gitlab/dependabot@bc087fcf621b3e43587dfc53fd29017ca8f797e5) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1261
- [Bump rubocop-performance from 1.13.1 to 1.13.2](dependabot-gitlab/dependabot@f4bd8e7e01d691718cbff7babbeb3c0ab68318d3) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1258
- [Bump rubocop-rails from 2.13.1 to 2.13.2](dependabot-gitlab/dependabot@4b648962cbb80935808ee93b9e8ae536a87eedef) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1256
- [Bump dependabot-omnibus from 0.171.1 to 0.171.2](dependabot-gitlab/dependabot@7a90756912fae3edad6b5a16aa9752d8244df2e9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1245
- [Bump reek from 6.0.6 to 6.1.0](dependabot-gitlab/dependabot@972fa565a88f34074b48f6b0a304e6506e4a9a64) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1252
- [Bump sentry-sidekiq from 4.9.0 to 4.9.1](dependabot-gitlab/dependabot@39ac4f5d2ee36b8b9e78ecabd31db955d1668f6e) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1251
- [Bump sentry-rails from 4.9.0 to 4.9.1](dependabot-gitlab/dependabot@f54e87dbad482fd1975a668647d0e06e431c4b2d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1250
- [Bump reek from 6.0.6 to 6.1.0](dependabot-gitlab/dependabot@4fc65cc75b45858946bf7d0f7df4235bba423a7a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1249
- [Bump sentry-ruby from 4.9.0 to 4.9.1](dependabot-gitlab/dependabot@59f595afb9e430428729b19bb0a6602935e176c7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1247
- [Bump dependabot-omnibus from 0.171.0 to 0.171.1](dependabot-gitlab/dependabot@eb3a2941b293c5fd9f6f11fbba7029f2393d1a44) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1243
- [Bump dependabot-omnibus from 0.170.0 to 0.171.0](dependabot-gitlab/dependabot@8620cc7b49afaa95885344712dff1dda0b675506) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1240
- [Bump sentry-rails, sentry-ruby and sentry-sidekiq](dependabot-gitlab/dependabot@c8cac9818a6d0f52126084fa27cdaa81f31a291a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1237
- [Bump bootsnap from 1.9.3 to 1.9.4](dependabot-gitlab/dependabot@eb7a26a7c5f82798173af2c9724550334b0dab02) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1236
- [Bump rubocop-rails from 2.13.0 to 2.13.1](dependabot-gitlab/dependabot@84927f4bf1264e7da62a6d46ebc4f47da3ca2904) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1235
- [Bump git from 1.10.1 to 1.10.2](dependabot-gitlab/dependabot@2712164b193255aafb120cf63b2fdf661fb4a62d) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1233
- [Bump dependabot-omnibus from 0.169.8 to 0.170.0](dependabot-gitlab/dependabot@0d343aa2538fac66bcc8b6f117a605112b18e8b4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1224
- [Bump rspec_junit_formatter from 0.5.0 to 0.5.1](dependabot-gitlab/dependabot@151221763c41e7e35e4167b81e0e188f289fb195) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1229
- [Bump sentry-rails, sentry-ruby and sentry-sidekiq](dependabot-gitlab/dependabot@28e6d7c1c0e2f7f550ebda971bffe26ecd261dd0) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1225
- [Bump rspec_junit_formatter from 0.4.1 to 0.5.0](dependabot-gitlab/dependabot@cbc77e35ec5d437d224c315e7a7aee372e9a1c41) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1223
- [Bump git from 1.10.0 to 1.10.1](dependabot-gitlab/dependabot@3ae497e2ff459d9a393039a9948221e5481c31c9) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1222
- [Bump rubocop-performance from 1.13.0 to 1.13.1](dependabot-gitlab/dependabot@26a265a9fbb8b78b702b20510a86deb00113ac05) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1220
- [Bump rubocop from 1.24.0 to 1.24.1](dependabot-gitlab/dependabot@a0b714fbc136055c3fb8fbab26a8835b0647cb3a) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1219
- [Bump rubocop-rspec from 2.6.0 to 2.7.0](dependabot-gitlab/dependabot@4478637320ee16a00fd5497a012643d732ce509f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1215
- [Bump rubocop-performance from 1.12.0 to 1.13.0](dependabot-gitlab/dependabot@7f46ca824811daf6258dbd065bb7abd6039cf5c8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1213
- [Bump rubocop from 1.23.0 to 1.24.0](dependabot-gitlab/dependabot@a9105c6d03291897243e152dddd51245daa5d845) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1212
- [Bump dependabot-omnibus from 0.169.7 to 0.169.8](dependabot-gitlab/dependabot@fc61e06d35d45536a28c8ca72ea85f67781299c8) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1209

### 📦🛠️ Development dependency updates (2 changes)

- [Bump gitlab-org/release-cli in /.gitlab/docker/ci](dependabot-gitlab/dependabot@0063c185d6396fdd6cfbcfdf0fdf8263ec87a68f) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1285
- [Bump helmpack/chart-testing from v3.4.0 to v3.5.0 in /.gitlab/docker/ci](dependabot-gitlab/dependabot@c4679a9aa37e31adf7df1f1f7ec0e5a7eaf36240) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1232

### 🔧 CI changes (4 changes)

- [Do not use dependency proxy for forks](dependabot-gitlab/dependabot@c131e711c12e09ee059e722b7a1fa3e81b999cb8) by @andrcuns. See merge request dependabot-gitlab/dependabot!1286
- [Add inline cache for built images](dependabot-gitlab/dependabot@4734591c867d7f2d21f6632df6b7a4773a7d8f24) by @andrcuns. See merge request dependabot-gitlab/dependabot!1221
- [Fix CI runner image names](dependabot-gitlab/dependabot@d6910e5a4d081a11087e57b21698a626e0aa850b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1218
- [Run e2e tests on dependency updates](dependabot-gitlab/dependabot@0a86f2bb6bb8937b4c23947d28e038a69318c04b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1211

### 🛠️ Chore (2 changes)

- [Improve class grouping by using separate modules](dependabot-gitlab/dependabot@8d498d923f47687456833a0b5e86892d0a209716) by @andrcuns. See merge request dependabot-gitlab/dependabot!1259
- [Rename and group merge request classes](dependabot-gitlab/dependabot@e1edcafa8fca28754b34afd5d40699c6361b0c33) by @andrcuns. See merge request dependabot-gitlab/dependabot!1257

### 📄 Documentation updates (2 changes)

- [Add docs for auto-merge allow/ignore rules](dependabot-gitlab/dependabot@21e5e8fe2fef7f25d51125aa9015fe674ad5d837) by @andrcuns. See merge request dependabot-gitlab/dependabot!1242
- [fix helm chart configuration link](dependabot-gitlab/dependabot@fb1d00b2b914f667c151c1ce280ebcc8895ffc62) by @solidnerd. See merge request dependabot-gitlab/dependabot!1217

## 0.12.0 (2021-12-21)

### 🔬 Improvements (3 changes)

- [Log conflicting dependencies when update is impossible](dependabot-gitlab/dependabot@22eb0b7d45733bacb1e95101702b90279bcaeefe) by @andrcuns. See merge request dependabot-gitlab/dependabot!1193
- [[BREAKING] Capture run errors on standalone run and fail if any present](dependabot-gitlab/dependabot@2c2acf3536ba2adac87417dbca087b896088c17f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1191
- [Automatically resolve bot command discussions](dependabot-gitlab/dependabot@6eb348fe563934eb72fcf6c2ee6a4b656b1f6c93) by @andrcuns. See merge request dependabot-gitlab/dependabot!1190

### 🐞 Bug Fixes (7 changes)

- [Strip protocol from private terraform registries](dependabot-gitlab/dependabot@7c5398c40464b8371092e7dc6cbfe81ef26f275a) by @andrcuns. See merge request dependabot-gitlab/dependabot!1208
- [Correctly pass registries credentials to core updaters](dependabot-gitlab/dependabot@06e0b82ddb57bcac6a480500d9bf5f964518f8ec) by @andrcuns. See merge request dependabot-gitlab/dependabot!1203
- [Strip protocol from private docker registries](dependabot-gitlab/dependabot@84b0c3440db8820fb2932e259373f17cc94d0add) by @andrcuns. See merge request dependabot-gitlab/dependabot!1200
- [Strip protocol from npm private registries](dependabot-gitlab/dependabot@e3e0f5d7d17f22c8a513b33bed9bf5a3d2335fec) by @andrcuns. See merge request dependabot-gitlab/dependabot!1199
- [Use correct cache key for config from different branches](dependabot-gitlab/dependabot@8b1bd7c6cf69a37b71805513ba5632f91a09f657) by @andrcuns. See merge request dependabot-gitlab/dependabot!1198
- [Correctly fetch milestone_id from title](dependabot-gitlab/dependabot@2156c5f94d1a52390498da5e68478eba0e2aba62) by @andrcuns. See merge request dependabot-gitlab/dependabot!1192
- [respect directory when closing superseeded merge requests](dependabot-gitlab/dependabot@9cf7b4a1bddc7a3fddbbac3be7f2096ce0e3c92b) by @andrcuns. See merge request dependabot-gitlab/dependabot!1189

### 📦 Dependency updates (7 changes)

- [Bump dependabot-omnibus from 0.169.6 to 0.169.7](dependabot-gitlab/dependabot@255a84d8c0e71956b75c7d481a01650c65113f15) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1204
- [Bump rails from 6.1.4.3 to 6.1.4.4](dependabot-gitlab/dependabot@5333a4c85498999a537370999bf77826a2f139f4) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1188
- [Bump simplecov-cobertura from 2.0.0 to 2.1.0](dependabot-gitlab/dependabot@d1786305838245f576ac26779b34eefa43e6d7b7) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1187
- [Bump rails from 6.1.4.2 to 6.1.4.3](dependabot-gitlab/dependabot@d2eeed5d4982ac291df7f54ca37700c78d0cebd2) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1186
- [Bump dependabot-omnibus from 0.169.5 to 0.169.6](dependabot-gitlab/dependabot@147d10b3842162ec79259304f7915bfdef40ec08) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1183
- [Bump rails from 6.1.4.1 to 6.1.4.2](dependabot-gitlab/dependabot@7c228b5dbf843bd442a3803f545cb3b0fd376254) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1184
- [Bump gitlab from `cfd0d9a` to `25f6f76`](dependabot-gitlab/dependabot@ceec855b3895295de9967e412725598eb1d43c57) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1173

### 📦🛠️ Development dependency updates (3 changes)

- [Bump git from 1.9.1 to 1.10.0](dependabot-gitlab/dependabot@17e772d96fb0dd8ddd1ff552e7c2645f94f9b632) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1205
- [Bump docker from 20.10.11 to 20.10.12 in /.gitlab/docker/ci](dependabot-gitlab/dependabot@521bbdca92637a3231e337e112818bc7c3735b1b) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1181
- [Update base image for gitlab mock](dependabot-gitlab/dependabot@ddcc662d39e9fff1a19f29fd7d0ba85c4adcfd8c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1174

### 🔧 CI changes (4 changes)

- [Reuse COMPOSE_PROJECT_NAME env var](dependabot-gitlab/dependabot@463c85cc140635747a2e90db8738c55c7a69b3f3) by @andrcuns. See merge request dependabot-gitlab/dependabot!1179
- [Update buildkit version](dependabot-gitlab/dependabot@7aeaf564247258a063a677ec0e8ca257dd3fcf5f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1178
- [Fix ci image latest tag](dependabot-gitlab/dependabot@da8b44cbc587d8a8e1a2d6408c13d960e83678a9) by @andrcuns.
- [Update CI setup](dependabot-gitlab/dependabot@bf519a07bac2c1d8022561d9fdc843f9df6d695f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1172

### 🛠️ Chore (6 changes)

- [Remove unused update_cache parameter in config fetcher](dependabot-gitlab/dependabot@c12ce283adcc802226de1b8a39edbbf046dc9298) by @andrcuns. See merge request dependabot-gitlab/dependabot!1202
- [Update cached config on ProjectCreator call](dependabot-gitlab/dependabot@ed96229a54e1a5df9c6d8999ffdf9c2216660fbf) by @andrcuns. See merge request dependabot-gitlab/dependabot!1201
- [Remove OpenStruct usage](dependabot-gitlab/dependabot@6365a90197834cedc9bffc33004f8b5303a62252) by @andrcuns. See merge request dependabot-gitlab/dependabot!1195
- [Update devcontainer setup](dependabot-gitlab/dependabot@af6015efd8ba1170ca051656b45f1ec613fd88b0) by @andrcuns. See merge request dependabot-gitlab/dependabot!1194
- [Refactor controller tests to use airborne](dependabot-gitlab/dependabot@c5901a783ade51cbf9bfb39f50f14a889c7a4902) by @andrcuns. See merge request dependabot-gitlab/dependabot!1182
- [Update gitlab mocking setup for testing](dependabot-gitlab/dependabot@46e2db2bd2fcc8a1e8deb38e345147623cc8c47f) by @andrcuns. See merge request dependabot-gitlab/dependabot!1177

## 0.11.0 (2021-12-11)

### 🚀 New features (1 change)

- [[BREAKING] add get, add, and delete projects api endpoints](dependabot-gitlab/dependabot@b281491ba435a5ef51b4014bdae37758608456fa) by @andrcuns. See merge request dependabot-gitlab/dependabot!1139

### 🔬 Improvements (1 change)

- [Add project update endpoint](dependabot-gitlab/dependabot@ca1f2a02a32c443e5e29a58688cc8a32ba3b172c) by @andrcuns. See merge request dependabot-gitlab/dependabot!1169

### 📦 Dependency updates (1 change)

- [Bump dependabot-omnibus from 0.169.3 to 0.169.4](dependabot-gitlab/dependabot@365663e571deb1459b1c037540d76c3a8062b329) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1163

### 🔧 CI changes (3 changes)

- [Allow redundant pipeline to be canceled](dependabot-gitlab/dependabot@88834748eeb6f17030177df9d0197aa16c50ff75) by @andrcuns. See merge request dependabot-gitlab/dependabot!1165
- [Add custom changelog template](dependabot-gitlab/dependabot@b2b9e5c51a4c24c3bcd99339ae5ea4142bfd5656) by @andrcuns. See merge request dependabot-gitlab/dependabot!1161
- [Fix gitlab release creation](dependabot-gitlab/dependabot@d4d68c9527d9a4374db1a913d8bd676018a082eb) by @andrcuns. See merge request dependabot-gitlab/dependabot!1160

### 💾 Deployment (1 change)

- [Bump google from 4.2.1 to 4.3.0 in /terraform](dependabot-gitlab/dependabot@7c399d8d488dfe715181aecd3ca9c2f26f6048d6) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1162

## 0.10.11 (2021-12-06)

### 🚀 New features (1 change)

- [add support for custom commit message trailers](dependabot-gitlab/dependabot@31843d3023641298877eadb5d615cbb8580ba3cf) by @andrcuns.

### 🔧 CI changes (3 changes)

- [use gitlab changelog generation functionality](dependabot-gitlab/dependabot@8c230ffe5e54e86423476cad17972ecb091bc526) by @andrcuns. See merge request dependabot-gitlab/dependabot!1158
- [use gitlab dependency proxy for docker images](dependabot-gitlab/dependabot@3e203c4309239ef2c43efc7ed84e884de1a0b77f) by @andrcuns.
- [bump ci ruby version, remove custom image](dependabot-gitlab/dependabot@bfae42210dd5e5e701bbd5c28d47de79106d4bcf) by @andrcuns.

### 💾 Deployment (1 change)

- [Bump kubernetes from 2.7.0 to 2.7.1 in /terraform](dependabot-gitlab/dependabot@5c6ae81878d9b0dc3e565b7a82411df49456b114) by @dependabot-bot. See merge request dependabot-gitlab/dependabot!1159

### 🛠️ Chore (1 change)

- [Remove ci dockerfile dependency updates](dependabot-gitlab/dependabot@6cd40e44bbc07f7a45bb9ca5fe7f97591907b99f) by @andrcuns.
