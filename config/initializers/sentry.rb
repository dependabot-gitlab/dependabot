# frozen_string_literal: true

# Load stackprof if sentry profiling is enabled
if [AppConfig.sentry_traces_sample_rate, AppConfig.sentry_profiles_sample_rate].all? { |rate| rate == 1 }
  require "stackprof"
end

require "sentry-ruby"
require "sentry-rails"
require "sentry-sidekiq"

# rubocop:disable Metrics/BlockLength
Sentry.init do |config|
  config.enabled_environments = ["production"]
  config.release = "dependabot-gitlab@#{ENV['APP_VERSION']}"
  config.skip_rake_integration = true
  config.logger = CustomLogger.logger(source: "sentry", logdev: :file)
  config.excluded_exceptions += AppConfig.sentry_ignored_errors if AppConfig.sentry_ignored_errors

  # Sentry performance and profiling
  config.profiles_sample_rate = AppConfig.sentry_profiles_sample_rate
  config.traces_sampler = lambda do |sampling_context|
    transaction_context = sampling_context[:transaction_context]
    op = transaction_context[:op]
    transaction_name = transaction_context[:name]

    case op
    when /http/
      case transaction_name
      when /healthcheck/
        0.0
      else
        AppConfig.sentry_traces_sample_rate
      end
    when /sidekiq/
      case transaction_name
      when /SidekiqAlive/
        0.0
      else
        AppConfig.sentry_traces_sample_rate
      end
    else
      0.0
    end
  end
end
# rubocop:enable Metrics/BlockLength

Sentry.configure_scope do |scope|
  scope.add_event_processor do |event, _hint|
    ApplicationHelper.execution_context.each { |key, value| event.tags[key] = value }

    event
  end
end
