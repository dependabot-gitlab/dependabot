# frozen_string_literal: true

require "dependabot/pull_request_updater"

Dependabot.logger = CustomLogger.logger(source: "dependabot-core", logdev: :file)

module SharedHelpersPatch
  def run_helper_subprocess(**kwargs)
    command = JSON.pretty_generate({
      cmd: kwargs[:command],
      function: kwargs[:function],
      args: convert_args(kwargs[:args])
    }.compact)
    ApplicationHelper.log(:debug, "Performing native helper command:\n#{command}", tags: ["core"])
    super(**kwargs.merge({ args: convert_args(kwargs[:args], sanitize: false) })).tap do |result|
      ApplicationHelper.log(:debug, "Native helper command finished successfully, result:\n'#{result}'", tags: ["core"])
    end
  rescue ::Dependabot::SharedHelpers::HelperSubprocessFailed => e
    log_helper_error(e.error_context)
    raise e
  end

  # Log helper result
  #
  # @param [Hash] error_context
  # @return [void]
  def log_helper_error(error_context)
    debug_message = error_context.merge({ args: convert_args(error_context[:args]) })

    ApplicationHelper.log(
      :error,
      "Native helper command failed:\n#{JSON.pretty_generate(debug_message)}", tags: ["core"]
    )
  rescue StandardError => e
    ApplicationHelper.log(:error, "Attempted to log core helper subprocess result, but failed: #{e}", tags: ["core"])
  end

  # Convert credentials objects to hash and optionally sanitize sensitive field for log output
  #
  # @param [Object] args
  # @param [Boolean] sanitize
  # @return [Object]
  def convert_args(args, sanitize: true)
    credentials_hash = args.is_a?(Hash) && args[:credentials]

    return args unless credentials_hash || args.is_a?(Array)
    return args.merge({ credentials: credentials_to_hash(args[:credentials], sanitize: sanitize) }) if credentials_hash

    args.map do |arg|
      next arg unless arg_is_credential?(arg)

      credentials_to_hash(arg, sanitize: sanitize)
    end
  end

  # Check if arg is a credential
  #
  # @param [Object] arg
  # @return [Boolean]
  def arg_is_credential?(arg)
    arg.is_a?(Array) && arg.any? do |item|
      item.is_a?(Dependabot::Credential) || item.is_a?(Hash) && ::Registries::AUTH_FIELDS.any? do |key|
        item.key?(key)
      end
    end
  end

  # Convert credentials to hash
  #
  # @param [Array] credentials
  # @param [Boolean] sanitize
  # @return [Array]
  def credentials_to_hash(credentials, sanitize: true)
    credentials.map do |cred|
      cred_hash = cred.to_h
      next cred_hash unless sanitize

      sanitize_credentials(cred_hash)
    end
  end

  # Sanitize sensitive fields in credentials hash
  #
  # @param [Hash] credentials_hash
  # @return [Hash]
  def sanitize_credentials(credentials_hash)
    credentials_hash.each_with_object({}) do |(key, value), hsh|
      next hsh[key] = value unless ::Registries::AUTH_FIELDS.any? { |name| name == key }
      next hsh[key] = sanitize_registry(value) if key == ::Registries::REGISTRY_FIELD

      hsh[key] = "*****"
    end
  end

  # Replace sensitive part in registry url
  #
  # @param [String] registry
  # @return [String]
  def sanitize_registry(registry)
    return registry unless registry.match?(/.+:.+@.+/)

    registry.gsub(/:.+@/, ":*****@")
  end
end

# Dependabot patches
#
module Dependabot
  class PullRequestUpdater
    class Gitlab
      # Hacky method override to be able to pass old commit message directly to pr updater
      #
      # @return [String]
      def commit_being_updated
        Struct.new(:title).new(old_commit)
      end
    end
  end

  # Log dependabot helpers output to debug level
  #
  module SharedHelpers
    class << self
      prepend ::SharedHelpersPatch
    end
  end

  module Clients
    # Add additional logging and retry-able errors
    #
    class GitlabWithRetries
      delegate :log, to: :ApplicationHelper

      alias_method :old_initialize, :initialize

      def initialize(**args)
        @retry_interval = AppConfig.gitlab_api_max_retry_interval
        old_initialize(**args, max_retries: AppConfig.gitlab_api_max_retry)
      end

      def retry_connection_failures
        retry_attempt = 0

        begin
          yield
        rescue Gitlab::Error::MethodNotAllowed, Gitlab::Error::NotAcceptable, *RETRYABLE_ERRORS => e
          retry_attempt += 1
          raise unless retry_attempt <= @max_retries

          log(:warn, "Gitlab request failed with: '#{e}'. Retrying...") && sleep(@retry_interval)
          retry
        end
      end
    end
  end
end
