# frozen_string_literal: true

class Experiments
  FEATURES = {
    enable_shared_helpers_command_timeout: true,
    nuget_native_updater: true
  }.freeze

  def self.set!
    FEATURES.each { |feature, enabled| Dependabot::Experiments.register(feature, enabled) }
  end
end

Experiments.set!
