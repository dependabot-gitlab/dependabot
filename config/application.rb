# frozen_string_literal: true

require_relative "boot"

require "rails"
require "action_cable"
require "active_model/railtie"
require "active_job/railtie"
require "action_controller/railtie"
require "action_view/railtie"

# Do not require assets group gems in production
Bundler.require(*(Rails.env.production? ? Rails.groups : Rails.groups + %w[assets]))

require_relative "custom_logger"

module DependabotGitlab
  class Application < Rails::Application
    Rainbow.enabled = AppConfig.log_color

    logger = ActiveSupport::TaggedLogging.new(CustomLogger.logger(source: "dependabot"))

    config.load_defaults 8.0
    config.active_job.queue_adapter = :sidekiq

    config.log_level = AppConfig.log_level
    config.logger = logger

    config.mongoid.logger = CustomLogger.logger(source: "mongodb", logdev: :file)

    config.lograge.enabled = true
    config.lograge.base_controller_class = ["ActionController::API", "ActionController::Base"]

    Warning.process { |warning| logger.warn(warning.strip) }
  end
end
