# frozen_string_literal: true

class AppConfig < ApplicationConfig
  URL_REGEXP = %r{^(.*?)(/+)?$}

  env_prefix :settings_

  attr_config :dependabot_url,
              gitlab_url: "https://gitlab.com",
              gitlab_api_max_retry: 3,
              gitlab_api_max_retry_interval: 1,
              standalone: false,
              log_level: "info",
              log_color: false,
              log_stdout: true,
              create_project_hook: true,
              # mr command prefix
              commands_prefix: "$dependabot",
              # /metrics endpoint
              metrics: false,
              metrics_direct_store_path: nil,
              # project registration
              project_registration: "manual",
              project_registration_cron: "0 6 * * *",
              project_registration_allow_pattern: nil,
              project_registration_ignore_pattern: nil,
              project_registration_run_on_boot: false,
              # sentry configuration
              sentry_traces_sample_rate: 0.0,
              sentry_profiles_sample_rate: 0.0,
              sentry_ignored_errors: nil,
              # authentication
              anonymous_access: true,
              # sidekiq health check configuration
              sidekiq_alive_key_ttl: 60,
              # force ssl
              force_ssl: false,
              # application component
              app_component: nil

  # Gitlab url with removed trailing slash
  #
  # @return [URI]
  def gitlab_url
    sanitize_url(super)
  end

  # Dependabot url with removed trailing slash
  #
  # @return [String]
  def dependabot_url
    sanitize_url(super)
  end

  # Enable metrics
  #
  # @return [Boolean]
  def metrics?
    service_mode? && metrics
  end

  # Deployment integrated with gitlab
  #
  # @return [Boolean]
  def integrated?
    dependabot_url && create_project_hook
  end

  # App running in service mode
  #
  # @return [Boolean]
  def service_mode?
    !standalone
  end

  # Redis configuration
  #
  # @return [Hash]
  def redis_config
    @redis_config ||= {
      url: ENV["REDIS_URL"],
      password: ENV["REDIS_PASSWORD"],
      timeout: ENV.fetch("REDIS_TIMEOUT", 1).to_i,
      reconnect_attempts: 2
    }.compact
  end

  # Rails max threads
  #
  # @return [Integer]
  def max_threads
    ENV.fetch("RAILS_MAX_THREADS", 5)
  end

  private

  # Remove trailing slash from url
  #
  # @param [String] url
  # @return [URI]
  def sanitize_url(url)
    return unless url

    URI.parse(url.match(URL_REGEXP)[1])
  end
end
