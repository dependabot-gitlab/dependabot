# frozen_string_literal: true

Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Ensures that a master key has been made available in either ENV["RAILS_MASTER_KEY"]
  # or in config/master.key. This key is used to decrypt credentials (and other encrypted files).
  # config.require_master_key = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.public_file_server.enabled = ENV["RAILS_SERVE_STATIC_FILES"].present?

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = false

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Assume all access to the app is happening through a SSL-terminating reverse proxy.
  config.assume_ssl = false

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  config.force_ssl = AppConfig.force_ssl?

  # Prepend all log lines with the following tags.
  config.log_tags = [:request_id]

  # Use a real queuing backend for Active Job (and separate queues per environment).
  # config.active_job.queue_adapter     = :resque
  # config.active_job.queue_name_prefix = "dependabot_gitlab_production"

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Prevent health checks from clogging up the logs.
  config.silence_healthcheck_path = "/healthcheck"

  # Use redis for caching
  config.cache_store = if AppConfig.standalone?
                         [:memory_store, { size: 64.megabytes }]
                       else
                         [:redis_cache_store,
                          {
                            url: ENV["REDIS_URL"],
                            password: ENV["REDIS_PASSWORD"],
                            expires_in: 24.hours,
                            namespace: "cache",
                            read_timeout: 0.2,
                            write_timeout: 0.2,
                            reconnect_attempts: 1,
                            error_handler: lambda do |method:, returning:, exception:| # rubocop:disable Lint/UnusedBlockArgument
                              Sentry.capture_exception(exception, tags: { method: method })
                            end
                          }.compact]
                       end
end
