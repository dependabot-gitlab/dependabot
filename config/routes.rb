# frozen_string_literal: true

require "sidekiq/web"
require "sidekiq/cron/web"

Sidekiq::Web.use(Auth)

Rails.application.routes.draw do
  concern :paginatable do
    get "(page/:page)", action: :projects_table, on: :collection, as: ""
  end

  mount Sidekiq::Web, at: "/sidekiq"
  mount AuthHelper.with_auth(Yabeda::Prometheus::Exporter), at: "/metrics", via: :get if AppConfig.metrics?
  mount API => "/"

  root AppConfig.anonymous_access ? "projects#index" : "sessions#new"

  get "healthcheck" => "health#show", as: :rails_health_check

  get "sign_in", to: "sessions#new"
  post "sign_in", to: "sessions#create"
  delete "logout", to: "sessions#destroy"

  resources :projects, only: %i[index create update destroy] do
    scope module: :update do
      resources :jobs, only: [] do
        put "execute", on: :member
        put "toggle", on: :member
      end

      resources :runs, only: [:show]
    end
  end

  get "/projects/table", to: "projects#table", concerns: :paginatable, as: "projects_table"
end
