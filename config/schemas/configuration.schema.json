{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Dependabot configuration file schema",
  "$defs": {
    "dependency-name": {
      "type": "string",
      "description": "Name of the dependency",
      "examples": [
        "lodash"
      ]
    },
    "dependency-type": {
      "description": "Type of dependency",
      "enum": [
        "direct",
        "indirect",
        "all",
        "production",
        "development"
      ]
    },
    "versions": {
      "type": "array",
      "items": {
        "type": "string",
        "description": "Use to ignore specific versions or ranges of versions.",
        "examples": [
          "^1.0.0",
          ">~> 2.0"
        ]
      }
    },
    "update-types": {
      "type": "array",
      "items": {
        "description": "Use to ignore types of updates, such as semver major, minor, or patch updates on version updates.",
        "enum": [
          "version-update:semver-major",
          "version-update:semver-minor",
          "version-update:semver-patch"
        ]
      }
    },
    "vulnerability-alerts": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "enabled": {
          "type": "boolean",
          "description": "Enable or disable vulnerability alerts.",
          "examples": [
            true
          ]
        },
        "assignees": {
          "type": "array",
          "items": {
            "type": "string",
            "description": "Assignees to add to the vulnerability alert.",
            "examples": [
              "dependabot"
            ]
          }
        },
        "confidential": {
          "type": "boolean",
          "description": "Create vulnerability alert issues as confidential.",
          "examples": [
            true
          ]
        }
      }
    },
    "ecosystem-config": {
      "type": "object",
      "additionalProperties": false,
      "properties": {
        "package-ecosystem": {
          "description": "Package ecosystem name.",
          "enum": [
            "bundler",
            "npm",
            "bun",
            "gomod",
            "pip",
            "docker",
            "composer",
            "pub",
            "cargo",
            "nuget",
            "maven",
            "gradle",
            "mix",
            "terraform",
            "elm",
            "gitsubmodule",
            "swift",
            "devcontainers",
            "docker-compose"
          ]
        },
        "directory": {
          "type": "string",
          "description": "Directory containing the package manager files."
        },
        "directories": {
          "type": "array",
          "items": {
            "type": "string",
            "description": "Directories containing the package manager files."
          }
        },
        "name": {
          "type": "string",
          "description": "Name of the config entry. Used in standalone mode to identify the config entry.",
          "examples": [
            "frontend",
            "backend"
          ]
        },
        "schedule": {
          "type": "object",
          "additionalProperties": false,
          "properties": {
            "interval": {
              "description": "Schedule interval.",
              "enum": [
                "daily",
                "weekly",
                "monthly",
                "weekday"
              ]
            },
            "day": {
              "description": "Day of the week to run the schedule.",
              "enum": [
                "monday",
                "tuesday",
                "wednesday",
                "thursday",
                "friday",
                "saturday",
                "sunday"
              ]
            },
            "time": {
              "type": "string",
              "description": "Time of day to run the schedule.",
              "format": "time",
              "examples": [
                "00:00",
                "12:00",
                "23:59"
              ],
              "x-error": "Time must be in the format '$HH:MM' (e.g. '23:59')"
            },
            "timezone": {
              "type": "string",
              "description": "Timezone to run the schedule.",
              "examples": [
                "UTC",
                "America/New_York",
                "Europe/Paris"
              ]
            },
            "hours": {
              "type": "string",
              "description": "Specified hours to randomly run the schedule.",
              "format": "hours",
              "examples": [
                "9-17"
              ],
              "x-error": "Hours must be in the format '$from-$to' (e.g. '9-17')"
            }
          },
          "required": [
            "interval"
          ]
        },
        "commit-message": {
          "type": "object",
          "additionalProperties": false,
          "properties": {
            "include": {
              "type": "string",
              "description": "Specifies that any prefix is followed by a list of the dependencies updated in the commit.",
              "examples": [
                "scope"
              ]
            },
            "prefix": {
              "type": "string",
              "description": "Commit message prefix for all commits.",
              "examples": [
                "chore(deps)"
              ]
            },
            "prefix-development": {
              "type": "string",
              "description": "Commit message prefix for development dependencies.",
              "examples": [
                "chore(deps-dev)"
              ]
            },
            "trailers": {
              "type": "array",
              "items": {
                "type": "object",
                "description": "Commit message trailers.",
                "additionalProperties": {
                  "type": "string"
                },
                "examples": [
                  {
                    "Changelog": "dependencies"
                  }
                ]
              }
            },
            "trailers-security": {
              "type": "array",
              "items": {
                "type": "object",
                "description": "Commit message trailers for security updates.",
                "additionalProperties": {
                  "type": "string"
                },
                "examples": [
                  {
                    "Changelog": "security"
                  }
                ]
              }
            },
            "trailers-development": {
              "type": "array",
              "items": {
                "type": "object",
                "description": "Commit message trailers for development dependencies.",
                "additionalProperties": true,
                "examples": [
                  {
                    "Changelog": "dependencies-dev"
                  }
                ]
              }
            }
          }
        },
        "author-details": {
          "type": "object",
          "additionalProperties": false,
          "properties": {
            "name": {
              "type": "string",
              "description": "Author name to use for commits.",
              "examples": [
                "Dependabot"
              ]
            },
            "email": {
              "type": "string",
              "description": "Author email to use for commits.",
              "examples": [
                "dependabot@example.com"
              ]
            }
          }
        },
        "allow": {
          "type": "array",
          "items": {
            "type": "object",
            "additionalProperties": false,
            "properties": {
              "dependency-name": {
                "$ref": "#/$defs/dependency-name"
              },
              "dependency-type": {
                "$ref": "#/$defs/dependency-type"
              }
            }
          }
        },
        "ignore": {
          "type": "array",
          "items": {
            "type": "object",
            "additionalProperties": false,
            "properties": {
              "dependency-name": {
                "$ref": "#/$defs/dependency-name"
              },
              "versions": {
                "$ref": "#/$defs/versions"
              },
              "update-types": {
                "$ref": "#/$defs/update-types"
              }
            },
            "required": [
              "dependency-name"
            ]
          }
        },
        "pull-request-branch-name": {
          "type": "object",
          "additionalProperties": false,
          "properties": {
            "separator": {
              "type": "string",
              "description": "Separator to use between the prefix and the dependency name.",
              "examples": [
                "-"
              ]
            },
            "prefix": {
              "type": "string",
              "description": "Prefix to use for the branch name.",
              "examples": [
                "dependabot"
              ]
            },
            "max-length": {
              "type": "integer",
              "description": "Maximum length of the branch name.",
              "examples": [
                64
              ]
            }
          }
        },
        "vulnerability-alerts": {
          "type": "object",
          "$ref": "#/$defs/vulnerability-alerts"
        },
        "assignees": {
          "type": "array",
          "items": {
            "type": "string",
            "description": "Assignees to add to the merge request.",
            "examples": [
              "dependabot"
            ]
          }
        },
        "reviewers": {
          "type": "array",
          "items": {
            "type": "string",
            "description": "Reviewers to add to the merge request.",
            "examples": [
              "dependabot"
            ]
          }
        },
        "approvers": {
          "type": "array",
          "items": {
            "type": "string",
            "description": "Approvers to add to the merge request.",
            "examples": [
              "dependabot"
            ]
          }
        },
        "labels": {
          "type": "array",
          "items": {
            "type": "string",
            "description": "Custom labels to add to the merge request.",
            "examples": [
              "dependencies"
            ]
          }
        },
        "milestone": {
          "type": "string",
          "description": "Milestone to add to the merge request.",
          "examples": [
            "1.0.0"
          ]
        },
        "vendor": {
          "type": "boolean",
          "description": "Use the vendor option to tell Dependabot to vendor dependencies when updating them.",
          "examples": [
            true
          ]
        },
        "open-pull-requests-limit": {
          "type": "integer",
          "description": "Maximum number of open pull requests.",
          "examples": [
            5
          ]
        },
        "open-security-pull-requests-limit": {
          "type": "integer",
          "description": "Maximum number of open security pull requests.",
          "examples": [
            5
          ]
        },
        "target-branch": {
          "type": "string",
          "description": "Target branch to create pull requests against. This option does not affect where application is fetching configuration from.",
          "examples": [
            "master"
          ]
        },
        "versioning-strategy": {
          "description": "Versioning strategy to use when updating dependencies.",
          "enum": [
            "auto",
            "lockfile-only",
            "widen",
            "increase",
            "increase-if-necessary"
          ]
        },
        "insecure-external-code-execution": {
          "type": "string",
          "description": "Allow external code execution for bundler, mix and pip package managers.",
          "examples": [
            "allow"
          ]
        },
        "updater-options": {
          "type": "object",
          "description": "Custom options to pass to internal dependabot-core updater classes.",
          "additionalProperties": true
        },
        "unsubscribe-from-mr": {
          "type": "boolean",
          "description": "Unsubscribe from the merge request after it is created.",
          "examples": [
            true
          ]
        },
        "auto-approve": {
          "type": "boolean",
          "description": "Automatically approve the merge request after it is created.",
          "examples": [
            true
          ]
        },
        "auto-merge": {
          "anyOf": [
            {
              "type": "boolean",
              "description": "Enable or disable auto merge for merge request.",
              "examples": [
                true
              ]
            },
            {
              "type": "object",
              "additionalProperties": false,
              "properties": {
                "squash": {
                  "type": "boolean",
                  "description": "Set squash option to true for merge request.",
                  "examples": [
                    true
                  ]
                },
                "delay": {
                  "type": "integer",
                  "description": "Delay in seconds before setting 'Merge when pipeline succeeds' option.",
                  "examples": [
                    10
                  ]
                },
                "merge-train": {
                  "type": "boolean",
                  "description": "Add merge request to merge train instead of merging directly.",
                  "examples": [
                    true
                  ]
                },
                "allow": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "additionalProperties": false,
                    "properties": {
                      "dependency-name": {
                        "$ref": "#/$defs/dependency-name"
                      },
                      "dependency-type": {
                        "$ref": "#/$defs/dependency-type"
                      },
                      "versions": {
                        "$ref": "#/$defs/versions"
                      },
                      "update-types": {
                        "$ref": "#/$defs/update-types"
                      }
                    },
                    "required": [
                      "dependency-name"
                    ]
                  }
                },
                "ignore": {
                  "type": "array",
                  "items": {
                    "type": "object",
                    "additionalProperties": false,
                    "properties": {
                      "dependency-name": {
                        "$ref": "#/$defs/dependency-name"
                      },
                      "dependency-type": {
                        "$ref": "#/$defs/dependency-type"
                      },
                      "versions": {
                        "$ref": "#/$defs/versions"
                      },
                      "update-types": {
                        "$ref": "#/$defs/update-types"
                      }
                    },
                    "required": [
                      "dependency-name"
                    ]
                  }
                }
              }
            }
          ]
        },
        "rebase-strategy": {
          "anyOf": [
            {
              "description": "Rebase strategy to use for merge request updates.",
              "enum": [
                "auto",
                "all",
                "none"
              ]
            },
            {
              "type": "object",
              "additionalProperties": false,
              "properties": {
                "strategy": {
                  "description": "Rebase strategy to use for merge request updates.",
                  "enum": [
                    "auto",
                    "all",
                    "none"
                  ]
                },
                "on-approval": {
                  "type": "boolean",
                  "description": "Rebase merge request on approval.",
                  "examples": [
                    true
                  ]
                },
                "with-assignee": {
                  "type": "string",
                  "description": "Define specific assignee that allows to automatically rebase merge request.",
                  "examples": [
                    "dependabot"
                  ]
                }
              }
            }
          ]
        },
        "registries": {
          "anyOf": [
            {
              "const": "*"
            },
            {
              "type": "array",
              "description": "Allowed registry names from top level registries definition.",
              "items": {
                "type": "string",
                "examples": [
                  "npm",
                  "docker"
                ]
              }
            }
          ]
        }
      }
    }
  },
  "type": "object",
  "additionalProperties": false,
  "properties": {
    "version": {
      "type": "integer",
      "description": "Version of the configuration file.",
      "examples": [
        2
      ]
    },
    "fork": {
      "type": "boolean",
      "description": "Create dependency update merge requests in a forked repository.",
      "examples": [
        true
      ]
    },
    "registries": {
      "type": "object",
      "description": "Custom registries to use when updating dependencies.",
      "additionalProperties": true
    },
    "update-options": {
      "description": "Common options applied to all entries in the updates array",
      "$ref": "#/$defs/ecosystem-config"
    },
    "updates": {
      "type": "array",
      "description": "List of ecosystem configurations",
      "minItems": 1,
      "items": {
        "description": "Single ecosystem update configuration entry",
        "$ref": "#/$defs/ecosystem-config",
        "oneOf": [
          {
            "required": [
              "package-ecosystem",
              "directory"
            ]
          },
          {
            "required": [
              "package-ecosystem",
              "directories"
            ]
          }
        ]
      }
    }
  },
  "required": [
    "version",
    "updates"
  ]
}
