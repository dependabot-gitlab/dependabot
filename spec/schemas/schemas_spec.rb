# frozen_string_literal: true

RSpec.describe Schemas do
  it "has valid configuration schema" do
    expect(JSONSchemer.schema(described_class.configuration_schema)).to be_valid_schema
  end
end
