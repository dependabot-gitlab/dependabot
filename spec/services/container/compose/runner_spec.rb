# frozen_string_literal: true

describe Container::Compose::Runner do
  subject(:create_container) do
    described_class.call(
      package_ecosystem: ecosystem,
      task_name: task_name,
      task_args: task_args
    )
  end

  let(:project) { "test-project" }
  let(:ecosystem) { "bundler" }
  let(:directory) { "/" }
  let(:sha) { "123" }
  let(:task_name) { "update" }
  let(:task_args) { [project, ecosystem, directory] }
  let(:always_pull) { false }
  let(:success) { true }
  let(:exitstatus) { 0 }
  let(:cert_file) { nil }

  let(:wait_thr) do
    instance_double(
      Process::Waiter,
      value: instance_double(Process::Status, success?: success, exitstatus: exitstatus)
    )
  end

  let(:stdin) { instance_double(IO, close: nil) }
  let(:err) { instance_double(IO, read: "command error output") }
  let(:out) { ["command output"] }

  let(:base_image) { "image:tag" }

  let(:compose_hash) do
    {
      "x-updater" => {
        "network_mode" => "host",
        "environment" => {
          "SETTINGS__METRICS" => "false",
          "SSL_CERT_FILE" => "/etc/ssl/custom/tls-ca-bundle.pem"
        }
      }
    }
  end

  let(:updater_conf) do
    {
      "services" => {
        "updater" => {
          "image" => "image-bundler:latest",
          **compose_hash["x-updater"].then do |conf|
            conf.merge("environment" => conf["environment"].except("SSL_CERT_FILE"))
          end
        }
      }
    }.to_yaml
  end

  let(:compose_cmd) do
    [
      "docker", "compose", "-f", "-", "run", "--no-TTY", "--quiet-pull",
      "--name", "updater-#{ecosystem}-#{sha}-#{sha}", "--rm", "updater",
      "bundle", "exec", "rake", "dependabot:#{task_name}[#{project},#{ecosystem},#{directory}]"
    ]
  end

  let(:pull_cmd) { ["docker", "pull", "--quiet", "image-bundler:latest"] }

  let(:env) do
    {
      "BASE_IMAGE" => base_image,
      "SETTINGS__DEPLOY_MODE" => "compose",
      "SETTINGS__UPDATER_IMAGE_PATTERN" => "image-%<package_ecosystem>s:latest",
      "SETTINGS__COMPOSE_UPDATER_ALWAYS_PULL" => always_pull.to_s,
      "SSL_CERT_FILE" => cert_file
    }.compact
  end

  before do
    allow(Open3).to receive(:popen3).and_yield(stdin, out, err, wait_thr)
    allow(YAML).to receive(:load_file).with("docker-compose.yml", aliases: true).and_return(compose_hash)
    allow(SecureRandom).to receive(:alphanumeric).and_return(sha)
    allow(stdin).to receive(:write).with(kind_of(String))
  end

  around do |example|
    with_env(env) { example.run }
  end

  context "with success", :aggregate_failures do
    it "executes updater compose command and prints output" do
      expect { create_container }.to output("#{out.first}\n").to_stdout

      expect(Open3).not_to have_received(:popen3).with(*pull_cmd)
      expect(Open3).to have_received(:popen3).with(*compose_cmd)
      expect(stdin).to have_received(:write).with(updater_conf)
      expect(stdin).to have_received(:close)
    end
  end

  context "with failure" do
    let(:success) { false }
    let(:exitstatus) { 1 }

    before do
      allow($stdout).to receive(:puts)
    end

    it "raises Container::Failure error with stderr output" do
      expect { create_container }.to raise_error(
        Container::Failure,
        "Command exited with code: #{exitstatus}!"
      )
    end
  end

  context "with pull updater image set to true" do
    let(:always_pull) { true }

    it "pulls updater image before running update", :aggregate_failures do
      expect { create_container }.to output("#{out.first}\n").to_stdout

      expect(Open3).to have_received(:popen3).with(*pull_cmd)
      expect(Open3).to have_received(:popen3).with(*compose_cmd)
    end
  end

  context "with custom ssl cert file" do
    let(:cert_file) { Tempfile.create.path.tap { |path| File.write(path, "cert content") } }
    let(:updater_conf) do
      {
        "services" => {
          "updater" => {
            "image" => "image-bundler:latest",
            **compose_hash["x-updater"].then do |conf|
              volumes = conf["volumes"] || []
              volumes << "#{cert_file}:#{cert_file}:ro"
              conf.merge("volumes" => volumes)
            end
          }
        }
      }.to_yaml
    end

    after { File.unlink(cert_file) }

    it "starts updater container with custom ssl cert file" do
      expect { create_container }.to output.to_stdout
      expect(stdin).to have_received(:write).with(updater_conf)
    end
  end
end
