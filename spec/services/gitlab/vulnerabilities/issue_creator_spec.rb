# frozen_string_literal: true

describe Gitlab::Vulnerabilities::IssueCreator, :integration do
  subject(:issue_creator) do
    described_class.new(
      project: project,
      vulnerability: vulnerability,
      dependency_file: dependency_file,
      assignees: assignees,
      confidential: true
    )
  end

  let(:gitlab) { instance_double(Gitlab::Client, issues: existing_issues) }
  let(:project) { create(:project) }
  let(:vulnerability) { create(:vulnerability) }
  let(:issue_body) { "issue_body" }
  let(:assignees) { ["username"] }
  let(:assignee_ids) { [1] }
  let(:existing_issues) { [] }
  let(:gitlab_vulnerability_issue) { Gitlab::ObjectifiedHash.new({ "iid" => 1, "web_url" => "url" }) }
  let(:labels) { { "security" => "#eee600", "severity:high" => "#ed9121" } }
  let(:issue_create_labels) { labels.keys.join(",") }

  let(:dependency_file) do
    Dependabot::DependencyFile.new(
      name: "Gemfile",
      directory: "./",
      content: ""
    )
  end

  before do
    allow(Gitlab::ClientWithRetry).to receive(:current) { gitlab }
    allow(Gitlab::UserFinder).to receive(:find).with(assignees).and_return(assignee_ids)
    allow(Gitlab::Vulnerabilities::IssueTemplate).to receive(:call)
      .with(vulnerability, dependency_file)
      .and_return(issue_body)
    allow(gitlab).to receive(:create_issue)
      .with(
        project.name,
        vulnerability.summary,
        description: issue_body,
        labels: issue_create_labels,
        confidential: true,
        assignee_id: 1
      )
      .and_return(gitlab_vulnerability_issue)

    labels.each do |name, color|
      allow(Gitlab::LabelCreator).to receive(:call).with(project_name: project.name, name: name, color: color)
    end
  end

  context "with standalone mode", :standalone do
    context "without existing issue" do
      it "creates vulnerability issue" do
        expect(issue_creator.call).to eq(gitlab_vulnerability_issue)
      end
    end

    context "with existing issue" do
      let(:existing_issues) { [gitlab_vulnerability_issue] }

      it "skips issue creation" do
        issue_creator.call

        expect(gitlab).not_to have_received(:create_issue)
      end
    end
  end

  context "without existing issue" do
    it "creates vulnerability issue", :aggregate_failures do
      created_issue = issue_creator.call

      expect(created_issue.attributes).to include({
        "iid" => gitlab_vulnerability_issue.iid,
        "directory" => dependency_file.directory,
        "package" => vulnerability.package,
        "package_ecosystem" => vulnerability.package_ecosystem,
        "web_url" => gitlab_vulnerability_issue.web_url,
        "status" => "opened"
      })
      expect(created_issue).to eq(VulnerabilityIssue.find_by(id: created_issue.id))
    end
  end

  context "with existing issue" do
    before do
      VulnerabilityIssue.create(
        iid: gitlab_vulnerability_issue.iid,
        project: project,
        directory: dependency_file.directory,
        package: vulnerability.package,
        package_ecosystem: vulnerability.package_ecosystem,
        vulnerability: vulnerability,
        web_url: gitlab_vulnerability_issue.web_url
      )
    end

    it "skips issue creation" do
      issue_creator.call

      expect(gitlab).not_to have_received(:create_issue)
    end
  end

  context "with label creation failure" do
    let(:issue_create_labels) { "" }
    let(:response) do
      Gitlab::ObjectifiedHash.new(
        code: 500,
        parsed_response: "Failure",
        request: { base_uri: "gitlab.com", path: "/merge_request" }
      )
    end

    before do
      allow(Gitlab::LabelCreator).to receive(:call).and_raise(Gitlab::Error::Forbidden.new(response))
    end

    it "creates vulnerability issue without labels", :aggregate_failures do
      created_issue = issue_creator.call

      expect(created_issue).to eq(VulnerabilityIssue.find_by(id: created_issue.id))
      expect(gitlab).to have_received(:create_issue).with(
        project.name,
        vulnerability.summary,
        hash_including(labels: "")
      )
    end
  end
end
