# frozen_string_literal: true

describe Webhooks::CommentEventHandler, :integration do
  subject(:action) do
    described_class.call(
      discussion_id: discussion_id,
      note: command,
      project_name: project.name,
      mr_iid: mr_iid,
      state: state
    )
  end

  let(:gitlab) { instance_double(Gitlab::Client, rebase_merge_request: nil, resolve_merge_request_discussion: nil) }
  let(:project) { build(:project_with_mr) }
  let(:mr_iid) { project.merge_requests.first.iid }
  let(:job) { Mr::RecreateJob }
  let(:discussion_id) { "11r4" }
  let(:state) { "opened" }

  before do
    allow(Gitlab::ClientWithRetry).to receive(:current) { gitlab }
    allow(Gitlab::MergeRequest::DiscussionReplier).to receive(:call)
  end

  context "with invalid command", :aggregate_failures do
    let(:command) { "$dependabot test" }
    let(:message) { "Comment did not match command pattern '(?-mix:^\\$dependabot (?<action>recreate)$)'" }

    it "skips action" do
      expect(action).to eq({ message: message, skipped: true })
    end
  end

  context "with non existing project" do
    let(:command) { "$dependabot recreate" }

    it "raises error" do
      expect { action }.to raise_error(Mongoid::Errors::DocumentNotFound)
    end
  end

  context "with non existing merge request" do
    let(:command) { "$dependabot recreate" }
    let(:project) { create(:project) }
    let(:mr_iid) { 0 }
    let(:message) { "Merge request is not managed by dependabot" }

    it "raises error and adds reply" do
      expect { action }.to raise_error(message)
      expect(Gitlab::MergeRequest::DiscussionReplier).to have_received(:call).with(
        project_name: project.name,
        discussion_id: discussion_id,
        mr_iid: mr_iid,
        note: ":x: #{message}"
      )
    end
  end

  context "with recreate action" do
    let(:project) { create(:project_with_mr) }
    let(:command) { "$dependabot recreate" }

    before do
      allow(Mr::RecreateJob).to receive(:perform_later)
    end

    it "triggers merge request recreate job" do
      action

      expect(Mr::RecreateJob).to have_received(:perform_later).with(project.name, mr_iid, discussion_id)
    end
  end

  context "with merged mr" do
    let(:command) { "$dependabot recreate" }
    let(:project) { create(:project_with_mr) }
    let(:state) { "merged" }

    it "raises error and adds reply" do
      expect(action).to eq({ message: "Merge request is not in open state", skipped: true })
      expect(Gitlab::MergeRequest::DiscussionReplier).to have_received(:call).with(
        project_name: project.name,
        discussion_id: discussion_id,
        mr_iid: mr_iid,
        note: ":x: Merge request is not in open state, please reopen it first!"
      )
    end
  end
end
