# frozen_string_literal: true

describe Dependabot::Files::Parser do
  subject(:parsed_deps) do
    described_class.call(
      config_entry: config_entry,
      credentials: credentials,
      multi_dir_file_fetcher: fetcher,
      repo_contents_path: nil,
      dependency_name: dependency_name
    )
  end

  include_context "with dependabot helper"

  let(:parser) { instance_double(Dependabot::Bundler::FileParser, parse: [dependency]) }
  let(:config_entry) { updates_config.first }
  let(:credentials) { [*Credentials.call(nil), *registries.values] }
  let(:dependency_name) { nil }

  before do
    allow(Dependabot::FileParsers).to receive(:for_package_manager)
      .with("bundler")
      .and_return(Dependabot::Bundler::FileParser)
    allow(Dependabot::Bundler::FileParser).to receive(:new)
      .with(credentials: credentials,
            reject_external_code: config_entry[:reject_external_code],
            options: config_entry[:updater_options],
            dependency_files: fetcher.files.first,
            source: fetcher.source,
            repo_contents_path: nil)
      .and_return(parser)
  end

  context "without specific dependency name" do
    it "returns parsed dependencies", :aggregate_failures do
      expect(parsed_deps).to eq([{ name: "/", files: fetcher.files.flatten, dependencies: [dependency] }])
    end
  end

  context "with specific dependency name" do
    let(:dependency_name) { "foo" }

    it "returns parsed dependencies", :aggregate_failures do
      expect(parsed_deps).to eq([{ name: "/", files: fetcher.files.flatten, dependencies: [] }])
    end
  end
end
