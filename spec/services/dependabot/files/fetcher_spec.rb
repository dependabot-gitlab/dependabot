# frozen_string_literal: true

require "tmpdir"

describe Dependabot::Files::Fetcher do
  subject(:file_fetcher) do
    described_class.call(
      project_name: project_name,
      config_entry: config_entry,
      credentials: credentials,
      repo_contents_path: repo_contents_path
    )
  end

  let(:project) { build(:project, config_yaml: config_yaml) }
  let(:commit) { "commit" }
  let(:repo_contents_path) { "path" }
  let(:project_name) { project.name }
  let(:package_ecosystem) { "bundler" }
  let(:config_entry) { project.configuration.entry(package_ecosystem: package_ecosystem) }

  let(:credentials) do
    [
      Dependabot::Credential.new({
        "type" => "git_source",
        "host" => URI(AppConfig.gitlab_url).host,
        "username" => "x-access-token",
        "password" => "token"
      })
    ]
  end

  let(:config_yaml) do
    <<~YAML
      version: 2
      updates:
        - package-ecosystem: bundler
          directory: "/"
          schedule:
            interval: weekly
    YAML
  end

  let(:fetcher) do
    instance_double(
      Dependabot::Bundler::FileFetcher,
      files: files,
      source: source,
      commit: commit,
      clone_repo_contents: nil
    )
  end

  def dependabot_source(directory: nil, directories: nil)
    Dependabot::Source.new(
      provider: "gitlab",
      hostname: AppConfig.gitlab_url.host,
      api_endpoint: "#{AppConfig.gitlab_url}/api/v4",
      repo: project_name,
      branch: nil,
      directory: directory,
      directories: directories
    )
  end

  before do
    allow(Dependabot::FileFetchers).to receive(:for_package_manager)
      .with(package_ecosystem)
      .and_return(Dependabot::Bundler::FileFetcher)

    allow(Dependabot::Bundler::FileFetcher).to receive(:new)
      .with(source: source, repo_contents_path: repo_contents_path, credentials: credentials)
      .and_return(fetcher)
  end

  context "without glob pattern" do
    let(:files) { ["file"] }
    let(:source) { dependabot_source(directory: "/") }

    before do
      allow(Dependabot::SourceWrapper).to receive(:call)
        .with(repo: project.name, directory: "/", directories: nil, branch: nil)
        .and_return(source)
    end

    it "returns file from single directory" do
      expect(file_fetcher.files).to eq([files])
      expect(file_fetcher.source).to eq(source)
      expect(file_fetcher.commit).to eq(commit)

      expect(fetcher).to have_received(:clone_repo_contents)
    end
  end

  context "with glob pattern" do
    let!(:repo_contents_path) { Dir.mktmpdir }

    let(:directories) { ["/ruby-1", "/ruby-2"] }
    let(:files) { %w[file-1 file-2] }
    let(:default_source) { dependabot_source }
    let(:source) { dependabot_source(directories: directories) }
    let(:default_fetcher) { instance_double(Dependabot::Bundler::FileFetcher, clone_repo_contents: nil) }

    let(:config_yaml) do
      <<~YAML
        version: 2
        updates:
          - package-ecosystem: bundler
            directory: "/ruby-*"
            schedule:
              interval: weekly
      YAML
    end

    before do
      allow(Dependabot::SourceWrapper).to receive(:call)
        .with(repo: project.name, directory: nil, directories: nil, branch: nil)
        .and_return(default_source)
      allow(Dependabot::SourceWrapper).to receive(:call)
        .with(repo: project.name, directories: directories, directory: nil, branch: nil)
        .and_return(source)
      allow(Dependabot::Bundler::FileFetcher).to receive(:new)
        .with(source: default_source, repo_contents_path: repo_contents_path, credentials: credentials)
        .and_return(default_fetcher)

      # Create mock repo structure and mock source and fetcher objects for each entry
      directories.each_with_index do |dir, index|
        Dir.mkdir(File.join(repo_contents_path, dir))
        src = dependabot_source(directory: dir)
        ff = instance_double(
          Dependabot::Bundler::FileFetcher,
          files: [files[index]],
          source: src,
          commit: commit
        )

        allow(Dependabot::SourceWrapper).to receive(:call)
          .with(repo: project.name, directory: dir, directories: nil, branch: nil)
          .and_return(src)
        allow(Dependabot::Bundler::FileFetcher).to receive(:new)
          .with(source: src, repo_contents_path: repo_contents_path, credentials: credentials)
          .and_return(ff)
      end
    end

    it "returns files from multiple directories" do
      expect(file_fetcher.files).to eq(files.map { |file| [file] })
      expect(file_fetcher.source).to eq(source)
      expect(file_fetcher.commit).to eq(commit)

      expect(default_fetcher).to have_received(:clone_repo_contents)
    end
  end
end
