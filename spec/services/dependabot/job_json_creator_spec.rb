# frozen_string_literal: true

require "tmpdir"

describe Dependabot::JobJsonCreator do
  subject(:job_json_path) do
    described_class.call(
      project_name: project_name,
      default_branch: "master",
      config_entry: updates_config.first
    )
  end

  include_context "with dependabot helper"

  let(:allowed_updates) { { "update-type": "all" } }
  let(:path) { File.join(Dir.tmpdir, project_name, "job.json") }
  let(:job_json) do
    {
      job: {
        "allowed-updates": allow_conf,
        "package-manager": package_manager,
        experiments: {
          enable_shared_helpers_command_timeout: true,
          nuget_native_updater: true
        },
        source: {
          provider: "gitlab",
          repo: project_name,
          directory: "/",
          branch: "master"
        }
      }
    }.to_json
  end

  before do
    allow(File).to receive(:write).with(path, kind_of(String))
    allow(FileUtils).to receive(:mkdir_p).with(File.join(Dir.tmpdir, project_name))
  end

  it "creates job json" do
    expect(job_json_path).to eq(path)
    expect(File).to have_received(:write).with(path, job_json)
  end
end
