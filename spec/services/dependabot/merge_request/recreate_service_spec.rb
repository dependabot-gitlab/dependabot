# frozen_string_literal: true

describe Dependabot::MergeRequest::RecreateService, :integration do
  subject(:update) do
    described_class.call(
      project_name: project.name,
      mr_iid: mr.iid,
      discussion_id: discussion_id
    )
  end

  include_context "with dependabot helper"

  let(:gitlab) do
    instance_double(Gitlab::Client, resolve_merge_request_discussion: nil, project: Gitlab::ObjectifiedHash.new({
      "default_branch" => branch
    }))
  end

  let(:pr_updater) { instance_double(Dependabot::PullRequestUpdater, update: nil) }
  let(:dependencies) { [dependency] }

  let(:config_yaml) do
    <<~YAML
      version: 2
      registries:
        dockerhub:
          type: docker-registry
          url: registry.hub.docker.com
          username: octocat
          password: password
      updates:
        - package-ecosystem: bundler
          directory: "/"
          schedule:
            interval: weekly
    YAML
  end

  let(:project) do
    create(
      :project_with_mr,
      config_yaml: config_yaml,
      dependency: "config",
      commit_message: "original-commit",
      update_to: update_to_versions,
      branch: branch
    )
  end

  let(:mr) { project.merge_requests.first }
  let(:config_entry) { project.configuration.entry(package_ecosystem: "bundler") }
  let(:registries) { project.configuration.registries }
  let(:credentials) { [*Credentials.call(nil), *registries.select(".*")] }
  let(:update_to_versions) { updated_dependency.current_versions.gsub("config-", "") }
  let(:dependency_state) { Dependabot::UpdatedDependency::HAS_UPDATES }
  let(:branch) { "update-branch" }
  let(:discussion_id) { 123 }
  let(:dependency_name) { "config" }

  let(:updated_dependency) do
    Dependabot::UpdatedDependency.new(
      dependency: dependency,
      dependency_files: [instance_double(Dependabot::DependencyFile)],
      state: dependency_state,
      updated_dependencies: updated_dependencies,
      updated_files: updated_files,
      vulnerable: false,
      auto_merge_rules: [],
      error: StandardError.new("Update failed!")
    )
  end

  let(:updater_args) do
    {
      credentials: kind_of(Array),
      source: fetcher.source,
      base_commit: fetcher.commit,
      old_commit: mr.commit_message,
      pull_request_number: mr.iid,
      files: updated_dependency.updated_files,
      provider_metadata: { target_project_id: nil }
    }
  end

  let(:repo_contents_path) do
    Rails.root.join("tmp", "repo-contents", project.name).to_s
  end

  before do
    allow(Gitlab::ClientWithRetry).to receive(:current) { gitlab }
    allow(Dependabot::Files::Fetcher).to receive(:call)
      .with(
        project_name: project.name,
        config_entry: config_entry,
        repo_contents_path: repo_contents_path,
        credentials: kind_of(Array)
      )
      .and_return(fetcher)
    allow(Dependabot::Files::Parser).to receive(:call)
      .with(
        multi_dir_file_fetcher: fetcher,
        repo_contents_path: repo_contents_path,
        config_entry: config_entry,
        dependency_name: dependency_name,
        credentials: kind_of(Array)
      )
      .and_return([{ files: fetcher.files.first, dependencies: dependencies }])

    allow(Dependabot::Update::Checker).to receive(:call)
      .with(
        dependency: dependency,
        dependency_files: fetcher.files.first,
        config_entry: config_entry,
        repo_contents_path: repo_contents_path,
        credentials: kind_of(Array),
        ignore_rules: true
      )
      .and_return(updated_dependency)

    allow(Dependabot::PullRequestUpdater).to receive(:new).with(**updater_args) { pr_updater }
    allow(Gitlab::MergeRequest::DiscussionReplier).to receive(:call)
    allow(Dependabot::JobJsonCreator).to receive(:call).and_return("job.json")
  end

  def expect_to_reply_status(message)
    expect(Gitlab::MergeRequest::DiscussionReplier).to have_received(:call).with(
      project_name: project.name,
      mr_iid: mr.iid,
      discussion_id: discussion_id,
      note: message
    )
  end

  def expect_to_reply_error(error)
    expect_to_reply_status(":x: `dependabot-gitlab` failed to recreate merge request. :x:\n\n```\n#{error}\n```")
  end

  context "without discussion id", :aggregate_failures do
    let(:discussion_id) { nil }

    context "with successfull dependency update" do
      it "recreates merge request and skips status reply" do
        update

        expect(pr_updater).to have_received(:update)
        expect(gitlab).not_to have_received(:resolve_merge_request_discussion)
        expect(Gitlab::MergeRequest::DiscussionReplier).not_to have_received(:call)
      end
    end

    context "with unsuccessfull update" do
      let(:dependency_state) { Dependabot::UpdatedDependency::UPDATE_IMPOSSIBLE }

      it "raises error" do
        expect { update }.to raise_error("Dependency update is impossible!")

        expect(Gitlab::MergeRequest::DiscussionReplier).not_to have_received(:call)
        expect(gitlab).not_to have_received(:resolve_merge_request_discussion)
      end
    end
  end

  context "with discussion id" do
    context "with successfull dependency update", :aggregate_failures do
      it "recreates merge request and replies status" do
        update

        expect(pr_updater).to have_received(:update)
        expect_to_reply_status(":white_check_mark: `dependabot-gitlab` successfully recreated merge request!")
        expect(gitlab).to have_received(:resolve_merge_request_discussion).with(
          project.name,
          mr.iid,
          discussion_id,
          resolved: true
        )
      end
    end

    context "with unsuccessful dependency update" do
      context "without updated dependency" do
        let(:dependency_state) { Dependabot::UpdatedDependency::UPDATE_IMPOSSIBLE }

        it "responds with unable to update error" do
          update

          expect_to_reply_error("Dependency update is impossible!")
        end
      end

      context "with newer versions to update" do
        let(:update_to_versions) { "2.3.0" }

        it "responds with newer version exists error" do
          update

          expect_to_reply_error("Newer dependency version exists, trigger dependency updates to create new mr!")
        end
      end

      context "with dependency already up to date" do
        let(:dependency_state) { Dependabot::UpdatedDependency::UP_TO_DATE }

        it "responds with dependency up to date error" do
          update

          expect_to_reply_error("Dependency already up to date!")
        end
      end

      context "with failed dependency update" do
        let(:dependency_state) { Dependabot::UpdatedDependency::ERROR }

        it "responds with dependency failed to update dependecy error" do
          update

          expect_to_reply_error("Dependency update failed due to error. Error: Update failed!")
        end
      end

      context "with dependency not found" do
        before do
          allow(Dependabot::Update::Checker).to receive(:call).and_return(nil)
        end

        it "responds with dependency not found error" do
          update

          expect_to_reply_error("Dependency '#{dependency.name}' not found in manifest file!")
        end
      end
    end
  end
end
