# frozen_string_literal: true

describe Job::Triggers::DependencyUpdate, :integration do
  subject(:service) { described_class }

  let(:project) { create(:project) }
  let(:update_job) { project.update_jobs.first }
  let(:update_run) { update_job.reload.runs.last }

  let(:args) do
    {
      project_name: project.name,
      package_ecosystem: "bundler",
      directory: "/"
    }
  end

  before do
    allow(Dependabot::Update::Runner).to receive(:call)
  end

  it "runs dependency updates and saves last enqued time", :aggregate_failures do
    service.call(*args.values)

    expect(Dependabot::Update::Runner).to have_received(:call).with(run_id: update_run.id.to_s, **args)
    expect(update_run.created_at).not_to be_nil
  end
end
