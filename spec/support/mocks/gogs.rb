# frozen_string_literal: true

require "httparty"

module Support
  class Gogs
    include HTTParty

    DEFAULT_NAMESPACE = "root"

    base_uri ENV["MOCK_URL"]

    # Create git repository
    #
    # @return [Hash]
    def create_git_repo(name)
      resp = self.class.post(
        "/api/v1/user/repos",
        headers: headers,
        body: { name: name.gsub("#{DEFAULT_NAMESPACE}/", ""), auto_init: true, readme: "Default" }.to_json
      )
      validate_response!(resp)

      "https://root:root@#{URI.parse(ENV['MOCK_URL']).host}/#{name}.git"
    end

    private

    # Parse response
    #
    # @param [Response] resp
    # @return [Hash]
    def validate_response!(resp)
      raise("Request to gogs server failed, code: #{resp.code}, body: #{resp.body}") if [200, 201].exclude?(resp.code)
    end

    def headers
      @headers ||= {
        "Content-Type" => "application/json",
        "Authorization" => "token 149f5d3c282198a31d3e65a1fa4c1e19085d7930",
        "X-Gogs-Event" => "true"
      }
    end
  end
end
