# frozen_string_literal: true

require "mustache"
require "git"

require_relative "../mocks/smocker"
require_relative "../mocks/gogs"
require_relative "../api_helper"

RSpec.shared_context "with system helper" do
  include_context "with api helper"

  let(:project_name) { "project-name" }
  let(:mock) { Support::Smocker.new }
  let(:gogs) { Support::Gogs.new }
  let(:mock_definitions) { [] }
  let(:gogs_proxy_definition_yml) { File.read("spec/fixture/gogs/mocks/dynamic/proxy.yml") }

  before do |example|
    mock.reset
    mock.add(gogs_proxy_definition_yml) if example.metadata[:with_gogs_proxy]
    mock.add(definition_yml)
  end

  def prepare_git_repo
    git_url = gogs.create_git_repo(project_name)
    gemfiles = Dir.glob(Rails.root.join("spec/fixture/gemfiles/*"))

    Dir.mktmpdir do |dir|
      repo_path = File.join(dir, "repo")
      git = Git.clone(git_url, repo_path)
      if git.config["user.name"].blank?
        git.config("user.name", "test")
        git.config("user.email", "test@example.com")
      end

      FileUtils.cp(gemfiles, repo_path)
      git.add(all: true)
      git.commit("Add dependency files")
      git.push
    end
  end

  def expect_all_mocks_called
    resp = mock.verify
    verified = resp.dig(:mocks, :verified)
    unused = resp.dig(:mocks, :unused)
    failures = resp.dig(:history, :failures)

    aggregate_failures do
      expect(verified).to be(true), "Expected all mocks to be verified"
      expect(unused).to be_nil, "Expected no unused mocks: #{JSON.pretty_generate(unused)}"
      expect(failures).to be_nil, "Expected to not have failures: #{JSON.pretty_generate(failures)}"
    end
  end

  def definition_yml # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity, Metrics/MethodLength
    yml_definitions = mock_definitions.map do |definition|
      if definition.is_a?(Symbol)
        name = definition
        type = :gitlab
        params = {}
      elsif definition.is_a?(Hash)
        name = definition[:name] || raise("Missing mock name")
        type = definition[:type] || :gitlab
        params = definition[:params] || {}
      else
        raise("Invalid mock definition: #{definition}")
      end

      dynamic_params = params.merge({ project_name: project_name.gsub("/", "%2F") })
      dynamic_params[:id] = Faker::Number.number(digits: 10) unless dynamic_params[:id]
      dynamic_params[:iid] = Faker::Number.number(digits: 5) unless dynamic_params[:iid]
      if name == :dep_file
        dynamic_params[:gemfile] = Base64.strict_encode64(File.read("spec/fixture/gemfiles/Gemfile"))
        dynamic_params[:gemfile_lock] = Base64.strict_encode64(File.read("spec/fixture/gemfiles/Gemfile.lock"))
      end

      Mustache.render(File.read("spec/fixture/#{type}/mocks/dynamic/#{name}.yml"), **dynamic_params)
    end

    yml_definitions.join("\n")
  end
end
