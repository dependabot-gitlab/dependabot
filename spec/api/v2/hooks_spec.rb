# frozen_string_literal: true

RSpec.shared_examples "project registration system hooks" do
  it "handles event", :aggregate_failures do
    receive_webhook

    expect_status(201)
    expect_json(message: "success")
    expect(Webhooks::SystemHookHandler).to have_received(:call).with(**handler_args)
  end
end

describe V2::Hooks, :aggregate_failures, type: :request do
  subject(:receive_webhook) { api_post(path, body, auth_token) }

  include_context "with api helper"

  let(:auth_token) { "auth_token" }

  before do
    allow(CredentialsConfig).to receive(:gitlab_auth_token) { auth_token }
  end

  describe "/api/v2/hooks" do
    let(:path) { "/api/v2/hooks" }
    let(:project) { build(:project, name: "dependabot-gitlab/test") }

    before do
      allow(Project).to receive(:find_by).with(name: project.name) { project }
    end

    context "with successful response" do
      before do
        allow(Webhooks::PushEventHandler).to receive(:call).and_return(project)
        allow(Webhooks::MergeRequestEventHandler).to receive(:call).and_return({ closed_merge_request: true })
        allow(Webhooks::CommentEventHandler).to receive(:call).and_return({ recreate_in_progress: true })
        allow(Webhooks::PipelineEventHandler).to receive(:call).and_return({ merge_request_accepted: true })
        allow(Webhooks::IssueEventHandler).to receive(:call).and_return(true)
      end

      context "with push event" do
        let(:body) { hash("spec/fixture/gitlab/webhooks/push.json") }

        it "handles event" do
          receive_webhook

          expect_status(201)
          expect_entity_response(Project::Entity, project)
          expect(Webhooks::PushEventHandler).to have_received(:call).with(
            project_name: project.name,
            commits: body[:commits]
          )
        end
      end

      context "with mr event" do
        let(:body) { hash("spec/fixture/gitlab/webhooks/mr_close.json") }

        it "handles event" do
          receive_webhook

          expect_status(201)
          expect_json({ closed_merge_request: true })
          expect(Webhooks::MergeRequestEventHandler).to have_received(:call).with(
            project_name: project.name,
            mr_iid: body.dig(:object_attributes, :iid),
            action: body.dig(:object_attributes, :action),
            merge_status: body.dig(:object_attributes, :merge_status)
          )
        end
      end

      context "with comment event" do
        let(:body) { hash("spec/fixture/gitlab/webhooks/comment.json") }

        it "handles event" do
          receive_webhook

          expect_status(201)
          expect_json({ recreate_in_progress: true })
          expect(Webhooks::CommentEventHandler).to have_received(:call).with(
            discussion_id: body.dig(:object_attributes, :discussion_id),
            note: body.dig(:object_attributes, :note),
            project_name: body.dig(:project, :path_with_namespace),
            mr_iid: body.dig(:merge_request, :iid),
            state: body.dig(:merge_request, :state)
          )
        end
      end

      context "with pipeline event" do
        let(:body) { hash("spec/fixture/gitlab/webhooks/pipeline.json") }

        it "handles event" do
          receive_webhook

          expect_status(201)
          expect_json({ merge_request_accepted: true })
          expect(Webhooks::PipelineEventHandler).to have_received(:call).with(
            source: body.dig(:object_attributes, :source),
            status: body.dig(:object_attributes, :status),
            project_name: body.dig(:project, :path_with_namespace),
            mr_iid: body.dig(:merge_request, :iid),
            merge_status: body.dig(:merge_request, :merge_status),
            source_project_id: body.dig(:merge_request, :source_project_id),
            target_project_id: body.dig(:merge_request, :target_project_id)
          )
        end
      end

      context "with issue event" do
        let(:body) { hash("spec/fixture/gitlab/webhooks/issue.json") }

        it "handles issue event" do
          receive_webhook

          expect_status(201)
          expect_json({ closed_vulnerability_issue: true })
          expect(Webhooks::IssueEventHandler).to have_received(:call).with(
            project_name: body.dig(:project, :path_with_namespace),
            issue_iid: body.dig(:object_attributes, :iid)
          )
        end
      end
    end

    context "with unsuccessful response" do
      let(:error) { StandardError.new("Unexpected") }

      before do
        allow(Sentry).to receive(:capture_exception)
      end

      context "with system error" do
        let(:body) { hash("spec/fixture/gitlab/webhooks/push.json") }

        before do
          allow(Webhooks::PushEventHandler).to receive(:call).and_raise(error)
        end

        it "returns 500" do
          receive_webhook

          expect_status(500)
          expect_json(error: "Unexpected")
          expect(Sentry).to have_received(:capture_exception).with(error)
        end
      end

      context "with invalid request" do
        let(:body) { { "funky" => "object" } }

        it "returns 400" do
          receive_webhook

          expect_status(400)
          expect_json(error: "object_kind is missing, project is missing, project[path_with_namespace] is missing")
        end
      end

      context "with unauthorized request" do
        let(:body) { hash("spec/fixture/gitlab/webhooks/push.json") }

        before do
          allow(CredentialsConfig).to receive(:gitlab_auth_token).and_return("invalid")
        end

        it "returns 401" do
          receive_webhook

          expect_status(401)
          expect_json(error: "Invalid gitlab authentication token")
        end
      end

      context "with invalid event" do
        let(:body) { { object_kind: "tag_push", project: { path_with_namespace: "project" } } }

        before do
          allow(Project).to receive(:find_by).and_raise(Mongoid::Errors::DocumentNotFound.new(Project, "not found"))
        end

        it "returns 400" do
          receive_webhook

          expect_status(400)
          expect_json(error: "Event 'tag_push' not supported")
        end
      end
    end
  end

  describe "/api/v2/hooks/system/project_registration" do
    let(:path) { "/api/v2/hooks/system/project_registration" }

    let(:project_name) { "project-name" }
    let(:old_project_name) { "old-project-name" }

    let(:request_args) { { path_with_namespace: project_name } }
    let(:handler_args) { { event_name: event_name, project_name: project_name, old_project_name: nil } }

    let(:body) { { event_name: event_name, **request_args } }

    before do
      allow(Webhooks::SystemHookHandler).to receive(:call).and_return("success")
    end

    context "with project namespace filter" do
      let(:event_name) { "project_create" }
      let(:project_name) { "namespace_1/project-name" }

      before do
        allow(AppConfig).to receive(:project_registration_allow_pattern).and_return("allowed-namespace")
      end

      it "does not register project from not allowed namespace", :aggregate_failures do
        receive_webhook

        expect_status(202)
        expect_json(message: "Skipped, does not match allowed namespace pattern")
      end
    end

    context "with project_create event" do
      let(:event_name) { "project_create" }

      it_behaves_like "project registration system hooks"
    end

    context "with project_destroy event" do
      let(:event_name) { "project_destroy" }

      it_behaves_like "project registration system hooks"
    end

    context "with project_rename event" do
      let(:event_name) { "project_rename" }
      let(:request_args) { { path_with_namespace: project_name, old_path_with_namespace: old_project_name } }
      let(:handler_args) { { event_name: event_name, project_name: project_name, old_project_name: old_project_name } }

      it_behaves_like "project registration system hooks"
    end

    context "with project_transfer event" do
      let(:event_name) { "project_transfer" }
      let(:request_args) { { path_with_namespace: project_name, old_path_with_namespace: old_project_name } }
      let(:handler_args) { { event_name: event_name, project_name: project_name, old_project_name: old_project_name } }

      it_behaves_like "project registration system hooks"
    end

    context "with unsuccessful response" do
      let(:error) { StandardError.new("Unexpected") }
      let(:auth_token) { "auth_token" }

      before do
        allow(Webhooks::SystemHookHandler).to receive(:call).and_raise(error)
      end

      context "with invalid request body" do
        let(:body) { { "funky" => "object" } }

        it "handles invalid request" do
          receive_webhook

          expect_status(400)
          expect_json(error: "event_name is missing")
        end
      end

      context "with unsupported event" do
        let(:body) { { event_name: "not_supported" } }

        it "handles unsupported event" do
          receive_webhook

          expect_status(202)
          expect_json(message: "Skipped, event not supported")
        end
      end
    end
  end
end
