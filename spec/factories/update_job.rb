# frozen_string_literal: true

FactoryBot.define do
  factory :update_job, class: "Update::Job" do
    name { "#{project.name}:#{package_ecosystem}:#{directory}" }
    package_ecosystem { "bundler" }
    directory { "/#{Faker::Alphanumeric.alpha(number: 10)}" }

    project

    factory :update_job_with_run, class: "Update::Job" do
      after(:build) do |update_job|
        build(
          :update_log_entry,
          run: build(:update_run, job: update_job)
        )
      end

      after(:create) do |update_job|
        create(
          :update_log_entry,
          run: create(:update_run, job: update_job)
        )

        update_job.reload
      end
    end
  end
end
