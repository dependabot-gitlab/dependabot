# frozen_string_literal: true

describe Dependabot::Schedule, :aggregate_failures do
  subject(:cron) { described_class }

  let(:day) { "sunday" }
  let(:entry_a) { "a-bundler-/" }
  let(:entry_b) { "b-docker-/" }

  it "generates random cron schedule" do
    expect(cron.new(entry: entry_a, interval: "daily").to_s).to eq("35 8 * * * UTC")
  end

  it "generates daily cron and ignores day input" do
    expect(cron.new(entry: entry_a, interval: "daily", day: day, time: "2:00").to_s).to eq("0 2 * * * UTC")
  end

  it "generates monthly cron and ignores day input" do
    expect(cron.new(entry: entry_a, interval: "monthly", day: day, time: "0:10").to_s).to eq("10 0 1 * * UTC")
  end

  it "generates weekly cron with correct day input" do
    expect(cron.new(entry: entry_a, interval: "weekly", day: day, time: "0:10").to_s).to eq("10 0 * * sun UTC")
  end

  it "generates randon cron hour for normal range" do
    expect(cron.new(entry: entry_a, interval: "daily", hours: "2-7").to_s).to eq("35 2 * * * UTC")
  end

  it "generates randon cron hour for reverse range" do
    expect(cron.new(entry: entry_b, interval: "daily", hours: "22-5").to_s).to eq("34 23 * * * UTC")
  end

  it "generates random cron day based on project name", :aggregate_failures do
    expect(cron.new(entry: entry_a, interval: "weekly", time: "2:00").to_s).to eq("0 2 * * 0 UTC")
    expect(cron.new(entry: entry_b, interval: "weekly", time: "2:00").to_s).to eq("0 2 * * 1 UTC")
  end
end
