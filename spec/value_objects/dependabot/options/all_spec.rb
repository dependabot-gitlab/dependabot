# frozen_string_literal: true

require "tempfile"

describe Dependabot::Options::All do
  subject(:parsed_config) { described_class.new(config_yml, project_name).transform }

  include_context "with dependabot helper"

  shared_examples "schema validation" do |filename = "dependabot.yml"|
    it "raises schema validation error" do
      expect { parsed_config }.to raise_error(Schemas::ValidationError) do |error|
        expect(error.message).to eq(<<~ERR.strip)
          Validation for #{filename} failed:
          #{schema_errors.join("\n")}
        ERR
      end
    end
  end

  context "with valid configuration" do
    context "with default configuration" do
      let(:config_yml) { File.read("spec/fixture/gitlab/responses/dependabot.yml") }

      it "returns parsed configuration" do
        expect(parsed_config).to eq({ updates: updates_config, registries: registries })
      end
    end

    context "with directories option" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directories:
                - "/app"
                - "/frontend"
              schedule:
                interval: daily
        YAML
      end

      it "creates entry for each directory" do
        expect(parsed_config[:updates][0][:directory]).to eq("/app")
        expect(parsed_config[:updates][1][:directory]).to eq("/frontend")
      end
    end

    context "with explicit auto-merge rules" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: daily
              auto-merge:
                squash: true
                allow:
                  - dependency-name: test
                ignore:
                  - dependency-name: test-2
        YAML
      end

      it "sets auto-merge rules" do
        expect(parsed_config[:updates].first[:auto_merge]).to eq({
          squash: true,
          merge_train: false,
          allow: [{ dependency_name: "test" }],
          ignore: [{ dependency_name: "test-2" }]
        })
      end
    end

    context "with registries" do
      let(:config_yml) do
        <<~YAML
          version: 2
          registries:
            dockerhub:
              type: docker-registry
              url: https://registry.hub.docker.com
              username: octocat
              password: password
            npm:
              type: npm-registry
              url: https://npm.pkg.github.com
              token: test_token
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: daily
                hours: 9-17
              registries:
                - npm
        YAML
      end

      it "sets reject_external_code: true" do
        expect(parsed_config[:updates].first[:reject_external_code]).to be(true)
      end
    end

    context "without registries" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
        YAML
      end

      it "sets reject_external_code: false by default" do
        expect(parsed_config[:updates].first[:reject_external_code]).to be(false)
      end
    end

    context "with disabled vulnerabilities alerts" do
      let(:config_yml) do
        <<~YAML
          version: 2
          update-options:
            vulnerability-alerts:
              enabled: false
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
        YAML
      end

      it "sets vulnerability alerts to disabled" do
        expect(parsed_config[:updates].first.dig(:vulnerability_alerts, :enabled)).to be(false)
      end
    end

    context "with valid config and rebase on approvals" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
              rebase-strategy:
                on-approval: true
        YAML
      end

      it "sets 'auto' strategy by default" do
        expect(parsed_config[:updates].first[:rebase_strategy]).to eq({
          strategy: "auto",
          on_approval: true,
          with_assignee: nil
        })
      end
    end

    context "with non-default author details" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              author-details:
                name: "Update Bot"
                email: "bot@example.com"
        YAML
      end

      it "sets author details" do
        expect(parsed_config[:updates].first[:author_details]).to eq({
          name: "Update Bot",
          email: "bot@example.com"
        })
      end
    end

    context "with common update options" do
      let(:config_yml) do
        <<~YAML
          version: 2
          update-options:
            auto-merge: true
          updates:
            - package-ecosystem: bundler
              directory: "/"
            - package-ecosystem: npm
              directory: "/"
        YAML
      end

      it "applies common options to all entries" do
        parsed_config[:updates].each { |entry| expect(entry[:auto_merge]).to eq({ squash: false }) }
      end
    end
  end

  context "with invalid configuration" do
    context "with missing or incorrect types in config" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              vendor: "true"
              schedule:
                interval: daily
                time: "19:00"
              milestone: 4
              ignore:
                - versions: ["3.x", "4.x"]
              commit-message:
                prefix: "dep"
                trailers:
                  - changelog: "dep"
        YAML
      end

      let(:schema_errors) do
        [
          "Object at `/updates/0/ignore/0` is missing required properties: dependency-name",
          "Value at `/updates/0/milestone` is not a string",
          "Value at `/updates/0/vendor` is not a boolean"
        ]
      end

      it_behaves_like "schema validation"
    end

    context "with missing directory in config" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
        YAML
      end

      let(:schema_errors) do
        [
          "Object at `/updates/0` is missing required properties: directories",
          "Object at `/updates/0` is missing required properties: directory"
        ]
      end

      it_behaves_like "schema validation"
    end

    context "with incorrect hours range format" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
                hours: "25"
        YAML
      end

      let(:schema_errors) do
        ["Hours must be in the format '$from-$to' (e.g. '9-17')"]
      end

      it_behaves_like "schema validation"
    end

    context "with invalid rebase-strategy configuration" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              schedule:
                interval: weekly
              rebase-strategy:
                approval: true
        YAML
      end

      let(:schema_errors) do
        [
          "Object property at `/updates/0/rebase-strategy/approval` is a disallowed additional property",
          'Value at `/updates/0/rebase-strategy` is not one of: ["auto", "all", "none"]'
        ]
      end

      it_behaves_like "schema validation"
    end

    context "with unsupported properties in update-options" do
      let(:config_yml) do
        <<~YAML
          version: 2
          update-options:
            invalid-option: test
            auto-merge: true
          updates:
            - package-ecosystem: bundler
        YAML
      end

      let(:schema_errors) do
        ["Object property at `/update-options/invalid-option` is a disallowed additional property"]
      end

      it_behaves_like "schema validation"
    end

    context "with unsupported properties in updates array" do
      let(:config_yml) do
        <<~YAML
          version: 2
          updates:
            - package-ecosystem: bundler
              directory: "/"
              invalid-option: true
        YAML
      end

      let(:schema_errors) do
        ["Object property at `/updates/0/invalid-option` is a disallowed additional property"]
      end

      it_behaves_like "schema validation"
    end
  end

  context "with base config template" do
    base_filename = "base.yml"

    let(:config_yml) do
      <<~YAML
        version: 2
        registries:
          npm:
            type: npm-registry
            url: https://npm.pkg.github.com
            token: test_token
        updates:
          - package-ecosystem: bundler
            schedule:
              interval: daily
      YAML
    end

    before do
      allow(DependabotConfig).to receive(:config_base_filename).and_return(base_filename)
      allow(File).to receive(:exist?).with(base_filename).and_return(true)
      allow(YAML).to receive(:load_file).with(base_filename, symbolize_names: true)
                                        .and_return(YAML.safe_load(base_yml, symbolize_names: true))
    end

    context "with valid base config" do
      let(:base_yml) do
        <<~YML
          version: 2
          update-options:
            directory: "/"
            milestone: "4"
            rebase-strategy:
              on-approval: true
        YML
      end

      it "merges base configuration" do
        expect(parsed_config[:registries]).to eq({
          "npm" => { "registry" => "npm.pkg.github.com", "token" => "test_token", "type" => "npm_registry" }
        })
        expect(parsed_config[:updates].first).to include({
          milestone: "4",
          directory: "/",
          rebase_strategy: { strategy: "auto", on_approval: true, with_assignee: nil }
        })
      end
    end

    context "with invalid updates base config" do
      let(:base_yml) do
        <<~YML
          version: 2
          updates:
            milestone: "4"
            directory: "/"
            invalid-option: true
        YML
      end

      let(:schema_errors) do
        ["Object property at `/updates/invalid-option` is a disallowed additional property"]
      end

      it_behaves_like "schema validation", base_filename
    end
  end
end
