# frozen_string_literal: true

describe "rake" do # rubocop:disable RSpec/DescribeClass
  include_context "with rake helper"

  describe "dependabot:update", :standalone do
    let(:errors) { [] }
    let(:args) do
      {
        project: "test-repo",
        package_ecosystem: "bundler",
        directory: "/"
      }
    end

    before do
      allow(Job::Triggers::DependencyUpdate).to receive(:call).and_return(errors)
    end

    it "runs updates for project" do
      task.invoke(*args.values)

      expect(Job::Triggers::DependencyUpdate).to have_received(:call).with(
        args[:project],
        args[:package_ecosystem],
        args[:directory]
      )
    end

    it "raises error on blank argument" do
      expect { task.invoke(*args.values[0..1]) }.to raise_error(SystemExit)
    end

    context "with errors in dependency updates" do
      let(:errors) { ["Some error!"] }
      let(:task_name) { "dependabot:update" }

      it "exits with non 0 result code" do
        expect { task.invoke(*args.values) }.to raise_error(SystemExit)
      end
    end

    context "with config entry name" do
      let(:task_name) { "dependabot:update" }
      let(:configuration) { instance_double(Configuration) }
      let(:config_name) { "config-name" }

      before do
        allow(Dependabot::Config::Fetcher).to receive(:call).with(args[:project]).and_return(configuration)
        allow(configuration).to receive(:entries)
          .with(name: config_name, package_ecosystem: args[:package_ecosystem])
          .and_return([{ directory: "/dir" }, { directory: "/other_dir" }])
      end

      it "runs updates for each config entry" do
        task.invoke(*args.values, config_name)

        ["/dir", "/other_dir"].each do |dir|
          expect(Job::Triggers::DependencyUpdate).to have_received(:call).with(
            args[:project],
            args[:package_ecosystem],
            dir
          )
        end
      end
    end
  end

  describe "dependabot:recreate_mr" do
    let(:args) do
      {
        project: "test-repo",
        mr_iid: 1,
        discussion_id: 123
      }
    end

    before do
      allow(Job::Triggers::MergeRequestRecreate).to receive(:call)
    end

    it "Recrates merge request" do
      task.invoke(*args.values)

      expect(Job::Triggers::MergeRequestRecreate).to have_received(:call).with(
        args[:project],
        args[:mr_iid],
        args[:discussion_id]
      )
    end
  end

  describe "dependabot:notify_release" do
    let(:args) do
      {
        dependency_name: "rspec",
        package_ecosystem: "bundler",
        project_name: "test-project",
        ignore_rules: false
      }
    end

    before do
      allow(Job::Triggers::NotifyRelease).to receive(:call)
    end

    it "Triggers update for specific dependency" do
      task.invoke(*args.values)

      expect(Job::Triggers::NotifyRelease).to have_received(:call).with(
        args[:dependency_name],
        args[:package_ecosystem],
        args[:project_name],
        args[:ignore_rules]
      )
    end
  end

  describe "dependabot:register", :integration do
    let(:project_name) { "test-project" }
    let(:project) { Project.new(name: project_name) }

    before do
      allow(Dependabot::Projects::Creator).to receive(:call) { project }
      allow(Cron::JobSync).to receive(:call).with(project).and_call_original
    end

    it "registers new project" do
      task.invoke(project_name)

      expect(Dependabot::Projects::Creator).to have_received(:call).with(project_name)
      expect(Cron::JobSync).to have_received(:call).with(project)
    end
  end

  describe "dependabot:register_project", :integration do
    let(:project_name) { "test-project" }
    let(:project_access_token) { "test-token" }
    let(:project) { Project.new(name: project_name) }

    before do
      allow(Dependabot::Projects::Creator).to receive(:call) { project }
      allow(Cron::JobSync).to receive(:call).with(project).and_call_original
    end

    it "registers new project" do
      task.invoke(project_name, project_access_token)

      expect(Dependabot::Projects::Creator).to have_received(:call)
        .with(project_name, access_token: project_access_token)
      expect(Cron::JobSync).to have_received(:call).with(project)
    end
  end

  describe "dependabot:automatic_registration" do
    before do
      allow(ProjectRegistrationJob).to receive(:perform_now)
    end

    it "triggers automatic project registration" do
      task.invoke

      expect(ProjectRegistrationJob).to have_received(:perform_now)
    end
  end

  describe "dependabot:remove", :integration do
    let(:project_name) { "test-project" }

    before do
      allow(Dependabot::Projects::Remover).to receive(:call)
    end

    it "removes project" do
      task.invoke(project_name)

      expect(Dependabot::Projects::Remover).to have_received(:call).with(project_name)
    end
  end

  # rubocop:disable RSpec/MessageChain
  describe "background_tasks:check_db" do
    before do
      allow(Mongoid).to receive_message_chain("client.database_names.present?")
    end

    it "passes successfully" do
      expect { task.invoke }.not_to raise_error
    end
  end

  describe "background_tasks:check_redis" do
    let(:redis) { instance_double(RedisClient, call: nil, close: nil) }

    before do
      allow(RedisClient).to receive_message_chain("config.new_client.with").and_yield(redis)
    end

    it "passes successfully", :aggregate_failure do
      task.invoke

      expect(redis).to have_received(:call).with("PING")
      expect(redis).to have_received(:close)
    end
  end

  describe "dependabot:create_user" do
    let(:username) { "test" }
    let(:password) { "test" }

    before do
      allow(User).to receive(:create!)
    end

    it "creates user" do
      task.invoke(username, password)

      expect(User).to have_received(:create!).with(username: username, password: password)
    end
  end

  describe "dependabot:delete_user" do
    let(:user) { instance_double(User, delete: nil) }
    let(:username) { "test" }

    before do
      allow(User).to receive(:find_by).with(username: username) { user }
    end

    it "deletes user" do
      task.invoke(username)

      expect(user).to have_received(:delete)
    end
  end
  # rubocop:enable RSpec/MessageChain
end
