# frozen_string_literal: true

describe BackgroundMigrationJob, type: :job do
  subject(:job) { described_class }

  let(:migrations_path) { "db/migrate" }
  let(:migrator) { instance_double(Mongoid::Migrator, migrate: nil) }

  let(:pending_migrations) { [1, 2] }

  before do
    allow(Mongoid::Migrator).to receive(:new)
      .with(:up, migrations_path, background_migration: true)
      .and_return(migrator)
  end

  it { is_expected.to be_retryable false }

  it "queues job in low queue" do
    expect { job.perform_later }.to enqueue_sidekiq_job.on("low")
  end

  it "performs migrations" do
    job.perform_now

    expect(migrator).to have_received(:migrate)
  end
end
