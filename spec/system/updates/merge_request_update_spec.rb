# frozen_string_literal: true

describe "dependency updates", :system, :with_gogs_proxy, type: :request do
  describe "merge request update" do
    subject(:update_mr) do
      Dependabot::MergeRequest::RecreateService.call(
        project_name: project_name,
        mr_iid: mr.iid
      )
    end

    include_context "with system helper"

    let(:project) do
      create(:project_with_mr, dependency: "rspec-retry", update_from: "0.6.1", update_to: "0.6.2", namespace: "root")
    end

    let(:project_name) { project.name }
    let(:mr) { project.merge_requests.first }

    let(:mock_definitions) do
      [
        { name: :branch, params: { dependency: mr.main_dependency } },
        { name: :project, params: { project_name: project_name } },
        { name: :create_commits, params: { dependency: mr.main_dependency } },
        { name: :mr, params: { iid: mr.iid, update_to: mr.update_to } }
      ]
    end

    before do
      prepare_git_repo
    end

    it "recreates merge request" do
      update_mr

      expect_all_mocks_called
    end
  end
end
