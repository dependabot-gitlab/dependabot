# frozen_string_literal: true

describe "dependency updates", :system, :with_gogs_proxy, type: :request do
  include_context "with system helper"
  include_context "with dependabot helper"

  let(:config_yaml) do
    {
      "version" => 2,
      "updates" => [
        {
          "package-ecosystem" => package_ecosystem,
          "directory" => directory,
          "schedule" => {
            "interval" => "daily"
          },
          "commit-message" => {
            "prefix" => "dep",
            "trailers-development" => [
              { "changelog" => "dev-dependencies" }
            ],
            "trailers-security" => [
              { "changelog" => "security" }
            ]
          },
          "assignees" => %w[
            john
            jane
          ],
          "vulnerability-alerts" => {
            "assignees" => [
              "john"
            ]
          },
          "auto-merge" => {
            "squash" => true,
            "allow" => [
              { "dependency-name" => "faker" }
            ]
          },
          "ignore" => ignored_deps,
          "rebase-strategy" => "all",
          "versioning-strategy" => "increase"
        }
      ]
    }.to_yaml
  end

  let(:ignored_deps) do
    [
      { "dependency-name" => "rspec-retry" },
      { "dependency-name" => "nokogiri" }
    ]
  end

  let(:project) { create(:project, config_yaml: config_yaml, namespace: "root") }
  let(:project_name) { project.name }
  let(:package_ecosystem) { "bundler" }
  let(:dependency_name) { nil }
  let(:directory) { "/" }
  let(:mrs) { project.reload.merge_requests }
  let(:vulnerabilities) { [] }

  before do
    allow(Vulnerability).to receive(:where) { vulnerabilities }

    prepare_git_repo
  end

  describe "with trigger service flow" do
    subject(:service) { Job::Triggers::DependencyUpdate }

    context "with dependency update job", :aggregate_failures do
      let(:args) do
        {
          "project_name" => project.name,
          "package_ecosystem" => package_ecosystem,
          "directory" => directory
        }
      end

      let(:update_job) { project.update_jobs.first }
      let(:update_run) { Update::Run.where(job: update_job).last }

      let(:mock_definitions) do
        [
          { name: :project },
          { name: :create_branch },
          { name: :commits },
          { name: :labels },
          { name: :mr_check },
          { name: :user, params: { username: "john" } },
          { name: :user, params: { username: "jane" } },
          { name: :no_branch, params: { dependency: "faker" } },
          { name: :no_branch, params: { dependency: "rubocop" } },
          { name: :create_commits, params: { dependency: "faker" } },
          { name: :create_commits, params: { dependency: "rubocop" } },
          { name: :create_mr, params: { dependency: "faker", iid: 1 } },
          { name: :create_mr, params: { dependency: "rubocop", iid: 2 } }
        ]
      end

      it "performs dependency update job and creates dependency update merge requests" do
        service.call(*args.values)

        expect(mrs.map(&:main_dependency)).to eq(%w[faker rubocop])
        expect(mrs.find { |mr| mr.main_dependency == "faker" }.auto_merge).to be(true)
        expect(mrs.find { |mr| mr.main_dependency == "rubocop" }.auto_merge).to be(false)
        expect(update_job.reload.dependency_files).to eq(["/Gemfile", "/Gemfile.lock"])
        expect(update_run.log_entries).not_to be_empty

        expect_all_mocks_called
      end
    end
  end

  describe "without active job" do
    subject(:update_dependencies) do
      Dependabot::Update::Runner.call(
        project_name: project.name,
        package_ecosystem: package_ecosystem,
        directory: directory,
        dependency_name: dependency_name
      )
    end

    context "with specific ignore rules" do
      let(:mock_definitions) do
        [
          { name: :project },
          { name: :create_branch },
          { name: :commits },
          { name: :labels },
          { name: :mr_check },
          { name: :user, params: { username: "john" } },
          { name: :user, params: { username: "jane" } },
          { name: :no_branch, params: { dependency: "rspec-retry" } },
          { name: :create_commits, params: { dependency: "rspec-retry" } },
          { name: :create_mr, params: { dependency: "rspec-retry", iid: 1 } }
        ]
      end

      let(:ignored_deps) do
        [
          { "dependency-name" => "nokogiri" },
          {
            "dependency-name" => "rspec-retry",
            "update-types" => ["version-update:semver-major", "version-update:semver-minor"]
          },
          {
            "dependency-name" => "rubocop",
            "update-types" => ["version-update:semver-major", "version-update:semver-minor"]
          },
          {
            "dependency-name" => "faker",
            "update-types" => ["version-update:semver-major"]
          }
        ]
      end

      it "correctly evaluates ignore rules" do
        expect(update_dependencies).to eq({ mr: Set[1], security_mr: Set.new })
        expect(mrs.map(&:main_dependency)).to eq(["rspec-retry"])

        expect_all_mocks_called
      end
    end

    context "with existing mr" do
      let(:project) { create(:project_with_mr, config_yaml: config_yaml, dependency: "rspec-retry", namespace: "root") }
      let(:mr) { mrs.first }

      let(:ignored_deps) do
        [
          { "dependency-name" => "rubocop" },
          { "dependency-name" => "faker" },
          { "dependency-name" => "nokogiri" }
        ]
      end

      let(:common_mock_definitions) do
        [
          { name: :project },
          { name: :mr_check },
          { name: :commits },
          { name: :user, params: { username: "john" } },
          { name: :user, params: { username: "jane" } },
          { name: :branch, params: { dependency: mr.main_dependency } },
          {
            name: :find_mr,
            params: { dependency: mr.main_dependency, id: mr.id, iid: mr.iid, has_conflicts: has_conflicts }
          }
        ]
      end

      context "with conflicts" do
        let(:has_conflicts) { true }

        let(:mock_definitions) do
          [
            *common_mock_definitions,
            { name: :mr, params: { iid: mr.iid, update_to: mr.update_to } },
            { name: :create_commits, params: { dependency: mr.main_dependency } }
          ]
        end

        it "recreates merge request" do
          expect(update_dependencies).to eq({ mr: Set[mr.iid], security_mr: Set.new })
          expect(mrs.map(&:main_dependency)).to eq([mr.main_dependency])
          expect_all_mocks_called
        end
      end

      context "without conflicts" do
        let(:has_conflicts) { false }

        let(:mock_definitions) do
          [
            *common_mock_definitions,
            { name: :rebase, params: { iid: mr.iid } }
          ]
        end

        it "rebases merge request" do
          expect(update_dependencies).to eq({ mr: Set[mr.iid], security_mr: Set.new })
          expect(mrs.map(&:main_dependency)).to eq([mr.main_dependency])
          expect_all_mocks_called
        end
      end
    end

    context "with single dependency", :aggregate_failures do
      let(:dependency_name) { "faker" }

      let(:mock_definitions) do
        [
          { name: :project },
          { name: :create_branch },
          { name: :labels },
          { name: :mr_check },
          { name: :commits },
          { name: :user, params: { username: "john" } },
          { name: :user, params: { username: "jane" } },
          { name: :no_branch, params: { dependency: dependency_name } },
          { name: :create_commits, params: { dependency: dependency_name } },
          { name: :create_mr, params: { dependency: dependency_name, iid: 1 } }
        ]
      end

      it "updates single dependency and creates merge request" do
        expect(update_dependencies).to eq({ mr: Set[1], security_mr: Set.new })
        expect(mrs.map(&:main_dependency)).to eq([dependency_name])
        expect_all_mocks_called
      end
    end

    context "with existing security vulnerability" do
      let(:dependency_name) { "nokogiri" }
      let(:vulnerabilities) { create_list(:vulnerability, 1) }
      let(:created_vulnerability_issue) do
        project.open_vulnerability_issues(
          package_ecosystem: package_ecosystem,
          directory: directory,
          package: dependency_name
        )
      end

      context "with successfull dependency update" do
        let(:ignored_deps) do
          [
            { "dependency-name" => "rubocop" },
            { "dependency-name" => "faker" },
            { "dependency-name" => "rspec-retry" }
          ]
        end

        let(:mock_definitions) do
          [
            { name: :project },
            { name: :create_branch },
            { name: :labels },
            { name: :mr_check },
            { name: :commits },
            { name: :user, params: { username: "john" } },
            { name: :user, params: { username: "jane" } },
            { name: :no_branch, params: { dependency: dependency_name } },
            { name: :create_commits, params: { dependency: dependency_name } },
            { name: :create_mr, params: { dependency: dependency_name, iid: 1 } },
            { name: :create_severity_label, params: { name: "high", color: "ed9121" } },
            { name: :update_mr, params: { iid: 1, severity: "high" } }
          ]
        end

        it "creates vulnerability fix merge request" do
          expect(update_dependencies).to eq({ mr: Set.new, security_mr: Set[1] })
          expect(mrs.map(&:main_dependency)).to eq([dependency_name])
          expect(created_vulnerability_issue.first).to be_nil
          expect_all_mocks_called
        end
      end

      context "with unsuccessfull dependency update" do
        let(:ignored_deps) do
          [
            { "dependency-name" => "rubocop" },
            { "dependency-name" => "faker" },
            { "dependency-name" => "rspec-retry" },
            { "dependency-name" => "nokogiri" }
          ]
        end

        let(:mock_definitions) do
          [
            { name: :project },
            { name: :labels },
            { name: :vulnerability_issue },
            { name: :user, params: { username: "john" } },
            { name: :create_severity_label, params: { name: "high", color: "ed9121" } }
          ]
        end

        it "creates vulnerability issue" do
          expect(update_dependencies).to eq({ mr: Set.new, security_mr: Set.new })
          expect(created_vulnerability_issue.first.status).to eq("opened")
          expect_all_mocks_called
        end
      end
    end
  end
end
