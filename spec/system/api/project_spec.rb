# frozen_string_literal: true

describe "project", :system, type: :request do
  include_context "with system helper"

  let(:project) { build(:project) }
  let(:project_name) { project.name }
  let(:created_project) { Project.where(name: project.name).first }
  let(:created_job) { created_project.update_jobs.first }

  context "with configuration file", :aggregate_failures do
    let(:mock_definitions) { %i[project hook set_hook raw_config] }

    it "adds a project" do
      api_post("/api/v2/projects", { project_name: project.name })

      expect_status(201)
      expect(created_project).to be_truthy
      expect(created_job.package_ecosystem).to eq("bundler")
      expect(created_job.directory).to eq("/")
      expect_all_mocks_called
    end
  end

  context "without configuration file", :aggregate_failures do
    let(:mock_definitions) { %i[project hook set_hook missing_config] }

    it "adds a project" do
      api_post("/api/v2/projects", { project_name: project.name })

      expect_status(201)
      expect(created_project).to be_truthy
      expect(created_project.configuration).to be_nil
      expect(created_job).to be_nil
      expect_all_mocks_called
    end
  end
end
