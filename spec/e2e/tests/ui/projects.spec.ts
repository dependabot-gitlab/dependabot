import { test as base } from "@playwright/test";
import { SignInPage } from "@pages/signIn";
import { ProjectsPage } from "@pages/projects";
import { Smocker } from "@support/mocks/smocker";
import { mocks } from "@support/mocks/mocks";
import { user } from "@support/user";

import randomstring from "randomstring";

const test = base.extend<{projectName: string; smocker: Smocker, projectsPage: ProjectsPage}>({
  projectName: async ({}, use) => {
    await use(randomstring.generate({ length: 10, charset: "alphabetic" }));
  },
  smocker: async ({}, use) => {
    const smocker = await new Smocker().init();
    await smocker.reset();
    await use(smocker);
  },
  projectsPage: async ({ page }, use) => {
    await use(new ProjectsPage(page));
  }
});

test.beforeEach(async ({ page }) => {
  const signInPage = new SignInPage(page);
  await signInPage.visit();
  await signInPage.signIn(user.name, user.password);
});

test.afterEach(async ({ smocker }) => {
  await smocker.verify();
  await smocker.dispose();
});

test("renders main page", async ({ projectsPage }) => {
  await projectsPage.expectPageToBeVisible();
});

test("adds new project without specific token", async ({ smocker, projectName, projectsPage }) => {
  await smocker.add(mocks.registerProject(projectName));
  await projectsPage.addProject(projectName);

  await projectsPage.searchForProject(projectName);
  await projectsPage.expectProjectToBeVisible(projectName);
});

test("adds new project with specific token", async ({ smocker, projectName, projectsPage }) => {
  const token = "token";

  await smocker.add(mocks.registerProject(projectName));
  await projectsPage.addProject(projectName, token);

  await projectsPage.searchForProject(projectName);
  await projectsPage.expectProjectToBeVisible(projectName);
});
