import { expect, test as base } from "@playwright/test";
import { SignInPage } from "@pages/signIn";
import { ProjectsPage } from "@pages/projects";

import { mocks } from "@support/mocks/mocks";
import { user } from "@support/user";
import { ProjectFabricator } from "@support/fabricators/projectFabricator";

import randomstring from "randomstring";

type JobSetup = {
  withGitRepo: boolean;
  projectName: string;
  projectFabricator: ProjectFabricator;
  projectsPage: ProjectsPage;
};

const test = base.extend<JobSetup>({
  withGitRepo: false,
  projectName: async ({}, use) => {
    await use(`root/${randomstring.generate({ length: 10, charset: "alphabetic" })}`);
  },
  projectFabricator: [
    async ({ withGitRepo, projectName, request }, use) => {
      const fabricator = new ProjectFabricator(request, user);
      await fabricator.create(projectName, { reset: true, withGitRepo });
      await use(fabricator);
    },
    { auto: true }
  ],
  projectsPage: [
    async ({ page, projectName }, use) => {
      const signInPage = new SignInPage(page);
      await signInPage.visit();
      await signInPage.signIn(user.name, user.password);

      const projectsPage = new ProjectsPage(page);
      await projectsPage.searchForProject(projectName);
      await projectsPage.expectProjectToBeVisible(projectName);
      await use(projectsPage);
    },
    { auto: true }
  ]
});

test.afterEach(async ({ projectFabricator }) => {
  await projectFabricator.dispose();
});

test("disables dependency update job", async ({ projectName, projectsPage, request }) => {
  await projectsPage.setUpdateJobState(projectName, "bundler", "/", false);

  const response = await request.get(`/api/v2/projects/${projectName.replace("/", "%2F")}/update_jobs`, {
    headers: user.basicAuthHeader
  });
  const responseJson = await response.json();

  expect(responseJson[0]["enabled"]).toBe(false);
});

test.describe(() => {
  test.use({ withGitRepo: true });

  test("executes dependency update job", async ({ projectName, projectsPage, projectFabricator, request }) => {
    test.slow(); // this spec is executing dependency update job which also requires pulling docker image

    const smocker = projectFabricator.smocker;
    await smocker.add(mocks.updateDependencies(projectName));
    await projectsPage.triggerUpdateJob(projectName, "bundler", "/");

    let updateJob;

    await expect
      .poll(
        async () => {
          const response = await request.get(`/api/v2/projects/${projectName.replace("/", "%2F")}/update_jobs`, {
            headers: user.basicAuthHeader
          });
          const responseJson = await response.json();
          updateJob = responseJson[0];

          return updateJob["last_finished"];
        },
        {
          message: "Expected update job to be executed",
          timeout: 60_000,
          intervals: [5_000]
        }
      )
      .not.toBe(null);
    expect(updateJob["last_failures"]).toBe("");

    await smocker.verify();
  });
});
