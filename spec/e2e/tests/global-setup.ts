import { test as setup } from '@playwright/test';
import { setupE2eEnvironment } from "@support/env-setup";

setup("set-up environment", async () => {
  setup.setTimeout(120_000);

  await setupE2eEnvironment();
});
