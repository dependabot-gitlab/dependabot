import { test as base, expect } from "@playwright/test";
import { Smocker } from "@support/mocks/smocker";
import { mocks } from "@support/mocks/mocks";
import { user } from "@support/user";
import { expectResponseOk } from "@support/util";

import randomstring from "randomstring";

const authHeader = user.basicAuthHeader;

const test = base.extend<{ projectName: string; smocker: Smocker }>({
  projectName: async ({}, use) => {
    await use(randomstring.generate({ length: 10, charset: "alphabetic" }));
  },
  smocker: [
    async ({ projectName }, use) => {
      const smocker = await new Smocker().init();
      await smocker.reset();
      await smocker.add(mocks.registerProject(projectName));

      await use(smocker);

      await smocker.verify();
      await smocker.dispose();
    },
    { auto: true }
  ]
});

test("adds new project without specific token", async ({ request, projectName }) => {
  const response = await request.post("/api/v2/projects", {
    headers: authHeader,
    data: { project_name: projectName }
  });

  expectResponseOk(response);
  expect((await response.json()).name).toEqual(projectName);
});

test("adds new project with specific token", async ({ request, projectName }) => {
  const response = await request.post("/api/v2/projects", {
    headers: authHeader,
    data: { project_name: projectName, gitlab_access_token: "token" }
  });

  expectResponseOk(response);
  expect((await response.json()).name).toEqual(projectName);
});
