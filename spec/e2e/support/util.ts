import { APIResponse, expect } from "@playwright/test";

export const expectResponseOk = (response: APIResponse) => {
  expect(response.ok(), `Expected response to be ok, was: ${response.status()}, ${response.statusText()}`).toBeTruthy();
};
