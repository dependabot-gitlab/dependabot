import * as compose from "docker-compose";
import yaml from "js-yaml";
import axios from "axios";
import { env } from "process";
import { readFileSync, writeFileSync } from "fs";
import { user } from "./user";
import { resolve, join } from "path";

const composeProjectName = env.COMPOSE_PROJECT_NAME || "dependabot";
const mockImage = env.MOCK_IMAGE || "registry.gitlab.com/dependabot-gitlab/ci-images/smocker-gogs:0.18-0.13";
const baseImage = env.APP_IMAGE || env.BASE_IMAGE || "andrcuns/dependabot-gitlab:nightly";
const updaterImagePattern = env.UPDATER_IMAGE_PATTERN || "andrcuns/dependabot-gitlab-%<package_ecosystem>s:nightly";

const composeYml = (() => {
  const yml = yaml.load(readFileSync("docker-compose.yml") as any) as any;
  yml["services"]["smocker"] = {
    image: mockImage,
    environment: {
      SMOCKER_MOCK_SERVER_LISTEN_PORT: "443"
    },
    ports: ["443:443", "8081:8081"]
  };

  return yaml.dump(yml, {
    styles: {
      "!!null": "empty"
    }
  });
})();

const composeEnv = {
  BASE_IMAGE: baseImage,
  UPDATER_IMAGE_PATTERN: updaterImagePattern,
  SETTINGS__GITLAB_URL: "https://smocker",
  SETTINGS__DEPENDABOT_URL: "http://dependabot-gitlab.com",
  SETTINGS__GITLAB_ACCESS_TOKEN: "e2e-test",
  SETTINGS__GITHUB_ACCESS_TOKEN: env.GITHUB_ACCESS_TOKEN_TEST,
  SETTINGS__LOG_COLOR: "true",
  SETTINGS__ANONYMOUS_ACCESS: "false"
};

const composeOpts = async () => {
  return {
    env: {
      ...env,
      ...composeEnv,
      GITLAB_SSL_CERT_FILE: await certFilePath()
    },
    composeOptions: ["--project-name", composeProjectName],
    log: false
  };
};

const certFilePath = async () => {
  if (env.GITLAB_SSL_CERT_FILE) return env.GITLAB_SSL_CERT_FILE;

  const resp = await axios.get("https://gitlab.com/dependabot-gitlab/ci-images/-/raw/main/scripts/gogs/certs/cert.pem");
  const certPath = join(resolve(), "tmp", "smocker-cert.pem");
  writeFileSync(certPath, resp.data);

  return certPath;
};

export const setupE2eEnvironment = async () => {
  const opts = await composeOpts();
  const options = { ...opts, configAsString: composeYml };

  console.log("*** Setting up E2E environment ***");
  console.log("Pulling images...");
  await compose.pullAll({ ...options, commandOptions: ["--include-deps", "-q"] });
  console.log("  successfully pulled images");

  console.log("Starting services...");
  await compose.upAll({ ...options, commandOptions: ["--wait"] });
  console.log("  successfully started test environment");

  try {
    console.log(`Creating test user '${user.name}'...`);
    await compose.exec("web", `bundle exec rake dependabot:create_user[${user.name},${user.password}]`, options);
    console.log("  successfully created test user");
  } catch (error) {
    if (error.err.includes(`Username '${user.name}' has already been taken`)) {
      console.log("  user already exists");
      return;
    }
    console.log("  failed to create test user");
    throw error;
  }
};

export const teardownE2eEnvironment = async () => {
  if (process.env.NO_TEARDOWN === "true") {
    console.log("*** NO_TEARDOWN is set to true, skipping teardown of E2E environment ***");
    return;
  }

  console.log("*** Tearing down E2E environment ***");
  console.log("Stopping services...");
  await compose.stop({ ...composeOpts, configAsString: composeYml });
  console.log("  successfully stopped test environment");
};
