import { APIRequestContext, APIResponse, expect, request } from "@playwright/test";
import { URL } from "url";
import { writeFileSync } from "fs";
import simpleGit from "simple-git";
import tmp from "tmp";
import path from "path";

import { gemFile, lockFile } from "./mocks";
import { expectResponseOk } from "../util";

export const defaultNamespace = "root";

export class Gogs {
  readonly baseUrl: string;
  readonly headers: { headers: { [key: string]: string } };

  context: APIRequestContext;

  constructor() {
    this.baseUrl = process.env.MOCK_URL || "https://localhost";
    this.headers = {
      headers: {
        "Content-Type": "application/json",
        Authorization: "token 149f5d3c282198a31d3e65a1fa4c1e19085d7930",
        "X-Gogs-Event": "true"
      }
    };
  }

  async init() {
    this.context = await request.newContext({
      baseURL: this.baseUrl,
      ignoreHTTPSErrors: true
    });

    return this;
  }

  async dispose() {
    await this.context.dispose();
  }

  async createGitRepo(projectName: string) {
    const response = await this.context.post("/api/v1/user/repos", {
      ...this.headers,
      data: JSON.stringify({
        name: projectName.replace(`${defaultNamespace}/`, ""),
        auto_init: true,
        readme: "Default"
      })
    });
    expectResponseOk(response);
    await this.prepareGitRepo(projectName);
  }

  private async prepareGitRepo(projectName: string) {
    const gitUrl = `https://root:root@${URL.parse(this.baseUrl).host}/${projectName}.git`;
    const tmpDir = tmp.dirSync({ unsafeCleanup: true });
    const repoPath = path.join(tmpDir.name, projectName.replace(`${defaultNamespace}/`, ""));

    const git = simpleGit({ baseDir: tmpDir.name });
    await git.clone(gitUrl);

    const userName = await git.cwd(repoPath).getConfig("user.name");
    if (!userName.value) {
      await git.cwd(repoPath).addConfig("user.name", "test");
      await git.cwd(repoPath).addConfig("user.email", "test@example.com");
    }

    writeFileSync(path.join(repoPath, "Gemfile"), gemFile);
    writeFileSync(path.join(repoPath, "Gemfile.lock"), lockFile);

    await git.cwd(repoPath).add(".");
    await git.cwd(repoPath).commit("Add dependency files");
    await git.cwd(repoPath).push();

    tmpDir.removeCallback();
  }
}
