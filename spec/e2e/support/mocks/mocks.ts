export const gemFile = `
source "https://rubygems.org"

gem "faker", "~> 3.1.0"
`;

export const lockFile = `
GEM
  remote: https://rubygems.org/
  specs:
    concurrent-ruby (1.2.2)
    faker (3.1.1)
      i18n (>= 1.8.11, < 2)
    i18n (1.14.1)
      concurrent-ruby (~> 1.0)

PLATFORMS
  ruby

DEPENDENCIES
  faker (~> 3.1.0)

BUNDLED WITH
   2.5.1
`;

/*
reusable dynamic mock definitions
  name: name of the file in spec/fixture/gitlab/mocks/dynamic
  params: params to be interpolated in the mock definition
*/
export const mocks = {
  gogsProxy: () => {
    return {
      definitions: [{ type: "gogs", name: "proxy" }]
    };
  },
  registerProject: (projectName: string) => {
    return {
      projectName: projectName,
      definitions: [{ name: "project" }, { name: "hook" }, { name: "set_hook" }, { name: "raw_config" }]
    };
  },
  updateDependencies: (projectName: string) => {
    return {
      projectName: projectName,
      definitions: [
        { name: "create_branch" },
        { name: "commits" },
        { name: "labels" },
        { name: "mr_check" },
        { name: "no_branch", params: { dependency: "faker" } },
        { name: "create_commits", params: { dependency: "faker" } },
        { name: "create_mr", params: { dependency: "faker", iid: 1 } }
      ]
    };
  }
};
