import Handlebars from "handlebars";

import { APIRequestContext, request, expect, APIResponse } from "@playwright/test";
import { randomInt } from "crypto";
import { readFileSync } from "fs";
import { expectResponseOk } from "../util";

export class Smocker {
  readonly baseUrl: string;
  readonly headers: { headers: { [key: string]: string } };

  context: APIRequestContext;

  constructor() {
    this.baseUrl = `${process.env.MOCK_URL || "https://localhost"}:8081`;
    this.headers = { headers: { "Content-Type": "application/x-yaml" } };
  }

  async init() {
    this.context = await request.newContext({
      baseURL: this.baseUrl,
      ignoreHTTPSErrors: true
    });

    return this;
  }

  async reset() {
    const response = await this.context.post("/reset");
    expectResponseOk(response);
  }

  async add(params: { projectName?: string; definitions: Array<{ name: string; type?: string; params?: {} }> }) {
    const yaml = this.definitionYml(params.projectName, params.definitions);

    const response = await this.context.post("/mocks", {
      ...this.headers,
      data: yaml
    });
    expectResponseOk(response);
  }

  async verify() {
    const response = await this.context.post("/sessions/verify");
    expectResponseOk(response);

    const responseJson = await response.json();
    const { verified, all_used, failures, unused } = responseJson.mocks;
    const message = ["Mock verify failed"];
    if (failures) message.push(`- failures: ${JSON.stringify(failures, null, 2)}`);
    if (unused) message.push(`- unused: ${JSON.stringify(unused, null, 2)}`);

    expect({ verified, all_used }, message.join("\n")).toEqual({ verified: true, all_used: true });
  }

  async dispose() {
    await this.context.dispose();
  }

  private defaultMockParams(projectName: string | undefined) {
    if (!projectName) return {};

    return {
      project_name: projectName.replace("/", "%2F"),
      id: randomInt(1000000),
      iid: randomInt(1000000)
    };
  }

  private definitionYml(
    projectName: string | undefined,
    definitions: Array<{ name: string; type?: string; params?: {} }>
  ) {
    const definitionYmls = definitions.map((definition) => {
      const fileName = `spec/fixture/${definition.type || "gitlab"}/mocks/dynamic/${definition.name}.yml`;
      const template = Handlebars.compile(readFileSync(fileName).toString(), { noEscape: true });

      return template({ ...this.defaultMockParams(projectName), ...definition.params });
    });

    return definitionYmls.join("\n");
  }
}
