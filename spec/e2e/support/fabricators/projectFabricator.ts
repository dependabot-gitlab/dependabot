import { APIRequestContext, APIResponse, expect } from "@playwright/test";
import { Smocker } from "../mocks/smocker";
import { Gogs } from "../mocks/gogs";
import { mocks } from "../mocks/mocks";

export class ProjectFabricator {
  private _initialized: boolean;
  private _headers: { [key: string]: string };
  private _smocker: Smocker;
  private _gogs: Gogs;
  private _request: APIRequestContext;

  constructor(request: APIRequestContext, user) {
    this._initialized = false;
    this._request = request;
    this._smocker = new Smocker();
    this._gogs = new Gogs();
    this._headers = {
      "Content-Type": "application/json",
      ...user.basicAuthHeader
    };
  }

  public get smocker() {
    this.init();

    return this._smocker;
  }

  async dispose() {
    await this.smocker.dispose();
    await this._gogs.dispose();
  }

  async create(
    projectName: string,
    options: { token?: string; reset?: boolean; withGitRepo?: boolean } = { reset: true, withGitRepo: false }
  ) {
    await this.init();

    if (options.reset) await this._smocker.reset();
    await this._smocker.add(mocks.registerProject(projectName));

    if (options.withGitRepo) {
      await this._smocker.add(mocks.gogsProxy());
      await this._gogs.createGitRepo(projectName);
    }

    const response = await this._request.post("/api/v2/projects", {
      headers: this._headers,
      data: { project_name: projectName, gitlab_access_token: options?.token }
    });
    const responseJson = await response.json();
    this.errorIfNotOk(response);

    return responseJson;
  }

  private errorIfNotOk(response: APIResponse) {
    expect(
      response.ok(),
      `Expected response to be ok, was: ${response.status()}, ${response.statusText()}`
    ).toBeTruthy();
  }

  private async init() {
    if (this._initialized) return;

    await this._smocker.init();
    await this._gogs.init();
    this._initialized = true;
  }
}
