import { Locator, Page, expect } from "@playwright/test";
import { defaultNamespace } from "../support/mocks/gogs";

export class ProjectsPage {
  private readonly page: Page;

  private readonly newProjectButton: Locator;
  private readonly searchInput: Locator;
  private readonly projectInput: Locator;
  private readonly tokenInput: Locator;
  private readonly addButton: Locator;

  constructor(page: Page) {
    this.page = page;
    this.newProjectButton = page.locator("id=new-project");
    this.searchInput = page.locator("id=search-input");
    this.projectInput = page.locator("id=project_name");
    this.tokenInput = page.locator("id=access_token");
    this.addButton = page.locator("id=add_project");
  }

  async visit() {
    await this.page.goto("/projects");
  }

  async expectPageToBeVisible() {
    await expect(this.page).toHaveTitle(/Dependabot GitLab/);
    await expect(this.newProjectButton).toBeVisible();
  }

  async expectProjectToBeVisible(projectName: string) {
    await expect(this.page.getByText(projectName)).toBeVisible();
  }

  async addProject(projectName: string, token?: string) {
    await this.newProjectButton.click();

    await this.projectInput.fill(projectName);
    if (token) {
      await this.tokenInput.fill(token);
    }
    await this.addButton.click();
  }

  async searchForProject(projectName: string) {
    await this.searchInput.fill(projectName.replace(`${defaultNamespace}/`, ""));
    await this.searchInput.press("Enter");
  }

  async setUpdateJobState(projectName: string, ecosystem: string, directory: string, state: boolean) {
    const checkbox = this.page.locator(`data-testid=${projectName}-${ecosystem}-${directory}-toggle`);

    await checkbox.setChecked(state);
  }

  async triggerUpdateJob(projectName: string, ecosystem: string, directory: string) {
    const button = this.page.locator(`data-testid=${projectName}-${ecosystem}-${directory}-execute`);

    await button.click();
  }
}
