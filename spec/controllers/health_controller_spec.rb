# frozen_string_literal: true

describe HealthController, :integration, type: :request do
  include_context "with api helper"

  around do |example|
    described_class.instance_variable_set(:@redis_connection_pool, nil)
    example.run
    described_class.instance_variable_set(:@redis_connection_pool, nil)
  end

  context "with successful health check" do
    it "returns 200 status" do
      get("/healthcheck")

      expect_status(200)
    end
  end

  context "with failed database health check" do
    let(:client) { instance_double(Mongo::Client) }

    before do
      allow(Mongoid).to receive(:default_client).and_return(client)
      allow(client).to receive(:database_names).and_raise(StandardError, "MongoDB connection failed")
    end

    it "returns 503 status" do
      get("/healthcheck")

      expect_status(503)
      expect_json({ status: "error", message: "MongoDB connection failed" })
    end
  end

  context "with failed redis health check" do
    let(:redis_config) { instance_double(RedisClient::Config, new_pool: redis_pool) }
    let(:redis_pool) { instance_double(RedisClient::Pooled) }

    before do
      allow(RedisClient).to receive(:config).with(**AppConfig.redis_config).and_return(redis_config)
      allow(redis_pool).to receive(:with).and_yield(redis_pool)
      allow(redis_pool).to receive(:call).with("PING").and_raise(StandardError, "Redis connection failed")
    end

    it "returns 503 status" do
      get("/healthcheck")

      expect_status(503)
      expect_json({ status: "error", message: "Redis connection failed" })
    end
  end
end
