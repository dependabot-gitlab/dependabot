# frozen_string_literal: true

describe Update::RunsController, :anonymous_access, :integration, type: :request do
  include_context "with api helper"

  let(:project) { create(:project) }
  let(:update_job) { project.update_jobs.first }
  let(:job_name) { update_job.name }
  let(:update_run) { create(:update_run_with_failures, job: update_job) }

  it "returns update run page", :aggregate_failures do
    api_get("/projects/#{project.id}/runs/#{update_run.id}")

    expect_status(200)
  end
end
