# frozen_string_literal: true

require "gitlab"
require "semver"
require "git"

require_relative "../task_helpers/release/creator"
require_relative "../task_helpers/release/checker"
require_relative "../task_helpers/chart_release_helper"
require_relative "../task_helpers/standalone_release_helper"
require_relative "../task_helpers/changelog_helper"

# rubocop:disable Rails/RakeEnvironment, Metrics/BlockLength
namespace :release do
  desc "Create new release tag"
  task(:app, %i[version dry_run]) do |_task, args|
    release_creator = Release::Creator.new(args[:version])
    next release_creator.print_changelog if args[:dry_run]

    release_creator.create_new_release
  end

  desc "Update helm chart version"
  task(:chart, [:version]) do |_task, args|
    ChartReleaseHelper.call(args[:version])
  end

  desc "Update standalone version"
  task(:standalone, [:version]) do |_task, args|
    StandaloneReleaseHelper.call(args[:version])
  end

  desc "Update changelog and closed issues"
  task(:changelog, %i[version changelog_file]) do |_task, args|
    changelog_helper = ChangelogHelper.new(args[:version], args[:changelog_file])

    changelog_helper.update_changelog
    changelog_helper.update_issues
  end

  desc "Check if release contains breaking changes"
  task(:check_breaking_changes) do
    checker = Release::Checker.new
    raise "Breaking changes detected" if checker.breaking_changes?

    puts "No breaking changes detected"
  end
end
# rubocop:enable Rails/RakeEnvironment, Metrics/BlockLength
