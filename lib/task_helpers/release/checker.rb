# frozen_string_literal: true

require "yaml"
require_relative "base"

module Release
  class Checker < Base
    def initialize
      super("major") # gitlab requires a version to get potential changelog and changes
    end

    # Check if release contains breaking changes
    #
    # @return [Boolean]
    def breaking_changes?
      changelog.include?(changelog_categories[:breaking])
    end

    private

    # Changelog categories
    #
    # @return [Hash]
    def changelog_categories
      YAML.load_file(Rails.root.join(".gitlab/changelog_config.yml").to_s, symbolize_names: true)[:categories]
    end
  end
end
