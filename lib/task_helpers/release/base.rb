# frozen_string_literal: true

require_relative "../util"

module Release
  class Base
    def initialize(version)
      @version_file_name = "VERSION"
      @version = version
    end

    # Log changes in new release
    #
    # @return [String]
    def changelog
      @changelog ||= gitlab
                     .get_changelog("dependabot-gitlab/dependabot", ref_to, trailer: "changelog", from: "v#{ref_from}")
                     .notes
    end

    private

    include Util

    attr_reader :version_file_name, :version

    delegate :parse, to: SemVer

    # Version file contents
    #
    # @return [String]
    def version_file_contents
      @version_file_contents ||= File.read(version_file_name)
    end

    # Current version
    #
    # @return [String]
    def ref_from
      @ref_from ||= version_file_contents.strip
    end

    # New version
    #
    # @return [SemVer]
    def ref_to
      send(version, ref_from)
    rescue NoMethodError
      parse(version)
    end

    # Update special version component
    #
    # @param [String] special
    # @return [String]
    def bump_special(special)
      return if special.blank?

      special.gsub(/\d+/) { |num| num.to_i.next }
    end

    # Reset special version component
    #
    # @param [String] special
    # @return [String]
    def reset_special(special)
      return if special.blank?

      special.gsub(/\d+/, "1")
    end

    # Increase patch version
    #
    # @param [String] ref_from
    # @return [SemVer]
    def patch(ref_from)
      parse(ref_from).tap do |ver|
        ver.patch += 1
        ver.special = reset_special(ver.special)
      end
    end

    # Increase minor version
    #
    # @param [String] ref_from
    # @return [SemVer]
    def minor(ref_from)
      parse(ref_from).tap do |ver|
        ver.minor += 1
        ver.patch = 0
        ver.special = reset_special(ver.special)
      end
    end

    # Increase major version
    #
    # @param [String] ref_from
    # @return [SemVer]
    def major(ref_from)
      parse(ref_from).tap do |ver|
        ver.major += 1
        ver.minor = 0
        ver.patch = 0
        ver.special = reset_special(ver.special)
      end
    end

    # Increase or set next major aplha version
    #
    # @param [String] ref_from
    # @return [SemVer]
    def pre(ref_from)
      parsed_ver = parse(ref_from)
      return major(ref_from).tap { |ver| ver.special = "alpha.1" } if parsed_ver.special.blank?

      parsed_ver.tap { |ver| ver.special = bump_special(ver.special) }
    end
  end
end
