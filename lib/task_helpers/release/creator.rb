# frozen_string_literal: true

require_relative "base"

module Release
  # Create release tag and update VERSION file
  #
  class Creator < Base
    def initialize(version)
      super

      @compose_file_name = "docker-compose.yml"
    end

    # Update changelog and create new tag
    #
    # @param [String] version
    # @return [void]
    def self.call(version)
      new(version).call
    end

    # Update changelog and create new tag
    #
    # @param [String] version
    # @return [void]
    def create_new_release
      update_version
      commit_and_tag
      print_changelog
    end

    # Update changelog
    #
    # @return [void]
    def update_version
      logger.info("Updating version to #{ref_to}")
      logger.info("Updating version.rb and docker-compose.yml file")

      new_version = ref_to.to_s.delete("v")
      updated_version_file = version_file_contents.gsub(ref_from, new_version)
      updated_compose_yml = compose_file_contents.gsub(ref_from, new_version)

      File.write(version_file_name, updated_version_file, mode: "w")
      File.write(compose_file_name, updated_compose_yml, mode: "w")
    end

    # Commit update changelog and create tag
    #
    # @return [void]
    def commit_and_tag
      logger.info("Committing changes")

      git = Git.init
      git.add(version_file_name)
      git.add(compose_file_name)
      git.commit("Update app version to #{ref_to}", no_verify: true)

      logger.info("Creating release tag")
      git.add_tag(ref_to.to_s)
    end

    # Log changes in new release
    #
    # @return [void]
    def print_changelog
      logger.info("Release contains following changes:\n#{changelog}")
    end

    private

    attr_reader :compose_file_name

    # Compose file contents
    #
    # @return [String]
    def compose_file_contents
      @compose_file_contents ||= File.read(compose_file_name)
    end
  end
end
