# frozen_string_literal: true

require_relative "util"

class ChartReleaseHelper
  CHART = "charts/dependabot-gitlab/Chart.yaml"
  README = "README.md"

  def initialize(version)
    @new_version = SemVer.parse(version)
    @chart_repo = "dependabot-gitlab/chart"
    @merge_attempts = 0
  end

  def self.call(version)
    new(version).update
  end

  # Update app version in helm chart
  #
  # @return [void]
  def update
    logger.info("Updating app version to #{new_numeric_version}")
    update_version
    create_merge_request
    merge
  end

  private

  include Util

  attr_reader :new_version, :chart_repo, :mr, :merge_attempts

  # Update app version
  #
  # @return [void]
  def update_version
    logger.info(" creating branch #{branch_name}")
    gitlab.create_branch(chart_repo, branch_name, "main")
    logger.info(" updating #{CHART}")
    gitlab.create_commit(
      chart_repo,
      branch_name,
      "Update app version to #{new_numeric_version}\n\nchangelog: dependency",
      file_actions
    )
  end

  # Create version update mr
  #
  # @return [void]
  def create_merge_request
    logger.info(" creating merge request")
    @mr = gitlab.create_merge_request(
      chart_repo,
      "Update app version to #{new_numeric_version}",
      source_branch: branch_name,
      target_branch: "main",
      remove_source_branch: true
    )
  end

  # Merge MR when pipeline succeeds
  #
  # @return [void]
  def merge
    return logger.error("Missing merge request info") unless mr

    logger.info(merge_attempts.zero? ? " setting merge request to merge when pipeline succeeds" : "  retrying")
    sleep(5) # wait for mr and the pipeline to be created
    gitlab.accept_merge_request(chart_repo, mr.iid, merge_when_pipeline_succeeds: true)
  rescue Gitlab::Error::MethodNotAllowed => e
    logger.warn("  auto-merge failed: #{e.message}.")
    @merge_attempts += 1

    merge_attempts < 3 ? retry : raise(e)
  end

  # Branch name
  #
  # @return [String]
  def branch_name
    @branch_name ||= "update-app-version-to-#{new_numeric_version}"
  end

  # Chart yaml
  #
  # @return [String]
  def chart
    @chart ||= gitlab.file_contents(chart_repo, CHART, "main")
  end

  # Chart README
  #
  # @return [String]
  def readme
    @readme ||= gitlab.file_contents(chart_repo, README, "main")
  end

  # Previous app version
  #
  # @return [Integer]
  def previous_version
    @previous_version = SemVer.parse(YAML.safe_load(chart)["appVersion"])
  end

  # New version withou v prefix
  #
  # @return [String]
  def new_numeric_version
    @new_numeric_version ||= new_version.format(Util::VERSION_FORMAT_PATTERN)
  end

  # Updated Chart.yaml
  #
  # @return [String]
  def updated_chart
    chart.gsub(previous_version.format(Util::VERSION_FORMAT_PATTERN), new_numeric_version)
  end

  # Updated readme file
  #
  # @return [String]
  def updated_readme
    readme
      .gsub(previous_version.format(Util::CLEAN_SEMVER_PATTERN), new_version.format(Util::CLEAN_SEMVER_PATTERN))
      # Special part is updated separately due to '--' in the badge link
      .gsub(previous_version.special, new_version.special)
  end

  # Gitlab commit actions
  #
  # @return [Array]
  def file_actions
    [
      {
        action: "update",
        file_path: CHART,
        content: updated_chart
      },
      {
        action: "update",
        file_path: README,
        content: updated_readme
      }
    ]
  end
end
