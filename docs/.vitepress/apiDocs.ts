import { readFileSync, writeFileSync } from "fs";
import OpenAPIParser from "@readme/openapi-parser";

const codeBlock = (code: string, type: string = "txt") => {
  let block = "\n```";
  block += `${type}\n`;
  block += `${code}\n`;
  block += "```\n\n";

  return block;
};

const tableHeader = (headers: string[]) => {
  const header = headers.join(" | ");
  const divider = headers.map(() => "---").join(" | ");

  return `| ${header} |\n| ${divider} |\n`;
};

const tableRow = (row: string[]) => {
  return `| ${row.join(" | ")} |\n`;
};

const markdownTableRow = (row: string[]) => {
  let tableRow = "<tr>\n";
  tableRow += row.map((cell) => `<td>${cell}</td>`).join("\n");
  tableRow += "\n</tr>\n";

  return tableRow;
};

const collapsedSection = (title: string, content: string) => {
  return `<details><summary>${title}</summary>\n\n${content}\n</details>\n\n`;
};

const getParameters = (parameters: Array<any>) => {
  return parameters.map((parameter) => {
    return parameter.schema
    ? Object.entries(parameter.schema.properties as { [path: string]: any }).map(([name, props]) => {
        return {
          name,
          in: parameter.in,
          type: props.type,
          required: parameter.schema.required?.includes(name),
          description: props.description
        };
      })
    : [parameter];
  }).flat();
};

const apiMethods = ["get", "post", "put", "delete", "patch"];
const docsFile = "tmp/api_swagger_doc.json";
const api = await OpenAPIParser.dereference(JSON.parse(readFileSync(docsFile, "utf-8")));

const v2Paths = Object.entries(api.paths as { [path: string]: any }).reduce((acc, [path, entry]) => {
  if (!path.includes("/v2/")) return acc;

  const tag = entry[Object.keys(entry)[0]].tags[0];

  if (!acc[tag]) acc[tag] = {};
  acc[tag][path] = entry;

  return acc;
}, {});

const apiDocMarkdown = Object.entries(v2Paths).reduce((acc, [type, paths]) => {
  const header = type
    .split("_")
    .map((word) => word[0].toUpperCase() + word.slice(1))
    .join(" ");
  acc += `## ${header}\n\n`;

  Object.entries(paths as any)
    .sort()
    .forEach(([path, entry]) => {
      Object.entries(entry as { [key: string]: any })
        .sort(([methodA], [methodB]) => apiMethods.indexOf(methodA) - apiMethods.indexOf(methodB))
        .forEach(([method, operation]) => {
          acc += `### ${operation.summary}\n\n`;
          acc += `${operation.description}\n`;
          acc += codeBlock(`${method.toUpperCase()} ${path}`);

          if (operation?.parameters?.length > 0) {
            acc += tableHeader(["Attribute", "In", "Type", "Required", "Description"]);
            getParameters(operation.parameters).forEach((parameter) => {
              acc += tableRow([
                parameter.name,
                parameter.in,
                parameter.type,
                parameter.required,
                parameter.description
              ]);
            });
            acc += `\n`;
          }

          acc += `#### Responses\n\n`;
          Object.entries(operation.responses as { [code: string]: any }).forEach(([code, response]) => {
            acc += "<table>\n";
            acc += markdownTableRow(["Code", "Description"]);
            const description = response.examples
              ? collapsedSection(response.description, codeBlock(JSON.stringify(response.examples, null, 2), "json"))
              : response.description;
            acc += markdownTableRow([code, description]);
            acc += "</table>\n";
          });
          acc += `\n`;
        });
    });

  return acc;
}, "");

export function apiDocs() {
  writeFileSync("docs/api/v2-endpoints.md", apiDocMarkdown);

  return {
    link: "/api/reference.html"
  };
}
