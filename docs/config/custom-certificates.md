# Self-signed certificates

In order to use GitLab instance with self signed certificates, path to certificate file must be provided via environment variable.
Depending on deployment method, following environment variable should be set.

## Standalone mode

In standalone mode, `gitlab_ssl_cert_file` input should be set to path of certificate file.

```yaml
include:
  - component: gitlab.com/dependabot-gitlab/dependabot-standalone/template@~latest
    inputs:
      gitlab_access_token: gitlab-access-token
      github_access_token: github-access-token
      gitlab_ssl_cert_file: /path/to/certificate.pem
```

Or via environment variable `SSL_CERT_FILE` directly in the job definition:

```yaml
dependabot-gitlab:
  ...
  variables:
    SSL_CERT_FILE: /path/to/certificate.pem
```

## Docker compose

For docker compose deployment, `GITLAB_SSL_CERT_FILE` environment should be set to path of certificate file.

```bash
export GITLAB_SSL_CERT_FILE=/path/to/certificate.pem
docker-compose up
```

## Kubernetes

See chart [values](https://gitlab.com/dependabot-gitlab/chart#values) section for details on how to set certificate file path.
