# Webhooks

::: warning
Not applicable for standalone mode
:::

If application is exposed to the network and external url is configured via `SETTINGS__DEPENDABOT_URL` environment variable, webhook will be created automatically when project is added.

## Configuration options

List of webhook related configuration options is described in [webhooks](./environment.md#webhooks) section.

## Adding webhooks manually

Webhooks can be created manually. For all integrations to work correctly, following [webhooks](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) with url `${dependabot_url}/api/hooks` and optional secret token have to be created in project manually:

- `Push events` - default repository branch
- `Merge request events`
- `Comments`
- `Pipeline events`

`${dependabot_url}` must be url of your app instance that can be accessed from `GitLab` instance being used. If `Secret token` field is configured, it must correspond to value configured via [`SETTINGS__GITLAB_AUTH_TOKEN`](./environment.md#webhooks) environment variable.
