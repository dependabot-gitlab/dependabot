# Common configuration

App supports base configuration template which can define common configuration options and merge them with project specific configuration options.

Path for base configuration file is configured via `SETTINGS__CONFIG_BASE_FILENAME` environment variable described in [configuration file](./environment.md#configuration-file) section.

Base configuration supports same values as the project specific `dependabot.yml` with the difference that [`update-options`](./configuration.md#common-update-options) key should be used to define common options that will be applied to all project specific configurations.

::: warning
`updates` key is used as an alias for `update-options` key and will be removed in future versions. `updates` is ignored if `update-options` key is present.
:::

Project specific options will override base configuration options if option exists in both configuration files.
