# Configuration environment variables

Different app specific functions can be configured via environment different environment variables.

## Concurrency

::: warning
Not applicable for standalone mode
:::

It is possible to configure concurrent thread amount for worker and web containers.

- `RAILS_MAX_THREADS` - sets amount of [sidekiq](https://github.com/mperham/sidekiq/wiki/Advanced-Options#concurrency) threads for worker container or [puma](https://github.com/puma/puma#thread-pool) threads for web container
- `RAILS_MIN_THREADS` - set minimum amount of [puma](https://github.com/puma/puma#thread-pool) threads for web container. Not applicable for worker container

## Databases

::: warning
Not applicable for standalone mode
:::

### MongoDb

Following environment variables are used for database connection configuration:

- `MONGODB_URI` - all configuration options in a single uri parameter

or

- `MONGODB_URL` - mongodb instance url
- `MONGODB_USER` - mongodb username
- `MONGODB_PASSWORD` - user password
- `MONGODB_DATABASE` - database name
- `MONGODB_RETRY_WRITES` - retry writes parameter, `true` by default

### Redis

- `REDIS_URL` - redis instance url
- `REDIS_PASSWORD` - redis password
- `REDIS_TIMEOUT` - redis timeout value, default: 1s

## Application

- `SECRET_KEY_BASE` - custom key used for stored credentials encryption, [Rails documentation](https://apidock.com/rails/v6.0.0/Rails/Application/secret_key_base)
- `SETTINGS__LOG_LEVEL` - logging level, `[debug, info, warn, error]`. Default: `info`
- `SETTINGS__LOG_COLOR` - prettier colorized log output. Default: `false`
- `SETTINGS__DRY_RUN` - run check for dependency updates but do not update anything. Default: `false`
- `SETTINGS__COMMANDS_PREFIX` - bot name/prefix for comment commands in merge requests
- `SETTINGS__ANONYMOUS_ACCESS` - unauthenticated access to projects page and API resources. Default: `true`
- `SETTINGS__FORCE_SSL` - Force all access to the app over SSL. Default: `false`

### Access

- `SETTINGS__GITLAB_URL` - url of gitlab instance, ex: `https://gitlab.com` by default
- `SETTINGS__GITLAB_ACCESS_TOKEN` - [gitlab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) personal access token with api scope
- `SETTINGS__GITHUB_ACCESS_TOKEN` - [github](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token) personal access token with repository read scope, without it you can run into rate limits when fetching changelog and release notes for all dependencies which code comes from github

### Gitlab API

- `SETTINGS__GITLAB_API_MAX_RETRY` - max number of retry for each gitlab api call, ex: `5`. Default: `3`
- `SETTINGS__GITLAB_API_MAX_RETRY_INTERVAL` - interval (in seconds) between each retry regarding the gitlab api call, ex: `2`. Default: `1`

### Webhooks

::: warning
Not applicable for standalone mode
:::

- `SETTINGS__DEPENDABOT_URL` - url application can be reached on, example: `https://dependabot-gitlab.com`. This url will be used to automatically
add necessary webhooks to project
- `SETTINGS__CREATE_PROJECT_HOOK` - enable or disable automated hook creation, default `true`. This can be useful if the user associated with used access token, does not have maintainer role
- `SETTINGS__GITLAB_AUTH_TOKEN` - optional gitlab webhook token which can be configured under webhook settings in gitlab, if not present,
token set in gitlab webhook configuration will be ignored

### Configuration file

- `SETTINGS__CONFIG_BASE_FILENAME` - path to base configuration file that will be merged with project specific `dependabot.yml` configuration file, see [common-configuration](./common-configuration.md) section for more details
- `SETTINGS__CONFIG_FILENAME` - custom configuration file location in project repository, default: `.gitlab/dependabot.yml`
- `SETTINGS__CONFIG_BRANCH` - custom branch to fetch `dependabot.yml`. This option does not affect target branch where dependabot checks for manifest files and raises merge requests against. For that, [target-branch](https://docs.github.com/en/code-security/dependabot/dependabot-version-updates/configuration-options-for-the-dependabot.yml-file#target-branch) option in configuration file itself must be used
- `SETTINGS__CONFIG_LOCAL_FILENAME` - local configuration file location, will be used if no configuration file is found in project repository
  ::: warning
  This configuration will be applied to all projects that do not have `dependabot.yml` configuration file in repository. This can lead to unexpected behavior if you have projects with different package managers and different folder structures.
  :::

### Service

::: warning
Not applicable for standalone mode
:::

- `SETTINGS__UPDATE_RETRY` - amount of retries for dependency update job or `false` to disable. Default: `2`
- `SETTINGS__EXPIRE_RUN_DATA` - purge persisted dependency update run logs from database after period of time in seconds, default 1 month
- `SETTINGS__UPDATER_TEMPLATE_PATH` - path to template file describing updater container setup, set automatically via helm chart deployment and set to `docker-compose.yml` in app root in case of docker-compose deployment
- `SETTINGS__DELETE_UPDATER_CONTAINER` - remove updater container after dependency updates finished. Default: `true`
- `SETTINGS__UPDATER_CONTAINER_STARTUP_TIMEOUT` - timeout in seconds before updater pod is considered to not have started successfully and update aborted. Default: `180`
- `SETTINGS__DEPLOY_MODE` - application deployment mode, `compose` or `k8s`. This setting is used internally to determine how updater containers should be created.
- `SETTINGS__UPDATER_IMAGE_PATTERN` - Ecosystem specific image pattern, where package_ecosystem is one of the supported ecosystems: `[bundler, npm, gomod, pip, docker, composer, pub, cargo, buget, maven, gradle, mix, terraform, elm, gitsubmodule]`, example: `andrcuns/dependabot-gitlab-%<package_ecosystem>s:latest`. Default: `none`
- `SETTINGS__COMPOSE_UPDATER_ALWAYS_PULL` - always pull updater image before running it. Only applicable when deploy mode is `compose`. Default: `false`
- `SETTINGS__SIDEKIQ_ALIVE_KEY_TTL` - allows to configure redis key ttl value for worker healthcheck implementation. More about this option in [sidekiq_alive `Time to live` value configuration section](https://github.com/arturictus/sidekiq_alive#options). Increasing this value can be useful if worker container is processing a lot of jobs and too small of a value can lead to sidekiq not being able to schedule healtcheck jobs in time. Default: `60`

### Project registration

::: warning
Not applicable for standalone mode
:::

- `SETTINGS__PROJECT_REGISTRATION`
  - `manual` - default value which disables automated project registration
  - `system_hook` - enable `api/project/registration` endpoint for system webhook configuration
  - `automatic` - creates a cron job which automatically scans all projects where user associated with used gitlab access token has at least developer role and adds project if `dependabot.yml` configuration file is present
- `SETTINGS__PROJECT_REGISTRATION_CRON` - cron expression for project registration job in `automatic` mode, default: `0 5 * * *`
- `SETTINGS__PROJECT_REGISTRATION_ALLOW_PATTERN` - allowed project path regex pattern for `automatic` of `system_hook` registration mode
- `SETTINGS__PROJECT_REGISTRATION_IGNORE_PATTERN` - ignored project path regex pattern for `automatic` of `system_hook` registration mode, will override allow pattern if both patterns match certain project path
- `SETTINGS__PROJECT_REGISTRATION_RUN_ON_BOOT` - when in `automatic` mode, trigger project registration job immediately after boot

### Metrics

::: warning
Not applicable for standalone mode
:::

- `SETTINGS__METRICS` - enable `/metrics` endpoint for prometheus compatible metrics

## Sentry

- `SENTRY_DSN` - [sentry dsn](https://docs.sentry.io/platforms/ruby/configuration/options/#environment-variables) value
- `SETTINGS__SENTRY_TRACES_SAMPLE_RATE` - [traces sample rate](https://docs.sentry.io/platforms/ruby/performance/#configure-the-sample-rate), default: 0.0
- `SETTINGS__SENTRY_IGNORED_ERRORS` - comma separated string of exceptions to exclude from reporting to sentry
