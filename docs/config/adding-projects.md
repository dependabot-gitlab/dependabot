# Adding projects

In order for application to start updating dependencies, projects have to be registered first which will create scheduled dependency update jobs. Several ways of adding projects exist.

Configuration options related to project registration can be found in [project registration](./environment.md#project-registration) section.

Automatic project registration requires a token with `maintainer` role in case automatic webhook creation is enabled via `SETTINGS__CREATE_PROJECT_HOOK` configuration option (default `true`).

## Automatically

### Project registration job

If `SETTINGS__PROJECT_REGISTRATION` is set to `automatic`, a scheduled job will be created which will periodically scan all projects where user associated with used gitlab access token has at least `developer` role. This job will automatically add projects that have valid configuration files as well as sync outdated configurations or schedule cron expressions.

To allow or ignore certain projects, `SETTINGS__PROJECT_REGISTRATION_ALLOW_PATTERN` and `SETTINGS__PROJECT_REGISTRATION_IGNORE_PATTERN` can be used.
Scheduled job can be triggered immediately after boot by setting `SETTINGS__PROJECT_REGISTRATION_RUN_ON_BOOT` to `true` and cron expression for how often to run the job can be set with `SETTINGS__PROJECT_REGISTRATION_CRON`.

### System webhook

If `SETTINGS__PROJECT_REGISTRATION` is set to `system_hook`, endpoint `api/project/registration` endpoint is enabled which listens for following [system hook](https://docs.gitlab.com/ee/system_hooks/system_hooks.html) events to automatically add/update/remove projects:

- `project_create`
- `project_destroy`
- `project_rename`
- `project_transfer`

## Manually

### UI

Projects can be added via `add project` form from the main page

### API

Project can be added using [add project](../api/reference.md#add-new-project) api endpoint

### Rake task

Projects can be added using [register](../administration/rake.md#register-single-project) rake task
