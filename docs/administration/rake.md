# Rake tasks

Several administrative [rake](https://github.com/ruby/rake) tasks exist which can be executed from app working directory of a running container.

All described commands should be executed from `/home/dependabot/app` directory.

## Register single project

Manually register project for updates. Repository must have valid dependabot config file.

```sh
bundle exec rake 'dependabot:register_project[project_name]'
```

`project_name` - project full path or multiple space separated project full paths, example: `dependabot-gitlab/dependabot`

## Register multiple projects

Manually register multiple projects. Repository must have valid dependabot config files.

```sh
bundle exec rake 'dependabot:register[projects]'
```

`projects` - list of projects full paths separated by space, example: `group/project_1 group/project_2`

## Register with specific access token

Manually register project for updates with specific gitlab access token

```sh
bundle exec rake 'dependabot:register_project[project_name,access_token]'
```

- `project_name` - project full path, example: `dependabot-gitlab/dependabot`
- `access_token` - project access token, example: [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)

## Bulk register

Manually trigger [project registration job](../config/adding-projects.md#project-registration-job)

```sh
bundle exec rake 'dependabot:automatic_registration'
```

## Remove

Manually remove project.

```sh
bundle exec rake 'dependabot:remove[project]'
```

`project` - project full path, example: `dependabot-gitlab/dependabot`

## Update

Trigger dependency update for specific project and package ecosystem. This task must be executed in appropriate ecosystem docker image for updates to work correctly.

```sh
bundle exec rake 'dependabot:update[project,package_ecosystem,directory]'
```

- `project` - project full path, example: `dependabot-gitlab/dependabot`
- `package_ecosystem` - `package-ecosystem` parameter like `bundler`
- `directory` - directory is path where dependency files are stored, usually `/`

This task is used to provide standalone use capability and used internally by updater containers in deployed mode and should not be used directly in most cases.

## Update vulnerability database

Trigger update of local vulnerability database which is used for security updates

```sh
bundle exec rake 'dependabot:update_vulnerability_db'
```

## Validate configuration

Validate `dependabot.yml` configuration file for a project

```sh
bundle exec rake 'dependabot:validate[project]'
```

`project` - project full path, example: `dependabot-gitlab/dependabot`

## Create user

Create a user to access projects page

```sh
bundle exec rake 'dependabot:create_user[username,password]'
```

## Delete user

Remove existing user

```sh
bundle exec rake 'dependabot:delete_user[username]'
```
