#!/bin/bash

function log() {
  echo -e "\033[1;33m$1\033[0m"
}

function log_error() {
  echo -e "\033[1;31m$1\033[0m"
}

function log_success() {
  echo -e "\033[1;32m$1\033[0m"
}

function log_info() {
  echo -e "\033[1;35m$1\033[0m"
}

function log_with_header() {
  length=$(echo "$1" | awk '{print length}')
  delimiter=$(head -c $length </dev/zero | tr '\0' "${2:-=}")

  log_info "$delimiter"
  log_info "$1"
  log_info "$delimiter"
}

function dependabot_version() {
  echo "$(awk '/dependabot-omnibus \([0-9.]+\)/ {print $2}' Gemfile.lock | sed 's/[()]//g')"
}

function setup_buildx() {
  docker context create builder
  docker buildx create --use --bootstrap builder
}

function docker_login() {
  echo "$CI_REGISTRY_PASSWORD" | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
}

function setup_git_user() {
  if [ -z "${GITLAB_USER_EMAIL}" ]; then
    log_error "GITLAB_USER_EMAIL is not present!"
    log_error "Using default email 'ci@example.com'"
    log_error "You may need to remove valid user validation under 'Repository > Push rules' settings"
    GITLAB_USER_EMAIL="ci@example.com"
  fi
  git config --global user.name "CI"
  git config --global user.email "$GITLAB_USER_EMAIL"
  echo "Successfully set git user to 'CI <$GITLAB_USER_EMAIL>'"
}

function setup_git() {
  local ref=${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME:-$CI_COMMIT_BRANCH}

  log_with_header "Setting up git for local operations"

  log_info "Checking if GIT_PUSH_TOKEN is present"
  if [ -z "${GIT_PUSH_TOKEN:-}" ]; then
    log_error "GIT_PUSH_TOKEN is not present!"
    log_error "Please create project access token with write_repository permissions and add GIT_PUSH_TOKEN variable in your GitLab project CI/CD settings"
    exit 1
  else
    log_success "GIT_PUSH_TOKEN is present"
  fi

  log_info "Configuring git"
  log "Setting up git user"
  setup_git_user

  log_info "Setting origin to 'gitlab.com/${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH:-$CI_PROJECT_PATH}'"
  git remote set-url origin "https://git-push-token:${GIT_PUSH_TOKEN}@gitlab.com/${CI_MERGE_REQUEST_SOURCE_PROJECT_PATH:-$CI_PROJECT_PATH}.git"
  log_success "Successfully set origin"

  log_info "Fetching origin and checking out branch"
  git fetch --no-tags && git checkout $ref
  log_success "Successfully configured git and set current branch to '$ref'"
}

function set_mock() {
  local mock="${1}"
  local url="${2:-https://smocker:8081}"

  mocks="$(cat spec/fixture/gitlab/mocks/gitlab-mock-${mock}.yml)"
  curl -k -X POST \
    --header "content-type: application/x-yaml" \
    --data "$mocks" \
    "${url}/mocks?reset=true"
}

function download_mock_cert() {
  local cert_file="${1:-cert.pem}"

  log "** Downloading mock server cert file **"
  curl -sSL https://gitlab.com/dependabot-gitlab/ci-images/-/raw/main/scripts/gogs/certs/cert.pem >$cert_file
  echo "saved to ${cert_file}"
}

function add_mock_certs() {
  local cert="${1}"
  local smocker_cert="/usr/local/share/ca-certificates/smocker.crt"

  log "** Adding mock certs to system **"
  if [ -f "${cert}" ]; then
    cp $cert $smocker_cert
  else
    curl -sSL https://gitlab.com/dependabot-gitlab/ci-images/-/raw/main/scripts/gogs/certs/cert.pem >$smocker_cert
  fi
  update-ca-certificates
  log_success "success!"
}
