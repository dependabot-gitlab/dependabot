#!/bin/bash

source "$(dirname "$0")/utils.sh"

function print_service_logs() {
  local service=$1

  log_with_header "${service} container logs"
  docker compose -f ${COMPOSE_PROJECT_NAME:-dependabot} logs --no-log-prefix $service
}

log_with_header "Container states"
docker ps -a --no-trunc

print_service_logs "migration"
print_service_logs "web"
print_service_logs "worker"
print_service_logs "background-tasks"
