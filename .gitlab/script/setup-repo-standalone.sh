#!/bin/bash

# Mocked test repository setup for standalone test run

set -euo pipefail

source "$(dirname "$0")/utils.sh"

repo_path=/tmp/repo-contents/${REPO_NAME}
gemfile="IyBmcm96ZW5fc3RyaW5nX2xpdGVyYWw6IHRydWUKCnNvdXJjZSAiaHR0cHM6Ly9ydWJ5Z2Vtcy5vcmciCgpnZW0gImZha2VyIiwgIn4+IDEuOSIKZ2VtICJnaXQiLCAifj4gMS44LjAiCmdlbSAicnVib2NvcCIsICJ+PiAxLjE5LjEiCg=="
lockfile="R0VNCiAgcmVtb3RlOiBodHRwczovL3J1YnlnZW1zLm9yZy8KICBzcGVjczoKICAgIGFzdCAoMi40LjIpCiAgICBjb25jdXJyZW50LXJ1YnkgKDEuMS45KQogICAgZmFrZXIgKDEuOS42KQogICAgICBpMThuICg+PSAwLjcpCiAgICBnaXQgKDEuOC4wKQogICAgICByY2hhcmRldCAofj4gMS44KQogICAgaTE4biAoMS44LjExKQogICAgICBjb25jdXJyZW50LXJ1YnkgKH4+IDEuMCkKICAgIHBhcmFsbGVsICgxLjIxLjApCiAgICBwYXJzZXIgKDMuMC4zLjIpCiAgICAgIGFzdCAofj4gMi40LjEpCiAgICByYWluYm93ICgzLjAuMCkKICAgIHJjaGFyZGV0ICgxLjguMCkKICAgIHJlZ2V4cF9wYXJzZXIgKDIuMi4wKQogICAgcmV4bWwgKDMuMi41KQogICAgcnVib2NvcCAoMS4xOS4xKQogICAgICBwYXJhbGxlbCAofj4gMS4xMCkKICAgICAgcGFyc2VyICg+PSAzLjAuMC4wKQogICAgICByYWluYm93ICg+PSAyLjIuMiwgPCA0LjApCiAgICAgIHJlZ2V4cF9wYXJzZXIgKD49IDEuOCwgPCAzLjApCiAgICAgIHJleG1sCiAgICAgIHJ1Ym9jb3AtYXN0ICg+PSAxLjkuMSwgPCAyLjApCiAgICAgIHJ1YnktcHJvZ3Jlc3NiYXIgKH4+IDEuNykKICAgICAgdW5pY29kZS1kaXNwbGF5X3dpZHRoICg+PSAxLjQuMCwgPCAzLjApCiAgICBydWJvY29wLWFzdCAoMS4xNS4wKQogICAgICBwYXJzZXIgKD49IDMuMC4xLjEpCiAgICBydWJ5LXByb2dyZXNzYmFyICgxLjExLjApCiAgICB1bmljb2RlLWRpc3BsYXlfd2lkdGggKDIuMS4wKQoKUExBVEZPUk1TCiAgeDg2XzY0LWRhcndpbi0yMAoKREVQRU5ERU5DSUVTCiAgZmFrZXIgKH4+IDEuOSkKICBnaXQgKH4+IDEuOC4wKQogIHJ1Ym9jb3AgKH4+IDEuMTkuMSkKCkJVTkRMRUQgV0lUSAogICAyLjIuMzEK"

add_mock_certs $CERT_FILE

log "** Setting up git user **"
setup_git_user
log "** Creating repo '${REPO_NAME}' **"
curl --fail -X POST "https://${MOCK_HOST}:3443/api/v1/user/repos" \
  -H "Content-Type: application/json" \
  -H "Authorization: token 149f5d3c282198a31d3e65a1fa4c1e19085d7930" \
  -d "{\"name\": \"${REPO_NAME}\", \"auto_init\": true, \"readme\": \"Default\"}"
echo ""

log "** Adding dependency files to repo **"
git config --global http.version HTTP/1.1
git clone https://root:root@${MOCK_HOST}:3443/root/${REPO_NAME}.git $repo_path
echo $gemfile | base64 -d >$repo_path/Gemfile
echo $lockfile | base64 -d >$repo_path/Gemfile.lock
cd $repo_path
git add --all
git commit -m "Add dep files"

log "** Pushing dependency files to repo **"
git push
