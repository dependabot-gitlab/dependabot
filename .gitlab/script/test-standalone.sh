#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

network="dependabot"
repo_name="testing"
app_image="${APP_IMAGE_NAME}/bundler:${CURRENT_TAG}"
cert_path="${CI_PROJECT_DIR}/tmp"
cert_file="cert.pem"

log_with_header "Setup gitlab mock"
download_mock_cert "$cert_path/$cert_file"

log "** Creating '${network}' network **"
docker network create $network

log "** Pulling image '${MOCK_IMAGE}' **"
docker pull --quiet $MOCK_IMAGE

log "** Starting gitlab mock service **"
docker run -d \
  --network $network \
  --name smocker \
  -e SMOCKER_LOG_LEVEL=warning \
  -e SMOCKER_MOCK_SERVER_LISTEN_PORT=443 \
  -p 443:443 \
  -p 8081:8081 \
  -p 3443:3443 \
  ${MOCK_IMAGE}

log "** Setting mock expectations **"
set_mock standalone https://docker:8081

# Repo setup is wrapped in a docker container due to strange gogs ssl issue when running with dind setup outside of docker network
log_with_header "Prepare testing git repo contents"
image=registry.gitlab.com/dependabot-gitlab/ci-images:latest
docker pull --quiet $image
docker run --rm -i \
  -v $CI_PROJECT_DIR/.gitlab/script:/scripts \
  -v $cert_path/$cert_file:/certs/$cert_file \
  -e GITLAB_USER_EMAIL=test@example.com \
  -e CERT_FILE=/certs/$cert_file \
  -e MOCK_HOST=smocker \
  -e REPO_NAME=$repo_name \
  --network $network \
  $image \
  /scripts/setup-repo-standalone.sh

log_with_header "Running standalone gem dependency updates"
log "** Pulling image '${app_image}' **"
docker pull --quiet $app_image

log "** Running rake task 'dependabot:update[root/${repo_name},bundler,/]' **"
exit_code=0
command time -o time.txt -f "%e" docker run --rm -i \
  -e SETTINGS__GITLAB_URL=https://smocker \
  -e SETTINGS__GITLAB_ACCESS_TOKEN=e2e-test \
  -e SETTINGS__GITHUB_ACCESS_TOKEN="${GITHUB_ACCESS_TOKEN_TEST:-}" \
  -e SETTINGS__STANDALONE=true \
  -e SETTINGS__LOG_LEVEL=debug \
  -e SETTINGS__LOG_COLOR=true \
  -e SECRET_KEY_BASE=secret \
  -e SSL_CERT_FILE=/home/dependabot/app/tmp/certs/$cert_file \
  -v $cert_path/$cert_file:/home/dependabot/app/tmp/certs/$cert_file \
  --network $network \
  $app_image \
  rake "dependabot:update[root/${repo_name},bundler,/]" || exit_code=$?

echo "# TYPE update_duration_seconds_sum summary" >$METRICS_REPORT
echo "update_duration_seconds_sum $(cat time.txt)" >>$METRICS_REPORT

log_with_header "Mock session info"
curl -k -X POST -s "https://docker:8081/sessions/verify" | jq
log_with_header "Mock logs"
docker logs smocker

exit $exit_code
