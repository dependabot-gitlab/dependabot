#!/bin/bash

set -e

source "$(dirname "$0")/utils.sh"

token=$1

if [ -n "$CI_MERGE_REQUEST_IID" ]; then
  log "MR pipeline detected, only showing changes"
  command="diff upgrade"
  install_args="--allow-unreleased --color --context 5"
else
  command="upgrade"
  install_args="--atomic --create-namespace"
fi

log_info "Adding gitlab chart repo"
helm repo add gitlab https://charts.gitlab.io && helm repo update

log_info "Installing GitLab Agent"
helm $command dependabot gitlab/gitlab-agent \
  --namespace gitlab-agent \
  --values deploy/agent/values.yaml \
  --set config.token=$token \
  $install_args
