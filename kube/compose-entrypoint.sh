#!/bin/bash

if [ -z "$SSL_CERT_FILE" ] || [ ! -f "$SSL_CERT_FILE" ] || [ ! -s "$SSL_CERT_FILE" ]; then
  unset SSL_CERT_FILE
fi

exec bundle exec "$@"
