# frozen_string_literal: true

module Dependabot
  # File fetcher wrapper with support for files from multiple directories
  #
  class MultiDirFileFetcher
    def initialize(source:, commit:, files:)
      @source = source
      @commit = commit
      @files = files
    end

    attr_reader :source, :commit
    # @return [Array<Array>] array of dependency files for each directory
    attr_reader :files
  end
end
