# frozen_string_literal: true

module Dependabot
  # :reek:TooManyMethods

  # Updated dependency container
  #
  class UpdatedDependency # rubocop:disable Metrics/ClassLength
    # rubocop:disable Metrics/ParameterLists

    HAS_UPDATES = 1
    UP_TO_DATE = 2
    UPDATE_IMPOSSIBLE = 3
    SKIPPED = 4
    ERROR = 5

    # :reek:LongParameterList

    # @param [Dependabot::Dependency] dependency
    # @param [Array<Dependabot::DependencyFile>] dependency_files
    # @param [Integer] state
    # @param [Array<Dependabot::Dependency>] updated_dependencies
    # @param [Array<Dependabot::DependencyFile>] updated_files
    # @param [Boolean] vulnerable
    # @param [Hash] auto_merge_rules
    # @param [Array<Vulnerability>] vulnerabilities
    # @param [StandardError] error
    def initialize(
      dependency:,
      dependency_files:,
      state:,
      updated_dependencies: nil,
      updated_files: nil,
      vulnerable: nil,
      auto_merge_rules: nil,
      vulnerabilities: [],
      error: nil
    )
      @dependency = dependency
      @dependency_files = dependency_files
      @state = state
      @updated_dependencies = updated_dependencies
      @updated_files = updated_files
      @vulnerable = vulnerable
      @vulnerabilities = vulnerabilities
      @auto_merge_rules = auto_merge_rules
      @error = error
    end
    # rubocop:enable Metrics/ParameterLists

    # @return [Dependabot::Dependency] main dependency name
    attr_reader :dependency
    # @return [Array<Dependabot::DependencyFile>] dependency files
    attr_reader :dependency_files
    # @return [Integer] update state
    attr_reader :state
    # @return [Array<Dependabot::Dependency>] updated dependencies
    attr_reader :updated_dependencies
    # @return [Array<Dependabot::DependencyFile>] updated files
    attr_reader :updated_files
    # @return [Boolean]
    attr_reader :vulnerable
    # @return [Array<Vulnerability>] vulnerabilities
    attr_reader :vulnerabilities
    # @return [Hash] merge rules
    attr_reader :auto_merge_rules
    # @return [StandardError] update error
    attr_reader :error

    delegate :name, :production?, :top_level?, :package_manager, to: :dependency

    alias_method :vulnerable?, :vulnerable

    # Directory of updated dependency
    #
    # @return [String]
    def directory
      @directory ||= dependency_files.first.directory
    end

    # Main dependency version
    #
    # @return [Object]
    def version
      @version ||= version_class.new(dependency.version)
    end

    # All dependencies to be updated with new versions
    #
    # @return [String]
    def current_versions
      @current_versions ||= updated_dependencies.map { |dep| "#{dep.name}-#{dep.version}" }.join("/")
    end

    # All dependencies being updated with previous versions
    #
    # @return [String]
    def previous_versions
      @previous_versions ||= updated_dependencies.map { |dep| "#{dep.name}-#{dep.previous_version}" }.join("/")
    end

    # Allow automerging dependency update
    #
    # @return [Boolean]
    def auto_mergeable?
      @auto_mergeable ||= !auto_merge_rules.nil? && (allow_automerge && !ignore_automerge)
    end

    # Fixed vulnerabilities in format for pr creator
    #
    # @return [Hash]
    def fixed_vulnerabilities
      @fixed_vulnerabilities ||= begin
        fixed = vulnerabilities.select { |entry| updated_dependencies&.any? { |dep| entry.fixed_by?(dep) } }

        # group fixed vulnerabilities by package name
        merge_vulnerabilities(fixed).values.each_with_object(Hash.new { |hsh, key| hsh[key] = [] }) do |vuln, hsh|
          hsh[vuln["package"]] << vuln.except("package")
        end
      end
    end

    # Vulnerabilities that affect dependency
    #
    # @return [Hash]
    def actual_vulnerabilities
      @actual_vulnerabilities ||= vulnerabilities.select { |entry| entry.vulnerable?(version) }
    end

    # Updates present
    #
    # @return [Boolean]
    def updates?
      state == HAS_UPDATES
    end

    # Update is not possible
    #
    # @return [Boolean]
    def update_impossible?
      state == UPDATE_IMPOSSIBLE
    end

    # Dependency is up to date
    #
    # @return [Boolean]
    def up_to_date?
      state == UP_TO_DATE
    end

    # Update was skipped
    #
    # @return [Boolean]
    def skipped?
      state == SKIPPED
    end

    # Dependency update failed due to error
    #
    # @return [Boolean]
    def errored?
      state == ERROR
    end

    # Object comparator
    # @param [UpdatedDependency] other
    # @return [Booelan]
    def ==(other)
      self.class == other.class && comparable == other.comparable
    end

    protected

    # Object state
    # @return [Array]
    def comparable
      instance_variables.map { |var| instance_variable_get(var) }
    end

    private

    # Version class
    #
    # @return [Class]
    def version_class
      @version_class ||= Utils.version_class_for_package_manager(package_manager)
    end

    # Requirements class
    #
    # @return [Class]
    def requirements_class
      @requirements_class ||= Utils.requirement_class_for_package_manager(package_manager)
    end

    # Checkers for rule dependency-type parameters
    #
    # @return [Hash<String, Proc>]
    def rule_type_handlers
      @rule_type_handlers ||= {
        "all" => proc { true },
        "direct" => proc { top_level? },
        "indirect" => proc { !top_level? },
        "production" => proc { production? },
        "development" => proc { !production? },
        "security" => proc { vulnerable? }
      }
    end

    # Convert vulnerability to hash format and merge vulnerabilities with same id
    #
    # @param [Array<Vulnerability>] entries
    # @return [Hash]
    def merge_vulnerabilities(entries)
      entries.each_with_object({}) do |vuln, hsh|
        id = vuln.id

        if hsh.key?(id)
          hsh[id]["patched_versions"] << vuln.first_patched_version if vuln.first_patched_version
          hsh[id]["affected_versions"] << vuln.vulnerable_version_range
          next
        end

        hsh[id] = vuln.to_hash
      end
    end

    # Allow automerge
    #
    # @return [Boolean]
    def allow_automerge
      return true unless auto_merge_rules[:allow]

      satisfies_rule?(auto_merge_rules[:allow])
    end

    # Ignore automerge
    #
    # @return [Boolean]
    def ignore_automerge
      return false unless auto_merge_rules[:ignore]

      satisfies_rule?(auto_merge_rules[:ignore])
    end

    # Rule satisfies version update
    #
    # @param [Array<Hash>] rules
    # @return [Boolean]
    def satisfies_rule?(rules)
      condition_rules = rules.map { |rule| rule.except(:dependency_type) }
      type_rules = rules.map { |rule| rule.slice(:dependency_name, :dependency_type) }

      updated_dependencies.any? do |dep|
        type_rules.any? { |rule| satisfies_type?(dep, rule) } && conditions(dep, condition_rules).any? do |condition|
          requirements_class.new(condition).satisfied_by?(version_class.new(dep.version))
        end
      end
    end

    # Dependency satisfies type rule
    #
    # @param [Dependabot::Dependency] dep
    # @param [Hash] rule
    # @return [Boolean]
    def satisfies_type?(dep, rule)
      return false unless Dependabot::Config::UpdateConfig.wildcard_match?(rule[:dependency_name], dep.name)

      rule_type_handlers[rule[:dependency_type] || "all"].call
    end

    # Version conditions for dependency
    #
    # @param [Dependabot::Dependency] dep
    # @param [Array] condition_rules
    # @return [Array]
    def conditions(dep, condition_rules)
      Update::RuleHandler.version_conditions(to_previous_version(dep), condition_rules)
    end

    # Convert updated dependency to previous state
    #
    # Previous state is needed for correct version condition creation because we need to compare update from -> to
    #
    # @param [Dependabot::Dependency] dep
    # @return [Dependabot::Dependency]
    def to_previous_version(dep)
      Dependabot::Dependency.new(
        name: dep.name,
        package_manager: package_manager,
        version: dep.previous_version,
        requirements: dep.previous_requirements
      )
    end
  end
end
