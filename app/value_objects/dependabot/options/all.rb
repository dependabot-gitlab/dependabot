# frozen_string_literal: true

require "yaml"
require "pathname"

module Dependabot
  module Options
    # :reek:MissingSafeMethod
    # :reek:InstanceVariableAssumption

    # All options contained in dependabot configuration file
    #
    class All
      include ApplicationHelper
      include OptionsHelper

      # @param [String] config dependabot.yml configuration file
      # @param [String] project name of the project
      def initialize(config, project)
        @config = config
        @project = project
        @config_filename = Pathname.new(DependabotConfig.config_filename).basename.to_s
      end

      delegate :config_base_filename, to: DependabotConfig

      # Transform dependabot configuration file into options
      #
      # @return [Hash]
      def transform
        # Validate in steps to fail early and avoid duplicate errors
        validate_base_config!
        validate_update_options!
        conf = validate_merged_config!

        {
          forked: conf[:fork],
          registries: Registries.new(conf[:registries]).transform,
          updates: conf[:updates].flat_map { |configuration| config_entries(configuration) }
        }.compact
      end

      private

      attr_reader :config, :project, :config_filename

      # Parsed dependabot yml config
      #
      # @return [Hash<Symbol, Object>]
      def yml
        @yml ||= YAML.safe_load(config, symbolize_names: true)
      end

      # Base configuration if exists
      #
      # @return [Hash]
      def base_config
        return @base_config if defined?(@base_config)
        return @base_config = {} unless config_base_filename

        unless File.exist?(config_base_filename)
          log(:warn, "Base configuration file configured but not found: '#{config_base_filename}'")
          return @base_config = {}
        end

        @base_config = YAML.load_file(config_base_filename, symbolize_names: true)
      end

      # Complete merged configuration
      #
      # @return [Hash]
      def merged_config
        @merged_config ||= base_config.deep_merge({
          **yml.except(:updates),
          updates: yml[:updates].map do |entry|
            (base_config[:"update-options"] || base_config[:updates] || {})
              .deep_merge(yml[:"update-options"] || {})
              .deep_merge(entry)
          end
        })
      end

      # Configuration entry
      #
      # @param [Hash] configuration
      # @return [Array<Hash>]
      def config_entries(configuration)
        directories = configuration[:directories] || [configuration[:directory]]

        directories.map do |directory|
          {
            fork: yml[:fork],
            **Main.new(project, configuration, yml[:registries], directory).transform,
            **Branch.new(configuration).transform,
            **CommitMessage.new(configuration).transform,
            **AuthorDetails.new(configuration).transform,
            **Rules.new(configuration).transform,
            **AutoMerge.new(configuration).transform,
            **Rebase.new(configuration).transform,
            **VulnerabilityAlerts.new(yml, configuration).transform,
            **DependencyGroups.new(configuration).transform
          }.compact
        end
      end

      # Validate base configuration
      #
      # @return [void]
      def validate_base_config!
        return unless base_config.any?

        if base_config[:updates]
          ApplicationHelper.log(:warn, "`updates` key in base configuration is deprecated, use `update-options!`")
        end
        validate!(base_config, Schemas.base_config_schema, config_base_filename)
      end

      # Validate config file update-options
      #
      # @return [void]
      def validate_update_options!
        validate!(yml.except(:updates), Schemas.update_options_schema) if yml[:"update-options"]&.any?
      end

      # Validate complete merged configuration
      #
      # @return [Hash]
      def validate_merged_config!
        validate!(merged_config)
      end

      # Validate data against schema
      #
      # @param [Hash] config
      # @param [Hash] schema
      # @param [String] filename
      # @return [Hash]
      def validate!(config, schema = Schemas.configuration_schema, filename = config_filename)
        schemer = JSONSchemer.schema(schema, formats: Schemas.formats)
        result = Schemas::ValidationResult.new(schemer.validate(config))
        return config if result.success?

        raise(Schemas::ValidationError, "Validation for #{filename} failed:\n#{result.error_message}")
      end
    end
  end
end
