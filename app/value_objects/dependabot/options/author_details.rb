# frozen_string_literal: true

module Dependabot
  module Options
    # Author detail related options
    #
    class AuthorDetails < OptionsBase
      # Transform author detail options
      #
      # @return [Hash]
      def transform
        author_details = opts[:"author-details"]
        return {} unless author_details

        {
          author_details: {
            name: author_details[:name],
            email: author_details[:email]
          }.compact
        }
      end
    end
  end
end
