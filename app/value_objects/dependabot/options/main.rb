# frozen_string_literal: true

module Dependabot
  module Options
    # Main update options
    #
    class Main < OptionsBase
      # @return [Hash] mapping for versioning strategies option
      VERSIONING_STRATEGIES = {
        "auto" => nil,
        "lockfile-only" => :lockfile_only,
        "widen" => :widen_ranges,
        "increase" => :bump_versions,
        "increase-if-necessary" => :bump_versions_if_necessary
      }.freeze

      def initialize(project, opts, registries, directory)
        super(opts)

        @project = project
        @registries = registries
        @directory = directory
      end

      # Transform general main options
      #
      # @return [Hash]
      def transform
        package_ecosystem = opts[:"package-ecosystem"]

        {
          # github native implementation modifies some of the names in the config file
          # https://docs.github.com/en/github/administering-a-repository/configuration-options-for-dependency-updates#package-ecosystem
          package_manager: DependabotEcosystem::PACKAGE_ECOSYSTEM_MAPPING.fetch(package_ecosystem, package_ecosystem),
          package_ecosystem: package_ecosystem,
          name: opts[:name],
          vendor: opts[:vendor],
          directory: directory,
          milestone: opts[:milestone],
          assignees: opts[:assignees],
          reviewers: opts[:reviewers],
          approvers: opts[:approvers],
          custom_labels: opts[:labels],
          registries: opts[:registries] || "*",
          versioning_strategy: versioning_strategy(opts[:"versioning-strategy"]),
          open_merge_requests_limit: opts[:"open-pull-requests-limit"] || 5,
          open_security_merge_requests_limit: opts[:"open-security-pull-requests-limit"] || 10,
          updater_options: opts[:"updater-options"] || {},
          unsubscribe_from_mr: opts[:"unsubscribe-from-mr"],
          auto_approve: opts[:"auto-approve"],
          **insecure_code_execution_options,
          **schedule_options
        }
      end

      private

      attr_reader :project, :registries, :directory

      # Insecure code execution options
      #
      # @return [Hash]
      def insecure_code_execution_options
        ext_execution = opts[:"insecure-external-code-execution"]
        return { reject_external_code: true } if registries && ext_execution.nil?

        { reject_external_code: ext_execution.nil? ? false : ext_execution != "allow" }
      end

      # Cron options
      #
      # @return [Hash]
      def schedule_options
        return {} unless opts[:schedule]

        {
          cron: Schedule.new(
            entry: "#{project}-#{opts[:"package-ecosystem"]}-#{opts[:directory]}", **opts[:schedule]
          ).to_s
        }
      end

      # Versioning strategy
      #
      # @param [String] strategy
      # @return [Symbol]
      def versioning_strategy(strategy)
        return unless strategy

        VERSIONING_STRATEGIES.fetch(strategy) do |el|
          log(:error, "Unsupported versioning-strategy #{el}")
          nil
        end
      end
    end
  end
end
