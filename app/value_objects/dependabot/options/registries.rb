# frozen_string_literal: true

module Dependabot
  module Options
    # Dependabot configuration registries options
    #
    class Registries # rubocop:disable Metrics/ClassLength
      include ApplicationHelper

      # Registries options
      #
      # @param [Hash] registries
      def initialize(registries)
        @registries = registries
      end

      # Fetch configured registries
      #
      # @return [Hash]
      def transform
        return {} unless registries

        registries
          .stringify_keys
          .transform_values { |registry| transform_registry_values(registry) }
          .compact
      end

      private

      # @return [Hash]
      attr_reader :registries

      delegate :registries_token_schema,
               :registries_no_auth_schema,
               :registries_hex_schema,
               :registries_key_schema,
               :registries_password_schema,
               to: Schemas

      # Update registry hash
      #
      # dependabot-core uses specific credentials hash depending on registry types with keys as strings
      # validate supported contracts and transform values to dependabot-core format unless all contracts fail
      #
      # @param [Hash] registry
      # @return [Hash]
      def transform_registry_values(registry)
        type = registry[:type]
        mapped_type = type_mapping[type]&.except(:schemas)
        return warn_unsupported_registry(type) unless mapped_type

        result = config_validation_result(registry, type_mapping[type][:schemas])
        return warn_incorrect_registry(type, result.reverse.find(&:failure?)) unless result.any?(&:success?)

        transformers.fetch(type, transformers["default"]).call(registry, mapped_type).compact
      end

      # Log warning for partially configured credentials
      #
      # @param [String] type
      # @param [Schemas::ValidationResult] result
      # @return [nil]
      def warn_incorrect_registry(type, result)
        log(
          :error,
          "Detected schema errors for registry type '#{type}': [#{result.error_message('; ')}]"
        )
        nil
      end

      # Log warning for unsupported registry type
      #
      # @param [String] type
      # @return [nil]
      def warn_unsupported_registry(type)
        log(:error, "Registry type '#{type}' is not supported!")
        nil
      end

      # Check if registry credentials block is valid
      #
      # @param [Hash] registry
      # @param [Array<Hash>] schemas
      # @return [Array<Schemas::ValidationResult>]
      def config_validation_result(registry, schemas)
        schemas.map { |schema| Schemas::ValidationResult.new(JSONSchemer.schema(schema).validate(registry)) }
      end

      # Strip protocol from registries of specific type
      #
      # Private npm and docker registries will not work if protocol is defined
      #
      # @param [String] type
      # @param [String] url
      # @return [String]
      def strip_protocol(type, url)
        return url unless %w[npm-registry docker-registry terraform-registry].include?(type)

        url.gsub(%r{https?://}, "")
      end

      # Type mapping from yml values in config file to supported schemas
      #
      # @return [Hash]
      def type_mapping
        @type_mapping ||= {
          "maven-repository" => {
            type: "maven_repository",
            url: "url",
            schemas: [registries_no_auth_schema, registries_password_schema]
          },
          "docker-registry" => {
            type: "docker_registry",
            url: "registry",
            schemas: [registries_no_auth_schema, registries_password_schema]
          },
          "npm-registry" => {
            type: "npm_registry",
            url: "registry",
            schemas: [registries_no_auth_schema, registries_token_schema, registries_password_schema]
          },
          "composer-repository" => {
            type: "composer_repository",
            url: "registry",
            schemas: [registries_no_auth_schema, registries_password_schema]
          },
          "git" => {
            type: "git_source",
            url: "host",
            schemas: [registries_no_auth_schema, registries_password_schema]
          },
          "nuget-feed" => {
            type: "nuget_feed",
            url: "url",
            schemas: [registries_no_auth_schema, registries_token_schema, registries_password_schema]
          },
          "python-index" => {
            type: "python_index",
            url: "index-url",
            schemas: [registries_no_auth_schema, registries_token_schema, registries_password_schema]
          },
          "rubygems-server" => {
            type: "rubygems_server",
            url: "host",
            schemas: [registries_no_auth_schema, registries_token_schema, registries_password_schema]
          },
          "terraform-registry" => {
            type: "terraform_registry",
            url: "host",
            schemas: [registries_no_auth_schema, registries_token_schema]
          },
          "hex-organization" => {
            type: "hex_organization",
            schemas: [registries_key_schema]
          },
          "hex-repository" => {
            type: "hex_repository",
            schemas: [registries_hex_schema]
          }
        }
      end

      # :reek:DuplicateMethodCall

      # Registry value transformers
      #
      # @return [Hash]
      def transformers
        @transformers ||= {
          "default" => lambda do |registry, mapped_type|
            {
              "type" => mapped_type[:type],
              mapped_type[:url] => strip_protocol(registry[:type], registry[:url]),
              **registry.except(:type, :url).transform_keys(&:to_s)
            }
          end,
          "hex-organizations" => lambda do |registry, mapped_type|
            {
              "type" => mapped_type[:type],
              "organization" => registry[:organization],
              "token" => registry[:key]
            }
          end,
          "hex-repository" => lambda do |registry, mapped_type|
            {
              "type" => mapped_type[:type],
              **registry.except(:type).transform_keys { |key| key.to_s.tr("-", "_") }
            }
          end,
          "python-index" => lambda do |registry, mapped_type|
            token = registry[:token] || "#{registry[:username]}:#{registry[:password]}"

            {
              "type" => mapped_type[:type],
              "token" => token,
              "replaces-base" => registry[:"replaces-base"] || false,
              mapped_type[:url] => registry[:url]
            }
          end
        }
      end
    end
  end
end
