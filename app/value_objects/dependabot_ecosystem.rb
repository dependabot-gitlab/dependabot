# frozen_string_literal: true

# :reek:TooManyConstants
class DependabotEcosystem
  BUNDLER = "bundler"
  COMPOSER = "composer"
  GO = "gomod"
  GRADLE = "gradle"
  MAVEN = "maven"
  NPM = "npm"
  BUN = "bun"
  PIP = "pip"
  NUGET = "nuget"
  GIT = "gitsubmodule"
  MIX = "mix"
  CARGO = "cargo"
  SWFT = "swift"
  DEVCONTAINERS = "devcontainers"
  DOCKER_COMPOSE = "docker-compose"

  PACKAGE_ECOSYSTEM_MAPPING = {
    NPM => Dependabot::NpmAndYarn::ECOSYSTEM,
    GO => Dependabot::GoModules::PACKAGE_MANAGER,
    MIX => Dependabot::Hex::PACKAGE_MANAGER,
    DOCKER_COMPOSE => Dependabot::DockerCompose::ECOSYSTEM,
    GIT => "submodules"
  }.freeze

  PACKAGE_MANAGER_MAPPING = {
    Dependabot::NpmAndYarn::ECOSYSTEM => NPM,
    Dependabot::GoModules::PACKAGE_MANAGER => GO,
    Dependabot::Hex::PACKAGE_MANAGER => MIX,
    Dependabot::DockerCompose::ECOSYSTEM => DOCKER_COMPOSE,
    "submodules" => GIT
  }.freeze
end
