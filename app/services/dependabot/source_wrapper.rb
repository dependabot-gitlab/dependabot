# frozen_string_literal: true

module Dependabot
  # Wrapper around Dependabot::Source
  #
  class SourceWrapper < ApplicationService
    attr_reader :repo, :branch, :directory, :directories

    # @param [String] repo
    # @param [String] branch
    # @param [String] directory
    # @param [Array<String>] directories
    def initialize(repo:, branch:, directory: nil, directories: nil)
      @repo = repo
      @branch = branch
      @directory = directory
      @directories = directories
    end

    # Get dependabot source
    #
    # @return [Dependabot::Source]
    def call
      Dependabot::Source.new(
        provider: "gitlab",
        hostname: AppConfig.gitlab_url.host,
        api_endpoint: "#{AppConfig.gitlab_url}/api/v4",
        repo: repo,
        branch: branch,
        directory: directory,
        directories: directories
      )
    end
  end
end
