# frozen_string_literal: true

module Dependabot
  # :reek:InstanceVariableAssumption
  # :reek:RepeatedConditional
  # :reek:LongParameterList
  # :reek:TooManyMethods

  # rubocop:disable Metrics/ClassLength, Metrics/ParameterLists
  module MergeRequest
    class CreateService < ApplicationService
      include ServiceModeConcern

      using Rainbow

      CREATED = "created"
      UPDATED = "updated"

      # @param [Dependabot::MultiDirFileFetcher] fetcher
      # @param [Project] project
      # @param [Hash] config_entry
      # @param [Dependabot::UpdatedDependency] updated_dependency
      # @param [Array] credentials
      # @param [Boolean] recreate
      def initialize(fetcher:, project:, config_entry:, updated_dependency:, credentials:, recreate: false)
        @fetcher = fetcher
        @project = project
        @config_entry = config_entry
        @updated_dependency = updated_dependency
        @credentials = credentials
        @recreate = recreate
      end

      # Create or update MR
      #
      # @return [<Gitlab::ObjectifiedHash, nil>]
      def call
        if find_mr("closed")
          log(:warn, "  closed mr exists, skipping!")
          return
        end

        if DependabotConfig.dry_run?
          log(:info, "  dependabot is running in dry-run mode, skipping merge request creation!")
          return
        end

        # update existing mr if nothing was created
        @result = create_mr || update_mr

        approve_mr
        accept_mr
        mr
      end

      private

      attr_reader :project,
                  :fetcher,
                  :updated_dependency,
                  :config_entry,
                  :credentials,
                  :recreate,
                  :result

      # Create mr and return true if created
      #
      # @return [Boolean]
      def create_mr
        # dependabot-core returns nil if branch && mr exists and nothing was created
        @mr = mr_creator.call || return
        gitlab.unsubscribe_from_merge_request(mr.project_id, mr.iid) if config_entry[:unsubscribe_from_mr]

        CREATED
      rescue Gitlab::Error::ResponseError => e
        # dependabot-core will try to create mr in the edge case when mr exists without the branch
        return if e.is_a?(Gitlab::Error::Conflict)
        # rescue in case mr is created but failed to add approvers/reviewers or failed to unsubscribe
        raise(e) unless mr

        capture_error(e)
        CREATED
      end

      # Update existing merge request
      #
      # @return [void]
      def update_mr
        return log(:info, "  #{skip_mr_update_message}") unless update_mr?
        return rebase_mr unless recreate || mr.to_h["has_conflicts"]

        Dependabot::PullRequestUpdater.new(
          credentials: credentials,
          source: fetcher.source,
          base_commit: fetcher.commit,
          old_commit: mr_creator.commit_message,
          pull_request_number: mr.iid,
          files: updated_dependency.updated_files,
          provider_metadata: { target_project_id: target_project_id }
        ).update
        log(:info, "  recreated merge request #{mr.web_url.bright}")

        UPDATED
      rescue Gitlab::Error::ResponseError => e
        capture_error(e)
      end

      # Rebase merge request
      #
      # @return [void]
      def rebase_mr
        gitlab.rebase_merge_request(target_project_id || project.name, mr.iid)
        log(:info, "  rebased merge request #{mr.web_url.bright}")
      end

      # Approve merge request
      #
      # @return [void]
      def approve_mr
        return if skip_approve?

        gitlab.approve_merge_request(mr.project_id, mr.iid)
        log(:info, "  approved merge request")
      rescue Gitlab::Error => e
        log_error(e, message_prefix: "  failed to approve merge request")
      end

      # Accept merge request and set to merge automatically
      #
      # @return [void]
      def accept_mr
        return unless mr && updated_dependency.auto_mergeable?

        delay_auto_merge
        config_entry.dig(:auto_merge, :merge_train) ? add_to_merge_train : merge
        log(:info, "  accepted merge request")
      rescue Gitlab::Error::MethodNotAllowed,
             Gitlab::Error::Unprocessable,
             Gitlab::Error::Conflict,
             Gitlab::Error::NotAcceptable => e
        log(:error, "  failed to accept merge request: #{e.message}")
      end

      # Merge mr immediately
      #
      # @return [void]
      def merge
        gitlab.accept_merge_request(
          mr.project_id,
          mr.iid,
          merge_when_pipeline_succeeds: true,
          squash: config_entry.dig(:auto_merge, :squash)
        )
      end

      # Add merge request to merge train
      #
      # @return [void]
      def add_to_merge_train
        gitlab.add_merge_request_to_merge_train(
          mr.project_id,
          mr.iid,
          when_pipeline_succeeds: true,
          squash: config_entry.dig(:auto_merge, :squash)
        )
      end

      # Find existing mr
      #
      # @return [Gitlab::ObjectifiedHash]
      def find_mr(state)
        Gitlab::MergeRequestFinder.find(
          project: target_project_id || fetcher.source.repo,
          source_branch: mr_creator.branch_name,
          target_branch: fetcher.source.branch,
          state: state
        )
      end

      # Delay auto merging
      #
      # @return [void]
      def delay_auto_merge
        delay = config_entry.dig(:auto_merge, :delay)
        return unless delay

        log(:info, "  delaying auto merge attempt for #{delay} seconds")
        sleep(delay)
      end

      # Target project id
      #
      # @return [Integer]
      def target_project_id
        return @target_project_id if defined?(@target_project_id)

        @target_project_id = config_entry[:fork] ? project.forked_from_id : nil
      end

      # Get existing mr
      #
      # @return [Gitlab::ObjectifiedHash]
      def mr
        @mr ||= find_mr("opened")
      end

      # MR creator service
      #
      # @return [Gitlab::MergeRequest::Creator]
      def mr_creator
        @mr_creator ||= Gitlab::MergeRequest::Creator.new(
          project: project,
          fetcher: fetcher,
          updated_dependency: updated_dependency,
          config_entry: config_entry,
          credentials: credentials,
          target_project_id: target_project_id
        )
      end

      # Configured rebase strategy
      #
      # @return [String]
      def rebase_strategy
        @rebase_strategy ||= config_entry.dig(:rebase_strategy, :strategy)
      end

      # Username for token used
      #
      # @return [String]
      def token_username
        @token_username ||= gitlab.user.username
      end

      # Explicit message for skipping mr update
      #
      # @return [String]
      def skip_mr_update_message
        base = "skipping merge request #{mr.web_url.bright} update"
        reason = if rebase_none?
                   "rebase strategy is set to #{'none'.bold}"
                 elsif !(rebase_auto? && mr["has_conflicts"])
                   "mr has no conflicts and rebase strategy is set to #{'auto'.bold}"
                 else
                   "mr is up to date"
                 end

        "#{base}, #{reason}"
      end

      # Automatically rebase MRs with conflicts only
      #
      # @return [Boolean]
      def rebase_auto?
        rebase_strategy == Options::Rebase::AUTO
      end

      # Automatically rebase all mr's
      #
      # @return [Boolean]
      def rebase_all?
        rebase_strategy == Options::Rebase::ALL
      end

      # Do not perform rebase/recreate
      #
      # @return [Boolean]
      def rebase_none?
        rebase_strategy == Options::Rebase::NONE
      end

      # Check if mr should be updated
      #
      # @return [Boolean]
      def update_mr?
        recreate || rebase_all? || (mr["has_conflicts"] && !rebase_none?)
      end

      # Skip auto approve
      #
      # @return [Boolean]
      def skip_approve?
        return true unless config_entry[:auto_approve] && mr
        return false if result == CREATED # approve without checking if mr was created
        return true if approved?

        false
      end

      # Check if mr is already approved
      #
      # @return [Boolean]
      def approved?
        gitlab.merge_request_approval_state(project.name, mr.iid).rules.any? do |rule|
          rule.approved_by.any? { |user| user.username == token_username }
        end
      end

      # rubocop:enable Metrics/ClassLength, Metrics/ParameterLists
    end
  end
end
