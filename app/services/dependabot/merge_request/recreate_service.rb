# frozen_string_literal: true

module Dependabot
  module MergeRequest
    class RecreateService < Update::Base
      using Rainbow
      prepend CertWrapper

      include ServiceHelpersConcern

      def initialize(project_name:, mr_iid:, discussion_id: nil)
        super(project_name, ignore_rules: true) # ignore rules for recreate to guarantee MR recreation

        @mr_iid = mr_iid
        @discussion_id = discussion_id
      end

      # Trigger merge request update
      #
      # @return [Array]
      def call
        init_gitlab(project)

        recreate_mr
        log(:info, "  recreated merge request !#{mr_iid.to_s.bright}")
        return unless discussion_id

        reply_status(":white_check_mark: `dependabot-gitlab` successfully recreated merge request!")
        resolve_discussion
      rescue StandardError => e
        log_error(e)
        # when discussion id is present, do not raise error so it's possible to distinguish container run failures
        # from update failures
        raise(e) unless discussion_id

        reply_status(":x: `dependabot-gitlab` failed to recreate merge request. :x:\n\n```\n#{e}\n```")
      ensure
        FileUtils.rm_r(repo_contents_path, force: true, secure: true) if repo_contents_path
      end

      private

      delegate :package_ecosystem, :directory, to: :mr
      delegate :configuration, to: :project, prefix: :project

      attr_reader :project_name, :mr_iid, :discussion_id

      # Find merge request
      #
      # @return [MergeRequest]
      def mr
        @mr ||= project.merge_requests.find_by(iid: mr_iid)
      end

      # Persisted project
      #
      # @return [Project]
      def project
        @project ||= Project.find_by(name: project_name)
      end

      # Updated dependency
      #
      # @return [Dependabot::UpdatedDependency]
      def updated_dependency
        @updated_dependency ||= begin
          directory = directories.find { |dir| dir[:files].any? { |file| file.directory == mr.dependency_file_dir } }
          raise "Dependency files not found! in #{mr.dependency_file_dir}" unless directory

          update_dependency(directory[:dependencies].first, directory[:files])
        end
      end

      # Main dependency name
      #
      # @return [String]
      def dependency_name
        mr.main_dependency
      end

      # Check if newer version exist
      #
      # @return [Boolean]
      def same_version?
        mr.update_to == updated_dependency.current_versions
      end

      # Recreate merge request
      #
      # @return [void]
      def recreate_mr
        # rubocop:disable Layout/LineLength
        raise("Dependency '#{dependency_name}' not found in manifest file!") unless updated_dependency
        raise("Dependency update failed due to error. Error: #{updated_dependency.error.message}") if updated_dependency.errored?
        raise("Dependency already up to date!") if updated_dependency.up_to_date?
        raise("Dependency update is impossible!") if updated_dependency.update_impossible?
        raise("Newer dependency version exists, trigger dependency updates to create new mr!") unless same_version?
        # rubocop:enable Layout/LineLength

        Dependabot::PullRequestUpdater.new(
          credentials: credentials,
          source: fetcher.source,
          base_commit: fetcher.commit,
          old_commit: mr.commit_message,
          pull_request_number: mr.iid,
          files: updated_dependency.updated_files,
          provider_metadata: { target_project_id: mr.target_project_id }
        ).update
      end

      # Add action status reply
      #
      # @param [String] message
      # @return [void]
      def reply_status(message)
        Gitlab::MergeRequest::DiscussionReplier.call(
          project_name: project_name,
          mr_iid: mr_iid,
          discussion_id: discussion_id,
          note: message
        )
      end

      # Resolve mr discussion
      #
      # @return [void]
      def resolve_discussion
        log(:debug, "Resolving discussion '#{discussion_id}'")
        gitlab.resolve_merge_request_discussion(project_name, mr_iid, discussion_id, resolved: true)
      rescue StandardError => e
        log_error(e)
      end
    end
  end
end
