# frozen_string_literal: true

module Dependabot
  class MissingConfigurationError < StandardError; end

  module Config
    class Fetcher < ApplicationService
      using Rainbow

      # @param [String] project_name
      # @param [String] branch
      def initialize(project_name, branch: DependabotConfig.config_branch)
        @project_name = project_name
        @branch = branch
      end

      # Dependabot config
      #
      # Cache raw config in standalone mode in case of multi directory updates
      #
      # @return [Configuration]
      def call
        opts = if AppConfig.standalone?
                 RequestStore.fetch("#{project_name}/#{branch}/opts") do
                   Options::All.new(raw_config, project_name).transform
                 end
               else
                 Options::All.new(raw_config, project_name).transform
               end

        ::Configuration.new(**opts)
      end

      private

      attr_reader :project_name

      delegate :config_filename, :config_local_filename, to: DependabotConfig

      # Branch to fetch configuration from
      #
      # @return [String]
      def branch
        @branch ||= gitlab.project(project_name).default_branch
      end

      # Local dependabot.yml configuration
      #
      # @return [String]
      def local_config
        unless ::File.exist?(config_local_filename)
          raise(MissingConfigurationError, "Local configuration file #{config_local_filename} not found!")
        end

        log(:info, "Found local configuration file #{config_local_filename.bright}")
        ::File.read(config_local_filename)
      end

      # Get dependabot.yml file contents
      #
      # @return [String]
      def raw_config
        log(:info, <<~INFO.strip)
          Fetching configuration file #{config_filename.bright} from branch #{branch.bright} for project #{project_name.bright}
        INFO
        gitlab.file_contents(project_name, config_filename, branch)
      rescue Gitlab::Error::NotFound
        if config_local_filename
          log(:info, "Configuration file #{config_filename.bright} not found, attempting to use local configuration!")
          return local_config
        end

        raise(MissingConfigurationError, "#{config_filename} not present in #{project_name}'s branch #{branch}")
      end
    end
  end
end
