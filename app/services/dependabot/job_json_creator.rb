# frozen_string_literal: true

require "tmpdir"

module Dependabot
  class JobJsonCreator < ApplicationService
    def initialize(project_name:, default_branch:, config_entry:)
      @project_name = project_name
      @branch = config_entry[:branch] || default_branch
      @package_manager = config_entry[:package_manager]
      @directory = config_entry[:directory]
      @allowed_updates = config_entry[:allow]
    end

    def call
      dir = File.join(Dir.tmpdir, project_name).tap { |path| FileUtils.mkdir_p(path) }

      File.join(dir, "job.json").tap do |path|
        File.write(path, job.to_json)
        log(:debug, "Created dependabot job json in '#{path}'")
      end
    end

    private

    attr_reader :package_manager, :project_name, :directory, :branch, :allowed_updates

    # Contents of job.json
    #
    # @return [Hash]
    def job
      {
        job: {
          "allowed-updates": allowed_updates,
          "package-manager": package_manager,
          experiments: ::Experiments::FEATURES,
          source: {
            provider: "gitlab",
            repo: project_name,
            directory: directory,
            branch: branch
          }
        }
      }
    end
  end
end
