# frozen_string_literal: true

module Dependabot
  module Files
    class Fetcher < ApplicationService
      include ConfigHelper

      # Get FileFetcher instance
      #
      # @param [String] project_name
      # @param [Hash] config_entry
      # @param [Array<Hash>] credentials
      # @param [String] repo_contents_path
      def initialize(project_name:, config_entry:, credentials:, repo_contents_path:)
        @project_name = project_name
        @config_entry = config_entry
        @credentials = credentials
        @repo_contents_path = repo_contents_path
      end

      attr_reader :project_name,
                  :config_entry,
                  :credentials,
                  :repo_contents_path

      # Get FileFetcher
      #
      # @return [Dependabot::MultiDirFileFetcher]
      def call
        return file_fetcher unless glob?(directory)

        multi_file_fetcher
      end

      private

      # File fetcher for single directory
      #
      # @return [Dependabot::MultiDirFileFetcher]
      def file_fetcher
        ff = create_file_fetcher(directory: directory)
        ff.clone_repo_contents
        Dependabot::MultiDirFileFetcher.new(source: ff.source, commit: ff.commit, files: [ff.files])
      end

      # File fetcher for multiple directories
      #
      # @return [Dependabot::MultiDirFileFetcher]
      def multi_file_fetcher
        log(:info, "Glob pattern detected, fetching files for all directories matching glob pattern")
        create_file_fetcher.clone_repo_contents # Clone repo contents to get all directories
        directories = Dir.chdir(repo_contents_path) do
          Dir.glob(directory.delete_prefix("/"), File::FNM_DOTMATCH)
             .select { |file| File.directory?(file) }
             .map { |dir| "/#{dir}" }
        end
        log(:info, "  following directories matched pattern '#{directory}' and will be processed: #{directories}")
        ff = create_file_fetcher(directories: directories)

        Dependabot::MultiDirFileFetcher.new(
          source: ff.source,
          commit: ff.commit,
          files: dependency_files_for_multi_directories(directories)
        )
      end

      # Directory
      #
      # @return [String]
      def directory
        config_entry[:directory]
      end

      # Fetch dependency files in multiple directories
      #
      # @param [Array<String>] directories
      # @return [Array<Array>]
      def dependency_files_for_multi_directories(directories)
        dep_files = directories.map do |dir|
          ff = create_file_fetcher(directory: dir)

          begin
            files = ff.files
          rescue Dependabot::DependencyFileNotFound
            log(:debug, "  no files found in #{dir}, skipping")
          end

          files
        end.compact
        raise Dependabot::DependencyFileNotFound, directory if dep_files.flatten.empty?

        dep_files
      end

      # Create file fetcher for specific directory
      #
      # @param [String] directory
      # @param [Array<String>] directories
      # @return [Dependabot::FileFetchers::Base]
      def create_file_fetcher(directory: nil, directories: nil)
        Dependabot::FileFetchers.for_package_manager(config_entry[:package_manager]).new(
          credentials: credentials,
          repo_contents_path: repo_contents_path,
          source: Dependabot::SourceWrapper.call(
            repo: project_name,
            branch: config_entry[:branch],
            directory: directory,
            directories: directories
          )
        )
      end
    end
  end
end
