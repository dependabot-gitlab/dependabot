# frozen_string_literal: true

module Dependabot
  module Files
    class Parser < ApplicationService
      # @param [Dependabot::MultiDirFileFetcher] multi_dir_file_fetcher
      # @param [String] repo_contents_path
      # @param [Hash] config_entry
      # @param [Array<Hash>] credentials
      def initialize(multi_dir_file_fetcher:, repo_contents_path:, config_entry:, credentials:, dependency_name: nil)
        @multi_dir_file_fetcher = multi_dir_file_fetcher
        @repo_contents_path = repo_contents_path
        @config_entry = config_entry
        @credentials = credentials
        @dependency_name = dependency_name
      end

      # Get parsed dependencies from files
      #
      # @return [Array<Hash>]
      def call
        # Core file parsers always process single instance of specific dependency file
        # If multiple directories are processed, each directory has to be processed separately
        multi_dir_file_fetcher.files.map do |files|
          deps = Dependabot::FileParsers.for_package_manager(config_entry[:package_manager]).new(
            dependency_files: files,
            source: multi_dir_file_fetcher.source,
            credentials: credentials,
            repo_contents_path: repo_contents_path,
            reject_external_code: config_entry[:reject_external_code],
            options: config_entry[:updater_options]
          ).parse

          {
            name: files.first.directory,
            files: files,
            dependencies: dependency_name ? deps.select { |dep| dep.name == dependency_name } : deps
          }
        end
      end

      private

      attr_reader :multi_dir_file_fetcher,
                  :repo_contents_path,
                  :config_entry,
                  :credentials,
                  :dependency_name
    end
  end
end
