# frozen_string_literal: true

module Dependabot
  module Projects
    module Registration
      ProjectData = Struct.new("ProjectData", :project, :existing, :config) do
        delegate :archived, to: :project
        delegate :name, to: :existing, prefix: :existing

        # Full project name in gitlab
        #
        # @return [String]
        def gitlab_name
          project.path_with_namespace
        end

        # Archived project not previously added for updates
        #
        # @return [Boolean]
        def archived_non_existing?
          !existing && archived
        end

        # Project that can be removed from updates
        #
        # @return [Boolean]
        def removable?
          archived || (existing && !config)
        end

        # Renamed project
        #
        # @return [Boolean]
        def renamed?
          existing && existing_name != gitlab_name
        end

        # Valid not registered project with config file
        #
        # @return [Boolean]
        def not_registered?
          !existing && config
        end

        # Configuration is synced
        #
        # @return [Boolean]
        def config_synced?
          existing.configuration == config
        end
      end

      # Sync all applicable projects
      #
      class Service < ApplicationService # rubocop:disable Metrics/ClassLength
        using Rainbow

        # :reek:TooManyStatements

        # Process all available projects
        #
        # @return [void]
        def call
          gitlab.projects(min_access_level: 30, per_page: 50).auto_paginate do |project|
            run_within_context({ job: "project-registration", project: project.name }) do
              log(:info, "Processing project #{project.path_with_namespace.bold}")
              next unless sync?(project)

              project_data = ProjectData.new(project, saved_project(id: project.id), configuration(project))
              next if skip(project_data)
              next if remove(project_data)
              next if rename(project_data)
              next if add_or_update(project_data)

              log(:info, "Project is up to date, skipping")
            end
          rescue StandardError => e
            handle_registration_error(e)
          end
        end

        private

        # Check if project should be synced
        #
        # @param [Gitlab::ObjectifiedHash] project
        # @return [Boolean]
        def sync?(project)
          unless project["default_branch"]
            log(:warn, "Project doesn't have a default branch, skipping")
            return false
          end

          allowed?(project.path_with_namespace).tap do |allowed|
            log(:info, "Project is not applicable due to allow/ignore rules, skipping") unless allowed
          end
        end

        # Check if project is allowed for automatic registration
        #
        # @param [String] path
        # @return [Boolean]
        def allowed?(path)
          return true if allow_pattern.nil? && ignore_pattern.nil?
          return false if ignore_pattern && path.match?(ignore_pattern)

          !allow_pattern || path.match?(allow_pattern)
        end

        # :reek:DuplicateMethodCall

        # Register or sync project
        #
        # @param [ProjectData] project
        # @return [Boolean]
        def add_or_update(project)
          return unless project.config

          not_synced = !jobs_synced?(project.gitlab_name, project.config)
          return unless project.not_registered? || not_synced || !project.config_synced?

          message = project.not_registered? ? "Not added for updates, registering" : "Project out of sync, updating"

          log(:info, message)
          new_project = Creator.call(project.gitlab_name, config: project.config)

          log(:info, "Adding dependency update jobs")
          Cron::JobSync.call(new_project)
          true
        end

        # Remove project
        #
        # @param [ProjectData] project
        # @return [Boolean]
        def remove(project)
          return unless project.removable?

          message = project.archived ? "Archived" : "#{config_file} has been removed"

          log(:info, "#{message}, removing from dependency updates")
          Remover.call(project.existing)
          true
        end

        # Rename project
        #
        # @param [ProjectData] project
        # @return [Boolean]
        def rename(project)
          return unless project.renamed?

          old_project_name = project.existing_name

          log(:info, "Renaming project '#{old_project_name}' to '#{project.gitlab_name}'")
          project.existing.update_attributes!(name: project.gitlab_name)
          Cron::JobRemover.call(old_project_name)
          Cron::JobSync.call(project.existing)
          true
        end

        # Skip any processing
        #
        # @param [ProjectData] project
        # @return [Boolean]
        def skip(project)
          return unless project.archived_non_existing?

          log(:info, "Project is archived, skipping")
          true
        end

        # Allowed project namespace pattern
        #
        # @return [String, nil]
        def allow_pattern
          @allow_pattern ||= AppConfig.project_registration_allow_pattern
        end

        def ignore_pattern
          @ignore_pattern ||= AppConfig.project_registration_ignore_pattern
        end

        # Config filename
        #
        # @return [String]
        def config_file
          @config_file ||= DependabotConfig.config_filename
        end

        # Get project config
        #
        # @param [Gitlab::ObjectifiedHash] project
        # @return [Configuration, nil]
        def configuration(project)
          Config::Fetcher.call(
            project.path_with_namespace,
            branch: DependabotConfig.config_branch || project.default_branch
          )
        rescue MissingConfigurationError
          nil
        end

        # Saved project
        #
        # @param [Hash] args
        # @return [Boolean]
        def saved_project(**args)
          Project.find_by(args)
        rescue Mongoid::Errors::DocumentNotFound
          nil
        end

        # Check jobs synced
        #
        # @param [String] name
        # @param [Configuration] config
        # @return [Boolean]
        def jobs_synced?(project_name, config)
          cron_jobs = all_project_jobs(project_name).map do |job|
            { name: job.name, cron: job.cron, class: job.klass }
          end

          configured_jobs = config.updates.map do |conf|
            {
              name: "#{project_name}:#{conf[:package_ecosystem]}:#{conf[:directory]}",
              cron: conf[:cron],
              class: UpdateRunnerJob.to_s
            }
          end

          cron_jobs.sort_by { |job| job[:name] } == configured_jobs.sort_by { |job| job[:name] }
        end

        # All project cron jobs
        #
        # @param [String] project_name
        # @return [Array<Sidekiq::Cron::Job>]
        def all_project_jobs(project_name)
          Sidekiq::Cron::Job.all.select { |job| job.name.match?(/^#{project_name}:.*/) }
        end

        # Handle registration error
        #
        # @param [StandardError] error
        # @return [void]
        def handle_registration_error(error)
          if error.is_a?(Schemas::ValidationError)
            log(:warn, "Project has invalid configuration: #{error}")
            Sentry.capture_exception(error)
            return
          end

          log_error(error)
        end
      end
    end
  end
end
