# frozen_string_literal: true

module Dependabot
  module Update
    # :reek:TooManyStatements
    # :reek:LongParameterList

    # Checker class for new version availability
    #
    class Checker < ApplicationService # rubocop:disable Metrics/ClassLength
      class UpdateImpossible < StandardError; end

      using Rainbow

      include ServiceModeConcern

      # rubocop:disable Metrics/ParameterLists

      # @param [Dependabot::Dependency] dependency
      # @param [Array<Dependabot::DependencyFile>] dependency_files
      # @param [Hash] config_entry
      # @param [String] repo_contents_path
      # @param [Array<Hash>] credentials
      # @param [Boolean] ignore_rules
      def initialize(
        dependency:,
        dependency_files:,
        config_entry:,
        repo_contents_path:,
        credentials:,
        ignore_rules: false
      )
        @dependency = dependency
        @dependency_files = dependency_files
        @config_entry = config_entry
        @versioning_strategy = config_entry[:versioning_strategy]
        @package_manager = config_entry[:package_manager]
        @repo_contents_path = repo_contents_path
        @credentials = credentials
        @ignore_rules = ignore_rules
      end

      # rubocop:enable Metrics/ParameterLists

      delegate :name, to: :dependency, prefix: :dependency

      # Get updated dependencies
      #
      # @return [Dependabot::UpdatedDependency, nil]
      def call
        # Allow rules are filtering out all indirect dependencies by default, so skipped is logged to debug to prevent
        # excessive log output
        return skipped unless ignore_rules || rule_handler.allowed?

        log(:info, "Processing dependency #{dependency_name.bright}")
        raise Dependabot::AllVersionsIgnored if completely_ignored?
        return up_to_date if checker.up_to_date?
        return update_impossible if requirements_to_unlock == :update_not_possible

        updated_dependency
      rescue Dependabot::AllVersionsIgnored
        log(
          :info,
          "  skipping #{name} update due to ignored versions rules: #{ignored_versions}"
        )
        dependency_container(state: UpdatedDependency::SKIPPED)
      rescue StandardError => e
        capture_error(e)
        dependency_container(state: UpdatedDependency::ERROR, error: e)
      end

      private

      attr_reader :dependency,
                  :dependency_files,
                  :config_entry,
                  :package_manager,
                  :repo_contents_path,
                  :credentials,
                  :ignore_rules

      # Dependency name with version
      #
      # @return [String]
      def name
        @name ||= "#{dependency_name}: #{dependency.version}".bright
      end

      # Rule handler
      #
      # @return [RuleHandler]
      def rule_handler
        @rule_handler ||= RuleHandler.new(
          dependency: dependency,
          checker: checker,
          config_entry: config_entry
        )
      end

      # Get update checker
      #
      # @return [Dependabot::UpdateChecker]
      def checker
        @checker ||= begin
          args = {
            dependency: dependency,
            dependency_files: dependency_files,
            credentials: credentials,
            ignored_versions: ignored_versions,
            security_advisories: vulnerabilities.map(&:advisory),
            raise_on_ignored: true,
            options: config_entry[:updater_options],
            requirements_update_strategy: versioning_strategy,
            repo_contents_path: repo_contents_path
          }.compact

          Dependabot::UpdateCheckers.for_package_manager(package_manager).new(**args)
        end
      end

      # Get requirements to unlock
      #
      # @return [Symbol]
      def requirements_to_unlock
        @requirements_to_unlock ||= begin
          unless checker.requirements_unlocked_or_can_be?
            return checker.can_update?(requirements_to_unlock: :none) ? :none : :update_not_possible
          end
          return :own if checker.can_update?(requirements_to_unlock: :own)
          return :all if checker.can_update?(requirements_to_unlock: :all)

          :update_not_possible
        end
      end

      # List of advisories for dependency
      #
      # @return [Array<Vulnerability>]
      def vulnerabilities
        @vulnerabilities ||= if config_entry.dig(:vulnerability_alerts, :enabled)
                               Github::Vulnerabilities::LocalStore
                                 .call(config_entry[:package_ecosystem])
                                 .select { |vulnerability| vulnerability.package == dependency.name }
                             else
                               []
                             end
      end

      # Versioning strategy enum
      #
      # @return [Dependabot::RequirementsUpdateStrategy, nil]
      def versioning_strategy
        return unless @versioning_strategy

        RequirementsUpdateStrategy.deserialize(@versioning_strategy.to_s)
      end

      # Ignored versions for dependency
      #
      # @return [Array]
      def ignored_versions
        @ignored_versions ||= ignore_rules ? [] : RuleHandler.version_conditions(dependency, config_entry[:ignore])
      end

      # Print skipped message
      #
      # @return [nil]
      def skipped
        log(:debug, "Skipping #{name} due to allow rules: #{config_entry[:allow]}")
        dependency_container(state: UpdatedDependency::SKIPPED)
      end

      # Print up to date message
      #
      # @return [nil]
      def up_to_date
        log(:info, "  #{name} is up to date")
        dependency_container(state: UpdatedDependency::UP_TO_DATE)
      end

      # Print update impossible message
      #
      # @return [nil]
      def update_impossible
        UpdateFailures.add(UpdateImpossible.new("Update for #{name} is impossible"))

        log(:warn, "  update for #{name} is impossible")
        unless checker.conflicting_dependencies.empty?
          log(:warn, "  following conflicting dependencies are present: #{checker.conflicting_dependencies}")
        end
        dependency_container(state: UpdatedDependency::UPDATE_IMPOSSIBLE)
      end

      # Get filtered updated dependencies
      #
      # @return [Dependabot::UpdatedDependency]
      def updated_dependency
        log(:debug, "Using following ignore version conditions: #{ignored_versions}")
        log(:info, "  updating #{name} => #{checker.latest_version.to_s.bright}")
        updated_dependencies = checker.updated_dependencies(requirements_to_unlock: requirements_to_unlock)
        files = updated_files(updated_dependencies)
        raise "dependabot-core returned empty file list for #{dependency_name} update" if files.empty?

        dependency_container(
          state: UpdatedDependency::HAS_UPDATES,
          updated_dependencies: updated_dependencies,
          updated_files: files
        )
      end

      # Array of updated files
      #
      # @return [Array<Dependabot::DependencyFile>]
      def updated_files(updated_dependencies)
        Dependabot::Files::Updater.call(
          dependencies: updated_dependencies,
          dependency_files: dependency_files,
          repo_contents_path: repo_contents_path,
          credentials: credentials,
          config_entry: config_entry
        )
      end

      # Versioning strategy set to lock file only
      #
      # @return [Boolean]
      def lockfile_only?
        versioning_strategy == :lockfile_only
      end

      # Check if dependency is completely ignored
      #
      # @return [Boolean]
      def completely_ignored?
        ignored_versions.any?(Dependabot::Config::IgnoreCondition::ALL_VERSIONS)
      end

      # UpdatedDependency container
      #
      # @param [Hash] **args
      # @return [Dependabot::UpdatedDependency]
      def dependency_container(**args)
        UpdatedDependency.new(
          dependency: dependency,
          dependency_files: dependency_files,
          vulnerable: checker.vulnerable?,
          auto_merge_rules: config_entry[:auto_merge],
          state: args[:state],
          updated_dependencies: args[:updated_dependencies],
          updated_files: args[:updated_files],
          vulnerabilities: vulnerabilities,
          error: args[:error]
        )
      end
    end
  end
end
