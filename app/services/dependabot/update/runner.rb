# frozen_string_literal: true

module Dependabot
  module Update
    class TooManyRequestsError < StandardError
      def message
        "GitHub API rate limit exceeded! See: https://docs.github.com/en/rest/overview/resources-in-the-rest-api#rate-limiting"
      end
    end

    class ExternalCodeExecutionError < StandardError
      def initialize(package_ecosystem, directory)
        super(<<~ERR)
          Unexpected external code execution detected.
          Option 'insecure-external-code-execution' must be set to 'allow' for entry:
            package_ecosystem - '#{package_ecosystem}'
            directory - '#{directory}'
        ERR
      end
    end

    # :reek:LongParameterList

    # rubocop:disable Metrics/ClassLength, Metrics/ParameterLists

    # Main entrypoint class for updating dependencies and creating merge requests
    #
    class Runner < Base
      include ServiceModeConcern
      include ConfigHelper
      prepend CertWrapper

      UPDATE_PERFORMED = "updated-or-created"
      LIMIT_REACHED = "limit-reached"
      SYSTEM_FAILURE = "system-failure"

      using Rainbow

      def initialize(
        project_name:,
        package_ecosystem:,
        directory:,
        dependency_name: nil,
        ignore_rules: false,
        run_id: nil
      )
        super(project_name, ignore_rules: ignore_rules, dependency_name: dependency_name, run_id: run_id)

        @package_ecosystem = package_ecosystem
        @directory = directory
      end

      # Create or update mr's for dependencies
      #
      # @return [void]
      def call
        fetch_gitlab_project
        fetch_vulnerabilities

        update
      rescue Octokit::TooManyRequests
        raise TooManyRequestsError
      rescue Dependabot::UnexpectedExternalCode
        raise ExternalCodeExecutionError.new(package_ecosystem, directory)
      ensure
        FileUtils.rm_r(repo_contents_path, force: true, secure: true)
      end

      private

      attr_reader :package_ecosystem, :directory

      # Project
      #
      # @return [Project]
      def project
        @project ||= Project.new(name: project_name)
      end

      # Project configuration
      #
      # @return [Configuration]
      def project_configuration
        @project_configuration ||= fetch_config.then do |config|
          fork_id = config.forked ? gitlab_project.to_h.dig("forked_from_project", "id") : nil

          project.forked_from_id = fork_id
          project.configuration = config
        end
      end

      # Open mr limits
      #
      # @return [Number]
      def limits
        @limits ||= {
          mr: config_entry[:open_merge_requests_limit],
          security_mr: config_entry[:open_security_merge_requests_limit]
        }
      end

      # Project configuration fetched from gitlab
      #
      # @return [Configuration]
      def fetch_config
        Config::Fetcher.call(project_name)
      end

      # Fetch package ecosystem vulnerability info
      #
      # @return [void]
      def fetch_vulnerabilities
        return unless vulnerability_alerts?(config_entry)

        Github::Vulnerabilities::LocalStore.call(package_ecosystem)
      end

      # Update project dependencies
      #
      # @return [void]
      def update
        directories.each_with_object({ mr: Set.new, security_mr: Set.new }) do |update_directory, created_mrs|
          execution_context.store(:directory, update_directory[:name]) if glob?(directory)

          update_directory[:dependencies].each do |dependency|
            process_dependency(dependency, update_directory[:files], created_mrs)
          end
        end
      end

      # Process dependency
      #
      # @param [Dependabot::Dependency] dependency
      # @param [Array<Dependabot::DependencyFile>] files
      # @param [Hash] created_mrs
      # @return [void]
      def process_dependency(dependency, files, created_mrs)
        execution_context.store(:dependency, dependency.name)
        updated_dependency = update_dependency(dependency, files)

        result = create_dependency_update_mr(updated_dependency, created_mrs)
        # go to next dep if
        # * new mr was created
        # * existing mr was updated
        return if result == UPDATE_PERFORMED

        Operations::VulnerabilityIssueCreation.call(project, updated_dependency, config_entry)
        Operations::ObsoleteVulnerabilityIssueCleanup.call(project, updated_dependency, config_entry)
        # skip obsolete mr cleanup if
        # * limit was reached, because it's possible that open mr exists if overall limit got reduced
        # * unknown error occurred, which could lead to closing open mr
        return if result

        Operations::ObsoleteMrCleanup.call(project, updated_dependency, updated_dependency.directory)
      end

      # Create or update dependency update merge request
      # Return iid of mr if created or updated
      #
      # @param [Dependabot::UpdatedDependency] updated_dependency
      # @param [Hash] mrs
      # @return [<String, nil>]
      def create_dependency_update_mr(updated_dependency, mrs)
        type = updated_dependency.vulnerable? ? :security_mr : :mr
        return unless updated_dependency.updates?
        return LIMIT_REACHED unless create_mr?(mrs, type)

        iid = MergeRequest::CreateService.call(
          project: project,
          fetcher: fetcher,
          config_entry: config_entry,
          updated_dependency: updated_dependency,
          credentials: credentials
        )&.iid

        if iid
          mrs[type] << iid
          UPDATE_PERFORMED
        end
      rescue StandardError => e
        capture_error(e)
        SYSTEM_FAILURE
      end

      # Check if mr should be created based on limits settings
      #
      # @param [Hash] mrs
      # @param [Symbol] type
      # @return [Boolean]
      def create_mr?(mrs, type)
        limit = limits[type]

        return true if limit.negative?
        return true if !limit.zero? && (mrs[type].length < limit)

        dep_type = type == :mr ? "dependency" : "vulnerable dependency"
        reason = if limits[type].zero?
                   "due to disabled mr creation setting!"
                 else
                   "due to max open mr limit reached, limit: '#{limit}'!"
                 end

        log(:info, "  skipping update of #{dep_type} #{reason}")
        false
      end
    end
  end
end
# rubocop:enable Metrics/ClassLength, Metrics/ParameterLists
