# frozen_string_literal: true

module Dependabot
  module Update
    # :reek:InstanceVariableAssumption

    # Base class for dependency updater implementations
    #
    class Base < ApplicationService
      using Rainbow

      def initialize(project_name, ignore_rules: false, dependency_name: nil, run_id: nil)
        @project_name = project_name
        @ignore_rules = ignore_rules
        @dependency_name = dependency_name
        @repo_contents_path = Rails.root.join("tmp", "repo-contents", project_name).to_s

        ENV["DEPENDABOT_JOB_ID"] = run_id.presence || SecureRandom.uuid
        ENV["DEPENDABOT_REPO_CONTENTS_PATH"] = @repo_contents_path
      end

      private

      attr_reader :project_name, :ignore_rules, :dependency_name, :repo_contents_path

      # :nocov:

      # Project
      #
      # @return [Project]
      def project
        raise("Implement #{self.class}#project")
      end

      # Project configuration
      #
      # @return [Configuration]
      def project_configuration
        raise("Implement #{self.class}#project_configuration")
      end

      # Gitlab project
      #
      # @return [Gitlab::ObjectifiedHash]
      def gitlab_project
        @gitlab_project ||= gitlab.project(project_name)
      end
      alias_method :fetch_gitlab_project, :gitlab_project

      # Directories with parsed dependencies and dependency files
      #
      # @return [Array<Hash>]
      def directories
        @directories ||= Semaphore.synchronize do
          Files::Parser.call(
            multi_dir_file_fetcher: fetcher,
            repo_contents_path: repo_contents_path,
            config_entry: config_entry,
            credentials: credentials,
            dependency_name: dependency_name
          )
        end
      end

      # Update dependency and return updated dependency container
      #
      # @param [Dependabot::Dependency] dependency
      # @param [Array<Dependabot::DependencyFile>] files
      # @return [Dependabot::UpdatedDependency]
      def update_dependency(dependency, files)
        Update::Checker.call(
          dependency: dependency,
          dependency_files: files,
          config_entry: config_entry,
          repo_contents_path: repo_contents_path,
          credentials: credentials,
          ignore_rules: ignore_rules
        )
      end

      # Get file fetcher
      #
      # @return [Dependabot::MultiDirFileFetcher]
      def fetcher
        return @fetcher if @fetcher

        # ensure job.json file is created before fetching files
        create_job_json

        @fetcher = Files::Fetcher.call(
          project_name: project_name,
          config_entry: config_entry,
          credentials: credentials,
          repo_contents_path: repo_contents_path
        )
      end

      # Config entry for specific package ecosystem and directory
      #
      # @param [Configuration] config
      # @return [Hash]
      def config_entry
        @config_entry ||= configuration_entry(project_configuration, package_ecosystem, directory)
      end

      # Combined credentials
      #
      # @return [Array<Dependabot::Credential>]
      def credentials
        @credentials ||= [
          *Credentials.call(project.gitlab_access_token),
          *registries
        ]
      end

      # Allowed private registries
      #
      # @return [Array<Hash>]
      def registries
        @registries ||= project_configuration.allowed_registries(
          package_ecosystem: package_ecosystem,
          directory: directory
        )
      end

      # Dependency groups
      #
      # @return [Array<Dependabot::DependencyGroup>]
      def dependency_groups
        @dependency_groups ||= config_entry[:groups]&.map do |name, rules|
          Dependabot::DependencyGroup.new(name: name, rules: rules)
        end
      end

      # Create job.json file for nuget ecosystem
      #
      # @return [void]
      def create_job_json
        ENV["DEPENDABOT_JOB_PATH"] ||= JobJsonCreator.call(
          project_name: project_name,
          default_branch: gitlab_project.default_branch,
          config_entry: config_entry
        )
      end
    end
  end
end
