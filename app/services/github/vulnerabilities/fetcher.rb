# frozen_string_literal: true

module Github
  module Vulnerabilities
    class Fetcher < ApplicationService # rubocop:disable Metrics/ClassLength
      class QueryError < StandardError; end

      using Rainbow
      include GraphqlHelper

      PACKAGE_ECOSYSTEMS = {
        DependabotEcosystem::BUNDLER => "RUBYGEMS",
        DependabotEcosystem::COMPOSER => "COMPOSER",
        DependabotEcosystem::GO => "GO",
        DependabotEcosystem::MAVEN => "MAVEN",
        DependabotEcosystem::NPM => "NPM",
        DependabotEcosystem::BUN => "NPM",
        DependabotEcosystem::PIP => "PIP",
        DependabotEcosystem::NUGET => "NUGET",
        DependabotEcosystem::CARGO => "RUST"
      }.freeze

      GQL_QUERY = <<~GRAPHQL
        pageInfo {
          endCursor
          hasNextPage
        }
        nodes {
          advisory {
            databaseId
            summary
            description
            origin
            permalink
            publishedAt
            withdrawnAt
            references {
              url
            }
            identifiers {
              value
            }
          }
          firstPatchedVersion {
            identifier
          }
          package {
            name
          }
          severity
          vulnerableVersionRange
        }
      GRAPHQL

      SecurityVulnerabilityIndex = GraphqlHelper.client.parse <<~GRAPHQL
        query($advisory_ecosystem: SecurityAdvisoryEcosystem) {
          securityVulnerabilities(first: 100, ecosystem: $advisory_ecosystem) {
            #{GQL_QUERY}
          }
        }
      GRAPHQL

      SecurityVulnerabilityMore = GraphqlHelper.client.parse <<~GRAPHQL
        query($advisory_ecosystem: SecurityAdvisoryEcosystem, $end_cursor: String) {
          securityVulnerabilities(first: 100, ecosystem: $advisory_ecosystem, after: $end_cursor) {
            #{GQL_QUERY}
          }
        }
      GRAPHQL

      # Security vulnerability fetcher
      #
      # @param [String] package_ecosystem
      def initialize(package_ecosystem)
        @package_ecosystem = package_ecosystem
        @advisory_ecosystem = PACKAGE_ECOSYSTEMS.fetch(package_ecosystem)
        @max_retries = 1
      end

      # :reek:DuplicateMethodCall
      # :reek:TooManyStatements

      # Get vulnerabilities for package_ecosystem
      #
      # @return [Array<Vulnerability>]
      def call
        log(:info, "  fetching #{advisory_ecosystem.bright} vulnerabilities")

        vulnerabilities = []
        page = 1

        current = query(page: page)
        page += 1

        vulnerabilities.push(*mapped_vulnerabilities(current.nodes))

        while current.page_info.has_next_page
          current = query(end_cursor: current.page_info.end_cursor, page: page)
          page += 1

          vulnerabilities.push(*mapped_vulnerabilities(current.nodes))
        end

        merged_vulnerabilities(vulnerabilities)
      end

      private

      attr_reader :advisory_ecosystem, :package_ecosystem, :max_retries

      # Package manager mapping
      #
      # @return [String]
      def package_manager
        @package_manager ||= DependabotEcosystem::PACKAGE_ECOSYSTEM_MAPPING.fetch(
          package_ecosystem,
          package_ecosystem
        )
      end

      # Query security vulnerabilities
      #
      # @param [String] end_cursor
      # @param [Integer] page
      # @return [Object]
      def query(end_cursor: nil, page: 1)
        schema = end_cursor ? SecurityVulnerabilityMore : SecurityVulnerabilityIndex
        variables = { advisory_ecosystem: advisory_ecosystem, end_cursor: end_cursor }.compact_blank

        log(:debug, "Running graphql query, page: #{page}")
        retry_query_failures do
          response = client.query(schema, variables: variables)
          raise(QueryError, response.errors[:data].join(", ")) if response.errors.any?

          response.data.security_vulnerabilities.tap { |it| log(:debug, "Fetched #{it.nodes.size} nodes") }
        end
      end

      # Retry on github querry error
      #
      # @return [Object]
      def retry_query_failures
        retry_attempt = 0

        begin
          yield
        rescue QueryError => e
          retry_attempt += 1
          raise unless retry_attempt <= max_retries

          log(:warn, "  graphql query failed with: '#{e}'. Retrying...")
          retry
        end
      end

      # Vulnerability object
      #
      # @param [Array] nodes
      # @return [Array<Vulnerability>]
      def mapped_vulnerabilities(nodes)
        nodes.map do |node|
          advisory = node.advisory

          Vulnerability.new(
            id: advisory.database_id,
            identifiers: advisory.identifiers.map(&:value),
            package: node.package.name,
            package_ecosystem: package_ecosystem,
            package_manager: package_manager,
            vulnerable_version_range: [node.vulnerable_version_range],
            severity: node.severity,
            summary: advisory.summary,
            description: advisory.description,
            permalink: advisory.permalink,
            origin: advisory.origin,
            references: advisory.references.map(&:url),
            first_patched_version: [node.first_patched_version&.identifier].compact,
            published_at: advisory.published_at,
            withdrawn_at: advisory.withdrawn_at
          )
        end
      end

      # Vulnerabilities with merged first_patched_version and vulnerable_version_range
      #
      # @param [Array<Vulnerability>] vulnerabilities
      # @return [Array<Vulnerability>]
      def merged_vulnerabilities(vulnerabilities)
        vulnerabilities.each_with_object({}) do |vulnerability, hash|
          id = "#{vulnerability.id}-#{vulnerability.package}"
          existing = hash[id]

          next hash[id] = vulnerability unless existing

          existing.first_patched_version.push(*vulnerability.first_patched_version)
          existing.vulnerable_version_range.push(*vulnerability.vulnerable_version_range)
        end.values
      end
    end
  end
end
