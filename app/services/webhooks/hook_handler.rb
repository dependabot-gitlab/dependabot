# frozen_string_literal: true

module Webhooks
  class HookHandler < ApplicationService
    class MissingConfigError < StandardError; end
    class MrNotFoundError < StandardError; end

    def initialize(project_name)
      @project_name = project_name
    end

    private

    attr_reader :project_name

    # Project
    #
    # @return [Project]
    def project
      @project ||= Project.find_by(name: project_name)
    end

    # Response for skipped processing
    #
    # @param [String] reason
    # @return [Hash]
    def skip_response(reason)
      { skipped: true, message: reason }
    end
  end
end
