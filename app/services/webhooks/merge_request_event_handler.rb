# frozen_string_literal: true

module Webhooks
  class MergeRequestEventHandler < HookHandler
    # Initialize merge request event
    # @param [String] project
    # @param [String] mr_iid
    def initialize(project_name:, mr_iid:, action:, merge_status:)
      super(project_name)

      @mr_iid = mr_iid
      @action = action
      @merge_status = merge_status
    end

    # Set merge request state to closed
    #
    # @return [void]
    def call
      return reopen_mr if reopened?
      return close_mr if closed?
      return update_ecosystem_mrs if merged?

      rebase_mr if approved?
    rescue MrNotFoundError => e
      skip_response(e.message)
    end

    private

    attr_reader :mr_iid, :action, :merge_status

    # Merge request to close
    #
    #
    # @param [String] state
    # @return [MergeRequest]
    def mr(state: "opened")
      @mr ||= project.merge_requests.find_by(iid: mr_iid, state: state)
    rescue Mongoid::Errors::DocumentNotFound
      raise MrNotFoundError, "Not dependency update merge request"
    end

    # Config entry for particular ecosystem and directory
    #
    # @return [Hash]
    def config
      @config ||= project.configuration.entry(package_ecosystem: mr.package_ecosystem, directory: mr.directory).tap do |conf| # rubocop:disable Layout/LineLength
        raise(MissingConfigError, "Failed to find config entry for directory: #{mr.directory}") unless conf
      end
    end

    # Merge requests to update
    #
    # @return [Array<MergeRequest>]
    def updateable_mrs
      @updateable_mrs ||= project.open_merge_requests(
        package_ecosystem: mr.package_ecosystem,
        directory: mr.dependency_file_dir
      )
    end

    # Reopen closed merge request
    #
    # @return [Hash]
    def reopen_mr
      log(:info, "Reopening merge request !#{mr(state: 'closed').iid} for project #{project_name}!")

      mr.update_attributes!(state: "opened")
      recreated = recreate_branch
      args = [project_name, mr_iid]
      recreated ? Mr::RecreateJob.perform_later(*args) : Mr::UpdateJob.perform_later(*args)

      { reopened_merge_request: true }
    end

    # Update open mrs of same package ecosystem when merge request is merged
    #
    # @return [Hash]
    def update_ecosystem_mrs
      log(:info, "Setting merge request !#{mr.iid} state to merged for project #{project_name}!")
      mr.update_attributes!(state: "merged")
      update_mrs unless rebase_strategy_none?

      {
        update_triggered: !rebase_strategy_none?,
        closed_merge_request: true
      }
    end

    # Run dependency updates for same package_ecosystem mrs
    #
    # @return [void]
    def update_mrs
      return false if updateable_mrs.empty?

      log(:info, "Triggering open mr update for #{project_name}=>#{mr.package_ecosystem}=>#{mr.directory}")
      updateable_mrs.each do |merge_request|
        Mr::UpdateJob.perform_later(project_name, merge_request.iid, ecosystem_update: true)
      end

      true
    end

    # Close merge request
    #
    # @return [Hash]
    def close_mr
      log(:info, "Setting merge request !#{mr.iid} state to closed for project #{project_name}!")

      mr.close

      Gitlab::BranchRemover.call(project_name, mr.branch)
      Gitlab::MergeRequest::Commenter.call(project_name, mr.iid, ignore_comment)

      { closed_merge_request: true }
    end

    # Rebase single merge request
    #
    # @return [Hash]
    def rebase_mr
      return unless config.dig(:rebase_strategy, :on_approval)

      log(:info, "Triggering rebase for !#{mr.iid} for project #{project_name}!")
      gitlab.rebase_merge_request(project_name, mr_iid)

      { merge_request_rebase_triggered: true }
    end

    # Check if reopen event action
    #
    # @return [Boolean]
    def reopened?
      action == "reopen"
    end

    # Check if merge event action
    #
    # @return [Boolean]
    def merged?
      action == "merge"
    end

    # Check if close event action
    #
    # @return [Boolean]
    def closed?
      action == "close"
    end

    def approved?
      action == "approved"
    end

    # Merge request rebase strategy is none
    #
    # @return [Boolean]
    def rebase_strategy_none?
      config.dig(:rebase_strategy, :strategy) == "none"
    end

    # Recreate mr branch if it doesn't exist
    #
    # @return [Boolean]
    def recreate_branch
      gitlab.branch(project_name, mr.branch) && false
    rescue Gitlab::Error::NotFound
      gitlab.create_branch(project_name, mr.branch, mr.target_branch) && true
    end

    # Closed mr message
    #
    # @return [String]
    def ignore_comment
      <<~TXT
        Dependabot won't notify anymore about this release, but will get in touch when a new version is available. \
        You can also ignore all major, minor, or patch releases for a dependency by adding an [`ignore` condition](https://docs.github.com/en/code-security/supply-chain-security/configuration-options-for-dependency-updates#ignore) with the desired `update_types` to your config file.
      TXT
    end
  end
end
