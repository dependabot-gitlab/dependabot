# frozen_string_literal: true

module Webhooks
  class CommentEventHandler < HookHandler
    # @param [String] discussion_id
    # @param [String] note
    # @param [String] project_name
    # @param [Number] mr_iid
    # @param [String] state
    def initialize(project_name:, discussion_id:, note:, mr_iid:, state:)
      super(project_name)

      @discussion_id = discussion_id
      @comment = note
      @mr_iid = mr_iid
      @state = state
    end

    def call
      return invalid_command unless actionable_comment?
      return mr_not_found unless mr
      return closed_mr if state != "opened"

      send(action)
    end

    private

    attr_reader :comment, :mr_iid, :discussion_id, :state

    # Skip commands for merge requests not in open state
    #
    # @return [Hash]
    def closed_mr
      reply_status(":x: Merge request is not in open state, please reopen it first!")

      skip_response("Merge request is not in open state")
    end

    # Skip commands not matching command pattern
    #
    # @return [Hash]
    def invalid_command
      skip_response("Comment did not match command pattern '#{comment_pattern}'")
    end

    # Raise error if merge request is not managed by dependabot
    #
    # @return [Hash]
    def mr_not_found
      message = "Merge request is not managed by dependabot"
      reply_status(":x: #{message}")

      raise(message)
    end

    # Comment action pattern
    #
    # @return [Regexp]
    def comment_pattern
      @comment_pattern ||= /^#{Regexp.quote(AppConfig.commands_prefix)} (?<action>recreate)$/
    end

    # Trigger merge request recreate
    #
    # @return [void]
    def recreate
      Mr::RecreateJob.perform_later(project_name, mr_iid, discussion_id)

      { recreate_in_progress: true }
    end

    # Check mr exists
    #
    # @return [MergeRequest, nil]
    def mr
      project.merge_requests.where(iid: mr_iid).first
    end

    # Valid comment
    #
    # @return [Boolean]
    def actionable_comment?
      comment.match?(comment_pattern)
    end

    # Action to run
    #
    # @return [String]
    def action
      comment.match(comment_pattern)[:action]
    end

    # Add action status reply
    #
    # @param [String] message
    # @return [void]
    def reply_status(message)
      Gitlab::MergeRequest::DiscussionReplier.call(
        project_name: project_name,
        mr_iid: mr_iid,
        discussion_id: discussion_id,
        note: message
      )
    end

    # Resolve mr discussion
    #
    # @return [void]
    def resolve_discussion
      gitlab.resolve_merge_request_discussion(project_name, mr_iid, discussion_id, resolved: true)
    end
  end
end
