# frozen_string_literal: true

module Webhooks
  class PipelineEventHandler < HookHandler
    # Handle pipeline event
    #
    # @param [String] source
    # @param [String] status
    # @param [String] project
    # @param [Number] mr_iid
    # @param [String] merge_status
    def initialize(**args)
      super(args[:project_name])

      @source = args[:source]
      @status = args[:status]
      @mr_iid = args[:mr_iid]
      @merge_status = args[:merge_status]
      @source_project_id = args[:source_project_id]
      @target_project_id = args[:target_project_id]
    end

    def call
      return skip_response("Not a merge request pipeline") unless mr_event?
      return skip_response("Pipeline failed") unless success?
      return skip_response("Merge request cannot be merged") unless can_merge?
      return skip_response("Project is a fork") if fork?
      return skip_response("Auto merge is not enabled") unless mr.auto_merge

      accept
    rescue MrNotFoundError => e
      skip_response(e.message)
    end

    private

    attr_reader :mr_iid,
                :source,
                :status,
                :merge_status,
                :source_project_id,
                :target_project_id

    # Accept merge request
    #
    # @return [Hash]
    def accept
      log(:info, "Accepting merge request !#{mr_iid}")
      merge_train? ? add_to_merge_train : merge

      { merge_request_accepted: true }
    end

    # Accept mr and merge immediately
    #
    # @return [void]
    def merge
      g_mr = gitlab.accept_merge_request(project_name, mr_iid, squash: mr.squash).to_h
      raise "Failed to accept merge request !#{mr_iid}. Error: #{g_mr['merge_error']}" if g_mr["merge_error"].present?
    rescue Gitlab::Error::MethodNotAllowed => e
      raise "Failed to accept merge request !#{mr_iid}. Error: #{e.message}"
    end

    # Add mr to merge train
    #
    # @return [void]
    def add_to_merge_train
      gitlab.add_merge_request_to_merge_train(project_name, mr_iid, squash: mr.squash)
    rescue Gitlab::Error::BadRequest => e
      # TODO: Handle pipeline event from merge train pipeline
      # Currently there is no way to distinguish between merge train pipeline and normal merge request pipeline
      # This causes to trigger adding to merge train pipeline based on the result of merge train pipeline itself
      log(:debug, "Failed to add merge request to merge train !#{mr_iid}. Error: #{e.message}")
    end

    # Is a merge request pipeline
    #
    # @return [Boolean]
    def mr_event?
      source == "merge_request_event"
    end

    # Successfull pipeline
    #
    # @return [Boolean]
    def success?
      status == "success"
    end

    # Can be merged
    #
    # @return [Boolean]
    def can_merge?
      merge_status != "cannot_be_merged"
    end

    # Is forked merge request pipeline
    #
    # @return [Boolean]
    def fork?
      source_project_id != target_project_id
    end

    # Merge train option set?
    #
    # @return [Boolean]
    def merge_train?
      project.configuration
             .entry(package_ecosystem: mr.package_ecosystem, directory: mr.directory)
             .dig(:auto_merge, :merge_train)
    rescue NoMethodError
      raise(MissingConfigError, "Failed to find config entry for directory: #{mr.directory}")
    end

    # Merge request
    #
    # @return [MergeRequest]
    def mr
      @mr ||= project.merge_requests.find_by(iid: mr_iid, state: "opened")
    rescue Mongoid::Errors::DocumentNotFound
      raise MrNotFoundError, "Not dependency update merge request"
    end
  end
end
