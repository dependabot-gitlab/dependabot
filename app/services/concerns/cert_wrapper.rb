# frozen_string_literal: true

require "open3"

# Self signed certs setup to be added before dependency update actions
#
module CertWrapper
  LOCAL_CA_CERT = "/usr/local/share/ca-certificates/custom-ca-bundle.crt"

  def call
    setup_self_signed_certs

    super
  end

  private

  # Create a local cert bundle file with the system CA certs and the custom cert
  #
  # @return [void]
  def setup_self_signed_certs
    return unless cert_file
    return if empty_cert_volume_on_compose_deploy?

    log(:info, "SSL_CERT_FILE variable set, setting up additional certs")
    return unless set_up_certs?

    out, status = setup_certs

    if status.success?
      log(:info, "  successfully set up additional certs")
    else
      log(:warn, "  failed to set up additional certs:\n#{out}")
    end
  end

  # Setup the custom cert file
  #
  # @return [Array]
  def setup_certs
    log(:info, "  creating local CA bundle '#{LOCAL_CA_CERT}'")
    FileUtils.cp(cert_file, LOCAL_CA_CERT)
    log(:info, "  adding cert path to NODE_EXTRA_CA_CERTS environment variable")
    ENV["NODE_EXTRA_CA_CERTS"] ||= cert_file
    log(:info, "  running update-ca-certificates")
    Open3.capture2e("update-ca-certificates")
  end

  # Set up certs if the cert file exists
  #
  # @return [Boolean]
  def set_up_certs?
    if !File.file?(cert_file)
      log(:warn, "  SSL_CERT_FILE is set to an invalid path: '#{cert_file}'")
      return false
    elsif File.read(cert_file).empty?
      log(:warn, "  SSL_CERT_FILE is empty: '#{cert_file}'")
      return false
    elsif File.file?(LOCAL_CA_CERT)
      log(:warn, "  local CA certs bundle '#{LOCAL_CA_CERT}' already exists, skipping!")
      return false
    end

    true
  end

  # Check if cert file is the default empty folder in compose deployment
  # Compose setup will create an empty volume mount for the cert file when variable for custom ssl cert is not set
  #
  # @return [Boolean]
  def empty_cert_volume_on_compose_deploy?
    !AppConfig.standalone? && UpdaterConfig.compose_deployment? && File.directory?(cert_file)
  end

  # Custom ssl cert file
  #
  # @return [String]
  def cert_file
    @cert_file ||= ENV["SSL_CERT_FILE"]
  end
end
