# frozen_string_literal: true

require "yaml"
require "open3"

module Container
  module Compose
    class Runner < Base
      def call
        super

        log(:debug, "Starting updater container using image '#{updater_image}'")
        pull_updater_image
        run_update
      end

      private

      attr_reader :compose_project_name

      # Raw compose yml
      #
      # @return [Hash]
      def compose_yml
        @compose_yml ||= YAML.load_file(UpdaterConfig.updater_template_path, aliases: true)
      end

      # Updater container configuration
      #
      # @return [Hash]
      def updater_conf
        @updater_conf ||= {
          "services" => {
            "updater" => {
              "image" => updater_image,
              # Disable metrics for updater containers
              **compose_yml["x-updater"].then do |conf|
                conf_with_env = conf.deep_merge("environment" => { "SETTINGS__METRICS" => "false" })
                unless valid_custom_cert?
                  next conf_with_env.merge("environment" => conf_with_env["environment"].except("SSL_CERT_FILE"))
                end

                volumes = conf_with_env["volumes"] || []
                volumes << "#{cert_file}:#{cert_file}:ro"
                conf_with_env.merge("volumes" => volumes)
              end
            }
          }
        }
      end

      # Updater docker image
      #
      # @return [String]
      def updater_image
        @updater_image ||= format(UpdaterConfig.updater_image_pattern, package_ecosystem: package_ecosystem)
      end

      # Compose run command
      #
      # @return [Array]
      def compose_run_cmd
        cmd_base = [
          "docker", "compose", "-f", "-", "run", "--no-TTY", "--quiet-pull",
          "--name", "updater-#{package_ecosystem}-#{unique_name_postfix}"
        ]
        cmd_base << "--rm" if UpdaterConfig.delete_updater_container?

        [*cmd_base, "updater", "bundle", "exec", "rake", rake_task]
      end

      # Force pull updater image
      #
      # @return [void]
      def pull_updater_image
        return unless UpdaterConfig.compose_updater_always_pull?

        log(:debug, "Pulling updater image")
        run_shell_cmd(["docker", "pull", "--quiet", updater_image], print_output: false)
      end

      # Run dependency update
      #
      # @return [void]
      def run_update
        log(:debug, "Using following configuration for updater container:\n#{updater_conf.to_yaml}")
        run_shell_cmd(compose_run_cmd, stdin_data: updater_conf.to_yaml)
      end

      # :reek:ControlParameter

      # Run shell command
      #
      # @param [Array] cmd <description>
      # @param [String] stdin_data <description>
      # @param [Boolean] print_output <description>
      # @return [void]
      def run_shell_cmd(cmd, stdin_data: nil, print_output: true)
        Open3.popen3(*cmd) do |stdin, out, err, wait_thr|
          if stdin_data
            stdin.write(stdin_data)
            stdin.close
          end

          out.each { |line| puts line.strip } if print_output
          next if wait_thr.value.success?

          handle_command_failure(err.read, wait_thr.value.exitstatus)
        end
      end

      # Handle failure of update run
      #
      # @param [String] stderr
      # @return [void]
      def handle_command_failure(stderr, exit_code)
        msg = "Command exited with code: #{exit_code}!"

        log(:error, msg)
        log(:error, "Command '#{compose_run_cmd.join(' ')}' stderr output:\n#{stderr}".strip) if stderr.present?
        raise(Failure, msg)
      end

      # Custom ssl cert file
      #
      # @return [String]
      def cert_file
        @cert_file ||= ENV["SSL_CERT_FILE"]
      end

      # Valid custom ssl cert file present
      #
      # @return [Boolean]
      def valid_custom_cert?
        cert_file && File.file?(cert_file) && !File.zero?(cert_file)
      end
    end
  end
end
