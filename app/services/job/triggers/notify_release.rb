# frozen_string_literal: true

module Job
  module Triggers
    class NotifyRelease < ApplicationService
      using Rainbow

      def initialize(dependency_name, package_ecosystem, project_name, ignore_rules)
        @dependency_name = dependency_name
        @package_ecosystem = package_ecosystem
        @project_name = project_name
        @ignore_rules = ignore_rules
      end

      def call
        errors = []

        configurations.each do |config|
          context = {
            job: "notify-release",
            project: config[:project_name],
            ecosystem: package_ecosystem,
            dependency: dependency_name
          }

          run_within_context(context) do
            Dependabot::Update::Runner.call(
              dependency_name: dependency_name,
              package_ecosystem: package_ecosystem,
              directory: config[:directory],
              project_name: config[:project_name],
              ignore_rules: ignore_rules
            )
          rescue StandardError => e
            log_error(e)
            errors << e
          end

          raise("One or more errors occurred while updating #{dependency_name} for #{package_ecosystem}") if errors.any?
        end
      end

      private

      attr_reader :dependency_name, :package_ecosystem, :project_name, :ignore_rules

      # Configurations for ecosystem
      #
      # @return [Array]
      def configurations
        (project_name ? ::Project.where(name: project_name) : ::Project.all).map do |project|
          configs = project.configuration&.entries(package_ecosystem: package_ecosystem)
          next if configs.blank?

          configs.map { |conf| { project_name: project.name, directory: conf[:directory] } }
        end.flatten.compact
      end
    end
  end
end
