# frozen_string_literal: true

module Job
  module Triggers
    # Dependency update trigger class called in updater container
    #
    class DependencyUpdate < ApplicationService
      include ServiceModeConcern

      def initialize(project_name, package_ecosystem, directory)
        @project_name = project_name
        @package_ecosystem = package_ecosystem
        @directory = directory
      end

      def call
        context = job_details(
          job: "dep-update",
          project: project_name,
          ecosystem: package_ecosystem,
          directory: directory
        )

        run_within_context(context, dependency_updates: true) do
          Dependabot::Update::Runner.call(
            project_name: project_name,
            package_ecosystem: package_ecosystem,
            directory: directory,
            run_id: update_run.id.to_s
          )
        end

        UpdateFailures.update_failures
      end

      private

      attr_reader :project_name, :package_ecosystem, :directory

      # Update job
      #
      # @return [Update::Job]
      def update_job
        @update_job ||= Update::Job.new(
          package_ecosystem: package_ecosystem,
          directory: directory
        )
      end

      # Dependency update run
      #
      # @return [Update::Run]
      def update_run
        @update_run ||= Update::Run.new(job: update_job)
      end
    end
  end
end
