# frozen_string_literal: true

# rubocop:disable Metrics/ParameterLists, Metrics/ClassLength
module Gitlab
  module MergeRequest
    # :reek:LongParameterList

    class Creator < ApplicationService
      include ServiceModeConcern
      include Vulnerabilities::Helpers

      using Rainbow

      MR_OPTIONS = %i[
        custom_labels
        branch_name_separator
        branch_name_prefix
        branch_name_max_length
      ].freeze

      # @param [Project] project
      # @param [Dependabot::Files::Fetchers::Base] fetcher
      # @param [Hash] config_entry
      # @param [Dependabot::UpdatedDependency] updated_dependency
      # @param [Number] target_project_id
      def initialize(project:, fetcher:, config_entry:, updated_dependency:, credentials:, target_project_id:)
        @project = project
        @fetcher = fetcher
        @config_entry = config_entry
        @updated_dependency = updated_dependency
        @credentials = credentials
        @target_project_id = target_project_id
      end

      delegate :commit_message, :branch_name, to: :gitlab_creator

      # Create merge request
      #
      # @return [Gitlab::ObjectifiedHash]
      def call
        @mr = gitlab_creator.create
        return unless mr

        log(:info, "  created merge request: #{mr.web_url.bright}")
        persist_mr
        add_severity_labels if vulnerable?
        close_superseded_mrs
        mr
      end

      private

      delegate :vulnerable?, :production?, to: :updated_dependency

      attr_reader :project,
                  :fetcher,
                  :updated_dependency,
                  :config_entry,
                  :credentials,
                  :target_project_id,
                  :mr

      # MR message footer with available commands
      #
      # @return [String]
      def message_footer; end

      # Save merge request in app database
      #
      # @return [void]
      def persist_mr; end

      # Add vulnerability severity labels
      #
      # @return [void]
      def add_severity_labels
        return unless vulnerable?

        log(:debug, "Adding severity labels to merge request")
        labels = updated_dependency.actual_vulnerabilities
                                   .map { |vuln| "severity:#{vuln.severity.downcase}" }
                                   .uniq
                                   .filter_map { |label| create_vulnerability_label(project.name, label) }
                                   .join(",")

        gitlab.update_merge_request(project.name, mr.iid, add_labels: labels)
      rescue Gitlab::Error::Error => e
        log_error(e, message_prefix: "Failed to add severity labels to merge request #{mr.web_url}")
      end

      # Close superseded merge requests
      #
      # @return [void]
      def close_superseded_mrs
        return if superseded_mrs.empty?

        log(:info, "Closing superseded merge requests for #{updated_dependency.name.bright}")
        superseded_mrs.each { |superseded_mr| close_superseded_mr(superseded_mr) }
      end

      # Close superseded merge request
      #
      # @param [<Gitlab::ObjectifiedHash, MergeRequest>] superseded_mr
      # @return [Boolean]
      def close_superseded_mr(superseded_mr)
        iid = superseded_mr.iid

        BranchRemover.call(project.name, superseded_mr.source_branch)
        Commenter.call(target_project_id || project.name, iid, "This merge request has been superseded by !#{mr.iid}+")
        log(:info, "  closed merge request !#{iid}")

        true
      rescue StandardError => e
        log_error(e, message_prefix: "  failed to close merge request !#{iid}")
        false
      end

      # Gitlab merge request creator
      #
      # @return [Dependabot::PullRequestCreator::Gitlab]
      def gitlab_creator
        @gitlab_creator ||= Dependabot::PullRequestCreator.new(
          source: fetcher.source,
          base_commit: fetcher.commit,
          dependencies: updated_dependency.updated_dependencies,
          files: updated_dependency.updated_files,
          credentials: credentials,
          github_redirection_service: "github.com",
          pr_message_footer: message_footer,
          automerge_candidate: updated_dependency.auto_mergeable?,
          vulnerabilities_fixed: updated_dependency.fixed_vulnerabilities,
          provider_metadata: { target_project_id: target_project_id },
          **mr_options
        ).send(:gitlab_creator)
      end

      # Dependency file directory
      #
      # @return [String]
      def dependency_file_dir
        updated_dependency.directory
      end

      # Get assignee ids
      #
      # @return [Array<Number>]
      def assignees
        UserFinder.find(config_entry[:assignees])
      end

      # Get reviewer ids
      #
      # @return [Array<Number>]
      def reviewers
        UserFinder.find(config_entry[:reviewers])
      end

      # Get approver ids
      #
      # @return [Array<Number>]
      def approvers
        UserFinder.find(config_entry[:approvers])
      end

      # Milestone id
      #
      # @return [Integer]
      def milestone_id
        MilestoneFinder.find(project, config_entry[:milestone])
      end

      # Merge request specific options from config
      #
      # @return [Hash]
      def mr_options
        {
          label_language: true,
          assignees: assignees,
          reviewers: { approvers: approvers, reviewers: reviewers }.compact,
          milestone: milestone_id,
          commit_message_options: commit_message_options,
          author_details: author_details,
          **config_entry.slice(*MR_OPTIONS)
        }
      end

      # Commit message options
      #
      # @return [Hash]
      def commit_message_options
        opts = config_entry[:commit_message_options].dup
        return {} unless opts

        trailers_security = opts.delete(:trailers_security)
        trailers_dev = opts.delete(:trailers_development)
        return opts.merge({ trailers: trailers_security }) if vulnerable? && trailers_security
        return opts.merge({ trailers: trailers_dev }) if !production? && trailers_dev

        opts
      end

      # Author details
      #
      # @return [Hash]
      def author_details
        config_entry[:author_details] || {}
      end

      # Squash merge request on auto-merge
      #
      # @return [Boolean]
      def squash?
        return false unless updated_dependency.auto_mergeable?

        config_entry.dig(:auto_merge, :squash).nil? ? false : true
      end

      # Superseded merge requests
      #
      # @return [Array<Gitlab::ObjectifiedHash>]
      def superseded_mrs
        log(:debug, "Fetching superseded merge requests")
        open_dependency_mrs
          .select { |existing_mr| title_matches?(existing_mr) }
          .tap { |mrs| log(:debug, "Merge requests fetched: #{mrs.count}") }
      rescue Gitlab::Error::Error => e
        log_error(e, message_prefix: "Failed to fetch superseded merge requests for #{project.name}")
        []
      end

      # Open dependency merge requests
      #
      # @return [Array<Gitlab::ObjectifiedHash>]
      def open_dependency_mrs
        gitlab.merge_requests(
          project.name,
          state: "opened",
          scope: "created_by_me",
          in: "title",
          search: updated_dependency.name,
          created_before: mr.created_at
        )
      end

      # Mr is superseded by existing mr
      #
      # @param [Gitlab::ObjectifiedHash] existing_mr
      # @return [Boolean]
      def title_matches?(existing_mr)
        return false if mr.iid == existing_mr.iid

        dep_pattern = "[Bb]ump #{updated_dependency.name} "
        title = existing_mr.title
        return title.match?(/(#{dep_pattern}).*(in #{dependency_file_dir})$/) if dependency_file_dir != "/"

        title.match?(/#{dep_pattern}/)
      end
    end
  end
end
# rubocop:enable Metrics/ParameterLists, Metrics/ClassLength
