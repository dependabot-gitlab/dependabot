# frozen_string_literal: true

class BackgroundMigrationJob < ApplicationJob
  queue_as :low

  sidekiq_options retry: false

  # Run background migration
  #
  # @return [void]
  def perform
    run_within_context({ job: "background-migrations" }) do
      log(:info, "Running background migrations")

      Mongoid::Migrator.new(:up, "db/migrate", background_migration: true).migrate
    end
  end
end
