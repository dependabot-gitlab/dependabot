# frozen_string_literal: true

class UpdateRunnerJob < ApplicationJob
  include RunnerHelperConcern

  queue_as :default

  sidekiq_options retry: UpdaterConfig.job_retries

  # Trigger update runner
  #
  # @param [Hash] args
  # @return [void]
  def perform(args)
    @project_name, @package_ecosystem, @directory = args.symbolize_keys.values_at(
      :project_name,
      :package_ecosystem,
      :directory
    )
    job_context = job_details(
      job: "dep-update",
      project: project_name,
      ecosystem: package_ecosystem,
      directory: directory
    )

    run_within_context(job_context) do
      next run_via_sidekiq if !Rails.env.production? && UpdaterConfig.deploy_mode.nil?

      run_via_container_runner
    end
  end

  private

  attr_reader :project_name, :package_ecosystem, :directory

  # Trigger update run directly via sidekiq
  #
  # @return [void]
  def run_via_sidekiq
    Job::Triggers::DependencyUpdate.call(project_name, package_ecosystem, directory)
  end

  # Trigger update run via container runner
  #
  # @return [void]
  def run_via_container_runner
    run_task_in_container(package_ecosystem, "update", [project_name, package_ecosystem, directory])
  end
end
