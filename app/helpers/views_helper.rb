# frozen_string_literal: true

require "cgi"

module ViewsHelper
  LANGUAGE_LABELS = {
    "nuget" => ".NET",
    "cargo" => "rust",
    "composer" => "php",
    "pub" => "dart",
    "mix" => "elixir",
    "gomod" => "go",
    "maven" => "java",
    "gradle" => "java",
    "bundler" => "ruby",
    "pip" => "python",
    "npm" => "javascript",
    "gitsubmodule" => "submodules"
  }.freeze

  # Fetch specific merge requests
  #
  # @param [Project] project
  # @param [String] package_ecosystem
  # @param [String] directory
  # @return [Mongoid::Criteria]
  def open_merge_requests(project, package_ecosystem, directory)
    project.merge_requests.where(package_ecosystem: package_ecosystem, directory: directory, state: "opened")
  end

  # :reek:LongParameterList

  # Open merge requests url
  #
  # @param [Porject] project
  # @param [Hash] config_entry
  # @return [String]
  def open_mrs_url(project, config_entry)
    ecosystem, directory = config_entry.values_at(:package_ecosystem, :directory)
    labels = config_entry[:custom_labels] || (["dependencies"] << LANGUAGE_LABELS.fetch(ecosystem, ecosystem))

    project_name = project.forked_from_name || project.name
    base_url = "#{AppConfig.gitlab_url}/#{project_name}/-/merge_requests"
    base_args = "scope=all&state=opened"
    label_args = "label_name[]=#{labels.join('&label_name[]=')}"
    search_arg = "search=#{CGI.escape("in #{directory}")}"

    "#{base_url}?#{base_args}&#{label_args}&#{search_arg}"
  end

  # App version
  #
  # @return [String]
  def version
    Version.fetch
  end

  # Attributes for pagination
  #
  # @param [Boolean] remote
  # @return [Hash]
  def pagination_attributes(remote)
    { class: "page-link", data: { remote: remote, action: "projects#selectPage" } }
  end

  # Status icon for update run
  #
  # @param [String] status
  # @return [String]
  def status_icon(status, **args)
    {
      Update::Run::SUCCESS => image_tag("status-success.svg", class: "status-icon", title: "success", **args),
      Update::Run::FAILED => image_tag("status-failed.svg", class: "status-icon", title: "failed", **args),
      Update::Run::RUNNING => image_tag("status-running.svg", class: "status-icon", title: "running", **args),
      Update::Run::WARNING => image_tag("status-warning.svg", class: "status-icon", title: "warning", **args)
    }.fetch(status, nil)
  end
end
