# frozen_string_literal: true

module ApplicationHelper
  # Get gitlab client
  #
  # @return [Gitlab::Client]
  def gitlab
    Gitlab::ClientWithRetry.current
  end

  # :reek:ControlParameter

  # Find project by name
  #
  # @param [Hash] args
  # @return [Project]
  def find_project(raise_on_missing: false, **args)
    Project.find_by(**args)
  rescue Mongoid::Errors::DocumentNotFound => e
    raise e if raise_on_missing

    nil
  end

  # Fetch configuration entry for specific package ecosystem and directory
  #
  # @param [Configuration] configuration
  # @param [String] package_ecosystem
  # @param [String] directory
  # @return [Hash]
  def configuration_entry(configuration, package_ecosystem, directory)
    configuration.entry(package_ecosystem: package_ecosystem, directory: directory).tap do |entry|
      next if entry

      raise("Configuration is missing entry with package-ecosystem: #{package_ecosystem}, directory: #{directory}")
    end
  end

  # Log error message and backtrace
  #
  # @param [StandardError] error
  # @param [String] message_prefix
  # @return [void]
  def log_error(error, message_prefix: nil)
    Sentry.capture_exception(error)

    msg = message_prefix ? "#{message_prefix}, error: #{error}" : error.message
    log(:error, msg)
    log(:error, error.backtrace.join("\n")) if error.backtrace
  end

  # Save dependency update error
  #
  # @param [StandardError] error
  # @return [void]
  def capture_error(error)
    log_error(error)
    UpdateFailures.add(error) if dependency_update_context?
  end

  # Log tagged message with execution context
  #
  # @param [Symbol] level
  # @param [String] message
  # @param [Array] tags
  # @return [void]
  def log(level, message, tags: [])
    context_tag = execution_context.then do |context|
      next if context.blank?

      job = context[:job]
      details = context.except(:job, :dependency, :component).values.join("=>")
      next job if details.blank?

      "#{job}: #{details}"
    end

    Rails.logger.tagged([context_tag, *tags].compact).send(level, message)
    UpdateLogs.add(level: level.to_s, message: message) if dependency_update_context?
  end

  # Currently stored execution context
  #
  # @return [Hash]
  def execution_context
    RequestStore.fetch(:context) { [] }.last || {}
  end

  # Process is running within dependency update job context
  #
  # @return [Boolean]
  def dependency_update_context?
    RequestStore[:dependency_updates]
  end

  # Run block within execution context
  #
  # @param [Hash] context
  # @param [Boolean] dependency_updates
  # @return [void]
  def run_within_context(context, dependency_updates: false)
    RequestStore.fetch(:context) { [] }.push({ **context, component: AppConfig.app_component }.compact)
    RequestStore[:dependency_updates] = dependency_updates

    yield
  ensure
    RequestStore[:dependency_updates] = false
    RequestStore.store[:context].pop
  end

  # Dependency update job details
  #
  # @param [String] job
  # @param [String] project
  # @param [String] ecosystem
  # @param [String] directory
  # @return [Hash]
  def job_details(job:, project:, ecosystem:, directory:)
    directory = directory == "/" ? nil : directory if directory

    { job: job, project: project, ecosystem: ecosystem, directory: directory }.compact
  end

  module_function :gitlab,
                  :log,
                  :log_error,
                  :execution_context,
                  :dependency_update_context?
end
