# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include ApplicationHelper
  include Authentication

  protect_from_forgery with: :exception

  private

  # Add success notification
  #
  # @param [String] message
  # @return [void]
  def notify_success(message)
    flash[:success] = message

    yield
  end

  # Add warning notification
  #
  # @param [String] message
  # @return [void]
  def notify_warning(message)
    flash[:warning] = message

    yield
  end

  # Handle error
  #
  # @param [StandardError] error
  # @return [void]
  def notify_error(error)
    flash[:danger] = "Oops, something went wrong: #{error}"

    yield
  end
end
