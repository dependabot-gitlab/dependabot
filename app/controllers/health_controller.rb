# frozen_string_literal: true

class HealthController < ApplicationController
  def self.redis_connection_pool
    @redis_connection_pool ||= RedisClient.config(**AppConfig.redis_config)
                                          .new_pool(size: ENV.fetch("RAILS_MAX_THREADS", 5))
  end

  def show
    check_database
    check_redis

    render json: { status: "ok" }
  rescue StandardError => e
    render json: { status: "error", message: e.message }, status: :service_unavailable
  end

  private

  def check_database
    Mongoid.default_client.database_names.present?
  rescue StandardError => e
    log(:error, "MongoDB healthcheck failed - #{e.message}", tags: ["Healthcheck"])
    raise e
  end

  def check_redis
    self.class.redis_connection_pool.with do |redis|
      redis.call("PING")
      redis.close
    end
  rescue StandardError => e
    log(:error, "Redis healthcheck failed - #{e.message}", tags: ["Healthcheck"])
    raise e
  end
end
