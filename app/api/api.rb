# frozen_string_literal: true

class API < Grape::API
  format :json

  rescue_from :grape_exceptions
  rescue_from Mongoid::Errors::DocumentNotFound do |e|
    error!(e.problem, 404)
  end
  rescue_from :all do |error|
    ApplicationHelper.log_error(error)
    error!(error, 500)
  end

  use GrapeLogging

  mount V2::API
end
