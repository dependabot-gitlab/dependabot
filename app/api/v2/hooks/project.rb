# frozen_string_literal: true

module V2
  module Hooks
    class Project < API # rubocop:disable Metrics/ClassLength
      helpers do
        include Helpers

        # Initialize Gitlab client with project access token
        #
        # @return [void]
        def init_gitlab!
          Gitlab::ClientWithRetry.client_access_token = project.gitlab_access_token
        rescue Mongoid::Errors::DocumentNotFound
          ApplicationHelper.log(:warn, "Received webhook for unknown project: #{project_name}")
        end

        # Handle push hook event
        #
        # @return [<Project, nil>]
        def push
          Webhooks::PushEventHandler.call(project_name: project_name, commits: params[:commits])
        end

        # Handle merge_request hook event
        #
        # @return [Hash]
        def merge_request
          return unless %w[close merge reopen approved].include?(params.dig(:object_attributes, :action))

          Webhooks::MergeRequestEventHandler.call(
            project_name: project_name,
            mr_iid: params.dig(:object_attributes, :iid),
            action: params.dig(:object_attributes, :action),
            merge_status: params.dig(:object_attributes, :merge_status)
          )
        end

        # Handle comment hook event
        #
        # @return [void]
        def note
          Webhooks::CommentEventHandler.call(
            project_name: project_name,
            mr_iid: params.dig(:merge_request, :iid),
            state: params.dig(:merge_request, :state),
            discussion_id: params.dig(:object_attributes, :discussion_id),
            note: params.dig(:object_attributes, :note)
          )
        end

        # Handle pipeline hook event
        #
        # @return [Hash]
        def pipeline
          Webhooks::PipelineEventHandler.call(
            project_name: project_name,
            source: params.dig(:object_attributes, :source),
            status: params.dig(:object_attributes, :status),
            mr_iid: params.dig(:merge_request, :iid),
            merge_status: params.dig(:merge_request, :merge_status),
            source_project_id: params.dig(:merge_request, :source_project_id),
            target_project_id: params.dig(:merge_request, :target_project_id)
          )
        end

        # Handle issue hook event
        #
        # @return [Hash]
        def issue
          return unless params.dig(:object_attributes, :action) == "close"

          result = Webhooks::IssueEventHandler.call(
            project_name: project_name,
            issue_iid: params.dig(:object_attributes, :iid)
          )
          { closed_vulnerability_issue: result }
        end

        # Skip work_item type issue hook events
        #
        # @return [void]
        def work_item; end
      end

      before { authenticate! }
      after_validation { init_gitlab! }

      params do
        requires :object_kind, type: String, desc: "Webhook event type"
        requires :project, type: Hash do
          requires :path_with_namespace, type: String, desc: "Project path with namespace"
        end

        # Push event attributes
        optional :commits, type: Array do
          requires :added, type: Array, desc: "Added files"
          requires :modified, type: Array, desc: "Modified files"
          requires :removed, type: Array, desc: "Removed files"
        end
        optional :merge_request, type: Hash do
          requires :iid, type: Integer, desc: "Merge request iid"
          # only present in pipeline webhhoks
          optional :merge_status, type: String, desc: "Merge request merge status"
          optional :source_project_id, type: Integer, desc: "Merge request source project id"
          optional :target_project_id, type: Integer, desc: "Merge request target project id"
        end
        # Object attributes hash can contain different kinds of attributes depending on object kind
        optional :object_attributes, type: Hash do
          optional :source, types: [String, Hash], desc: "Merge request source branch or pipeline source"
          # (Merge request|issue) attributes
          optional :iid, type: Integer, desc: "Merge request or issue iid"
          optional :action, type: String, desc: "Merge request or issue action"
          optional :merge_status, type: String, desc: "Merge request merge status"
          # Comment attributes
          optional :note, type: String, desc: "Comment text"
          optional :discussion_id, type: String, desc: "Comment discussion id"
          # Pipeline attributes
          optional :status, type: String, desc: "Pipeline status"
        end
      end

      desc "Process project webhook" do
        detail <<~TXT
          Perform action based on project webhook event type:
            - `push`: update project based on changes to dependabot.yml file
            - `merge_request`: perform various actions based on merge request status
            - `note`: process dependabot comment commands
            - `pipeline`: accept and auto merge merge request on successful pipeline
            - `issue`: update internal status of vulnerability on issue close
          See https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html for more details
        TXT
        headers :"X-Gitlab-Token" => {
          description: "Webhook secret token. Required if SETTINGS__GITLAB_AUTH_TOKEN is set",
          required: false
        }
        success message: "Successful response", examples: { message: "Hook processed message" }
        failure [
          { code: 400, message: "Event not supported" },
          { code: 401, message: "Invalid gitlab authentication token" }
        ]
      end
      post :hooks do
        handlers = {
          push: -> { push },
          merge_request: -> { merge_request },
          pipeline: -> { pipeline },
          note: -> { note },
          issue: -> { issue },
          work_item: -> { work_item }
        }

        present handlers.fetch(
          params[:object_kind].to_sym, -> { error!("Event '#{params[:object_kind]}' not supported", 400) }
        ).call
      end
    end
  end
end
