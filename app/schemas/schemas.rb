# frozen_string_literal: true

module Schemas
  HOURS_PATTERN = /^(0?\d|1\d|2[0-3])-(0?\d|1\d|2[0-3])$/
  TIME_PATTERN = /^(0?\d|1\d|2[0-3]):([0-5]\d)$/

  ValidationError = Class.new(StandardError)

  class ValidationResult
    def initialize(result)
      @result = result
    end

    def success?
      errors.empty?
    end

    def failure?
      errors.any?
    end

    def error_message(separator = "\n")
      return "" if success?

      errors.join(separator)
    end

    private

    attr_reader :result

    def errors
      @errors ||= result.to_a
                        .map { |error| error["error"]&.capitalize }
                        .compact
                        .sort
    end
  end

  class << self
    # Main configuration file schema
    #
    # @return [Hash]
    def configuration_schema
      @configuration_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/configuration.schema.json").to_s)
      )
    end

    # Schema to validate common base configuration file
    #
    # @return [Hash]
    def base_config_schema
      @base_config_schema ||= configuration_schema.deep_dup.tap do |schema|
        schema["additionalProperties"] = true
        schema["required"] = ["version"]
        schema["properties"]["updates"] = schema.dig("properties", "update-options")
      end
    end

    # Schema to validate update-options part of configuration file
    #
    # @return [Hash]
    def update_options_schema
      @update_options_schema ||= configuration_schema.merge({ "required" => ["version"] })
    end

    # Schema for hex type
    #
    # @return [Hash]
    def registries_hex_schema
      @registries_hex_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/hex.schema.json").to_s)
      )
    end

    # Schema for configuration with key
    #
    # @return [Hash]
    def registries_key_schema
      @registries_key_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/key.schema.json").to_s)
      )
    end

    # Schema for configuration without auth
    #
    # @return [Hash]
    def registries_no_auth_schema
      @registries_no_auth_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/no-auth.schema.json").to_s)
      )
    end

    # Schema for configuration with password and username
    #
    # @return [Hash]
    def registries_password_schema
      @registries_password_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/password.schema.json").to_s)
      )
    end

    # Schema for configuration with token
    #
    # @return [Hash]
    def registries_token_schema
      @registries_token_schema ||= JSON.parse(
        File.read(Rails.root.join("config/schemas/registries/token.schema.json").to_s)
      )
    end

    # Custom formats for JSONSchemer
    #
    # @return [Hash]
    def formats
      @formats ||= {
        "hours" => ->(instance, _format) do HOURS_PATTERN.match?(instance) end,
        "time" => ->(instance, _format) do TIME_PATTERN.match?(instance) end
      }
    end
  end
end
