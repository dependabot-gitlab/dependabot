# frozen_string_literal: true

class MergeRequest
  include Mongoid::Document
  include Mongoid::Timestamps

  unalias_attribute :id

  field :id, type: Integer
  field :iid, type: Integer
  field :package_ecosystem, type: String
  field :directory, type: String # directory entry of configuration file, can be a glob pattern
  field :dependency_file_dir, type: String # actual directory of the dependency file
  field :state, type: String
  field :auto_merge, type: Boolean
  field :squash, type: Boolean
  field :update_from, type: String
  field :update_to, type: String
  field :main_dependency, type: String
  field :branch, type: String
  field :target_branch, type: String
  field :target_project_id, type: String
  field :commit_message, type: String
  field :fixes_vulnerability, type: Boolean, default: false
  field :web_url, type: String, default: ""

  belongs_to :project

  index({ state: 1, package_ecosystem: 1, directory: 1, iid: -1 })

  alias_method :source_branch, :branch

  # Set merge request status to closed
  #
  # @return [void]
  def close
    update_attributes!(state: "closed")
  end

  class Entity < Grape::Entity
    expose :id, documentation: { type: Integer, desc: "Merge Request ID" }
    expose :iid, documentation: { type: Integer, desc: "Merge Request IID" }
    expose :package_ecosystem, documentation: { type: String, desc: "Package ecosystem" }
    expose :directory, documentation: { type: String, desc: "Directory" }
    expose :state, documentation: { type: String, desc: "Merge Request state" }
    expose :auto_merge, documentation: { type: "Boolean", desc: "Auto merge flag" }
    expose :squash, documentation: { type: "Boolean", desc: "Squash flag" }
    expose :update_from, documentation: { type: String, desc: "Package version updating from" }
    expose :update_to, documentation: { type: String, desc: "Package version updating to" }
    expose :main_dependency, documentation: { type: String, desc: "Main dependency being updated" }
    expose :branch, documentation: { type: String, desc: "Branch name" }
    expose :target_branch, documentation: { type: String, desc: "Target branch name" }
    expose :target_project_id, documentation: { type: String, desc: "Target project ID" }
    expose :commit_message, documentation: { type: String, desc: "Commit message" }
    expose :web_url, documentation: { type: String, desc: "Merge Request URL" }
    expose :fixes_vulnerability, documentation: { type: "Boolean", desc: "Merge request fixes a vulnerability" }

    # Example response hash
    #
    # @return [Hash]
    def self.example_response
      {
        id: 1,
        iid: 1,
        package_ecosystem: "npm_and_yarn",
        directory: "/",
        state: "opened",
        auto_merge: false,
        squash: false,
        update_from: "1.0.0",
        update_to: "1.0.1",
        main_dependency: "lodash",
        branch: "dependabot/npm_and_yarn/lodash-1.0.1",
        target_branch: "main",
        target_project_id: nil,
        commit_message: "Update lodash to version 1.0.1",
        web_url: "https://gitlab.com/foo/bar/-/merge_requests/1",
        fixes_vulnerability: false
      }
    end
  end
end
