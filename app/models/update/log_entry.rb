# frozen_string_literal: true

module Update
  # Job execution log entry
  #
  # @!attribute timestamp
  #   @return [DateTime]
  # @!attribute level
  #   @return [String]
  # @!attribute message
  #   @return [String]
  #
  class LogEntry
    include Mongoid::Document
    include Mongoid::Timestamps

    field :timestamp, type: DateTime
    field :level, type: Integer
    field :message, type: String

    belongs_to :run, class_name: "Update::Run"

    index({ created_at: 1 }, { expire_after_seconds: UpdaterConfig.expire_run_data })
    index({ level: 1 })
    index({ run_id: 1 })

    class Entity < Grape::Entity
      format_with(:utc) { |dt| dt&.utc }

      expose :timestamp, format_with: :utc, documentation: { type: DateTime, desc: "Log entry timestamp" }
      expose :message, documentation: { type: String, desc: "Log entry message" }
      expose :level, as: :log_level, documentation: { type: String, desc: "Log entry level" } do |entry, _options|
        LogLevel.to_s(entry.level)
      end

      # Example response hash
      #
      # @return [Hash]
      def self.example_response
        {
          timestamp: Time.zone.now.utc,
          log_level: "INFO",
          message: "Log message"
        }
      end
    end
  end
end
