# frozen_string_literal: true

# # :reek:DataClump

# Gitlab project
#
# @!attribute id
#   @return [Integer]
# @!attribute name
#   @return [String]
# @!attribute forked_from_id
#   @return [Integer]
# @!attribute forked_from_name
#   @return [String]
# @!attribute webhook_id
#   @return [Integer]
# @!attribute web_url
#   @return [String]
# @!attribute gitlab_access_token
#   @return [String]
# @!attribute merge_requests
#   @return [Array<MergeRequest>]
# @!attribute vulnerability_issues
#   @return [Array<VulnerabilityIssue>]
# @!attribute update_jobs
#   @return [Array<Update::Job>]
# @!attribute configuration
#   @return [Configuration]
class Project
  include Mongoid::Document

  paginates_per 10

  unalias_attribute :id

  field :name, type: String
  field :id, type: Integer
  field :forked_from_id, type: Integer
  field :forked_from_name, type: String
  field :webhook_id, type: Integer
  field :web_url, type: String
  field :gitlab_access_token, type: EncryptedString

  field :last_run_status, type: String
  field :last_update_run_at, type: DateTime

  has_many :merge_requests, dependent: :destroy
  has_many :vulnerability_issues, dependent: :destroy
  has_many :update_jobs, class_name: "Update::Job", dependent: :destroy

  embeds_one :configuration

  index({ id: 1 }, { unique: true })
  index({ name: 1 }, { unique: true })
  index(name: "text")

  # All open merge requests
  #
  # @param [String] package_ecosystem
  # @param [String] directory
  # @return [Array<MergeRequest>]
  def open_merge_requests(package_ecosystem:, directory:)
    merge_requests.where(
      package_ecosystem: package_ecosystem,
      directory: directory,
      state: "opened"
    )
  end

  # Project open merge requests
  #
  # @param [String] dependency_name
  # @param [String] directory
  # @return [Array<MergeRequest>]
  def open_dependency_merge_requests(dependency_name, directory)
    merge_requests
      .where(main_dependency: dependency_name, dependency_file_dir: directory, state: "opened")
      .compact
  end

  # :reek:LongParameterList

  # Open superseded merge requests
  #
  # @param [String] package_ecosystem
  # @param [String] directory
  # @param [String] update_from
  # @param [Integer] mr_iid
  # @return [Array<MergeRequest>]
  def superseded_mrs(package_ecosystem:, directory:, update_from:, mr_iid:)
    merge_requests
      .where(
        update_from: update_from,
        package_ecosystem: package_ecosystem,
        dependency_file_dir: directory,
        state: "opened"
      )
      .not(iid: mr_iid)
      .compact
  end

  # Open vulnerability issues
  #
  # @param [String] package_ecosystem
  # @param [String] directory
  # @param [String] package
  # @return [Array<VulnerabilityIssue>]
  def open_vulnerability_issues(package_ecosystem:, directory:, package:)
    vulnerability_issues
      .where(
        directory: directory,
        package: package,
        package_ecosystem: package_ecosystem,
        status: "opened"
      )
  end

  # Return projects hash representation
  #
  # @return [Hash]
  def to_hash
    {
      id: id,
      name: name,
      forked_from_id: forked_from_id,
      forked_from_name: forked_from_name,
      webhook_id: webhook_id,
      web_url: web_url,
      configuration: configuration&.updates
    }
  end

  class Entity < Grape::Entity
    expose :id, documentation: { type: Integer, desc: "Project ID" }
    expose :name, documentation: { type: String, desc: "Project full path" }
    expose :forked_from_id, documentation: { type: Integer, desc: "Forked from project ID" }
    expose :forked_from_name, documentation: { type: String, desc: "Forked from project full path" }
    expose :webhook_id, documentation: { type: Integer, desc: "Project webhook ID" }
    expose :web_url, documentation: { type: String, desc: "Project web URL" }
    expose :configuration, using: Configuration::Entity

    # Example response hash
    #
    # @return [Hash]
    def self.example_response
      {
        id: 1,
        name: "foo/bar",
        forked_from_id: 2,
        forked_from_name: nil,
        webhook_id: 3,
        web_url: "https://gitlab.com/foo/bar",
        configuration: Configuration::Entity.example_response
      }
    end
  end
end
