# Application vars
variable "gitlab_access_token" {
  description = "Gitlab access token"
  type        = string
}

variable "image_registry" {
  default     = "registry.gitlab.com/dependabot-gitlab/dependabot"
  description = "Application docker image registry"
  type        = string
}

variable "domain_email" {
  description = "Email for dependabot-gitlab certificate generation"
  type        = string
}

variable "github_access_token" {
  default     = ""
  description = "Github access token"
  type        = string
}

variable "gitlab_hooks_auth_token" {
  default     = ""
  description = "Gitlab webhook token"
  type        = string
}

variable "secret_key_base" {
  default     = ""
  description = "Application base key"
  type        = string
}

variable "redis_password" {
  default     = ""
  description = "Redis password"
  type        = string
}

variable "sentry_dsn" {
  default     = ""
  description = "Sentry DSN"
  type        = string
}

variable "dependabot_host" {
  default     = ""
  description = "Application hostname"
  type        = string
}

variable "mongodb_username" {
  default     = "dependabot"
  description = "Monodb username"
  type        = string
}

variable "mongodb_password" {
  default     = ""
  description = "Mongodb password"
  type        = string
}

variable "mongodb_db_name" {
  default     = "dependabot"
  description = "Mongodb database name"
  type        = string
}

variable "image_tag" {
  default     = "latest"
  description = "Application docker image tag"
  type        = string
}

variable "log_level" {
  default     = "info"
  description = "Application log level"
  type        = string
}

variable "kube_context" {
  default     = "default"
  description = "Kubernetes context"
  type        = string
}

variable "kube_config" {
  default     = "~/.kube/config"
  description = "Kubernetes config"
  type        = string
}
