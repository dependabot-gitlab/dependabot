terraform {
  backend "http" {}

  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.3"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.32"
    }
  }
}

locals {
  namespace = "dependabot"
}

provider "helm" {
  kubernetes {
    config_path    = var.kube_config
    config_context = var.kube_context
  }
}

provider "kubernetes" {
  config_path    = var.kube_config
  config_context = var.kube_context
}

resource "helm_release" "cert_manager" {
  name       = "cert-manager"
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"
  version    = "v1.16.0"

  lint             = true
  atomic           = true
  wait             = true
  create_namespace = true

  namespace = "cert-manager"

  timeout = 300

  set {
    name  = "crds.enabled"
    value = true
  }
}

resource "kubernetes_manifest" "cert_manager_issuer" {
  manifest = {
    apiVersion = "cert-manager.io/v1"
    kind       = "ClusterIssuer"
    metadata = {
      name = "letsencrypt-prod"
    }
    spec = {
      acme = {
        email  = var.domain_email
        server = "https://acme-v02.api.letsencrypt.org/directory"
        privateKeySecretRef = {
          name = "letsencrypt-prod"
        }
        solvers = [
          {
            http01 = {
              ingress = {
                class = "traefik"
              }
            }
          }
        ]
      }
    }
  }

  depends_on = [helm_release.cert_manager]
}

resource "kubernetes_manifest" "https_redirect" {
  manifest = {
    apiVersion = "traefik.io/v1alpha1"
    kind       = "Middleware"
    metadata = {
      name      = "redirect-https"
      namespace = "kube-system"
    }
    spec = {
      redirectScheme = {
        scheme    = "https"
        permanent = true
      }
    }
  }

  depends_on = [helm_release.cert_manager]
}

resource "helm_release" "dependabot" {
  name  = "dependabot-gitlab"
  chart = "https://gitlab.com/api/v4/projects/21102953/packages/generic/chart/dev/dependabot-gitlab-dev.tgz"

  lint              = true
  atomic            = true
  wait              = true
  dependency_update = true
  create_namespace  = true

  namespace = local.namespace

  timeout = 300

  values = [
    yamlencode({
      image = {
        repository = var.image_registry
      }
    }),
    yamlencode({
      ingress = {
        enabled   = true
        className = "traefik"
        annotations = {
          "traefik.ingress.kubernetes.io/router.middlewares" = "kube-system-redirect-https@kubernetescrd"
          "cert-manager.io/cluster-issuer"                   = kubernetes_manifest.cert_manager_issuer.manifest.metadata.name
        }
        hosts = [
          {
            host     = var.dependabot_host
            pathType = "Exact"
            paths = [
              "/",
              "/favicon.ico",
              "/sign_in",
              "/logout",
              "/metrics"
            ]
          },
          {
            host     = var.dependabot_host
            pathType = "Prefix"
            paths = [
              "/projects",
              "/api",
              "/sidekiq",
              "/jobs/",
              "/assets/"
            ]
          }
        ]
        tls = [
          {
            hosts      = [var.dependabot_host]
            secretName = "dependabot-gitlab-tls"
          }
        ]
      }
    }),
    yamlencode({
      auth = {
        enabled = true
      }
    }),
    yamlencode({
      env = {
        dependabotUrl            = "https://${var.dependabot_host}"
        redisTimeout             = 3
        commandsPrefix           = "@dependabot-bot"
        updateRetry              = false
        logColor                 = false
        logLevel                 = var.log_level
        expireRunData            = 604800
        sentryTracesSampleRate   = "1.0"
        sentryProfilesSampleRate = "1.0"
        sentryIgnoredErrors = [
          "RedisClient::CannotConnectError",
          "Dependabot::PrivateSourceAuthenticationFailure"
        ]
      }
    }),
    yamlencode({
      migrationJob = {
        activeDeadlineSeconds = 600
        enableCleanup         = true
        resources = {
          requests = {
            cpu    = "250m"
            memory = "512Mi"
          }
        }
      }
      backgroundTasksJob = {
        resources = {
          requests = {
            cpu    = "250m"
            memory = "512Mi"
          }
        }
      }
    }),
    yamlencode({
      project_registration = {
        mode           = "automatic"
        allow_pattern  = "dependabot-gitlab"
        ignore_pattern = "standalone|release"
      }
    }),
    yamlencode({
      worker = {
        maxConcurrency = 5
        updateStrategy = {
          type = "Recreate"
        }
        startupProbe = {
          initialDelaySeconds = 30
        }
        resources = {
          requests = {
            cpu    = "250m"
            memory = "768Mi"
          }
        }
      }
    }),
    yamlencode({
      web = {
        minConcurrency = 1
        maxConcurrency = 4
        startupProbe = {
          initialDelaySeconds = 30
        }
        resources = {
          requests = {
            cpu    = "250m"
            memory = "512Mi"
          }
        }
      }
    }),
    yamlencode({
      updater = {
        resources = {
          requests = {
            cpu    = 1
            memory = "512Mi"
          }
        }
      }
    }),
    yamlencode({
      metrics = {
        enabled = true
      }
    }),
    yamlencode({
      global = {
        defaultStorageClass = "local-path"
      }
      redis = {
        enabled = true,
        master = {
          resourcesPreset = "small"
        }
      }
      mongodb = {
        enabled         = true
        resourcesPreset = "small"
        auth = {
          databases = [var.mongodb_db_name]
          usernames = [var.mongodb_username]
          passwords = [var.mongodb_password]
        }
      }
    })
  ]

  set {
    name  = "image.tag"
    value = var.image_tag
  }

  set {
    name  = "updater.imagePattern"
    value = "${var.image_registry}/%<package_ecosystem>s:${var.image_tag}"
  }

  # App credentials
  set_sensitive {
    name  = "credentials.gitlab_access_token"
    value = var.gitlab_access_token
  }

  set_sensitive {
    name  = "credentials.github_access_token"
    value = var.github_access_token
  }

  set_sensitive {
    name  = "credentials.gitlab_auth_token"
    value = var.gitlab_hooks_auth_token
  }

  set_sensitive {
    name  = "credentials.secretKeyBase"
    value = var.secret_key_base
  }

  # Registry credentials
  set_sensitive {
    name  = "registriesCredentials.GITLAB_DOCKER_REGISTRY_TOKEN"
    value = var.gitlab_access_token
  }

  set_sensitive {
    name  = "registriesCredentials.GITLAB_NPM_REGISTRY_TOKEN"
    value = var.gitlab_access_token
  }

  set_sensitive {
    name  = "registriesCredentials.GITLAB_TF_REGISTRY_TOKEN"
    value = var.gitlab_access_token
  }

  set_sensitive {
    name  = "registriesCredentials.GITLAB_PYPI_TOKEN"
    value = var.gitlab_access_token
  }

  # Env
  set_sensitive {
    name  = "env.sentryDsn"
    value = var.sentry_dsn
  }

  depends_on = [kubernetes_manifest.cert_manager_issuer]
}
